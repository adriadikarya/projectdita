<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Mmaster extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function insertdata($data)
	{
		return $this->db->insert("tr_warna",$data);
	}

	function daftarwarna()
	{
		return $this->db->get("tr_warna")->result();
	}

	function checkwarna($namawarna)
	{
		$this->db->like('upper(e_nama_warna)',$namawarna);
		return $this->db->get("tr_warna");
	}

	function cariwarna($idwarna)
	{
		$this->db->where("i_id_warna",$idwarna);
		return $this->db->get("tr_warna");
	}

	function deletedata($ikode)
	{
		$this->db->where("i_id_warna",$ikode);
		return $this->db->delete("tr_warna");
	}

	function updatedata($data,$idwarna)
	{
		$this->db->where("i_id_warna",$idwarna);
		return $this->db->update("tr_warna",$data);
	}
}