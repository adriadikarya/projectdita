<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Mmaster extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function insertdata($data)
	{

		return $this->db->insert("tr_satuan",$data);
	}

	function daftarsatuan()
	{
		return $this->db->get("tr_satuan")->result();
	}

	function checksatuan($namasatuan)
	{
		$this->db->like('upper(e_satuan)',$namasatuan);
		return $this->db->get("tr_satuan");
	}

	function carisatuan($idsatuan)
	{
		$this->db->where("i_satuan",$idsatuan);
		return $this->db->get("tr_satuan");
	}

	function deletedata($ikode)
	{
		$this->db->where("i_satuan",$ikode);
		return $this->db->delete("tr_satuan");
	}

	function updatedata($data,$idsatuan)
	{
		$this->db->where("i_satuan",$idsatuan);
		return $this->db->update("tr_satuan",$data);
	}
}