<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class M_pelanggan_grup extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function daftargrup($ikodeprsh)
	{
		$this->db->where("i_kode_perusahaan",$ikodeprsh);
		return $this->db->get("tr_grup_pelanggan");
	}

	function insertdata($data)
	{
		return $this->db->insert("tr_grup_pelanggan",$data);
	}

	function updatedata($data,$ikode)
	{
		$this->db->where("i_id_grup",$ikode);
		return $this->db->update("tr_grup_pelanggan",$data);
	}

	function carigrup($ikode)
	{
		$this->db->where("i_id_grup",$ikode);
		return $this->db->get("tr_grup_pelanggan")->row();
	}

	function deletedata($ikode)
	{
		$this->db->where("i_id_grup",$ikode);
		return $this->db->delete("tr_grup_pelanggan");
	}
}