-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.38-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for sysdbdita
DROP DATABASE IF EXISTS `sysdbdita`;
CREATE DATABASE IF NOT EXISTS `sysdbdita` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sysdbdita`;

-- Dumping structure for table sysdbdita.tm_nota
DROP TABLE IF EXISTS `tm_nota`;
CREATE TABLE IF NOT EXISTS `tm_nota` (
  `i_id_nota` int(11) NOT NULL,
  `i_no_nota` varchar(16) NOT NULL,
  `i_no_order` varchar(25) NOT NULL,
  `d_nota` date NOT NULL,
  `d_jth_tempo` date NOT NULL,
  `i_pelanggan` int(11) NOT NULL,
  `v_ppn` double NOT NULL DEFAULT '0',
  `v_pengiriman` double NOT NULL DEFAULT '0',
  `v_total_diskon` double NOT NULL DEFAULT '0',
  `v_total_fppn` double NOT NULL DEFAULT '0',
  `v_total_fppn_sisa` double NOT NULL DEFAULT '0',
  `f_nota_cancel` tinyint(1) NOT NULL DEFAULT '0',
  `f_status_lunas` tinyint(1) NOT NULL DEFAULT '0',
  `i_jenis_pembayaran` int(11) NOT NULL COMMENT '1=Cash, 2=Transfer',
  `e_gudang` varchar(25) DEFAULT NULL,
  `e_kepada` varchar(50) DEFAULT NULL,
  `i_kode_perusahaan` varchar(4) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_id_nota`,`i_no_nota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sysdbdita.tm_nota: ~0 rows (approximately)
/*!40000 ALTER TABLE `tm_nota` DISABLE KEYS */;
INSERT INTO `tm_nota` (`i_id_nota`, `i_no_nota`, `i_no_order`, `d_nota`, `d_jth_tempo`, `i_pelanggan`, `v_ppn`, `v_pengiriman`, `v_total_diskon`, `v_total_fppn`, `v_total_fppn_sisa`, `f_nota_cancel`, `f_status_lunas`, `i_jenis_pembayaran`, `e_gudang`, `e_kepada`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
	(1, 'Tesnota1', 'Tesorder1', '2019-05-15', '2019-06-30', 2, 15000, 0, 0, 165000, 165000, 0, 0, 1, '1', 'Bp. Amir', 'A', '2019-05-15 14:05:09', NULL),
	(2, 'tesnota2', 'tesorder2', '2019-05-15', '2019-06-14', 1, 45000, 0, 0, 495000, 495000, 0, 0, 1, '2', 'bp. umar', 'A', '2019-05-15 14:29:13', NULL);
/*!40000 ALTER TABLE `tm_nota` ENABLE KEYS */;

-- Dumping structure for table sysdbdita.tm_nota_item
DROP TABLE IF EXISTS `tm_nota_item`;
CREATE TABLE IF NOT EXISTS `tm_nota_item` (
  `i_id_item_nota` int(11) NOT NULL,
  `i_id_nota` int(11) NOT NULL,
  `i_kode_brg` varchar(10) NOT NULL,
  `i_id_warna` int(11) NOT NULL DEFAULT '0',
  `n_qty` int(11) NOT NULL,
  `v_harga_satuan` int(11) NOT NULL,
  `n_diskon` double NOT NULL DEFAULT '0',
  `e_desc` varchar(255) DEFAULT NULL,
  `i_no_item` int(11) NOT NULL DEFAULT '1',
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_id_item_nota`,`i_id_nota`,`i_no_item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sysdbdita.tm_nota_item: ~0 rows (approximately)
/*!40000 ALTER TABLE `tm_nota_item` DISABLE KEYS */;
INSERT INTO `tm_nota_item` (`i_id_item_nota`, `i_id_nota`, `i_kode_brg`, `i_id_warna`, `n_qty`, `v_harga_satuan`, `n_diskon`, `e_desc`, `i_no_item`, `d_input`, `d_update`) VALUES
	(1, 1, 'A0001', 0, 10, 4, 0, 'tesbarang1', 1, '2019-05-15 14:05:09', NULL),
	(2, 1, 'A0002', 0, 30, 3, 0, 'tesbarang2', 2, '2019-05-15 14:05:09', NULL),
	(3, 2, 'A0003', 0, 10, 10, 0, '', 1, '2019-05-15 14:29:13', NULL),
	(4, 2, 'A0004', 0, 50, 1, 0, '', 2, '2019-05-15 14:29:13', NULL),
	(5, 2, 'A0001', 0, 60, 4, 0, '', 3, '2019-05-15 14:29:13', NULL);
/*!40000 ALTER TABLE `tm_nota_item` ENABLE KEYS */;

-- Dumping structure for table sysdbdita.tm_nota_item_warna
DROP TABLE IF EXISTS `tm_nota_item_warna`;
CREATE TABLE IF NOT EXISTS `tm_nota_item_warna` (
  `i_id_item_warna` int(11) NOT NULL AUTO_INCREMENT,
  `i_id_item_nota` int(11) NOT NULL,
  `i_kode_brg` varchar(10) NOT NULL,
  `i_id_warna` int(11) NOT NULL,
  `n_qty_warna` int(11) NOT NULL,
  `i_no_item` int(11) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_id_item_warna`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table sysdbdita.tm_nota_item_warna: ~0 rows (approximately)
/*!40000 ALTER TABLE `tm_nota_item_warna` DISABLE KEYS */;
INSERT INTO `tm_nota_item_warna` (`i_id_item_warna`, `i_id_item_nota`, `i_kode_brg`, `i_id_warna`, `n_qty_warna`, `i_no_item`, `d_input`, `d_update`) VALUES
	(1, 2, 'A0002', 4, 15, 1, NULL, NULL),
	(2, 2, 'A0002', 5, 15, 2, NULL, NULL),
	(3, 3, 'A0003', 1, 10, 1, NULL, NULL);
/*!40000 ALTER TABLE `tm_nota_item_warna` ENABLE KEYS */;

-- Dumping structure for table sysdbdita.tm_user
DROP TABLE IF EXISTS `tm_user`;
CREATE TABLE IF NOT EXISTS `tm_user` (
  `i_nama_user` varchar(20) NOT NULL,
  `e_nama_user` varchar(50) NOT NULL,
  `i_password` varchar(50) NOT NULL,
  `i_level` int(3) NOT NULL,
  `i_user_aktif` int(1) NOT NULL,
  `d_user_gabung` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `i_kode_perusahaan` varchar(4) NOT NULL,
  PRIMARY KEY (`i_nama_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sysdbdita.tm_user: ~2 rows (approximately)
/*!40000 ALTER TABLE `tm_user` DISABLE KEYS */;
INSERT INTO `tm_user` (`i_nama_user`, `e_nama_user`, `i_password`, `i_level`, `i_user_aktif`, `d_user_gabung`, `i_kode_perusahaan`) VALUES
	('adminA', 'adriadikarya', '21232f297a57a5a743894a0e4a801fc3', 1, 1, '2019-05-12 07:29:25', 'A'),
	('adminB', 'karya', '21232f297a57a5a743894a0e4a801fc3', 1, 1, '2019-05-12 07:29:56', 'B');
/*!40000 ALTER TABLE `tm_user` ENABLE KEYS */;

-- Dumping structure for table sysdbdita.tr_barang
DROP TABLE IF EXISTS `tr_barang`;
CREATE TABLE IF NOT EXISTS `tr_barang` (
  `i_kode_brg` varchar(10) NOT NULL,
  `e_nama_brg` varchar(120) DEFAULT NULL,
  `i_satuan` int(11) NOT NULL DEFAULT '0',
  `v_hjp` double NOT NULL DEFAULT '0',
  `v_hpp` double DEFAULT '0',
  `i_kode_perusahaan` varchar(4) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_kode_brg`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sysdbdita.tr_barang: ~4 rows (approximately)
/*!40000 ALTER TABLE `tr_barang` DISABLE KEYS */;
INSERT INTO `tr_barang` (`i_kode_brg`, `e_nama_brg`, `i_satuan`, `v_hjp`, `v_hpp`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
	('A0001', 'Barang Tes', 1, 4500, 2000, 'A', '2019-05-13 08:13:44', '2019-05-13 08:41:37'),
	('A0002', 'Barang Tes 2', 1, 3500, 2000, 'A', '2019-05-14 00:59:35', NULL),
	('A0003', 'Barang Tes 3', 1, 10000, 8500, 'A', '2019-05-14 00:59:49', NULL),
	('A0004', 'Barang Tes 4', 1, 1600, 800, 'A', '2019-05-14 01:00:08', NULL);
/*!40000 ALTER TABLE `tr_barang` ENABLE KEYS */;

-- Dumping structure for table sysdbdita.tr_barang_warna
DROP TABLE IF EXISTS `tr_barang_warna`;
CREATE TABLE IF NOT EXISTS `tr_barang_warna` (
  `i_kode_brg` varchar(10) NOT NULL,
  `i_id_warna` int(11) NOT NULL,
  `i_kode_perusahaan` varchar(4) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_kode_brg`,`i_id_warna`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sysdbdita.tr_barang_warna: ~8 rows (approximately)
/*!40000 ALTER TABLE `tr_barang_warna` DISABLE KEYS */;
INSERT INTO `tr_barang_warna` (`i_kode_brg`, `i_id_warna`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
	('A0001', 1, 'A', '2019-05-14 01:00:35', NULL),
	('A0001', 2, 'A', '2019-05-14 01:00:39', NULL),
	('A0001', 3, 'A', '2019-05-14 01:00:45', NULL),
	('A0002', 4, 'A', '2019-05-14 01:01:08', NULL),
	('A0002', 5, 'A', '2019-05-14 01:00:55', NULL),
	('A0003', 1, 'A', '2019-05-14 01:01:15', NULL),
	('A0004', 1, 'A', '2019-05-14 01:01:25', NULL),
	('A0004', 5, 'A', '2019-05-14 01:01:20', NULL);
/*!40000 ALTER TABLE `tr_barang_warna` ENABLE KEYS */;

-- Dumping structure for table sysdbdita.tr_cabang
DROP TABLE IF EXISTS `tr_cabang`;
CREATE TABLE IF NOT EXISTS `tr_cabang` (
  `i_cabang` int(11) NOT NULL AUTO_INCREMENT,
  `i_pelanggan` int(11) NOT NULL,
  `i_kode_cabang` varchar(10) DEFAULT NULL,
  `e_nama_cabang` varchar(200) NOT NULL,
  `e_alamat_cabang` varchar(255) NOT NULL,
  `e_kota_cabang` varchar(50) DEFAULT NULL,
  `e_inisial` varchar(10) DEFAULT NULL,
  `i_kode_perusahaan` varchar(2) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_cabang`,`i_pelanggan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table sysdbdita.tr_cabang: ~0 rows (approximately)
/*!40000 ALTER TABLE `tr_cabang` DISABLE KEYS */;
INSERT INTO `tr_cabang` (`i_cabang`, `i_pelanggan`, `i_kode_cabang`, `e_nama_cabang`, `e_alamat_cabang`, `e_kota_cabang`, `e_inisial`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
	(2, 1, 'WKM1', 'PT. Wahana Kasih Mulia (01)', 'Jl. Jawa', 'Jawa Timur', 'WKM(01)', 'A', '2019-05-12 10:18:39', '2019-05-12 10:18:49');
/*!40000 ALTER TABLE `tr_cabang` ENABLE KEYS */;

-- Dumping structure for table sysdbdita.tr_grup_pelanggan
DROP TABLE IF EXISTS `tr_grup_pelanggan`;
CREATE TABLE IF NOT EXISTS `tr_grup_pelanggan` (
  `i_id_grup` int(11) NOT NULL AUTO_INCREMENT,
  `i_kode_grup` varchar(4) NOT NULL,
  `e_nama_grup` varchar(50) NOT NULL,
  `i_kode_perusahaan` varchar(4) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_id_grup`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table sysdbdita.tr_grup_pelanggan: ~3 rows (approximately)
/*!40000 ALTER TABLE `tr_grup_pelanggan` DISABLE KEYS */;
INSERT INTO `tr_grup_pelanggan` (`i_id_grup`, `i_kode_grup`, `e_nama_grup`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
	(1, 'Dst', 'Distributor Update', 'A', '2019-05-12 05:58:11', '2019-05-12 07:23:53'),
	(2, 'TK', 'Toko Kelontong', 'A', '2019-05-12 07:23:30', NULL),
	(4, 'Dis', 'Distributor', 'B', '2019-05-12 07:30:52', NULL);
/*!40000 ALTER TABLE `tr_grup_pelanggan` ENABLE KEYS */;

-- Dumping structure for table sysdbdita.tr_pelanggan
DROP TABLE IF EXISTS `tr_pelanggan`;
CREATE TABLE IF NOT EXISTS `tr_pelanggan` (
  `i_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `i_kode_pelanggan` varchar(6) NOT NULL,
  `e_nama_pelanggan` varchar(255) NOT NULL,
  `e_alamat_pelanggan` varchar(200) NOT NULL,
  `f_pelanggan_pkp` tinyint(1) NOT NULL DEFAULT '0',
  `e_npwp` varchar(20) NOT NULL DEFAULT '000.000.000.000.000',
  `n_top` int(5) NOT NULL DEFAULT '30',
  `e_telp_pelanggan` varchar(50) DEFAULT NULL,
  `e_fax_pelanggan` varchar(50) DEFAULT NULL,
  `e_kontak_pelanggan` varchar(150) DEFAULT NULL,
  `i_id_grup` int(11) NOT NULL DEFAULT '0',
  `e_ktp_pelanggan` varchar(25) DEFAULT NULL,
  `n_ekspedisi_pelanggan` int(11) DEFAULT '0',
  `i_kode_perusahaan` varchar(4) NOT NULL DEFAULT '0',
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_pelanggan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table sysdbdita.tr_pelanggan: ~2 rows (approximately)
/*!40000 ALTER TABLE `tr_pelanggan` DISABLE KEYS */;
INSERT INTO `tr_pelanggan` (`i_pelanggan`, `i_kode_pelanggan`, `e_nama_pelanggan`, `e_alamat_pelanggan`, `f_pelanggan_pkp`, `e_npwp`, `n_top`, `e_telp_pelanggan`, `e_fax_pelanggan`, `e_kontak_pelanggan`, `i_id_grup`, `e_ktp_pelanggan`, `n_ekspedisi_pelanggan`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
	(1, 'WKM', 'PT. Wahana Kasih Mulia', 'jl.industri 1 no.1', 1, '000.000.000.000.000', 30, '(022)7452154', '', 'Dita', 1, '', 0, 'A', NULL, NULL),
	(2, 'DGU', 'PT.DIalogue Garmindo Utama', 'Jl. Industri 1 No.1 (Dekat Rs Kasih Bunda)', 1, '015.485.717.441.000', 45, '(022)7452153', '', 'Ana', 1, '', 0, 'A', '2019-05-12 08:58:09', NULL);
/*!40000 ALTER TABLE `tr_pelanggan` ENABLE KEYS */;

-- Dumping structure for table sysdbdita.tr_perusahaan
DROP TABLE IF EXISTS `tr_perusahaan`;
CREATE TABLE IF NOT EXISTS `tr_perusahaan` (
  `i_kode_perusahaan` varchar(3) NOT NULL,
  `i_tipe_perusahaan` int(11) NOT NULL,
  `e_nama_perusahaan` varchar(100) NOT NULL,
  `e_alamat` varchar(200) DEFAULT NULL,
  `e_npwp` varchar(25) DEFAULT NULL,
  `e_telepon` varchar(16) DEFAULT NULL,
  `e_fax` varchar(30) DEFAULT NULL,
  `e_kontak` varchar(100) DEFAULT NULL,
  `e_no_rek1` varchar(30) DEFAULT NULL,
  `e_nama_bank1` varchar(30) DEFAULT NULL,
  `e_atas_nama1` varchar(100) DEFAULT NULL,
  `e_no_rek2` varchar(30) NOT NULL,
  `e_nama_bank2` varchar(30) NOT NULL,
  `e_atas_nama2` varchar(100) NOT NULL,
  `i_status_aktif` int(11) DEFAULT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_kode_perusahaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sysdbdita.tr_perusahaan: ~5 rows (approximately)
/*!40000 ALTER TABLE `tr_perusahaan` DISABLE KEYS */;
INSERT INTO `tr_perusahaan` (`i_kode_perusahaan`, `i_tipe_perusahaan`, `e_nama_perusahaan`, `e_alamat`, `e_npwp`, `e_telepon`, `e_fax`, `e_kontak`, `e_no_rek1`, `e_nama_bank1`, `e_atas_nama1`, `e_no_rek2`, `e_nama_bank2`, `e_atas_nama2`, `i_status_aktif`, `d_input`, `d_update`) VALUES
	('A', 1, 'Perusahaan 1', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 1, '2019-05-11 07:52:38', '2019-05-11 07:52:38'),
	('B', 1, 'Perusahaan 2', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 1, '2019-05-11 07:52:36', '2019-05-11 07:52:36'),
	('C', 1, 'Perusahaan 3', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 1, '2019-05-11 07:52:31', NULL),
	('D', 1, 'Perusahaan 4', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 1, '2019-05-11 07:52:57', NULL),
	('E', 2, 'Perusahaan 5', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 1, '2019-05-11 07:53:07', NULL);
/*!40000 ALTER TABLE `tr_perusahaan` ENABLE KEYS */;

-- Dumping structure for table sysdbdita.tr_satuan
DROP TABLE IF EXISTS `tr_satuan`;
CREATE TABLE IF NOT EXISTS `tr_satuan` (
  `i_satuan` int(11) NOT NULL,
  `e_satuan` varchar(25) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_satuan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sysdbdita.tr_satuan: ~3 rows (approximately)
/*!40000 ALTER TABLE `tr_satuan` DISABLE KEYS */;
INSERT INTO `tr_satuan` (`i_satuan`, `e_satuan`, `d_input`, `d_update`) VALUES
	(1, 'Pcs', '2019-05-15 06:33:23', '2019-05-15 06:33:23'),
	(2, 'KG', '2019-05-15 06:33:23', '2019-05-15 06:33:23'),
	(3, 'Roll', '2019-05-15 06:33:23', '2019-05-15 06:33:23');
/*!40000 ALTER TABLE `tr_satuan` ENABLE KEYS */;

-- Dumping structure for table sysdbdita.tr_warna
DROP TABLE IF EXISTS `tr_warna`;
CREATE TABLE IF NOT EXISTS `tr_warna` (
  `i_id_warna` int(11) NOT NULL AUTO_INCREMENT,
  `e_nama_warna` varchar(25) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_id_warna`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table sysdbdita.tr_warna: ~9 rows (approximately)
/*!40000 ALTER TABLE `tr_warna` DISABLE KEYS */;
INSERT INTO `tr_warna` (`i_id_warna`, `e_nama_warna`, `d_input`, `d_update`) VALUES
	(1, 'Biru', '2019-05-14 00:58:06', NULL),
	(2, 'Merah', '2019-05-14 00:58:09', NULL),
	(3, 'Merah Maroon', '2019-05-14 00:58:21', NULL),
	(4, 'Kuning', '2019-05-14 00:58:26', NULL),
	(5, 'Coklat', '2019-05-14 00:58:31', NULL),
	(6, 'Pink', '2019-05-14 00:58:37', NULL),
	(7, 'Ungu', '2019-05-14 00:58:41', NULL),
	(8, 'Hijau', '2019-05-14 00:58:45', NULL),
	(9, 'Hitam', '2019-05-14 00:58:49', NULL);
/*!40000 ALTER TABLE `tr_warna` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
