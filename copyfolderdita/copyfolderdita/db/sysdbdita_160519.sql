-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: sysdbdita
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tm_nota`
--

DROP TABLE IF EXISTS `tm_nota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_nota` (
  `i_id_nota` int(11) NOT NULL,
  `i_no_nota` varchar(16) NOT NULL,
  `i_no_order` varchar(25) NOT NULL,
  `d_nota` date NOT NULL,
  `d_jth_tempo` date NOT NULL,
  `i_pelanggan` int(11) NOT NULL,
  `v_ppn` double NOT NULL DEFAULT '0',
  `v_pengiriman` double NOT NULL DEFAULT '0',
  `v_total_diskon` double NOT NULL DEFAULT '0',
  `v_total_fppn` double NOT NULL DEFAULT '0',
  `v_total_fppn_sisa` double NOT NULL DEFAULT '0',
  `f_nota_cancel` tinyint(1) NOT NULL DEFAULT '0',
  `f_status_lunas` tinyint(1) NOT NULL DEFAULT '0',
  `i_jenis_pembayaran` int(11) NOT NULL COMMENT '1=Cash, 2=Transfer',
  `e_gudang` varchar(25) DEFAULT NULL,
  `e_kepada` varchar(50) DEFAULT NULL,
  `i_kode_perusahaan` varchar(4) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_id_nota`,`i_no_nota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tm_nota`
--

LOCK TABLES `tm_nota` WRITE;
/*!40000 ALTER TABLE `tm_nota` DISABLE KEYS */;
INSERT INTO `tm_nota` VALUES (1,'Tesnota1','Tesorder1','2019-05-15','2019-06-30',2,30500,0,0,335500,335500,0,0,1,'1','Bp. Amir','A','2019-05-15 07:05:09','2019-05-16 06:47:33'),(2,'tesnota2','tesorder2','2019-05-15','2019-06-14',1,34500,0,0,379500,379500,0,0,1,'2','bp. umar','A','2019-05-15 07:29:13','2019-05-16 06:46:52'),(3,'Tesnota3','TesOrder3','2019-05-16','2019-06-30',2,9450,0,0,103950,103950,0,0,1,'','bp. imar','A','2019-05-16 02:15:17','2019-05-16 06:46:57'),(4,'Tesnota4','Tesorder4','2019-05-16','2019-06-30',2,8750,0,0,96250,96250,0,0,1,'','Bp. Ustad Kodir','A','2019-05-16 05:54:51','2019-05-16 06:47:03'),(5,'Tesnota5','Tesorder5','2019-05-16','2019-06-30',2,5400,0,0,59400,59400,0,0,1,'','Bp. Ustad Kodir','A','2019-05-16 05:59:31','2019-05-16 06:47:09'),(6,'Tesnota6','Tesorder6','2019-05-16','2019-06-30',2,98500,0,0,1083500,1083500,0,0,1,'','Bp. Ustad Kodir','A','2019-05-16 06:21:32','2019-05-16 06:42:38');
/*!40000 ALTER TABLE `tm_nota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tm_nota_item`
--

DROP TABLE IF EXISTS `tm_nota_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_nota_item` (
  `i_id_item_nota` int(11) NOT NULL,
  `i_id_nota` int(11) NOT NULL,
  `i_kode_brg` varchar(10) NOT NULL,
  `i_id_warna` int(11) NOT NULL DEFAULT '0',
  `n_qty` int(11) NOT NULL,
  `v_harga_satuan` int(11) NOT NULL,
  `n_diskon` double NOT NULL DEFAULT '0',
  `e_desc` varchar(255) DEFAULT NULL,
  `i_no_item` int(11) NOT NULL DEFAULT '1',
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_id_item_nota`,`i_id_nota`,`i_no_item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tm_nota_item`
--

LOCK TABLES `tm_nota_item` WRITE;
/*!40000 ALTER TABLE `tm_nota_item` DISABLE KEYS */;
INSERT INTO `tm_nota_item` VALUES (12,6,'A0002',0,10,3500,0,'',1,'2019-05-16 06:42:38',NULL),(13,6,'A0004',0,50,1600,0,'',2,'2019-05-16 06:42:38',NULL),(14,6,'A0003',0,60,10000,0,'',3,'2019-05-16 06:42:38',NULL),(15,6,'A0001',0,60,4500,0,'',4,'2019-05-16 06:42:38',NULL),(17,2,'A0004',0,50,1500,0,'',1,NULL,'2019-05-16 06:46:52'),(18,2,'A0001',0,60,4500,0,'',2,NULL,'2019-05-16 06:46:52'),(19,3,'A0002',0,27,3500,0,'',1,NULL,'2019-05-16 06:46:57'),(20,4,'A0002',0,25,3500,0,'',1,NULL,'2019-05-16 06:47:03'),(21,5,'A0001',0,12,4500,0,'',1,NULL,'2019-05-16 06:47:09'),(22,1,'A0002',0,30,3500,0,'tesbarang2',1,NULL,'2019-05-16 06:47:33'),(23,1,'A0003',0,20,10000,0,'',2,NULL,'2019-05-16 06:47:33');
/*!40000 ALTER TABLE `tm_nota_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tm_nota_item_warna`
--

DROP TABLE IF EXISTS `tm_nota_item_warna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_nota_item_warna` (
  `i_id_item_warna` int(11) NOT NULL AUTO_INCREMENT,
  `i_id_item_nota` int(11) NOT NULL,
  `i_kode_brg` varchar(10) NOT NULL,
  `i_id_warna` int(11) NOT NULL,
  `n_qty_warna` int(11) NOT NULL,
  `i_no_item` int(11) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_id_item_warna`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tm_nota_item_warna`
--

LOCK TABLES `tm_nota_item_warna` WRITE;
/*!40000 ALTER TABLE `tm_nota_item_warna` DISABLE KEYS */;
INSERT INTO `tm_nota_item_warna` VALUES (19,14,'A0003',1,60,1,NULL,NULL),(20,15,'A0001',1,5,1,NULL,NULL),(21,15,'A0001',2,5,2,NULL,NULL),(26,21,'A0001',1,6,1,NULL,'2019-05-16 06:47:09'),(27,21,'A0001',2,6,2,NULL,'2019-05-16 06:47:09'),(28,22,'A0002',4,15,1,NULL,'2019-05-16 06:47:33'),(29,22,'A0002',5,15,2,NULL,'2019-05-16 06:47:33'),(30,23,'A0003',1,20,1,NULL,'2019-05-16 06:47:33');
/*!40000 ALTER TABLE `tm_nota_item_warna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tm_user`
--

DROP TABLE IF EXISTS `tm_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tm_user` (
  `i_nama_user` varchar(20) NOT NULL,
  `e_nama_user` varchar(50) NOT NULL,
  `i_password` varchar(50) NOT NULL,
  `i_level` int(3) NOT NULL,
  `i_user_aktif` int(1) NOT NULL,
  `d_user_gabung` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `i_kode_perusahaan` varchar(4) NOT NULL,
  PRIMARY KEY (`i_nama_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tm_user`
--

LOCK TABLES `tm_user` WRITE;
/*!40000 ALTER TABLE `tm_user` DISABLE KEYS */;
INSERT INTO `tm_user` VALUES ('adminA','adriadikarya','21232f297a57a5a743894a0e4a801fc3',1,1,'2019-05-12 00:29:25','A'),('adminB','karya','21232f297a57a5a743894a0e4a801fc3',1,1,'2019-05-12 00:29:56','B');
/*!40000 ALTER TABLE `tm_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tr_barang`
--

DROP TABLE IF EXISTS `tr_barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tr_barang` (
  `i_kode_brg` varchar(10) NOT NULL,
  `e_nama_brg` varchar(120) DEFAULT NULL,
  `i_satuan` int(11) NOT NULL DEFAULT '0',
  `v_hjp` double NOT NULL DEFAULT '0',
  `v_hpp` double DEFAULT '0',
  `i_kode_perusahaan` varchar(4) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_kode_brg`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tr_barang`
--

LOCK TABLES `tr_barang` WRITE;
/*!40000 ALTER TABLE `tr_barang` DISABLE KEYS */;
INSERT INTO `tr_barang` VALUES ('A0001','Barang Tes',1,4500,2000,'A','2019-05-13 01:13:44','2019-05-13 01:41:37'),('A0002','Barang Tes 2',1,3500,2000,'A','2019-05-13 17:59:35',NULL),('A0003','Barang Tes 3',1,10000,8500,'A','2019-05-13 17:59:49',NULL),('A0004','Barang Tes 4',1,1600,800,'A','2019-05-13 18:00:08',NULL);
/*!40000 ALTER TABLE `tr_barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tr_barang_warna`
--

DROP TABLE IF EXISTS `tr_barang_warna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tr_barang_warna` (
  `i_kode_brg` varchar(10) NOT NULL,
  `i_id_warna` int(11) NOT NULL,
  `i_kode_perusahaan` varchar(4) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_kode_brg`,`i_id_warna`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tr_barang_warna`
--

LOCK TABLES `tr_barang_warna` WRITE;
/*!40000 ALTER TABLE `tr_barang_warna` DISABLE KEYS */;
INSERT INTO `tr_barang_warna` VALUES ('A0001',1,'A','2019-05-13 18:00:35',NULL),('A0001',2,'A','2019-05-13 18:00:39',NULL),('A0001',3,'A','2019-05-13 18:00:45',NULL),('A0002',4,'A','2019-05-13 18:01:08',NULL),('A0002',5,'A','2019-05-13 18:00:55',NULL),('A0003',1,'A','2019-05-13 18:01:15',NULL),('A0004',1,'A','2019-05-13 18:01:25',NULL),('A0004',5,'A','2019-05-13 18:01:20',NULL);
/*!40000 ALTER TABLE `tr_barang_warna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tr_cabang`
--

DROP TABLE IF EXISTS `tr_cabang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tr_cabang` (
  `i_cabang` int(11) NOT NULL AUTO_INCREMENT,
  `i_pelanggan` int(11) NOT NULL,
  `i_kode_cabang` varchar(10) DEFAULT NULL,
  `e_nama_cabang` varchar(200) NOT NULL,
  `e_alamat_cabang` varchar(255) NOT NULL,
  `e_kota_cabang` varchar(50) DEFAULT NULL,
  `e_inisial` varchar(10) DEFAULT NULL,
  `i_kode_perusahaan` varchar(2) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_cabang`,`i_pelanggan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tr_cabang`
--

LOCK TABLES `tr_cabang` WRITE;
/*!40000 ALTER TABLE `tr_cabang` DISABLE KEYS */;
INSERT INTO `tr_cabang` VALUES (2,1,'WKM1','PT. Wahana Kasih Mulia (01)','Jl. Jawa','Jawa Timur','WKM(01)','A','2019-05-12 03:18:39','2019-05-12 03:18:49');
/*!40000 ALTER TABLE `tr_cabang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tr_grup_pelanggan`
--

DROP TABLE IF EXISTS `tr_grup_pelanggan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tr_grup_pelanggan` (
  `i_id_grup` int(11) NOT NULL AUTO_INCREMENT,
  `i_kode_grup` varchar(4) NOT NULL,
  `e_nama_grup` varchar(50) NOT NULL,
  `i_kode_perusahaan` varchar(4) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_id_grup`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tr_grup_pelanggan`
--

LOCK TABLES `tr_grup_pelanggan` WRITE;
/*!40000 ALTER TABLE `tr_grup_pelanggan` DISABLE KEYS */;
INSERT INTO `tr_grup_pelanggan` VALUES (1,'Dst','Distributor Update','A','2019-05-11 22:58:11','2019-05-12 00:23:53'),(2,'TK','Toko Kelontong','A','2019-05-12 00:23:30',NULL),(4,'Dis','Distributor','B','2019-05-12 00:30:52',NULL);
/*!40000 ALTER TABLE `tr_grup_pelanggan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tr_pelanggan`
--

DROP TABLE IF EXISTS `tr_pelanggan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tr_pelanggan` (
  `i_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `i_kode_pelanggan` varchar(6) NOT NULL,
  `e_nama_pelanggan` varchar(255) NOT NULL,
  `e_alamat_pelanggan` varchar(200) NOT NULL,
  `f_pelanggan_pkp` tinyint(1) NOT NULL DEFAULT '0',
  `e_npwp` varchar(20) NOT NULL DEFAULT '000.000.000.000.000',
  `n_top` int(5) NOT NULL DEFAULT '30',
  `e_telp_pelanggan` varchar(50) DEFAULT NULL,
  `e_fax_pelanggan` varchar(50) DEFAULT NULL,
  `e_kontak_pelanggan` varchar(150) DEFAULT NULL,
  `i_id_grup` int(11) NOT NULL DEFAULT '0',
  `e_ktp_pelanggan` varchar(25) DEFAULT NULL,
  `n_ekspedisi_pelanggan` int(11) DEFAULT '0',
  `i_kode_perusahaan` varchar(4) NOT NULL DEFAULT '0',
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_pelanggan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tr_pelanggan`
--

LOCK TABLES `tr_pelanggan` WRITE;
/*!40000 ALTER TABLE `tr_pelanggan` DISABLE KEYS */;
INSERT INTO `tr_pelanggan` VALUES (1,'WKM','PT. Wahana Kasih Mulia','jl.industri 1 no.1',1,'000.000.000.000.000',30,'(022)7452154','','Dita',1,'',0,'A',NULL,NULL),(2,'DGU','PT.DIalogue Garmindo Utama','Jl. Industri 1 No.1 (Dekat Rs Kasih Bunda)',1,'015.485.717.441.000',45,'(022)7452153','','Ana',1,'',0,'A','2019-05-12 01:58:09',NULL);
/*!40000 ALTER TABLE `tr_pelanggan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tr_perusahaan`
--

DROP TABLE IF EXISTS `tr_perusahaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tr_perusahaan` (
  `i_kode_perusahaan` varchar(3) NOT NULL,
  `i_tipe_perusahaan` int(11) NOT NULL,
  `e_nama_perusahaan` varchar(100) NOT NULL,
  `e_alamat` varchar(200) DEFAULT NULL,
  `e_npwp` varchar(25) DEFAULT NULL,
  `e_telepon` varchar(16) DEFAULT NULL,
  `e_fax` varchar(30) DEFAULT NULL,
  `e_kontak` varchar(100) DEFAULT NULL,
  `e_no_rek1` varchar(30) DEFAULT NULL,
  `e_nama_bank1` varchar(30) DEFAULT NULL,
  `e_atas_nama1` varchar(100) DEFAULT NULL,
  `e_no_rek2` varchar(30) NOT NULL,
  `e_nama_bank2` varchar(30) NOT NULL,
  `e_atas_nama2` varchar(100) NOT NULL,
  `i_status_aktif` int(11) DEFAULT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_kode_perusahaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tr_perusahaan`
--

LOCK TABLES `tr_perusahaan` WRITE;
/*!40000 ALTER TABLE `tr_perusahaan` DISABLE KEYS */;
INSERT INTO `tr_perusahaan` VALUES ('A',1,'Perusahaan 1','Cimahi','000.000.000.000.000',NULL,NULL,NULL,NULL,NULL,NULL,'','','',1,'2019-05-11 00:52:38','2019-05-11 00:52:38'),('B',1,'Perusahaan 2','Cimahi','000.000.000.000.000',NULL,NULL,NULL,NULL,NULL,NULL,'','','',1,'2019-05-11 00:52:36','2019-05-11 00:52:36'),('C',1,'Perusahaan 3','Cimahi','000.000.000.000.000',NULL,NULL,NULL,NULL,NULL,NULL,'','','',1,'2019-05-11 00:52:31',NULL),('D',1,'Perusahaan 4','Cimahi','000.000.000.000.000',NULL,NULL,NULL,NULL,NULL,NULL,'','','',1,'2019-05-11 00:52:57',NULL),('E',2,'Perusahaan 5','Cimahi','000.000.000.000.000',NULL,NULL,NULL,NULL,NULL,NULL,'','','',1,'2019-05-11 00:53:07',NULL);
/*!40000 ALTER TABLE `tr_perusahaan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tr_satuan`
--

DROP TABLE IF EXISTS `tr_satuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tr_satuan` (
  `i_satuan` int(11) NOT NULL,
  `e_satuan` varchar(25) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_satuan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tr_satuan`
--

LOCK TABLES `tr_satuan` WRITE;
/*!40000 ALTER TABLE `tr_satuan` DISABLE KEYS */;
INSERT INTO `tr_satuan` VALUES (1,'Pcs','2019-05-14 23:33:23','2019-05-14 23:33:23'),(2,'KG','2019-05-14 23:33:23','2019-05-14 23:33:23'),(3,'Roll','2019-05-14 23:33:23','2019-05-14 23:33:23');
/*!40000 ALTER TABLE `tr_satuan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tr_warna`
--

DROP TABLE IF EXISTS `tr_warna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tr_warna` (
  `i_id_warna` int(11) NOT NULL AUTO_INCREMENT,
  `e_nama_warna` varchar(25) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`i_id_warna`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tr_warna`
--

LOCK TABLES `tr_warna` WRITE;
/*!40000 ALTER TABLE `tr_warna` DISABLE KEYS */;
INSERT INTO `tr_warna` VALUES (1,'Biru','2019-05-13 17:58:06',NULL),(2,'Merah','2019-05-13 17:58:09',NULL),(3,'Merah Maroon','2019-05-13 17:58:21',NULL),(4,'Kuning','2019-05-13 17:58:26',NULL),(5,'Coklat','2019-05-13 17:58:31',NULL),(6,'Pink','2019-05-13 17:58:37',NULL),(7,'Ungu','2019-05-13 17:58:41',NULL),(8,'Hijau','2019-05-13 17:58:45',NULL),(9,'Hitam','2019-05-13 17:58:49',NULL);
/*!40000 ALTER TABLE `tr_warna` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-16 15:47:34
