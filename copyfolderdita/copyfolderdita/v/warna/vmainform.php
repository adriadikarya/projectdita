<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">List Warna</a></li>
              <li><a href="#tab_2" data-toggle="tab">Form Input Warna</a></li>
              <!-- <li><a href="#tab_3" data-toggle="tab">Tab 3</a></li> -->
          	</ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane  active" id="tab_1">
            	<div class="row">
					<div class="col-lg-12 col-sm-12">
						<div class="box">
			            <div class="box-header">
			              <h3 class="box-title">Data Warna</h3>
			            <?php if($message = $this->session->flashdata('message')): ?>
			                <div class="alert <?php echo ($message['status']) ? 'alert-success' : 'alert-danger'; ?>" >
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $message['message']; ?>
			                </div>
			            <?php endif; ?>
			            </div>
			            <!-- /.box-header -->
			            <div class="box-body">
			              <table id="datatabel" class="table table-bordered table-hover display nowrap" style="width:100%">
			              	<thead>
			              		<th>Nama Warna</th>
			              		<th>Tanggal Input</th>
			              		<th>Tangga Update</th>
			              		<th>Action</th>
			              	</thead>
			              	<tbody>
			              		<?php
			              			if(!empty($warna))
			              			{	
			              				foreach ($warna as $row) {
			              				$urldelete = "Cform/delete/".$row->i_id_warna.'/'.$row->e_nama_warna;
			              				$di = strtotime($row->d_input);
			              				$datei = date('d M Y h:i:sa',$di);
			              				$dupdate = $row->d_update==''?'':strtotime($row->d_update);
			              				$dateu = $dupdate==''?'':date('d M Y h:i:sa',$dupdate);
			              		?>
			              			<tr>
			              				<td><?php echo $row->e_nama_warna; ?></td>
			              				<td><?php echo $datei; ?></td>
			              				<td><?php echo $dateu; ?></td>
			              				<td align="center">
			              					<a href="<?php echo base_url('warna/Cform/edit/' .$row->i_id_warna); ?>"><button class="btn btn-success btn-rounded btn-sm"><i class="glyphicon glyphicon-edit"></i></button></a>
			                                <a alt="Delete" id="delete" href="#"><button class="btn btn-danger btn-rounded btn-sm" onclick="return hapus('<?php echo $urldelete; ?>');"><i class="glyphicon glyphicon-trash"></i></button></a>
			              				</td>
			              			</tr>
			              		<?php
			              				}
			              			} else {
								?>
									<tr>
										<td colspan="4" style="text-align: center">Maaf Tidak Ada Data!</td>
									</tr>
								<?php              				
			              			}
			              		?>
			              	</tbody>
			              </table>
			          	</div>
						</div>
					</div>
				</div>
            </div>
            <div class="tab-pane" id="tab_2">
            	<div class="row">
			        <!-- left column -->
			        <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
			          	<!-- general form elements -->
			          	<div class="box box-primary">
			            	<div class="box-header with-border">
			        		    <h3 class="box-title">From Input Warna</h3>
			    	        </div>
			    	        <!-- /.box-header -->
				            <!-- form start -->
				            <form role="form" method="post" action="<?php echo base_url() ?>warna/Cform">
				              <div class="box-body">
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="namawarna">Nama Warna</label>
				                  <input type="text" class="form-control" id="namawarna" name="namawarna" placeholder="Isi Nama Warna" required maxlength="25">
				                </div>
				              </div>
				              <!-- /.box-body -->
				              <div class="box-footer">
				                <button type="submit" name="submit" class="btn btn-success">Simpan</button>
				                <a href="<?php echo base_url() ?>warna/Cform" class="btn btn-default">Kembali</a>
				              </div>
				            </form>
				        </div>
			        </div>
			    </div>
            </div>
        </div>
	</div>
    
</section>