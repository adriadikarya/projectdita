<style type="text/css">
	@page{
    /*size: 21cm 29.7cm;*/
    	margin: 0.05in 0.79in 0.10in 0.60in /*(29mm top) (29mm right) (30mm bottom) (30mm left)*/
    ;}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
</style>
<?php include_once ("funcs/terbilang.php"); ?>

<table width="100%" border="0">
	<tr>
		<td colspan="7"><hr></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3" style="font-size: 16px"><b>FAKTUR</b></td>
		<td colspan="2" style="font-size: 12px; font-style: italic; text-align: right;"><b>Zahir Sample</b></td>
		<td colspan="2" style="font-size: 12px">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="7"><hr></td>
	</tr>
</table>
<table width="100%" border="0">
	<tr>
		<td>&nbsp;</td>
		<td>Nomor Nota :</td>
		<td>00000001</td>
		<td>&nbsp;</td>
		<td>Tanggal :</td>
		<td>Rabu, April 21, 2019</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nomor Order :</td>
		<td>00000001</td>
		<td>&nbsp;</td>
		<td>Up :</td>
		<td>Bp. Amir</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Kepada :</td>
		<td>PT. ABCD Contoh</td>
		<td>&nbsp;</td>
		<td>Mata Uang :</td>
		<td>IDR - (Rupiah)</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Alamat :</td>
		<td>Jalan Jenderal Sudirman</td>
		<td>&nbsp;</td>
		<td>Term :</td>
		<td>Cash/Tunai</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>Jakarta</td>
		<td>&nbsp;</td>
		<td>Gudang :</td>
		<td>1</td>
	</tr>
</table>
<table width="100%" border="0">
	<thead>
		<tr>
			<td colspan="7"><hr></td>
		</tr>
		<tr>
			<th>Nama Barang/Pesanan</th>
			<th>Jumlah</th>
			<th>Unit</th>
			<th>Harga@</th>
			<th>Disc</th>
			<th>Sub Total</th>
		</tr>
		<tr>
			<td colspan="7"><hr></td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="text-align: center;">ZHR-001 Zahir Enterprise</td>
			<td style="text-align: right;">1.00</td>
			<td style="text-align: center;">Pcs</td>
			<td style="text-align: right;">15,000,000</td>
			<td style="text-align: right;">0.00%</td>
			<td style="text-align: right;">15,000,000</td>
		</tr>
		<tr>
			<td style="text-align: center;">ZHR-001 Zahir Enterprise</td>
			<td style="text-align: right;">1.00</td>
			<td style="text-align: center;">Pcs</td>
			<td style="text-align: right;">15,000,000</td>
			<td style="text-align: right;">0.00%</td>
			<td style="text-align: right;">15,000,000</td>
		</tr>
	</tbody>
</table>
<table width="100%" border="0">
	<tr>
		<td colspan="7"><hr></td>
	</tr>
	<tr>
		<td rowspan="2" style="width: 65%">Tiga Puluh Lima Juta Tujuh Ratur Lima Puluh Ribu Rupiah</td>
		<td>Total :</td>
		<td style="text-align: right;"><b>Rp. 1,357,500,000</b></td>
	</tr>
	<tr>
		<td colspan="3"><hr></td>
	</tr>
</table>
<table width="100%" border="0">
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2" style="text-align: center;">PT. ABCD Contoh</td>
		<td colspan="3" style="width: 42%">&nbsp;</td>
		<td colspan="2" style="text-align: center; font-style: italic;">Zahir Sample</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2" style="text-align: center;">Bp. Amir</td>
		<td colspan="3">&nbsp;</td>
		<td colspan="2" style="text-align: center;">Bp. Dian</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2"><hr></td>
		<td colspan="3">&nbsp;</td>
		<td colspan="2"><hr></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2" style="text-align: center;">Manajer Keuangan</td>
		<td colspan="3">&nbsp;</td>
		<td colspan="2" style="text-align: center;">Manajer Pemasaran</td>
		<td>&nbsp;</td>
	</tr>
</table>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>