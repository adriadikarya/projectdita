<style type="text/css">
	@page{
    /*size: 21cm 29.7cm;*/
    	margin: 0.05in 0.79in 0.10in 0.60in /*(29mm top) (29mm right) (30mm bottom) (30mm left)*/
    ;}
    .border { 
        border: solid black 1px; 
    }
    .kepala {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    	font-size: 25px;
	}
	.kepalaxx {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
	}
	.isihead {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    	font-size: 12px;
	}
	.judul {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    	font-size: 16px;
    	text-align: center;
	}
	.isicar{
		border-right: solid black 1px;
		text-align: center;
	}
	.isiangka{
		border-right: solid black 1px;
		text-align: right;
	}
	#kotak {
        border-collapse: collapse;
        border: 1px solid black;
    }
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
</style>
<?php include_once ("funcs/terbilang.php"); ?>
<table width="100%" border="0">
	<tr>
		<td class="kepala"><b>PT. Bintang Makmur</b></td>
	</tr>
	<tr>
		<td class="kepalaxx">Jl. Pajajaran No 200 Ujungberung Bandung</td>
	</tr>
	<tr>
		<td class="judul">&nbsp;</td>
	</tr>
	<tr>
		<td class="judul"><b>INVOICE</b></td>
	</tr>
	<tr>
		<td class="judul"><b>No. 452156325</b></td>
	</tr>
	<tr>
		<td class="judul">&nbsp;</td>
	</tr>
	<tr>
		<td class="judul">&nbsp;</td>
	</tr>
</table>
<table width="100%" border="0">
	<table width="100%" border="0">
		<tr>
			<td colspan="5" class="isihead">Customer:</td>
			<td>&nbsp;</td>
			<td style="width: 50%">&nbsp;</td>
			<td class="isihead" style="text-align: right;">Tanggal:</td>
			<td class="isihead">02-Dec-2010</td>
		</tr>	
	</table>
	<table width="100%" border="0">
		<tr>
			<td class="isihead">Nama</td>
			<td class="isihead">:</td>
			<td class="isihead">Anugerah Toko Buku</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td class="isihead">Alamat</td>
			<td class="isihead">:</td>
			<td class="isihead">Jl.Pasirkoja No.10</td>
			<td class="isihead">&nbsp;</td>
			<td class="isihead" style="text-align: right;">No. Order:</td>
			<td class="isihead">7347324</td>
		</tr>
		<tr>
			<td class="isihead">Kota</td>
			<td class="isihead">:</td>
			<td class="isihead">Bandung</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td class="isihead">Telepon</td>
			<td class="isihead">:</td>
			<td class="isihead">022-5697645</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</table>
<table width="100%" border="0" id="kotak">
	<thead>
		<tr>
			<th style="border-right: solid black 1px; border-bottom: solid black 1px" class="kepalaxx">Nama Barang</th>
			<th style="border-right: solid black 1px; border-bottom: solid black 1px" class="kepalaxx">Satuan</th>
			<th style="border-right: solid black 1px; border-bottom: solid black 1px" class="kepalaxx">Jumlah</th>
			<th style="border-right: solid black 1px; border-bottom: solid black 1px" class="kepalaxx">Harga</th>
			<th style="border-bottom: solid black 1px" class="kepalaxx">Total</th>
		</tr>
	</thead>
	<tbody id="kotak">
		<tr>
			<td class="isicar isihead">Buku Tulis</td>
			<td class="isicar isihead">Box</td>
			<td class="isicar isihead">2</td>
			<td class="isiangka isihead">34,500</td>
			<td style="text-align: right;" class="isihead">69,000</td>
		</tr>
		<tr>
			<td class="isicar isihead">Buku Tulis</td>
			<td class="isicar isihead">Box</td>
			<td class="isicar isihead">2</td>
			<td class="isiangka isihead">34,500</td>
			<td style="text-align: right;" class="isihead">69,000</td>
		</tr>
		<tr>
			<td class="isicar isihead">Buku Tulis</td>
			<td class="isicar isihead">Box</td>
			<td class="isicar isihead">2</td>
			<td class="isiangka isihead">34,500</td>
			<td style="text-align: right;" class="isihead">69,000</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td style="text-align: center;"><b>Total</b></td>
			<td>&nbsp;</td>
			<td style="text-align: right; width: 31%;" id="kotak" class="isihead">2,381,600</td>
		</tr>
	</tfoot>
</table>
<table width="100%" border="0" align="right">
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>	
	<tr>
		<td align="right"><b>PT. Bintang Makmur</b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="right"><b>(Manager Penjualan)</b></td>
	</tr>
</table>