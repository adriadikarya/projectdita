<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <!-- <li class="active"><a href="#tab_1" data-toggle="tab">List satuan</a></li> -->
              <li><a href="#tab_1" data-toggle="tab">Form Edit Satuan</a></li>
              <!-- <li><a href="#tab_3" data-toggle="tab">Tab 3</a></li> -->
          	</ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
            	<div class="row">
			        <!-- left column -->
			        <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
			          	<!-- general form elements -->
			          	<div class="box box-primary">
			            	<div class="box-header with-border">
			        		    <h3 class="box-title">From Edit Satuan</h3>
			    	        </div>
			    	        <!-- /.box-header -->
				            <!-- form start -->
				            <form role="form" method="post" action="<?php echo base_url() ?>satuan/Cform/edit">
				              <div class="box-body">
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="namasatuan">Nama Satuan</label>
				                  <input type="text" class="form-control" id="namasatuan" name="namasatuan" placeholder="Isi Nama Satuan" value="<?php echo $namasatuan; ?>" required maxlength="25">
				                  <input type="hidden" name="idsatuan" value="<?php echo $idsatuan; ?>">
				                </div>
				              </div>
				              <!-- /.box-body -->
				              <div class="box-footer">
				                <button type="submit" name="submit" class="btn btn-info">Update</button>
				                <a href="<?php echo base_url() ?>satuan/Cform" class="btn btn-default">Kembali</a>
				              </div>
				            </form>
				        </div>
			        </div>
			    </div>
            </div>
        </div>
	</div>
    
</section>