<section class="content">
	<div class="modal fade" id="myModal" role="dialog">
  		<div class="modal-dialog">
  			<!-- Modal content-->
        	<div class="modal-content">
  			</div>     
  		</div>
  	</div>
	<div class="row">
		<!-- left column -->
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
			    	<h3 class="box-title">List SJ (Nota)</h3>
			    </div>
			    <div class="box-body">
			    <?php if($message = $this->session->flashdata('message')): ?>
			                <div class="alert <?php echo ($message['status']) ? 'alert-success' : 'alert-danger'; ?>" >
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $message['message']; ?>
			                </div>
			    <?php endif; ?>
			    <div class="row">
			    	<div class="col-sm-12 col-lg-12">&nbsp;</div>
			    </div>
			    <div class="row">
			    	<div class="col-sm-3 col-lg-3">
			    		<a href="<?php echo base_url() ?>sjnota/Cform/tambah" title="tambah data" class="btn btn-success btn-rounded"><i class="fa fa-plus"></i> &nbsp;Tambah Data</a>
			    	</div>	
			    </div>
			    <div class="row">
			    	<div class="col-sm-12 col-lg-12">&nbsp;</div>
			    </div>
			    	<table id="datatabel" class="table table-bordered table-hover display nowrap" style="width:100%">
			              	<thead>
			              		<th>No</th>
			              		<th>No Nota</th>
			              		<th>Status Lunas</th>
			              		<th>Tgl Nota</th>
			              		<th>Tgl Jth Tempo</th>
			              		<th>Pelanggan</th>
			              		<th>Total</th>
			              		<!-- <th>Tanggal Input</th> -->
			              		<th>Action</th>
			              	</thead>
			              	<tbody>
			              		<?php
			              			if(!empty($isi))
			              			{	
			              				$i=1;
			              				foreach ($isi as $row) {
			              				$urldelete = "Cform/cancel/".$row->i_id_nota.'/'.$row->i_no_nota;
			              				$dn = strtotime($row->d_nota);
			              				$dnota = date('d M Y',$dn);
			              				$dj = strtotime($row->d_jth_tempo);
			              				$djthtempo = date('d M Y',$dj);
			              				$di = strtotime($row->d_input);
			              				$datei = date('d M Y h:i',$di);

			              				$statuslunas = $row->f_status_lunas==0?"Belum":"Sudah";
			              				// $dupdate = $row->d_update==''?'':strtotime($row->d_update);
			              				// $dateu = $dupdate==''?'':date('d M Y h:i:sa',$dupdate);
			              		?>
			              			<tr>
			              				<td align="center"><?php echo $i++; ?></td>
			              				<td><?php echo $row->i_no_nota; ?></td>
			              				<td><?php echo $statuslunas; ?></td>
			              				<td><?php echo $dnota; ?></td>
			              				<td><?php echo $djthtempo; ?></td>
			              				<td><?php echo $row->e_nama_pelanggan; ?></td>
			              				<td><?php echo number_format($row->v_total_fppn); ?></td>
			              				<!-- <td><?php echo $datei; ?></td> -->
			              				<td align="center">
			              					<a href="#" title="Print"><button class="btn btn-primary btn-rounded btn-sm" onclick='showprint("<?php echo $row->i_id_nota ?>");' title="Print"><i class="glyphicon glyphicon-print"></i></button></a>
			              					<a href="<?php echo base_url('sjnota/Cform/edit/' .$row->i_id_nota); ?>" title="Edit"><button class="btn btn-success btn-rounded btn-sm" title="Edit"><i class="glyphicon glyphicon-edit"></i></button></a>
			              					<a alt="Detail" id="detail" title="Lihat Detail" data-toggle="modal" data-target="#myModal" href="<?php echo base_url('sjnota/Cform/listdetailnota/'.$row->i_id_nota.'/'.$row->i_no_nota); ?>"><button class="btn btn-info btn-rounded btn-sm"><i class="glyphicon glyphicon-info-sign"></i></button></a>
			                                <a alt="Cancel" id="cancel" title="Cancel" href="#"><button class="btn btn-danger btn-rounded btn-sm" onclick="return hapus('<?php echo $urldelete; ?>');"><i class="glyphicon glyphicon-trash"></i></button></a>
			              				</td>
			              			</tr>
			              		<?php
			              				}
			              			} else {
								?>
									<tr>
										<td colspan="8" style="text-align: center">Maaf Tidak Ada Data!</td>
									</tr>
								<?php              				
			              			}
			              		?>
			        		</tbody>
			        </table>
			    </div>

			</div>
		</div>
	</div>
</section>
<script>
function showprint(z) {
	lebar =750;
	tinggi=800;
	eval('window.open("<?php echo site_url(); ?>"+"sjnota/Cform/printnota/"+z,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,menubar=1, top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
}
</script>