<section class="content">
	<div class="modal fade" id="myModal" role="dialog">
  		<div class="modal-dialog">
  			<!-- Modal content-->
        	<div class="modal-content">
  			</div>     
  		</div>
  	</div>
  	<div class="row">
		<!-- left column -->
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
			    	<h3 class="box-title">From Input SJ (Nota)</h3>
			    </div>
			    <!-- /.box-header -->
				<!-- form start -->
				<form role="form" id="myForm" method="post" action="<?php echo base_url() ?>sjnota/Cform/tambah">
					<div class="box-body">
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="pelanggan">Pelanggan</label>
				            <select class="select2 form-control" id="pelanggan" name="pelanggan" style="width: 100%;" onchange="ambilDataPel(this.value)">
				            	<option value="">Pilih Pelanggan</option>
					        <?php 
					           	if(!empty($pel))
					           	{

					           		foreach ($pel as $row) {
					        ?>
					    	     		<option value="<?php echo $row->i_pelanggan?>"><?php echo $row->e_nama_pelanggan ?></option>
					        <?php			
					        		}
					           	} else {
					           		echo "<option value=\"\">Maaf Tidak Ada Pelanggan!</option>";
					           	}
					        ?>
					        </select>
				            
				        </div>
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="nonota">No Nota</label>
				            <input type="text" class="form-control" id="nonota" name="nonota" placeholder="Isi No Nota" value="" onkeydown="spasitidak();" maxlength="16">
				        </div>
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="alamat">Alamat</label>
				            <input type="text" class="form-control" id="alamat" name="alamat" readonly>
				        </div>
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="noorder">No Order</label>
				            <input type="text" class="form-control" id="noorder" name="noorder" placeholder="Isi No Order" onkeydown="spasitidak();">
				        </div>
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="datepicker"><i class="fa fa-calendar"></i>&nbsp;Tgl Nota</label>
				            <input type="text" class="form-control" id="datepicker" name="dnota" placeholder="Tanggal Nota" onchange="getJatuhTempo(this.value,$('#datepicker1'),$('#jangka').val());" readonly>
				        </div>
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="Jenis Bayar">Jenis Bayar</label>
				            <select class="form-control" name="jnsbayar">
				            	<option value="1">Cash</option>
				            	<option value="2">Transfer</option>
				            </select>
				        </div>
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="datepicker1"><i class="fa fa-calendar"></i>&nbsp;Tgl Jatuh Tempo</label>
				            <input type="text" class="form-control col-sm-11" id="datepicker1" name="djthtempo" placeholder="Tanggal Jatuh Tempo" readonly>
				        </div>
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="kepada">Kepada</label>
				            <input type="text" class="form-control" id="kepada" name="kepada" placeholder="Isi Kepada dengan orang yang ditunjuk">
				        </div>
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	&nbsp;
				        </div>
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="gudang">Gudang</label>
				            <input type="text" class="form-control" id="gudang" name="gudang" placeholder="Isi Gudang">
				        </div>
				    </div>
				    <!-- /.box-body -->
					<div class="box-footer">
						<input type="hidden" id="totrow" name="totrow" value="1">
						<input type="hidden" class="form-control" id="jangka" name="jangka" value="30">
				    	<input type="button" name="submit" id="submit" class="btn btn-success" onclick="return validasi();" value="Simpan">
				        <a href="<?php echo base_url() ?>sjnota/Cform" class="btn btn-default">Kembali</a>
				        <button type="button" onclick="addrow();" class="btn btn-primary">Tambah</button>
				        <button type="button" onclick="deleterow();" class="btn btn-warning">Kurang</button>
				    </div>
				    <div class="box box-warning">
						<div class="box-header with-border">
					    	<h3 class="box-title">Detail Item Nota</h3>
					    </div>
					    <div id="detailhead" class="box-body table-responsive">
				    	<table class="table table-hover condensed" style="width:100%;">
				    		<thead>
				    			<th class="col-sm-1" style="text-align: center;">No</th>
				    			<th class="col-sm-3" style="text-align: center;">Nama Barang</th>
				    			<th class="col-sm-2" style="text-align: center;">Warna</th>
				    			<th class="col-sm-1"style="text-align: center;">Jumlah</th>
				    			<th class="col-sm-1" style="text-align: center;">Harga</th>
				    			<th class="col-sm-2" style="text-align: center;">Keterangan</th>
				    			<th class="col-sm-2" style="text-align: center;">Subtotal</th>
				    		</thead>
				    		<tbody id="detailbody">
				    			<tr id="tr1">
				    				<td class="col-sm-1" align="center"><input type="hidden" class="form-control" name="baris1" id="baris1" value="1">1</td>
				    				<td class="col-sm-3"><a id="myBtn" data-toggle="modal" data-target="#myModal" href="<?php echo base_url('sjnota/Cform/listbrg/1'); ?>"><input type="text" class="form-control input-sm" name="namabrg1" id="namabrg1" placeholder="Klik kolom Nama Barang" readonly></a></td>
				    				<td class="col-sm-2">
				    					<select name="wrn1" id="wrn1" class="form-control input-sm" onchange="cariwarna('1',this.value)">
				    						<option value="0" selected>Tidak</option>
				    						<option value="1">Ya</option>
				    					</select>
				    					<div id="selectwrn1"></div>
				    					<input type="hidden" name="totqtywrn1" id="totqtywrn1" value="0">
				    				</td>
				    				<td class="col-sm-1"><input type="text" class="form-control input-sm" name="jml1" id="jml1" placeholder="0" style="text-align: right;" onkeyup="hitungnilai('1'); hitungtotal();" onkeydown="hanyaangka()"></td>
				    				<td class="col-sm-1"><input type="text" class="form-control input-sm" name="hrg1" id="hrg1" value="0" style="text-align: right;" readonly></td>
				    				<td class="col-sm-2">
				    					<input type="text" name="e_desc1" id="e_desc1" class="form-control input-sm" maxlength="255">
				    				</td>
				    				<td class="col-sm-2"><input type="text" class="form-control input-sm" name="subtot1" id="subtot1" style="text-align: right;" value="0" readonly>
				    				<input type="hidden" class="form-control input-sm" name="kodebrg1" id="kodebrg1"></td>
				    			</tr>
				    		</tbody>
				    	</table>
				    	</div>
				    	<div class="box-footer">
				    		<div class="row">
								<div class="form-group pull-right">
				                  <label for="totalnilai" class="col-sm-4 control-label">Total Nilai</label>
				                  <div class="col-sm-8">
				                    <input type="text" class="form-control input-sm" id="totalnilai" name="totalnilai" placeholder="0" style="text-align: right;" readonly>
				                  </div>
				                </div>
			            	</div>
			            	<div class="row">
				                <div class="form-group pull-right">
				                  <label for="pajak" class="col-sm-3 control-label">PPN</label>
				                  <div class="col-sm-9">
				                    <input type="text" class="form-control input-sm" id="pajak" name="pajak" placeholder="0" style="text-align: right;" readonly>
				                  </div>
				                </div>
				            </div>
				            <div class="row">
				                <div class="form-group pull-right">
				                  <label for="grandtot" class="col-sm-5 control-label">Grand Total</label>
				                  <div class="col-sm-7">
				                    <input type="text" class="form-control input-sm" id="grandtot" name="grandtot" placeholder="0" style="text-align: right;" readonly>
				                  </div>
				                </div>
				            </div>
				    	</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	function addrow()
	{
		var counter = parseInt($('#totrow').val())+1;
		// var newRow = $("<tr>");
	    var cols = "";
	    cols += '<tr id="tr'+counter+'">';
	    cols += '<td class="col-sm-1" align="center"><input type="hidden" class="form-control input-sm" name="baris' + counter + '" id="baris' + counter + '" value="'+counter+'"/>'+counter+'</td>';
	    // cols += '<td class="col-sm-5"><input type="text" class="form-control input-sm" name="namabrg' + counter + '" id="namabrg' + counter + '"/></td>';
	    cols += '<td class="col-sm-3"><a id="myBtn" data-toggle="modal" data-target="#myModal" href="<?php echo base_url() ?>sjnota/Cform/listbrg/'+counter+'"><input type="text" class="form-control input-sm" name="namabrg' + counter + '" id="namabrg' + counter + '" placeholder="Klik kolom Nama Barang" readonly></a></td>';
	    cols += '<td class="col-sm-2"><select name="wrn'+counter+'" id="wrn'+counter+'" onchange="cariwarna('+counter+',this.value)" class="form-control input-sm"><option value="0" selected>Tidak</option><option value="1">Ya</option></select><div id="selectwrn'+counter+'"></div><input type="hidden" name="totqtywrn'+counter+'" id="totqtywrn'+counter+'" value="0"></td>';
	    cols += '<td class="col-sm-1"><input type="text" class="form-control input-sm" name="jml' + counter + '" id="jml' + counter + '" placeholder="0" style="text-align: right;"/ onkeyup="hitungnilai(\''+counter+'\'); hitungtotal();" onkeydown="hanyaangka()"></td>';
	    cols += '<td class="col-sm-1"><input type="text" class="form-control input-sm" name="hrg' + counter + '" id="hrg' + counter + '" value="0" style="text-align: right;" readonly/></td>';
	    cols += '<td class="col-sm-2"><input type="text" name="e_desc'+counter+'" id="e_desc'+counter+'" class="form-control input-sm" maxlength="255"></td>';
	    cols += '<td class="col-sm-2"><input type="text" class="form-control input-sm" name="subtot' + counter + '" id="subtot' + counter + '" style="text-align: right;" value="0" readonly/><input type="hidden" class="form-control form-control-sm" name="kodebrg' + counter + '" id="kodebrg' + counter + '"/></td>';
	    cols += '</tr>';
	    // cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  onclick="deleterow()" value="Delete"></td>';
	    // newRow.append(cols);
	    // $("#detailbody").append(newRow);
	    $("#detailbody").append(cols);
	    $('#totrow').val(counter);
	}
	function deleterow()
	{
		var counter = parseInt($('#totrow').val());
		if(counter>1)
		{
			// $('#baris'+counter).remove();       
			$('#tr'+counter).remove();       
		    counter -= 1;
		    $('#totrow').val(counter);	
		    hitungtotal();
		} else {
			$('#tr'+counter).remove();
			$('#totrow').val(0);   
			hitungtotal();
		}
	}
	function getJatuhTempo(source,destination,range) { // d/m/Y
		var pel = $("#pelanggan").val();
		if(pel=='')
		{
			swal(
		      'Pilih Pelanggan',
		      'Pilih Dulu Pelanggan Pliss! :)',
		      'warning'
		    );
		    $('#datepicker').val("");
		} else {
			var nilai;

			// 27-11-2014
			var jangka=$("#jangka").val();
			var tsplit	= source.split('/');
			var baru = tsplit['2']+'/'+tsplit['1']+'/'+tsplit['0'];
			var firstDay = new Date(baru);
			var test = firstDay.getTime() + parseInt(parseInt(jangka) * 24 * 60 * 60 * 1000);
			var tanggal = new Date(test);
			// var tgl	= tanggal.getDate();
			var tgl = tanggal.getDate();
			tglr = (tgl<10?"0":"")+tgl;
			var bln = tanggal.getMonth()+1; // +1 karena getMonth itu startnya dari 0 (Januari), 1 (Februari), dst
			blnr = (bln<10?"0":"")+bln;
			var thn = tanggal.getFullYear();

			nilai = tglr+'/'+blnr+'/'+thn;
			destination.val(nilai);
		}
	}

	function ambilDataPel(nilai)
	{
		$.ajax({
			url: "<?php echo base_url() ?>sjnota/Cform/ambilDataPel/", 
			method: "post",
			data: "ipel="+nilai,
			dataType: "json",
			success: function(data){      			
      			$("#jangka").val(data.jangka);
      			$("#alamat").val(data.alamat)
    		}
    	});
	}

	function cariwarna(baris,nilai)
	{
		var kodebrg = $('#kodebrg'+baris).val();
		var baris2 = $('#totqtywrn'+baris).val();
		if(nilai==1){
			if(kodebrg=='')
			{
				swal(
					'Pilih!',
					'Pilih dulu barangnya! :)',
					'warning'
				);
				$('#wrn'+baris).val(0);
				$('#totqtywrn'+baris).val(0);
			} else {
				$.ajax({
					url: "<?php echo base_url() ?>sjnota/Cform/cariwarna/", 
					method: "post",
					data: {
						ikode: kodebrg,
						brs: baris,
					},
					dataType: "json",
					success: function(respon){    
	      				$("#selectwrn"+baris).html(respon.out);
	      				$('#totqtywrn'+baris).val(respon.totrowqty);
	    			}
	    		});
			}
    	} else {
    		swal(
		      'Peringatan!',
		      'Batal Memilih Warna! :)',
		      'warning'
		    );
		    if(baris2!=0)
			{
				for(var i=1; i<=baris2; i++)
				{
					$("#lwrn"+baris+i).remove();	
				}	
			}
			$('#totqtywrn'+baris).val(0);
    	}
	}

	function hitungnilai(brs)
	{
		var kodebrg = $('#kodebrg'+brs).val();
		if(kodebrg=='')
		{
			swal(
				'Peringatan!',
				'Isi dulu kode Barang! :)',
				'warning'
			);
			$('#jml'+brs).val('');
		} else {
	  		var total = 0;
	  		var qty = $('#jml'+brs).val()==''?$('#jml'+brs).val(0):qty;
	  			qty = $('#jml'+brs).val() || 0;
	  		var hrg = formatulang($('#hrg'+brs).val());
	  		total = parseFloat(qty) * parseFloat(hrg);
	  		$('#subtot'+brs).val(formatuang(total));
	  	}
	}

	function hitungtotal()
	{
		var totaln = 0;
		var ppn = 0;
		var grandtot = 0;
		var count  = $('#totrow').val();
		
		for(var i=1; i<=count; i++)
		{
			totaln = parseFloat(totaln) + parseFloat(formatulang($('#subtot'+i).val()));
		}

		$('#totalnilai').val(formatuang(totaln));
		ppn = Math.round(parseFloat(totaln)*0.1);
		$('#pajak').val(formatuang(ppn));
		grandtot = parseFloat(totaln)+parseFloat(ppn);
		$('#grandtot').val(formatuang(Math.round(grandtot)));
	}

	function hitungtotwarna(baris,baris2)
	{
		var statwrn = $('#wrn'+baris).val();
		var qtyplus = 0;
		var totwarna = $('#totqtywrn'+baris).val();
		var jmlqty = $('#jml'+baris).val();
		if(jmlqty==0)
		{
			swal(
				'Peringatan!',
				'Jumlah Total harus diisi!',
				'warning'
			);
			$('#qtywrn'+baris+baris2).val('');
		} else {
			if(statwrn==1)
			{
				for(var i=1; i<=totwarna; i++)
				{
					qtyplus = parseFloat(qtyplus) + parseFloat($('#qtywrn'+baris+i).val());
					if(parseFloat(qtyplus)>jmlqty)
					{
						swal(
							'Peringatan!',
							'Jumlah warna melebihi jumlah total !',
							'warning'
						);
						$('#qtywrn'+baris+baris2).val('');
						break;
					}
				}
			}
		}
	}

	function validasi()
	{
		var totrow = $('#totrow').val();
		var j=1;
		var s=0;
		var pelanggan = $('#pelanggan').val();
		var tglnota = $('#datepicker').val();
		var nonota = $('#nonota').val();
		var noorder= $('#noorder').val();
		if(pelanggan=='')
		{
			swal(
				'Peringatan!',
				'Pelanggan Tidak Boleh Kosong! :)',
				'warning'
			);
			return false;
		} else if(nonota=='')
		{
			swal(
				'Peringatan!',
				'No Nota Tidak Boleh Kosong! :)',
				'warning'
			);
			return false;
		} else if(noorder=='')
		{
			swal(
				'Peringatan!',
				'No Order Tidak Boleh Kosong! :)',
				'warning'
			);
			return false;
		} else if(tglnota=='')
		{
			swal(
				'Peringatan!',
				'Tanggal Nota Tidak Boleh Kosong! :)',
				'warning'
			);
			return false;
		} else {
			while(j<=totrow){
				var qty = $('#jml'+j).val();
				if(qty==0 || qty==''){
					swal(
						'Peringatan!',
						'Jumlah Barang Tidak Boleh 0! :)',
						'warning'
					);
					s=1;
					return false;
				}
				j+=1;
			}
			if(s==0){
				    swal({
				        title: "Yakin Simpan?",
				        text: "Yakin data-data sudah terisi dengan benar?",
				        type: "warning",
				        showCancelButton: true,
				        confirmButtonColor: "#6A9944",
				        confirmButtonText: "Ya, Simpan aja",
				        cancelButtonText: "Tidak, Jangan dulu",
				        closeOnConfirm: true,
  	  					closeOnCancel: false
				      },
				      function(isConfirm) {
				        if (isConfirm) {
				          $('#submit').removeAttr("type").attr("type", "submit");			        	  
			        	  $('#submit').trigger('click', {'send': true});
				          // $('#myForm').submit();
				          return true;
				        } else {
				        	swal('Tidak Jadi','Data tidak jadi disimpan','error');
				        	return false;
				        }
				      }
				    );
			} else {
				$('#jml'+j).focus();
				return false;
			}
		}
	}
</script>
