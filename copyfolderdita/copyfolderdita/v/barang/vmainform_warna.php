<!-- Main Content -->
<section class="content">
	<div class="row">
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">
						Form Input Barang + Warna
					</h3>
				</div>
				<form class="form-horizontal" method="post" action="<?php echo base_url() ?>barang/C_brgwarna">
					<div class="box-body">
						<?php if(validation_errors()): ?>
				            <!-- <div class="errorForm"> -->
				                <!-- <label class="error"><?php echo validation_errors('<p>', '</p>'); ?></label> -->
				            <!-- </div> -->
				            <?php echo validation_errors(); ?>
				        <?php endif; ?>
				        <?php if($message = $this->session->flashdata('message')): ?>
			                <div class="alert <?php echo ($message['status']) ? 'alert-success' : 'alert-danger'; ?>" >
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $message['message']; ?>
			                </div>
			            <?php endif; ?>
		                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				            <label>Kode Barang</label> 
				            <select class="select2 form-control" id="kodebrg" name="kodebrg" style="width: 100%;">
				            	<option value="">Pilih Kode Barang</option>
					        <?php 
					           	if(!empty($brg))
					           	{

					           		foreach ($brg as $row) {
					        ?>
					    	     		<option value="<?php echo $row->i_kode_brg?>"><?php echo $row->e_nama_brg ?></option>
					        <?php			
					        		}
					           	} else {
					           		echo "<option value=\"\">Maaf Tidak Ada Barang!</option>";
					           	}
					        ?>
					        </select>
				        </div>
		                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				            <label>Warna</label> 
				            <select class="select2 form-control" id="warna" name="warna" style="width: 100%;">
				            	<option value="">Pilih Warna</option>
					        <?php 
					           	if(!empty($warna))
					           	{
					           		foreach ($warna as $row) {
					        ?>
					    	     		<option value="<?php echo $row->i_id_warna?>"><?php echo $row->e_nama_warna ?></option>
					        <?php			
					        		}
					           	} else {
					           		echo "<option value=\"\">Maaf Tidak Ada Warna!</option>";
					           	}
					        ?>
					        </select>
				        </div>
		              </div>
		              <!-- /.box-body -->
		              <div class="box-footer">
		                <!-- <button type="reset" class="btn btn-default">Reset</button> -->
		                <button type="submit" onclick="return validasi();" class="btn btn-success pull-right">Simpan</button>
		              </div>
		              <!-- /.box-footer -->
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-sm-12">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Barang + Warna</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="datatabel" class="table table-bordered table-hover display nowrap" style="width:100%">
              	<thead>
              		<th>Kode Barang</th>
              		<th>Warna</th>
              		<th>Tanggal Input</th>
              		<th>Action</th>
              	</thead>
              	<tbody>
              		<?php
              			if(!empty($isi))
              			{	
              				foreach ($isi as $row) {
              				$urldelete = "C_brgwarna/delete/".$row->i_kode_brg.'/'.$row->i_id_warna;
              				$di = strtotime($row->d_input);
			              	$datei = date('d M Y h:i:sa',$di);
              		?>
              			<tr>
              				<td><?php echo $row->i_kode_brg; ?></td>
              				<td><?php echo $row->e_nama_warna; ?></td>
              				<td><?php echo $datei; ?></td>
              				<td align="center">
              					<a href="<?php echo base_url('barang/C_brgwarna/edit/' . $row->i_kode_brg . '/' . $row->i_id_warna); ?>"><button class="btn btn-success btn-rounded btn-sm"><i class="glyphicon glyphicon-edit"></i></button></a>
                                <a alt="Delete" id="delete" href="#"><button class="btn btn-danger btn-rounded btn-sm" onclick="return hapus('<?php echo $urldelete; ?>');"><i class="glyphicon glyphicon-trash"></i></button></a>
              				</td>
              			</tr>
              		<?php
              				}
              			} else {
					?>
						<tr>
							<td colspan="4" style="text-align: center">Maaf Tidak Ada Data!</td>
						</tr>
					<?php              				
              			}
              		?>
              	</tbody>
              </table>
          	</div>
			</div>
		</div>
	</div>
</section>
<script>
	function validasi()
	{
		var kodebrg = $('#kodebrg').val();
		var warna = $('#warna').val();

		if(kodebrg=='' || warna=='')
		{
			swal(
		      'Tolong Isi',
		      'Data kode barang dan warna :)',
		      'warning'
		    );
			return false;
		} else {
			return true;
		}
	}
</script>

