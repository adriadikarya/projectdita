<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">List Cabang</a></li>
              <li><a href="#tab_2" data-toggle="tab">Form Input Cabang</a></li>
              <!-- <li><a href="#tab_3" data-toggle="tab">Tab 3</a></li> -->
          	</ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane  active" id="tab_1">
            	<div class="row">
					<div class="col-lg-12 col-sm-12">
						<div class="box">
			            <div class="box-header">
			              <h3 class="box-title">Data Cabang</h3>
			            <?php if($message = $this->session->flashdata('message')): ?>
			                <div class="alert <?php echo ($message['status']) ? 'alert-success' : 'alert-danger'; ?>" >
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $message['message']; ?>
			                </div>
			            <?php endif; ?>
			            </div>
			            <!-- /.box-header -->
			            <div class="box-body">
			              <table id="datatabel" class="table table-bordered table-hover display nowrap" style="width:100%">
			              	<thead>
			              		<th>Kode Cabang</th>
			              		<th>Nama Cabang</th>
			              		<th>Alamat</th>
			              		<th>Kota</th>
			              		<th>Inisial</th>
			              		<th>Nama Pelanggan Pusat</th>
			              		<th>Action</th>
			              	</thead>
			              	<tbody>
			              		<?php
			              			if(!empty($cab))
			              			{	
			              				foreach ($cab as $row) {
			              				$urldelete = "C_cabang/delete/".$row->i_cabang;
			              		?>
			              			<tr>
			              				<td><?php echo $row->i_kode_cabang; ?></td>
			              				<td><?php echo $row->e_nama_cabang; ?></td>
			              				<td><?php echo $row->e_alamat_cabang; ?></td>
			              				<td><?php echo $row->e_kota_cabang; ?></td>
			              				<td><?php echo $row->e_inisial; ?></td>
			              				<td><?php echo $row->e_nama_pelanggan; ?></td>
			              				<td align="center">
			              					<a href="<?php echo base_url('pelanggan/C_cabang/edit/' . $row->i_cabang); ?>"><button class="btn btn-success btn-rounded btn-sm"><i class="glyphicon glyphicon-edit"></i></button></a>
			                                <a alt="Delete" id="delete" href="#"><button class="btn btn-danger btn-rounded btn-sm" onclick="return hapus('<?php echo $urldelete; ?>');"><i class="glyphicon glyphicon-trash"></i></button></a>
			              				</td>
			              			</tr>
			              		<?php
			              				}
			              			} else {
								?>
									<tr>
										<td colspan="7" style="text-align: center">Maaf Tidak Ada Data!</td>
									</tr>
								<?php              				
			              			}
			              		?>
			              	</tbody>
			              </table>
			          	</div>
						</div>
					</div>
				</div>
            </div>
            <div class="tab-pane" id="tab_2">
            	<div class="row">
			        <!-- left column -->
			        <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
			          	<!-- general form elements -->
			          	<div class="box box-primary">
			            	<div class="box-header with-border">
			        		    <h3 class="box-title">From Input Cabang</h3>
			    	        </div>
			    	        <!-- /.box-header -->
				            <!-- form start -->
				            <form role="form" method="post" action="<?php echo base_url() ?>pelanggan/C_cabang">
				              <div class="box-body">
				              	<div class="form-group col-md-12 col-lg-12 col-sm-12">
				              	  <label>Kode Cabang</label> 
				              	  <select class="select2 form-control" id="idpel" name="idpel" style="width: 100%;">
					                <?php 
					                	if(!empty($pel))
					                	{
					                		foreach ($pel as $row) {
					                ?>
					                		<option value="<?php echo $row->i_pelanggan?>"><?php echo $row->e_nama_pelanggan ?></option>
					                <?php			
					                		}
					                	} else {
					                		echo "<option value=\"\">Maaf Tidak Ada Pelanggan!</option>";
					                	}
					                ?>
					              </select>
				              	</div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="kodecab">Kode Cabang</label>
				                  <input type="text" class="form-control" id="kodecab" name="kodecab" placeholder="Isi Kode Cabang" required maxlength="10">
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="namacab">Nama Cabang</label>
				                  <input type="text" class="form-control" id="namacab" name="namacab" placeholder="Isi Nama Cabang" required maxlength="200">
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="alamatcab">Alamat</label>
				                   <textarea class="form-control" rows="3" id="alamatcab" name="alamatcab" placeholder="Isikan Alamat ..." maxlength="255" required ></textarea>
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="kotacab">Kota</label>
				                  <input type="text" class="form-control" id="kotacab" name="kotacab" placeholder="Isi Kota Cabang" value="" maxlength="50">
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="inisialcab">Inisial</label>
				                  <input type="text" class="form-control" id="inisialcab" name="inisialcab" value="" placeholder="Isi Dengan Inisial" maxlength="10">
				                </div>
				              </div>
				              <!-- /.box-body -->

				              <div class="box-footer">
				                <button type="submit" name="submit" class="btn btn-success">Simpan</button>
				                <a href="<?php echo base_url() ?>pelanggan/C_cabang" class="btn btn-default">Kembali</a>
				              </div>
				            </form>
				        </div>
			        </div>
			    </div>
            </div>
        </div>
	</div>
    
</section>