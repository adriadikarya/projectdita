<!-- Main Content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">
						Form Grup Pelanggan
					</h3>
				</div>
				<form class="form-horizontal" method="post" action="<?php echo base_url() ?>pelanggan/C_pelanggan_grup/edit">
					<div class="box-body">
						<?php if(validation_errors()): ?>
				            <!-- <div class="errorForm"> -->
				                <!-- <label class="error"><?php echo validation_errors('<p>', '</p>'); ?></label> -->
				            <!-- </div> -->
				            <?php echo validation_errors(); ?>
				        <?php endif; ?>
				        <?php if($message = $this->session->flashdata('message')): ?>
			                <div class="alert <?php echo ($message['status']) ? 'alert-success' : 'alert-danger'; ?>" >
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $message['message']; ?>
			                </div>
			            <?php endif; ?>
		                <div class="form-group">
		                  <label for="kodegrup" class="col-sm-2 control-label">Kode :</label>
		                  <div class="col-sm-10">
		                    <input type="text" class="form-control" id="kodegrup" name="kodegrup" placeholder="Masukan kode grup pelanggan" required oninvalid="this.setCustomValidity('Tolong Isi Kolom Ini !')" maxlength="4" value="<?php echo $isidata->i_kode_grup ?>" readonly>
		                    <input type="hidden" name="idgrup" value="<?php echo $isidata->i_id_grup ?>">
		                  </div>
		                </div>
		                <div class="form-group">
		                  <label for="namagrup" class="col-sm-2 control-label">Nama :</label>
		                  <div class="col-sm-10">
		                    <input type="text" class="form-control" id="namagrup" name="namagrup" placeholder="Masukan nama grup pelanggan" required oninvalid="this.setCustomValidity('Tolong Isi Kolom Ini !')" value="<?php echo $isidata->e_nama_grup ?>">
		                  </div>
		                </div>
		              </div>
		              <!-- /.box-body -->
		              <div class="box-footer">
		                <button type="submit" class="btn btn-info pull-right">Update</button>
		                <a href="<?php echo base_url()?>pelanggan/C_pelanggan_grup" class="btn btn-default">Kembali</a>
		              </div>
		              <!-- /.box-footer -->
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-sm-12">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Grup Pelanggan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="datatabel" class="table table-bordered table-hover display nowrap" style="width:100%">
              	<thead>
              		<th>Kode Grup</th>
              		<th>Nama Grup</th>
              		<th>Action</th>
              	</thead>
              	<tbody>
              		<?php
              			if(!empty($isi))
              			{	
              				foreach ($isi as $row) {
              		?>
              			<tr>
              				<td><?php echo $row->i_kode_grup; ?></td>
              				<td><?php echo $row->e_nama_grup; ?></td>
              				<td align="center">
              					<a href="<?php echo base_url('pelanggan/C_pelanggan_grup/edit/' . $row->i_id_grup); ?>"><button class="btn btn-success btn-rounded btn-sm"><i class="glyphicon glyphicon-edit"></i></button></a>
              				</td>
              			</tr>
              		<?php
              				}
              			} else {
					?>
						<tr>
							<td colspan="3" style="text-align: center">Maaf Tidak Ada Data!</td>
						</tr>
					<?php              				
              			}
              		?>
              	</tbody>
              </table>
          	</div>
		</div>
	</div>
</section>

