<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
		<!-- general form elements -->
			<div class="box box-danger">
				<div class="box-header with-border">
			    	<h3 class="box-title">From Perusahaan</h3>
				</div>
				<form role="form" method="post" action="<?php echo base_url() ?>satuan/Cform">
					<div class="box-body">
						<div class="form-group col-md-12 col-lg-12 col-sm-12">
				            <label for="kodeprsh">Kode Perusahaan</label>
				            <input type="text" class="form-control" id="kodeprsh" name="kodeprsh" placeholder="Kode Perusahaan" required maxlength="3">
				        </div>
				        <div class="form-group col-md-12 col-lg-12 col-sm-12">
				            <label for="tipeprsh">Tipe Perusahaan</label>
				            <select name="tipeprsh" id="tipeprsh" class="form-control">
				            	
				            </select>
				        </div>
					</div>
				</form>
			</div>
		</div>
	</div>		
</section>