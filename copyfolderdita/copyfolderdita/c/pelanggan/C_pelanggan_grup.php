<?php 

/**
 * 
 */
class C_Pelanggan_grup extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('pelanggan/M_pelanggan_grup');
	}

	function index()
	{
		if($this->session->userdata('loggedin')){
			$data['page_title'] = "Grup Pelanggan";
			$data['litle_title'] = "Form Input";
			$ikodeprsh = $this->session->userdata('kode_perusahaan');
			$data['isi'] = $this->M_pelanggan_grup->daftargrup($ikodeprsh)->result();
			$datalay['content'] = $this->load->view('pelanggan/vmainform_grup',$data,TRUE);
			$this->load->view('template/layout',$datalay);	
		} else {
			redirect('Main');
		}
		// $this->load->view('pelanggan/vmainform',$data);
	}

	function simpan()
	{
		if($this->session->userdata('loggedin')){
			$qnow = $this->db->query('select now() as now');
			$rnow = $qnow->row();
			$now  = $rnow->now;

			$data['i_kode_grup'] = $this->input->post("kodegrup",TRUE);
			$data['e_nama_grup'] = $this->input->post("namagrup",TRUE);
			$data['i_kode_perusahaan'] = $this->session->userdata('kode_perusahaan');
			$data['d_input'] 	 = $now;

			$this->db->trans_begin();
			$this->form_validation->set_rules('kodegrup', 'Kode Grup', 'required|max_length[4]');
		    $this->form_validation->set_rules('namagrup', 'Nama Grup', 'required');

		    //set message form validation
		    $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px"><div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
		    if ($this->form_validation->run() === TRUE) {
				$query = $this->M_pelanggan_grup->insertdata($data);

				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
				// cek jika query berhasil
		        if ($query) {
		          // Set success message
		          $message = array('status' => true, 'message' => 'Berhasil Menambahkan Data Grup Pelanggan');
		        } else {
		          // Set error message
		          $message = array('status' => false, 'message' => 'Gagal Menambahkan Data Grup Pelanggan');
		      	}
		        // simpan message sebagai session
		        $this->session->set_flashdata('message', $message);
		        // refresh page
		        redirect('pelanggan/C_pelanggan_grup');
		    }
		} else {
			redirect('Main');
		}
	}

	function edit()
	{
		if($this->session->userdata('loggedin')){
			if($this->input->post("kodegrup"))
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$idgrup = $this->input->post("idgrup",TRUE);
				$data['i_kode_grup'] = $this->input->post("kodegrup",TRUE);
				$data['e_nama_grup'] = $this->input->post("namagrup",TRUE);
				$data['i_kode_perusahaan'] = $this->session->userdata('kode_perusahaan');
				$data['d_update'] 	 = $now;

				$this->db->trans_begin();
				$this->form_validation->set_rules('kodegrup', 'Kode Grup', 'required|max_length[4]');
			    $this->form_validation->set_rules('namagrup', 'Nama Grup', 'required');

			    //set message form validation
			    $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px"><div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
			    if ($this->form_validation->run() === TRUE) {
					$query = $this->M_pelanggan_grup->updatedata($data,$idgrup);

					if($this->db->trans_status()===FALSE)
					{
						$this->db->trans_rollback();
					} else {
						$this->db->trans_commit();
					}
					// cek jika query berhasil
			        if ($query) {
			          // Set success message
			          $message = array('status' => true, 'message' => 'Berhasil Mengubah Data Grup Pelanggan');
			        } else {
			          // Set error message
			          $message = array('status' => false, 'message' => 'Gagal Mengubah Data Grup Pelanggan');
			      	}
			        // simpan message sebagai session
			        $this->session->set_flashdata('message', $message);
			        // refresh page
			        redirect('pelanggan/C_pelanggan_grup');
			    }
		    }
		    $ikode = $this->uri->segment(4);
			$data['page_title'] = "Grup Pelanggan";
			$data['litle_title'] = "Form Edit";
			$data['isi'] = $this->M_pelanggan_grup->daftargrup($ikodeprsh)->result();
			$data['isidata'] = $this->M_pelanggan_grup->carigrup($ikode);
			$datalay['content'] = $this->load->view('pelanggan/vformedit_grup',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function delete()
	{
		if($this->session->userdata('loggedin')){
			$ikode = $this->uri->segment(4);
			$this->db->trans_begin();
			$qdelete = $this->M_pelanggan_grup->deletedata($ikode);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
			// cek jika query berhasil
			if ($qdelete) {
				// Set success message
			    $message = array('status' => true, 'message' => 'Berhasil Menghapus Data Grup Pelanggan '.$ikode);
			} else {
				// Set error message
			    $message = array('status' => false, 'message' => 'Gagal Mengubah Data Grup Pelanggan '.$ikode);
			}
			// simpan message sebagai session
			$this->session->set_flashdata('message', $message);
			// refresh page
			redirect('pelanggan/C_pelanggan_grup');
		} else {
			redirect('Main');
		}
	}
}