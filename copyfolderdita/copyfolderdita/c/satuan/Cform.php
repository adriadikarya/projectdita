<?php 

/**
 * 
 */
class Cform extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('satuan/Mmaster');
	}

	function index()
	{
		if($this->session->userdata('loggedin'))
		{
			if($this->input->post("namasatuan")!='')
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$carisatuan				= strtoupper($this->input->post("namasatuan",TRUE));
				$data['e_satuan']   	= $this->input->post("namasatuan",TRUE);
				$data['d_input']		= $now;

				$this->db->trans_begin();

				$qcheck = $this->Mmaster->checksatuan($carisatuan);
				if($qcheck->num_rows()==0)
				{
					$insert = $this->Mmaster->insertdata($data);

					if($this->db->trans_status()===FALSE)
					{
						$this->db->trans_rollback();
					} else {
						$this->db->trans_commit();
					}
					// cek jika query berhasil
				    if ($insert) {
				    	// Set success message
				    	$message = array('status' => true, 'message' => 'Berhasil Menambah Data Satuan '.$data['e_satuan']);
					} else {
				    	// Set error message
				    	$message = array('status' => false, 'message' => 'Gagal Menambah Data Satuan '.$data['e_satuan']);
				    }
			    } else {
			    	// Set error message
				    $message = array('status' => false, 'message' => 'Nama Satuan '.$data['e_satuan'].' Sudah Ada!');
			    }
			    // simpan message sebagai session
			    $this->session->set_flashdata('message', $message);
			    // refresh page
			    redirect('satuan/Cform');
			}
			$data['page_title'] = "Satuan";
			$data['litle_title'] = "Form Input";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$data['satuan'] = $this->Mmaster->daftarsatuan();
			$datalay['content'] = $this->load->view('satuan/vmainform',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function edit()
	{
		if($this->session->userdata('loggedin'))
		{
			if($this->input->post("namasatuan")!='')
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$idsatuan 				= strtoupper($this->input->post("idsatuan",TRUE));
				$data['e_satuan']   = $this->input->post("namasatuan",TRUE);
				$data['d_update']		= $now;

				$this->db->trans_begin();

				$insert = $this->Mmaster->updatedata($data,$idsatuan);

				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
				// cek jika query berhasil
			    if ($insert) {
			    	// Set success message
			    	$message = array('status' => true, 'message' => 'Berhasil Mengubah Data satuan '.$data['e_satuan']);
				} else {
			    	// Set error message
			    	$message = array('status' => false, 'message' => 'Gagal Mengubah Data satuan '.$data['e_satuan']);
			    }
			    // simpan message sebagai session
			    $this->session->set_flashdata('message', $message);
			    // refresh page
			    redirect('satuan/Cform');
			}
			$idsatuan = $this->uri->segment(4);
			$data['page_title'] = "Satuan";
			$data['litle_title'] = "Form Edit";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$qcari = $this->Mmaster->carisatuan($idsatuan);
			if($qcari->num_rows()>0)
			{
				$rcari = $qcari->row();
				$data['idsatuan'] = $rcari->i_satuan;
				$data['namasatuan'] = $rcari->e_satuan;
			} else {
				$data['idsatuan'] = '';
				$data['namasatuan'] = '';
			}
			$datalay['content'] = $this->load->view('satuan/veditform',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function delete()
	{
		if($this->session->userdata('loggedin')){
			$ikode = $this->uri->segment(4);
			$nama  = $this->uri->segment(5);
			$this->db->trans_begin();
			$qdelete = $this->Mmaster->deletedata($ikode);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
			// cek jika query berhasil
			if ($qdelete) {
				// Set success message
			    $message = array('status' => true, 'message' => 'Berhasil Menghapus Data Satuan '.$nama);
			} else {
				// Set error message
			    $message = array('status' => false, 'message' => 'Gagal Menghapus Data Satuan '.$nama);
			}
			// simpan message sebagai session
			$this->session->set_flashdata('message', $message);
			// refresh page
			redirect('satuan/Cform');
		} else {
			redirect('Main');
		}
	}
}