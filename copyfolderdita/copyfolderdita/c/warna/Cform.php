<?php 

/**
 * 
 */
class Cform extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('warna/Mmaster');
	}

	function index()
	{
		if($this->session->userdata('loggedin'))
		{
			if($this->input->post("namawarna")!='')
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$cariwarna 				= strtoupper($this->input->post("namawarna",TRUE));
				$data['e_nama_warna']   = $this->input->post("namawarna",TRUE);
				$data['d_input']		= $now;

				$this->db->trans_begin();

				$qcheck = $this->Mmaster->checkwarna($cariwarna);
				if($qcheck->num_rows()==0)
				{
					$insert = $this->Mmaster->insertdata($data);

					if($this->db->trans_status()===FALSE)
					{
						$this->db->trans_rollback();
					} else {
						$this->db->trans_commit();
					}
					// cek jika query berhasil
				    if ($insert) {
				    	// Set success message
				    	$message = array('status' => true, 'message' => 'Berhasil Menambah Data Warna '.$data['e_nama_warna']);
					} else {
				    	// Set error message
				    	$message = array('status' => false, 'message' => 'Gagal Menambah Data Warna '.$data['e_nama_warna']);
				    }
			    } else {
			    	// Set error message
				    $message = array('status' => false, 'message' => 'Nama Warna '.$data['e_nama_warna'].' Sudah Ada!');
			    }
			    // simpan message sebagai session
			    $this->session->set_flashdata('message', $message);
			    // refresh page
			    redirect('warna/Cform');
			}
			$data['page_title'] = "Warna";
			$data['litle_title'] = "Form Input";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$data['warna'] = $this->Mmaster->daftarwarna();
			$datalay['content'] = $this->load->view('warna/vmainform',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function edit()
	{
		if($this->session->userdata('loggedin'))
		{
			if($this->input->post("namawarna")!='')
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$idwarna 				= strtoupper($this->input->post("idwarna",TRUE));
				$data['e_nama_warna']   = $this->input->post("namawarna",TRUE);
				$data['d_update']		= $now;

				$this->db->trans_begin();

				$insert = $this->Mmaster->updatedata($data,$idwarna);

				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
				// cek jika query berhasil
			    if ($insert) {
			    	// Set success message
			    	$message = array('status' => true, 'message' => 'Berhasil Mengubah Data Warna '.$data['e_nama_warna']);
				} else {
			    	// Set error message
			    	$message = array('status' => false, 'message' => 'Gagal Mengubah Data Warna '.$data['e_nama_warna']);
			    }
			    // simpan message sebagai session
			    $this->session->set_flashdata('message', $message);
			    // refresh page
			    redirect('warna/Cform');
			}
			$idwarna = $this->uri->segment(4);
			$data['page_title'] = "Warna";
			$data['litle_title'] = "Form Input";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$qcari = $this->Mmaster->cariwarna($idwarna);
			if($qcari->num_rows()>0)
			{
				$rcari = $qcari->row();
				$data['idwarna'] = $rcari->i_id_warna;
				$data['namawarna'] = $rcari->e_nama_warna;
			} else {
				$data['idwarna'] = '';
				$data['namawarna'] = '';
			}
			$datalay['content'] = $this->load->view('warna/veditform',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function delete()
	{
		if($this->session->userdata('loggedin')){
			$ikode = $this->uri->segment(4);
			$nama  = $this->uri->segment(5);
			$this->db->trans_begin();
			$qdelete = $this->Mmaster->deletedata($ikode);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
			// cek jika query berhasil
			if ($qdelete) {
				// Set success message
			    $message = array('status' => true, 'message' => 'Berhasil Menghapus Data Warna '.$nama);
			} else {
				// Set error message
			    $message = array('status' => false, 'message' => 'Gagal Menghapus Data Warna '.$nama);
			}
			// simpan message sebagai session
			$this->session->set_flashdata('message', $message);
			// refresh page
			redirect('warna/Cform');
		} else {
			redirect('Main');
		}
	}
}