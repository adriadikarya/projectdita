<?php

/**
 * 
 */
class Cform extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('sjnota/Mmaster');
	}


	function index()
	{
		if($this->session->userdata('loggedin'))
		{
			$data['page_title'] = "SJ (Nota)";
			$data['litle_title'] = "List SJ (Nota)";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$data['isi'] = $this->Mmaster->daftarnota($kodeprsh)->result();
			$datalay['content'] = $this->load->view('sjnota/vformview',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function tambah()
	{
		if($this->session->userdata('loggedin'))
		{	
			if($this->input->post("submit"))
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$tmp  = explode('/',$this->input->post("dnota",TRUE));
				// contoh 15/05/2019 dirombak jadi tmp[2]=2019, tmp[1]=05, tmp[0]=15
				$dnota = $tmp[2].'-'.$tmp[1].'-'.$tmp[0]; 

				$tmp1 = explode('/', $this->input->post("djthtempo",TRUE));
				$djthtempo =  $tmp1[2].'-'.$tmp1[1].'-'.$tmp1[0]; 

				$data['i_id_nota'] 			= $this->Mmaster->cekidnota();
				$data['i_no_nota'] 			= $this->input->post("nonota",TRUE);
				$data['i_no_order']			= $this->input->post("noorder",TRUE);
				$data['d_nota']				= $dnota;
				$data['d_jth_tempo']		= $djthtempo;
				$data['i_pelanggan']		= $this->input->post("pelanggan",TRUE);
				$data['v_ppn']				= str_replace(',', '', $this->input->post("pajak",TRUE));
				$data['v_total_fppn']		= str_replace(',','',$this->input->post("grandtot",TRUE));
				$data['v_total_fppn_sisa']	= str_replace(',','',$this->input->post("grandtot",TRUE));
				$data['i_jenis_pembayaran'] = $this->input->post("jnsbayar",TRUE);
				$data['e_gudang']			= $this->input->post("gudang",TRUE);
				$data['e_kepada']			= $this->input->post("kepada",TRUE);
				$data['i_kode_perusahaan']  = $this->session->userdata('kode_perusahaan');
				$data['d_update']			= $now;

				$this->db->trans_begin();
				$totalrow = $this->input->post("totrow",TRUE);
				$insert = $this->Mmaster->insertheader($data);

				for($i=1; $i<=$totalrow; $i++)
				{
					$data2['i_id_item_nota']= $this->Mmaster->cekiditemnota();
					$data2['i_id_nota'] 	= $data['i_id_nota'];
					$data2['i_kode_brg']	= $this->input->post("kodebrg".$i,TRUE);
					$data2['n_qty']			= $this->input->post("jml".$i,TRUE);
					$data2['v_harga_satuan']= str_replace(',', '', $this->input->post("hrg".$i,TRUE));
					$data2['e_desc']		= $this->input->post("e_desc".$i,TRUE);
					$data2['d_input']		= $now;
					$data2['i_no_item']		= $i;

					$warna = $this->input->post("totqtywrn".$i,TRUE);
					if($warna!=0)
					{
						for($j=1; $j<=$warna; $j++)
						{
							$data3['i_id_item_nota'] 	= $data2['i_id_item_nota'];
							$data3['i_kode_brg']		= $data2['i_kode_brg'];
							$data3['i_id_warna']		= $this->input->post("idwrn".$i.$j,TRUE);
							$data3['n_qty_warna']		= $this->input->post("qtywrn".$i.$j,TRUE);
							$data3['d_input']			= $now;
							$data3['i_no_item']			= $j;

							if($data3['n_qty_warna']!=0)
							{
								$insertwrn = $this->Mmaster->insertwarna($data3);
							}
						}
					}

					$insertdetail = $this->Mmaster->insertdetail($data2);
				}

				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
				// cek jika query berhasil
				if ($insert && $insertdetail) {
				   	// Set success message
					$message = array('status' => true, 'message' => 'Berhasil Menambah Data SJ(Nota), Kode ('.$data['i_no_nota'].')');
				} else {
					// Set error message
					$message = array('status' => false, 'message' => 'Gagal Menambah Data SJ(Nota), Kode ('.$data['i_no_nota'].')');
				}

				// simpan message sebagai session
			    $this->session->set_flashdata('message', $message);
			    // refresh page
			    redirect('sjnota/Cform');
			}
			$data['page_title'] = "SJ (Nota)";
			$data['litle_title'] = "Form Input SJ (Nota)";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$data['pel'] = $this->Mmaster->daftarpel($kodeprsh)->result();
			$datalay['content'] = $this->load->view('sjnota/vmainform',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function edit()
	{
		if($this->session->userdata('loggedin'))
		{
			// hapus item sekaligus update data
			if($this->input->post("submit",TRUE))
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$tmp  = explode('/',$this->input->post("dnota",TRUE));
				// contoh 15/05/2019 dirombak jadi tmp[2]=2019, tmp[1]=05, tmp[0]=15
				$dnota = $tmp[2].'-'.$tmp[1].'-'.$tmp[0]; 

				$tmp1 = explode('/', $this->input->post("djthtempo",TRUE));
				$djthtempo =  $tmp1[2].'-'.$tmp1[1].'-'.$tmp1[0]; 
				
				$idnota 					= $this->input->post("idnota",TRUE);				
				// $data['i_id_nota'] 			= $this->Mmaster->cekidnota();
				$data['i_no_nota'] 			= $this->input->post("nonota",TRUE);
				$data['i_no_order']			= $this->input->post("noorder",TRUE);
				$data['d_nota']				= $dnota;
				$data['d_jth_tempo']		= $djthtempo;
				$data['i_pelanggan']		= $this->input->post("pelanggan",TRUE);
				$data['v_ppn']				= str_replace(',', '', $this->input->post("pajak",TRUE));
				$data['v_total_fppn']		= str_replace(',','',$this->input->post("grandtot",TRUE));
				$data['v_total_fppn_sisa']	= str_replace(',','',$this->input->post("grandtot",TRUE));
				$data['i_jenis_pembayaran'] = $this->input->post("jnsbayar",TRUE);
				$data['e_gudang']			= $this->input->post("gudang",TRUE);
				$data['e_kepada']			= $this->input->post("kepada",TRUE);
				$data['i_kode_perusahaan']  = $this->session->userdata('kode_perusahaan');
				$data['d_update']			= $now;

				$this->db->trans_begin();

				$updatehead = $this->Mmaster->updatehead($data,$idnota);

				$deleteitem = $this->Mmaster->deletenota($idnota);

				$totrow = $this->input->post("totrow",TRUE);
				for($i=1; $i<=$totrow; $i++)
				{
					$iditemnota = $this->input->post("iditemnota".$i,TRUE);
					$deletewarna = $this->Mmaster->deletewarna($iditemnota);

					$data2['i_id_item_nota']= $this->Mmaster->cekiditemnota();
					$data2['i_id_nota'] 	= $idnota;
					$data2['i_kode_brg']	= $this->input->post("kodebrg".$i,TRUE);
					$data2['n_qty']			= $this->input->post("jml".$i,TRUE);
					$data2['v_harga_satuan']= str_replace(',', '', $this->input->post("hrg".$i,TRUE));
					$data2['e_desc']		= $this->input->post("e_desc".$i,TRUE);
					$data2['d_update']		= $now;
					$data2['i_no_item']		= $i;

					$warna = $this->input->post("totqtywrn".$i,TRUE);
					$hapus = $this->input->post("deleteitem".$i,TRUE);
					if($hapus!='y')
					{
						if($warna!=0)
						{
							for($j=1; $j<=$warna; $j++)
							{
								$data3['i_id_item_nota'] 	= $data2['i_id_item_nota'];
								$data3['i_kode_brg']		= $data2['i_kode_brg'];
								$data3['i_id_warna']		= $this->input->post("idwrn".$i.$j,TRUE);
								$data3['n_qty_warna']		= $this->input->post("qtywrn".$i.$j,TRUE);
								$data3['d_update']			= $now;
								$data3['i_no_item']			= $j;

								if($data3['n_qty_warna']!=0)
								{
									$insertwrn = $this->Mmaster->insertwarna($data3);
								}
							}
						}

						$insertdetail = $this->Mmaster->insertdetail($data2);
					}
				}
				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
				// cek jika query berhasil
				if ($updatehead && $insertdetail) {
				   	// Set success message
					$message = array('status' => true, 'message' => 'Berhasil Mengubah Data SJ(Nota) No Nota: '.$data['i_no_nota']);
				} else {
					// Set error message
					$message = array('status' => false, 'message' => 'Gagal Mengubah Data SJ(Nota) No Nota: '.$data['i_no_nota']);
				}
				// simpan message sebagai session
			    $this->session->set_flashdata('message', $message);

				redirect('sjnota/Cform');
			}
			$idnota = $this->uri->segment(4);			
			$data['page_title'] = "SJ (Nota)";
			$data['litle_title'] = "Form Edit SJ (Nota)";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$data['pel'] = $this->Mmaster->daftarpel($kodeprsh)->result();
			$data['isi'] = $this->Mmaster->enota($idnota)->row();
			$data['isiitem'] = $this->Mmaster->enotaitem($idnota)->result();
			$data['totalitem'] = $this->Mmaster->enotaitem($idnota)->num_rows();
			// $data['isiwarna']= $this->Mmaster->enotaitemwarna($iditemnota)->result();
			$datalay['content'] = $this->load->view('sjnota/veditform',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function cancel()
	{
		if($this->session->userdata('loggedin'))
		{	
			$idnota = $this->uri->segment(4);
			$nonota = $this->uri->segment(5);
			$this->db->trans_begin();
			$qdelete = $this->Mmaster->canceldata($idnota);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
			// cek jika query berhasil
			if ($qdelete) {
				// Set success message
			    $message = array('status' => true, 'message' => 'Berhasil Cancel Data Nota '.$nonota);
			} else {
				// Set error message
			    $message = array('status' => false, 'message' => 'Gagal Cancel Data Barang '.$nonota);
			}
			// simpan message sebagai session
			$this->session->set_flashdata('message', $message);
			// refresh page
			redirect('sjnota/Cform');
		} else {
			redirect('Main');
		}
	}

	function ambilDataPel()
	{
		$ipel = $this->input->post("ipel");
		$qcari = $this->db->query("select * from tr_pelanggan where i_pelanggan='$ipel'");
		if($qcari->num_rows()>0)
		{
			$rpel = $qcari->row();
			$top  = $rpel->n_top;
			$alamat = $rpel->e_alamat_pelanggan;
		} else {
			$top  = 30;
			$alamat = "Tidak Ada";
		}

		echo json_encode(array("jangka"=>$top,"alamat"=>$alamat));
	}

	function cariwarna()
	{
		$kodebrg = $this->input->post("ikode");
		$baris   = $this->input->post("brs");
		$kodeprsh = $this->session->userdata("kode_perusahaan");
		$qcari = $this->db->query("select a.*, b.e_nama_warna from tr_barang_warna a, tr_warna b where a.i_id_warna=b.i_id_warna and a.i_kode_brg='$kodebrg' and a.i_kode_perusahaan='$kodeprsh'");
		$output = "";
		$total = $qcari->num_rows();
		if($qcari->num_rows()>0)
		{
			$i=1;
			foreach ($qcari->result() as $row) {
				$output.= '<div id="lwrn'.$baris.$i.'" class="input-group input-group-sm"><span class="input-group-addon">'.$row->e_nama_warna.'</span><input type="text" class="form-control" name="qtywrn'.$baris.$i.'" id="qtywrn'.$baris.$i.'" style="text-align:right;" placeholder="0" onkeyup="hitungtotwarna(\''.$baris.'\',\''.$i.'\')"><input type="hidden" name="idwrn'.$baris.$i.'" id="idwrn'.$baris.$i.'" value="'.$row->i_id_warna.'"></div>';
				$i++;
			}
		} else {
			$output.="";
		}

		echo json_encode(array("out"=>$output,"totrowqty"=>$total));
	}

	function listbrg()
	{
		if($this->session->userdata('loggedin'))
		{
			$data_modal['baris'] = $this->uri->segment(4);
			$kodeprsh = $this->session->userdata("kode_perusahaan");
			$data_modal['isibrg'] = $this->Mmaster->daftarbrg($kodeprsh)->result();
			$data_modal['title_modal'] = "List Barang";
			$this->load->view("sjnota/vlistbrg",$data_modal);
		} else {
			redirect('Main');
		}
	}

	function listdetailnota()
	{
		if($this->session->userdata('loggedin'))
		{
			$idnota = $this->uri->segment(4);
			$data_modal['nonota'] = $this->uri->segment(5);
			$kodeprsh = $this->session->userdata("kode_perusahaan");
			$data_modal['isi'] = $this->Mmaster->daftardetnota($idnota,$kodeprsh)->result();
			$data_modal['title_modal'] = "List Detail Nota";
			$this->load->view("sjnota/vlistdetnota",$data_modal);	
		} else {
			redirect('Main');
		}
	}

	function printnota()
	{
		// $this->load->view('sjnota/vprintf2');
		$this->load->view('sjnota/vprintf3');
	}
}