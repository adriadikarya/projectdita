<?php
// Irawan 2018-11-02
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('pp/Mmaster');
	}

	function index(){

		$cari = trim($this->input->post('cari'));
		
		//Pagination start
		$config['base_url'] = base_url().'index.php/pp/Cform/'.$cari.'/';
		$config['total_rows'] = $this->Mmaster->getppCari($cari)->num_rows();
		$config['per_page'] = '10';
		$config['full_tag_open'] = '<ul class="pagination a">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'Awal';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Akhir';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Selanjutnya';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Sebelumnya';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_page'] = $this->uri->segment(5);
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$config['offset'] = $this->uri->segment(5);
		$this->pagination->initialize($config);
		
		//data for view
		$data['search'] = $cari;
		$data['isi'] = $this->Mmaster->getpp($config['per_page'], $config['offset'],$cari)->result();
		$data['page_title'] = $this->lang->line('pp');
		$this->load->view('pp/vformview', $data);
	
	}

	function add(){

		if($this->input->post('i_supplier')){
			$this->form_validation->set_rules('i_supplier', 'Kode Supplier', 'required');
			$this->form_validation->set_rules('e_supplier_name', 'Nama Supplier', 'required');
			$this->form_validation->set_message('required', '%s Tidak boleh kosong!');
		
			if ($this->form_validation->run() === TRUE) {
			$query 	= $this->db->query("SELECT current_timestamp as c");
	   		$row   	= $query->row();
	    	$now	  = $row->c;
			
			$data['i_supplier'] = $this->input->post("i_supplier",TRUE);
			$data['e_supplier_name'] = $this->input->post("e_supplier_name",TRUE);
			$data['e_supplier_address'] = $this->input->post('e_supplier_address',TRUE);
			$data['e_supplier_city'] = $this->input->post('e_supplier_city',TRUE);
			$data['e_supplier_cp'] = $this->input->post('e_supplier_cp',TRUE);
			$data['e_supplier_tlp'] = $this->input->post('e_supplier_tlp',TRUE);
			$data['e_supplier_fax'] = $this->input->post('e_supplier_fax',TRUE);
			$data['n_top'] = $this->input->post('n_top',TRUE);
			if($this->input->post('f_pkp')){
				$fpkp ='true';
			}else{
				$fpkp = 'false';
			}
			$data['f_pkp'] = $fpkp;
			$data['i_npwp'] = $this->input->post('i_npwp',TRUE);
			$data['e_npwp_name'] = $this->input->post('e_npwp_name',TRUE);
			$data['f_tipe_pajak'] = $this->input->post('f_tipe_pajak',TRUE);
			$data['i_category'] = $this->input->post('i_category',TRUE);
			$data['i_type'] = $this->input->post('i_type',TRUE);
			$data['d_supplier_input'] = $now;

			$query = $this->Mmaster->insert($data);
		
			if ($query){
			 $message = array('status' => true, 'message' => 'Berhasil Menambahkan '.$data['e_supplier_name']);
        	}else{
        	 $message = array('status' => false, 'message' => 'Gagal Menambahkan');
			}
			$this->session->set_flashdata('message', $message);

			redirect('pp/Cform');
			}
		}

		$data['page_title'] = $this->lang->line('pp');
		$data['suppliercategory'] = $this->Mmaster->getSuppliercategory()->result();
		$data['suppliertype'] = $this->Mmaster->getSuppliertype()->result();
		$this->load->view('pp/vform', $data);
		
	}

		function edit($isupplier=null){

		if($this->input->post('i_supplier')){

			$this->form_validation->set_rules('i_supplier', 'Kode Supplier', 'required');
			$this->form_validation->set_rules('e_supplier_name', 'Nama Supplier', 'required');
			$this->form_validation->set_message('required', '%s Tidak boleh kosong!');
		
			if ($this->form_validation->run() === TRUE) {
			$query 	= $this->db->query("SELECT current_timestamp as c");
	   		$row   	= $query->row();
	    	$now	= $row->c;
			$supplier =  $this->input->post("i_supplier",TRUE);
			//$data['i_supplier'] = $this->input->post("i_supplier",TRUE);
			$data['e_supplier_name'] = $this->input->post("e_supplier_name",TRUE);
			$data['e_supplier_address'] = $this->input->post('e_supplier_address',TRUE);
			$data['e_supplier_city'] = $this->input->post('e_supplier_city',TRUE);
			$data['e_supplier_cp'] = $this->input->post('e_supplier_cp',TRUE);
			$data['e_supplier_tlp'] = $this->input->post('e_supplier_tlp',TRUE);
			$data['e_supplier_fax'] = $this->input->post('e_supplier_fax',TRUE);
			$data['n_top'] = $this->input->post('n_top',TRUE);
			if($this->input->post('f_pkp')){
				$fpkp ='true';
			}else{
				$fpkp = 'false';
			}
			$data['f_pkp'] = $fpkp;
			$data['i_npwp'] = $this->input->post('i_npwp',TRUE);
			$data['e_npwp_name'] = $this->input->post('e_npwp_name',TRUE);
			$data['f_tipe_pajak'] = $this->input->post('f_tipe_pajak',TRUE);
			$data['i_category'] = $this->input->post('i_category',TRUE);
			$data['i_type'] = $this->input->post('i_type',TRUE);
			$data['d_supplier_input'] = $now;

			$query = $this->Mmaster->update($supplier,$data);
		
			if ($query){
			 $message = array('status' => true, 'message' => 'Berhasil Update '.$data['e_supplier_name']);
        	}else{
        	 $message = array('status' => false, 'message' => 'Gagal Update');
			}
			$this->session->set_flashdata('message', $message);

			redirect('pp/Cform');
			}
		}

		$data['page_title'] = $this->lang->line('pp');
		$data['isi'] = $this->Mmaster->get_where($isupplier)->row();
		$data['suppliercategory'] = $this->Mmaster->getSuppliercategory()->result();
		$data['suppliertype'] = $this->Mmaster->getSuppliertype()->result();
		$this->load->view('pp/vformupdate', $data);
		
	}

}