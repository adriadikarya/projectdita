<style>
tr td{
    padding: 3px !important;
}
</style>
<div id="tmp"> 
    <!-- <?php echo $this->pquery->form_remote_tag(array('url'=>'index.php/op/Cform_aplikasi/view/','update'=>'#main','type'=>'post'));?> -->
    <section class="panel panel-default"><h2><?php echo $page_title ?></h2>
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-3 col-xs-3">                
                  <a onclick='show("<?php echo 'fakturbtb/Cform_aplikasi/add'; ?>","#main");' href='#' title='<?php echo "Tambah Data"; ?>' class='btn btn-success btn-sm' data-tooltip='tooltip' data-placement='top'><i class='glyphicon glyphicon-plus'></i></a>
                </div>
                <div class="col-md-4 col-xs-9">             
                 <?php 
                  echo $this->pquery->form_remote_tag(array('url'=>'fakturbtb/Cform_aplikasi/index/','update'=>'#main','type'=>'post'));
                 ?>   
                           <div class="input-group pull-right">                      
                                 <input type="text" class="form-control" placeholder="Cari data" name="cari" id="cari" style="height:30px;" value="<?php echo $cari; ?>"> 
                                 <input type="hidden" name="is_cari" value= "1" >
                                 <span class="input-group-btn">
                                      <button class="btn btn-primary btn-sm" type="submit"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                 </span>
                           </div>
                  <?php echo form_close(); ?>   
                </div>
            </div>
        </header>


        <div class="">
            <?php if($message = $this->session->flashdata('message')): ?>
              <div class="alert alert-<?php echo ($message['status']) ? 'success' : 'danger'; ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><?php echo $message['message']; ?></strong>
              </div>
            <?php endif; ?>
            <table class="table table-bordered table-condensed" style="font-size:12px;">

                <thead>
                   <tr>
                      <th>No</th>
                      <th>No Nota</th>
                      <th>Tanggal Nota</th>
                      <!-- <th>Keterangan</th>                     -->
                      <th>Supplier</th>
                      <th>Status</th>
                      <th>Action</th>
                  </tr>
                </thead>
              <?php if($isi){ 
                      $i=0;
                      foreach($isi as $row) {
                      $i++; 
                  
                        // if(strpos($row->i_sj, '/') == true)
                        // {
                        //   $sjfake = str_replace('/', '_', $row->i_sj);
                        // } else {
                        //   $sjfake = $row->i_sj;
                        // }

                        if($row->f_nota_cancel=='t'){
                            $status = 'Batal';
                        }else{
                            $status = 'Tidak';
                        } 

                        if($row->d_nota){
                          $dnota = date('d F Y', strtotime($row->d_nota)); 
                        }else{
                          $dnota="";
                        } 
            ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row->i_nota; ?></td>
                        <td><?php echo $dnota; ?></td>
                        <!-- <td><?php echo $row->e_desc; ?></td> -->
                        <td><?php echo $row->e_supplier_name; ?></td>
                        <td><?php echo $status; ?></td>
                        <!-- <td>&nbsp;</td> -->
                        <?php 
                          if($row->f_nota_cancel=='f'){
                        ?>
                        <td><a onclick='show("<?php echo 'fakturbtb/Cform_aplikasi/edit/'.$row->i_nota.'/'.$row->i_supplier.'/'.$row->i_payment_type.'/'?>","#main");' href='#' title='<?php echo "Edit"; ?>' class='btn btn-sm btn-success' data-tooltip='tooltip' data-placement='top'><i class="glyphicon glyphicon-edit"></i></a>
                          <a onclick='hapus("<?php echo 'fakturbtb/Cform_aplikasi/cancel/'.$row->i_nota.'/'?>","#main");' href='#' title='<?php echo "Cancel"; ?>' class='btn btn-sm btn-danger' data-tooltip='tooltip' data-placement='top'><i class="glyphicon glyphicon-trash"></i></a></td>
                        <?php
                          } else {
                            echo "<td>&nbsp;</td>";
                          }
                        ?>
                      </tr>
                <?php } 
                    } ?>
              </table>
        </div>


      <div class="panel-footer">
        <div class="row">
            <?php echo "<center>".$this->pagination->create_links()."</center>";?>
            <div class="col-md-3">
             Total Data
             <span class="label label-info">
                <?php echo $total; ?>
            </span>

        </div>  
        <div class="col-md-9">

        </div>
    </div>
</div>
</section>
</div>
