<?php
/**
 * 
 */
class Cform_aplikasi extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('fakturbtb/Mmaster_aplikasi','Mmaster');
	}

	function index()
	{
		$cari = strtoupper(trim($this->input->post('cari')));
		
		//Pagination start
		$config['base_url'] = base_url().'index.php/fakturbtb/Cform_aplikasi/index/';
		$config['total_rows'] = $this->Mmaster->getnotaCari($cari)->num_rows();
		$config['per_page'] = '10';
		$config['full_tag_open'] = '<ul class="pagination a">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'Awal';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Akhir';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Selanjutnya';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Sebelumnya';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_page'] = $this->uri->segment(4);
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$config['offset'] = $this->uri->segment(4);
		$this->pagination->initialize($config);
		
		//data for view
		$data['total'] = $config['total_rows'];
		$data['page_title'] = "Nota Pembelian (BTB) Aplikasi";
		$data['cari'] = $cari;
		$data['isi'] = $this->Mmaster->getnota($config['per_page'], $config['offset'],$cari)->result();
		$this->load->view('fakturbtb/vformview_aplikasi', $data);
	}

	function add()
	{
		if($this->input->post('nofaktur')){

			// $this->form_validation->set_rules('dfaktur','Tanggal Faktur','required');
			$this->form_validation->set_rules('nofaktur','Nomor Faktur','required');

			if ($this->form_validation->run() === TRUE) {
				$query 	= $this->db->query("SELECT current_timestamp as c");
		   		$row   	= $query->row();
		    	$now	  = $row->c;

		    	$dfaktur = $this->input->post("dfaktur",true);
		    	if($dfaktur){
		    		 $tmp 	= explode('-', $dfaktur);
		    		 $day 	= $tmp[0];
		    		 $month = $tmp[1];
		    		 $year	= $tmp[2];
		    		 $yearmonth = $year.$month;
		    		 $datefaktur = $year.'-'.$month.'-'.$day;
		    	}

				$this->db->trans_begin(); 
				$data['i_nota'] = $this->input->post("nofaktur",TRUE);
				$data['d_nota'] = $datefaktur;
				$data['i_supplier'] = $this->input->post("i_supplier",TRUE);
				$data['i_payment_type'] = $this->input->post('jnspemb',TRUE);
				$data['f_pkp'] = $this->input->post("f_pkp");
				$data['v_total'] = str_replace(',','',$this->input->post("totfaktur",TRUE));
				$data['v_sisa'] = str_replace(',','',$this->input->post("totfaktur",TRUE));
				$data['d_entry'] = $now;
				$query = $this->Mmaster->insertheader($data);

				$count = $this->input->post("jml",true);

				for($i=1;$i<=$count;$i++){
					if($this->input->post('cek'.$i)=='cek'){
						$data2['i_nota']     = $data['i_nota'];
						$data2['i_sj'] 		 = $this->input->post("nosj".$i,TRUE);
						$data2['v_tot_sj'] = $this->input->post("total".$i,TRUE);
						$data2['d_entry'] 	 = $now;
						$data2['i_no_item']	 = $i;
						$querydetail = $this->Mmaster->insertdetail($data2);
						$update = $this->Mmaster->updatesj($data2['i_sj']);
					}
				}

			if (($this->db->trans_status() === FALSE))
			{
			   	$this->db->trans_rollback();
			}
			else{
				$this->db->trans_commit();	
			}
			if ($query){
				$message = array('status' => true, 'message' => 'Berhasil Menambahkan Nota ('.$data['i_nota'].')');
        	}else{
        	 	$message = array('status' => false, 'message' => 'Gagal Menambahkan Nota');
			}
			$this->session->set_flashdata('message', $message);
			redirect('fakturbtb/Cform_aplikasi');

	    	}
		}
		
		$data['page_title'] = "Nota Pembelian (BTB) Aplikasi";
		$this->load->view('fakturbtb/vform_aplikasi', $data);
	}

	function edit($inota,$supplier,$ipayment)
	{
		if($this->input->post('nofaktur')){

			// $this->form_validation->set_rules('dfaktur','Tanggal Faktur','required');
			$this->form_validation->set_rules('nofaktur','Nomor Faktur','required');

			if ($this->form_validation->run() === TRUE) {
				$query 	= $this->db->query("SELECT current_timestamp as c");
		   		$row   	= $query->row();
		    	$now	  = $row->c;

		    	$dfaktur = $this->input->post("dfaktur",true);
		    	if($dfaktur){
		    		 $tmp 	= explode('-', $dfaktur);
		    		 $day 	= $tmp[0];
		    		 $month = $tmp[1];
		    		 $year	= $tmp[2];
		    		 $yearmonth = $year.$month;
		    		 $datefaktur = $year.'-'.$month.'-'.$day;
		    	}

				$this->db->trans_begin(); 
				$i_nota = $this->input->post("nofaktur",TRUE);
				$data['d_nota'] = $datefaktur;
				// $data['i_supplier'] = $this->input->post("i_supplier",TRUE);
				$data['i_payment_type'] = $this->input->post('jnspemb',TRUE);
				// $data['f_pkp'] = $this->input->post("f_pkp");
				$data['v_total'] = str_replace(',','',$this->input->post("totfaktur",TRUE));
				$data['v_sisa'] = str_replace(',','',$this->input->post("totfaktur",TRUE));
				$data['d_update'] = $now;
				$query = $this->Mmaster->updateheader($data,$i_nota);

				$qback = $this->Mmaster->deletenotasj($i_nota);
				$count = $this->input->post("jml",true);

				for($i=1;$i<=$count;$i++){
					if($this->input->post('cek'.$i)=='cek'){
						$data2['i_nota']     = $i_nota;
						$data2['i_sj'] 		 = $this->input->post("nosj".$i,TRUE);
						$data2['v_tot_sj'] 	 = $this->input->post("total".$i,TRUE);
						$data2['d_entry'] 	 = $now;
						$data2['i_no_item']	 = $i;
						$querydetail = $this->Mmaster->insertdetail($data2);
						$update = $this->Mmaster->updatesj($data2['i_sj']);
					}
				}

			if (($this->db->trans_status() === FALSE))
			{
			   	$this->db->trans_rollback();
			}
			else{
				$this->db->trans_commit();	
			}
			if ($query){
				$message = array('status' => true, 'message' => 'Berhasil Edit Nota ('.$i_nota.')');
        	}else{
        	 	$message = array('status' => false, 'message' => 'Gagal Edit Nota');
			}
			$this->session->set_flashdata('message', $message);
			redirect('fakturbtb/Cform_aplikasi');

	    	}
		}

		$data['page_title'] = "Nota Pembelian (BTB) Aplikasi";
		$data['header'] = $this->Mmaster->getheader($inota);
		$data['isi'] = $this->Mmaster->getitemedit($inota,$supplier,$ipayment);
		$this->load->view('fakturbtb/vformedit_aplikasi',$data);
	}

	function listSJ($supplier,$jnspemb)
	{
		$data['isi'] = $this->Mmaster->getbtbitem($jnspemb,$supplier);
		$output = $this->load->view("fakturbtb/vform_isi_aplikasi",$data,TRUE);
		echo $output;
	}

	function listSJedit($supplier,$jnspemb,$inota)
	{
		$data['isi'] = $this->Mmaster->getitemedit($inota,$supplier,$jnspemb);
		$nilaitotal = 0;
		if(!empty($data['isi'])){
			foreach($data['isi'] as $row){ 
				if(!empty($row->sjnota)){
					$nilaitotal+=$row->v_total;
				}
			}
		} else {
			$nilaitotal=0;
		}				
		$output = $this->load->view("fakturbtb/vform_isiedit_aplikasi",$data,TRUE);
		// echo $output;
		echo json_encode(array('output'=>$output,'nilai'=>number_format($nilaitotal,2)));
	}

	function cancel($inota)
	{
		$this->db->trans_begin();
		$query = $this->Mmaster->cancelnota($inota);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}

		if ($query){
			$message = array('status' => true, 'message' => 'Berhasil Cancel '.$inota);
        }else{
         	$message = array('status' => false, 'message' => 'Gagal Cancel');
		}
		$this->session->set_flashdata('message', $message);
		redirect('fakturbtb/Cform_aplikasi');
	}

	function supplier()
	{
		$cari=strtoupper($this->input->post("cari"));
		if($this->uri->segment(5)!='x01'){
        	if($cari=='') $cari=$this->uri->segment(5);
       		$config['base_url'] = base_url().'index.php/fakturbtb/Cform_aplikasi/supplier/'.$cari.'/';
     	}else{
        	$config['base_url'] = base_url().'index.php/fakturbtb/Cform_aplikasi/supplier/x01/';
      	}

		$config['total_rows'] = $this->Mmaster->bacasuppliercari($cari)->num_rows();
		$config['per_page'] = '10';
		$config['full_tag_open'] = '<ul class="pagination a">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'Awal';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Akhir';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Selanjutnya';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Sebelumnya';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_page'] = $this->uri->segment(6);
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$data['page_title'] = "List Supplier";
		$data['isi']=$this->Mmaster->bacasupplier($config['per_page'],$this->uri->segment(6),$cari)->result();
		$this->load->view('fakturbtb/vlistsupplier_aplikasi', $data);
	}
}