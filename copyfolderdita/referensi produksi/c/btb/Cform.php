<?php
// Irawan 2018-11-02
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('btb/Mmaster');
	}

	function index(){

		$cari = strtoupper(trim($this->input->post('cari')));
		
		//Pagination start
		$config['base_url'] = base_url().'index.php/btb/Cform/index/';
		$config['total_rows'] = $this->Mmaster->getsjCari($cari)->num_rows();
		$config['per_page'] = '10';
		$config['full_tag_open'] = '<ul class="pagination a">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'Awal';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Akhir';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Selanjutnya';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Sebelumnya';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_page'] = $this->uri->segment(4);
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$config['offset'] = $this->uri->segment(4);
		$this->pagination->initialize($config);
		
		//data for view
		$data['total'] = $config['total_rows'];
		$data['page_title'] = "Bukti Terima Barang";
		$data['cari'] = $cari;
		$data['isi'] = $this->Mmaster->getsj($config['per_page'], $config['offset'],$cari)->result();
		$this->load->view('btb/vformview', $data);
	
	}

	function cariindex()
	{
		$cari = strtoupper(trim($this->input->post('cari')));
		//Pagination start
		if($cari=='')$cari=$this->uri->segment(4);
		$config['base_url'] = base_url().'index.php/btb/Cform/cariindex/'.$cari.'/';
		$config['total_rows'] = $this->Mmaster->getsjCari($cari)->num_rows();
		$config['per_page'] = '10';
		$config['full_tag_open'] = '<ul class="pagination a">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'Awal';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Akhir';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Selanjutnya';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Sebelumnya';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$config['cur_page'] = $this->uri->segment(5);
		$config['offset'] = $this->uri->segment(5);
		$this->pagination->initialize($config);
		
		//data for view
		$data['total'] = $this->Mmaster->getsjCari($cari)->num_rows();
		$data['cari'] = $cari;
		$data['isi'] = $this->Mmaster->getsj($config['per_page'], $config['offset'],$cari)->result();
		$data['page_title'] = 'Order Pembelian';
		$this->load->view('btb/vformview', $data);
	}

	function add($proses){
		if($proses!='1'){
			if($this->input->post("tidakadaop",TRUE))
			{	
				$data['page_title'] = "Bukti Terima Barang Tanpa OP";
				$this->load->view('btb/vformlain',$data);
			} else {
				$jnspemb = $this->input->post("jnspemb",TRUE);
				$supplier = $this->input->post("i_supplier",TRUE);
				$data['jnspemb'] = $jnspemb;
				$data['page_title'] = "List Order Pembelian";
				$data['isi'] = $this->Mmaster->getOPitem($jnspemb,$supplier);
				$this->load->view('btb/vform', $data);
			}
		} else {
			$data['page_title'] = "Bukti Terima Barang";
			$this->load->view('btb/vformawal',$data);
		}
	}

	function edit($isj=null){

		if($this->input->post('dsj')){

			$this->form_validation->set_rules('dsj','Tanggal SJ','required');
			$this->form_validation->set_rules('isj','Nomor SJ','required');

			if ($this->form_validation->run() === TRUE) {
				$query 	= $this->db->query("SELECT current_timestamp as c");
		   		$row   	= $query->row();
		    	$now	  = $row->c;

		    	$dsj = $this->input->post("dsj",true);
		    	if($dsj){
		    		 $tmp 	= explode('-', $dsj);
		    		 $day 	= $tmp[0];
		    		 $month = $tmp[1];
		    		 $year	= $tmp[2];
		    		 $yearmonth = $year.$month;
		    		 $datesj = $year.'-'.$month.'-'.$day;
		    	}

				$this->db->trans_begin(); 
				$i_sj = $this->input->post("isj",TRUE);
				$data['d_sj'] = $datesj;
				// $data['i_supplier'] = $this->input->post("isupplier",TRUE);
				$data['i_payment_type'] = $this->input->post('paymenttype',TRUE);
				// $data['i_tax_type'] = $this->input->post("tipepajak",TRUE);
				$data['v_tax'] = str_replace(',','',$this->input->post("totppn",TRUE));
				// $data['f_pkp'] = $this->input->post("pkp")=='cek'?'t':'f';
				$data['v_total'] = str_replace(',','',$this->input->post("grandtot",TRUE));
				// $data['v_total_op'] = str_replace(',','',$this->input->post("grandtotop",TRUE));
				$data['v_sisa'] = str_replace(',','',$this->input->post("grandtot",TRUE));
				$data['e_desc'] = $this->input->post('eremark',TRUE);
				$data['d_update'] = $now;
				$query = $this->Mmaster->updateheader($data,$i_sj);

				$querydelete = $this->Mmaster->delete($i_sj);
				$count = $this->input->post("jml",true);

				for($i=1;$i<=$count;$i++){
					$data2['i_sj'] 		 = $this->input->post("isj",TRUE);
					$data2['i_op'] 		 = $this->input->post("iop".$i,TRUE);
					$data2['i_material'] = $this->input->post("imaterial".$i,TRUE);
					$data2['n_qty'] 	 = $this->input->post("nquantity".$i,TRUE);
					$data2['n_discount'] = $this->input->post("diskon".$i,TRUE);
					$data2['v_unit_price'] = str_replace(',','',$this->input->post("vprice".$i,TRUE));
					$data2['v_unit_price_op'] = str_replace(',','',$this->input->post("vpriceop".$i,TRUE));
					$data2['i_unit'] 	 = $this->input->post("isatuan".$i,TRUE);
					$data2['i_unit_conversion'] = $this->input->post("isatkonv".$i,TRUE);
					$data2['d_entry'] 	 = $now;
					$data2['f_unit_conversion'] = $this->input->post("konversi".$i)=="tidak"?'f':'t';
					$data2['i_formula'] = $this->input->post("iformula".$i,TRUE);
					$data2['n_formula_factor'] = $this->input->post("nformulafactor".$i,TRUE);
					$data2['i_no_item']	 = $i;
					if($data2['i_op']!=0){
						$querycek = $this->Mmaster->cekqty($data2['i_op'],$data2['i_material']);
						$qtyop = $querycek->op;
						$qtysj = $querycek->sj;
						$totsj = $qtysj+$data2['n_qty'];
						// var_dump($qtypp,$qtyop,$totop);die;
						if($totsj >= $qtyop){
							$data2['n_qty'] = $qtyop-$qtysj;
							$op=$data2['i_op'];
							$material=$data2['i_material'];
							$satuan=$data2['i_satuan'];
							$dataop['f_complete'] = true;
							$complete = $this->Mmaster->itemComplete($dataop,$op,$material,$satuan);
						}
					}
					$querydetail = $this->Mmaster->insertdetail($data2);
				}

			if (($this->db->trans_status() === FALSE))
			{
			   	$this->db->trans_rollback();
			}
			else{
				$this->db->trans_commit();	
			}
			if ($query){
				$message = array('status' => true, 'message' => 'Berhasil Menambahkan '.$data['i_op']);
        	}else{
        	 	$message = array('status' => false, 'message' => 'Gagal Menambahkan');
			}
			$this->session->set_flashdata('message', $message);
			redirect('btb/Cform');

	    	}

		}

		$data['page_title'] = $this->lang->line("Edit BTB");
		if(strpos($isj, '_') == true)
		{
			$sjfake = str_replace('_', '/', $isj);
		} else {
			$sjfake = $isj;
		}
		$data['isi'] = $this->Mmaster->getHeader($sjfake);
		// $data['listpp'] = $this->Mmaster->getListPP($sjfake);
		$data['detail'] = $this->Mmaster->getItem($sjfake);
		$this->load->view('btb/vformupdate', $data);
		
	}

	function cancel($isj)
	{
		if(strpos($isj,'_') == true)
		{
			$sjfake = str_replace('_', '/', $isj);
		} else {
			$sjfake = $isj;
		}
		$this->db->trans_begin();
		$query = $this->Mmaster->cancelsj($sjfake);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}

		if ($query){
			$message = array('status' => true, 'message' => 'Berhasil Cancel '.$sjfake);
        }else{
         	$message = array('status' => false, 'message' => 'Gagal Cancel');
		}
		$this->session->set_flashdata('message', $message);
		redirect('btb/Cform');
	}

	function material()
	{		
			$cari = strtoupper($this->input->post('cari', TRUE));
      		$baris = strtoupper($this->input->post('baris', TRUE));
      		$gudang = strtoupper($this->input->post('gudang',TRUE));
 			if($baris==''){ 
 				$baris=$this->uri->segment(4); 
 			}
 			if($gudang==''){
 				$gudang=$this->uri->segment(5);
 			}
     	    if($this->uri->segment(6)!='x01'){
        	if($cari=='') $cari=$this->uri->segment(6);
       		$config['base_url'] = base_url().'index.php/btb/Cform/material/'.$baris.'/'.$gudang.'/'.$cari.'/';
     		}else{
        	$config['base_url'] = base_url().'index.php/btb/Cform/material/'.$baris.'/'.$gudang.'/x01/';
      		}

			$config['total_rows'] = $this->Mmaster->getmaterialCari($cari,$gudang)->num_rows(); 
			$config['per_page'] = '10';
			$config['full_tag_open'] = '<ul class="pagination a">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = 'Awal';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Akhir';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			$config['next_link'] = 'Selanjutnya';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = 'Sebelumnya';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			$config['cur_page'] = $this->uri->segment(7);
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);

			$this->load->model('btb/Mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$baris;
			$data['isi']=$this->Mmaster->bacamaterial($config['per_page'],$this->uri->segment(7),$cari,$gudang);
			$this->load->view('btb/vlistmaterial', $data);
	}

	function satkonversi()
	{
		$cari = strtoupper($this->input->post('cari', TRUE));
      	$baris = strtoupper($this->input->post('baris', TRUE));

 		if($baris==''){ 
 			$baris=$this->uri->segment(4); 
 		}
 		$isatuan = $this->uri->segment(5);
     	if($this->uri->segment(6)!='x01'){
        if($cari=='') $cari=$this->uri->segment(6);
       		$config['base_url'] = base_url().'index.php/btb/Cform/material/'.$baris.'/'.$isatuan.'/'.$cari.'/';
     	}else{
        	$config['base_url'] = base_url().'index.php/btb/Cform/material/'.$baris.'/'.$isatuan.'/x01/';
      	}

		$config['total_rows'] = $this->Mmaster->getsatkonvCari($cari,$isatuan)->num_rows(); 
		$config['per_page'] = '10';
		$config['full_tag_open'] = '<ul class="pagination a">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'Awal';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Akhir';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Selanjutnya';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Sebelumnya';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_page'] = $this->uri->segment(7);
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$this->load->model('btb/Mmaster');
		$data['page_title'] = $this->lang->line('list_product');
		$data['baris']=$baris;
		$data['isi']=$this->Mmaster->bacasatkonv($config['per_page'],$this->uri->segment(7),$cari,$isatuan);
		$this->load->view('btb/vlistsatkonv', $data);
	}

	function itemsupp(){
		$material = $this->uri->segment(4);
		$baris = $this->uri->segment(5);
		$data['page_title'] = $this->lang->line("listsupp");
		$data['baris'] = $baris;
		$data['list_price'] = $this->Mmaster->getPrice($material);
		$this->load->view('btb/vlistprice',$data);
	}

	function ppitem(){

		$data['page_title'] = $this->lang->line("op");
		$data['isi'] = $this->Mmaster->getPPitem();
		$this->load->view("btb/vlistpp",$data);
	}

	function gudang()
	{		
			$cari = strtoupper(trim($this->input->post('cari')));
			$baris = $this->input->post("baris");
			if($baris==''){ 
 				$baris=$this->uri->segment(4); 
 			}
     	    if($this->uri->segment(5)!='x01'){
        	if($cari=='') $cari=$this->uri->segment(5);
       		$config['base_url'] = base_url().'index.php/btb/Cform/gudang/'.$baris.'/'.$cari.'/';
     		}else{
        	$config['base_url'] = base_url().'index.php/btb/Cform/gudang/'.$baris.'/x01/';
      		}
			// $config['base_url'] = base_url().'index.php/btb/Cform/gudang/'.$cari.'/';
			$config['total_rows'] = $this->Mmaster->getgudangCari($cari)->num_rows(); 
			$config['per_page'] = '10';
			$config['full_tag_open'] = '<ul class="pagination a">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = 'Awal';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Akhir';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			$config['next_link'] = 'Selanjutnya';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = 'Sebelumnya';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			$config['cur_page'] = $this->uri->segment(6);
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('gudang');
			$data['baris']=$baris;
			$data['isi']=$this->Mmaster->bacagudang($config['per_page'],$config['cur_page'],$cari);
			$this->load->view('btb/vlistgudang', $data);	
	}

	function supplier()
	{
		
      		$cari=strtoupper($this->input->post("cari"));
      	if($this->uri->segment(4)=='1')	{
      		if($this->uri->segment(5)!='x01'){
        	if($cari=='') $cari=$this->uri->segment(5);
       		$config['base_url'] = base_url().'index.php/btb/Cform/supplier/1/'.$cari.'/';
     		}else{
        	$config['base_url'] = base_url().'index.php/btb/Cform/supplier/1/x01/';
      		}

			$config['total_rows'] = $this->Mmaster->bacasuppliercari($cari)->num_rows();
			$config['per_page'] = '10';
			$config['full_tag_open'] = '<ul class="pagination a">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = 'Awal';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Akhir';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			$config['next_link'] = 'Selanjutnya';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = 'Sebelumnya';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			$config['cur_page'] = $this->uri->segment(6);
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->Mmaster->bacasupplier($config['per_page'],$this->uri->segment(6),$cari)->result();
			$this->load->view('btb/vlistsupplier', $data);
		} else {
			if($this->uri->segment(5)!='x01'){
        	if($cari=='') $cari=$this->uri->segment(5);
       		$config['base_url'] = base_url().'index.php/btb/Cform/supplier/2/'.$cari.'/';
     		}else{
        	$config['base_url'] = base_url().'index.php/btb/Cform/supplier/2/x01/';
      		}

			$config['total_rows'] = $this->Mmaster->bacasuppliercari($cari)->num_rows();
			$config['per_page'] = '10';
			$config['full_tag_open'] = '<ul class="pagination a">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = 'Awal';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Akhir';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			$config['next_link'] = 'Selanjutnya';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = 'Sebelumnya';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			$config['cur_page'] = $this->uri->segment(6);
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->Mmaster->bacasupplier($config['per_page'],$this->uri->segment(6),$cari)->result();
			$this->load->view('btb/vlistsupplier1', $data);
		}
	}

	function proses(){

		if($this->input->post('dsj')){

			$this->form_validation->set_rules('dsj','Tanggal SJ','required');
			$this->form_validation->set_rules('isj','Nomor SJ','required');

			if ($this->form_validation->run() === TRUE) {
				$query 	= $this->db->query("SELECT current_timestamp as c");
		   		$row   	= $query->row();
		    	$now	  = $row->c;

		    	$dsj = $this->input->post("dsj",true);
		    	if($dsj){
		    		 $tmp 	= explode('-', $dsj);
		    		 $day 	= $tmp[0];
		    		 $month = $tmp[1];
		    		 $year	= $tmp[2];
		    		 $yearmonth = $year.$month;
		    		 $datesj = $year.'-'.$month.'-'.$day;
		    	}

				$this->db->trans_begin(); 
				$data['i_sj'] = $this->input->post("isj",TRUE);
				$data['d_sj'] = $datesj;
				$data['i_supplier'] = $this->input->post("isupplier",TRUE);
				$data['i_payment_type'] = $this->input->post('paymenttype',TRUE);
				$data['i_tax_type'] = $this->input->post("tipepajak",TRUE);
				$data['v_tax'] = str_replace(',','',$this->input->post("totppn",TRUE));
				$data['f_pkp'] = $this->input->post("pkp")=='cek'?'t':'f';
				$data['v_total'] = str_replace(',','',$this->input->post("grandtot",TRUE));
				$data['v_total_op'] = str_replace(',','',$this->input->post("grandtotop",TRUE));
				$data['v_sisa'] = str_replace(',','',$this->input->post("grandtot",TRUE));
				$data['e_desc'] = $this->input->post('eremark',TRUE);
				$data['d_entry'] = $now;
				$query = $this->Mmaster->insertheader($data);

				$count = $this->input->post("jml",true);

				for($i=1;$i<=$count;$i++){
					$data2['i_sj'] 		 = $data['i_sj'];
					$data2['i_op'] 		 = $this->input->post("iop".$i,TRUE);
					$data2['i_material'] = $this->input->post("imaterial".$i,TRUE);
					$data2['n_qty'] 	 = $this->input->post("nquantity".$i,TRUE);
					$data2['n_discount'] = $this->input->post("diskon".$i,TRUE);
					$data2['v_unit_price'] = str_replace(',','',$this->input->post("vprice".$i,TRUE));
					$data2['v_unit_price_op'] = str_replace(',','',$this->input->post("vpriceop".$i,TRUE));
					$data2['i_unit'] 	 = $this->input->post("isatuan".$i,TRUE);
					$data2['i_unit_conversion'] = $this->input->post("isatkonv".$i,TRUE);
					$data2['d_entry'] 	 = $now;
					$data2['f_unit_conversion'] = $this->input->post("konversi".$i)=="tidak"?'f':'t';
					$data2['i_formula'] = $this->input->post("iformula".$i,TRUE);
					$data2['n_formula_factor'] = $this->input->post("nformulafactor".$i,TRUE);
					$data2['i_no_item']	 = $i;
					$querycek = $this->Mmaster->cekqty($data2['i_op'],$data2['i_material']);
					$qtyop = $querycek->op;
					$qtysj = $querycek->sj;
					$totsj = $qtysj+$data2['n_qty'];
					// var_dump($qtypp,$qtyop,$totop);die;
					if($totsj >= $qtyop){
						$data2['n_qty'] = $qtyop-$qtysj;
						$op=$data2['i_op'];
						$material=$data2['i_material'];
						$satuan=$data2['i_satuan'];
						$dataop['f_complete'] = true;
						$complete = $this->Mmaster->itemComplete($dataop,$op,$material,$satuan);
					}
					
					$querydetail = $this->Mmaster->insertdetail($data2);
				}

			if (($this->db->trans_status() === FALSE))
			{
			   	$this->db->trans_rollback();
			}
			else{
				$this->db->trans_commit();	
			}
			if ($query){
				$message = array('status' => true, 'message' => 'Berhasil Menambahkan '.$data['i_op']);
        	}else{
        	 	$message = array('status' => false, 'message' => 'Gagal Menambahkan');
			}
			$this->session->set_flashdata('message', $message);
			redirect('btb/Cform');

	    	}

		}

		$jum = $this->input->post('jml',TRUE);
		for($i=1;$i<=$jum;$i++){
			$imaterial = $this->input->post("i_material".$i,TRUE);
			$isupplier = $this->input->post("i_supplier".$i,TRUE);
			$i_op = $this->input->post("noop".$i,TRUE);
			$cek = $this->input->post('cek'.$i,TRUE);
 			if($cek){
 				$op[] = $i_op;
 				$data['op'] = array_unique($op);
				$data['isi'][] = $this->Mmaster->getHarga($imaterial,$i_op,$isupplier);
			}
		}
		$data['jnspemb'] = $this->input->post("jnspemb",TRUE);
		$data['isupplier'] = $this->input->post("i_supplier1",TRUE);
		$data['getpkp'] = $this->Mmaster->getpkp($data['isupplier'])->row();
		$data['esuppliername'] = $this->input->post("e_supplier1",TRUE);
		$data['page_title'] = $this->lang->line('op');
		$this->load->view("btb/test",$data);
	}

	function simpantnpop()
	{
		if($this->input->post('dsj')){

			$this->form_validation->set_rules('dsj','Tanggal SJ','required');
			$this->form_validation->set_rules('isj','Nomor SJ','required');

			if ($this->form_validation->run() === TRUE) {
				$dsj = $this->input->post("dsj",true);
				$query 	= $this->db->query("SELECT current_timestamp as c");
				$row   	= $query->row();
				$now	  = $row->c;

				if($dsj){
			 		$tmp 	= explode('-', $dsj);
				    $day 	= $tmp[0];
				    $month  = $tmp[1];
				    $year	= $tmp[2];
				    $yearmonth = $year.$month;
				    $datesj = $year.'-'.$month.'-'.$day;
				}

				$this->db->trans_begin(); 
				$data['i_sj'] = $this->input->post("isj",TRUE);
				$data['d_sj'] = $datesj;
				$data['i_supplier'] = $this->input->post("isupplier",TRUE);
				$data['i_payment_type'] = $this->input->post('paymenttype',TRUE);
				$data['i_tax_type'] = $this->input->post("tipepajak",TRUE);
				$data['v_tax'] = str_replace(',','',$this->input->post("totppn",TRUE));
				$data['f_pkp'] = $this->input->post("pkp")=='cek'?'t':'f';
				$data['v_total'] = str_replace(',','',$this->input->post("grandtot",TRUE));
				// $data['v_total_op'] = ;
				$data['v_sisa'] = str_replace(',','',$this->input->post("grandtot",TRUE));
				$data['e_desc'] = $this->input->post('eremark',TRUE);
				$data['d_entry'] = $now;
				$query = $this->Mmaster->insertheader($data);

				$count = $this->input->post("jml",true);
				for($i=1;$i<=$count;$i++){
					$data2['i_sj'] 		 = $data['i_sj'];
					$data2['i_op'] 		 = "0";
					$data2['i_material'] = $this->input->post("imaterial".$i,TRUE);
					$data2['n_qty'] 	 = $this->input->post("nquantity".$i,TRUE);
					$data2['n_discount'] = $this->input->post("diskon".$i,TRUE);
					$data2['v_unit_price'] = str_replace(',','',$this->input->post("vprice".$i,TRUE));
					// $data2['v_unit_price_op'] = 0;
					$data2['i_unit'] 	 = $this->input->post("isatuan".$i,TRUE);
					$data2['i_unit_conversion'] = $this->input->post("isatkonv".$i,TRUE);
					$data2['d_entry'] 	 = $now;
					$data2['f_unit_conversion'] = $this->input->post("konversi".$i)=="tidak"?'f':'t';
					$data2['i_formula'] = $this->input->post("iformula".$i,TRUE);
					$data2['n_formula_factor'] = $this->input->post("nformulafactor".$i,TRUE);
					$data2['i_no_item']	 = $i;
					
					$querydetail = $this->Mmaster->insertdetail($data2);
				}
					if (($this->db->trans_status() === FALSE))
					{
					   	$this->db->trans_rollback();
					}
					else{
						$this->db->trans_commit();	
					}
					if ($query){
						$message = array('status' => true, 'message' => 'Berhasil Menambahkan '.$data['i_sj']);
			        }else{
			         	$message = array('status' => false, 'message' => 'Gagal Menambahkan '.$data['i_sj'].' Karna Nomor Sudah Ada atau ada kesalahan program tanyakan ke bagian MIS');
					}
					$this->session->set_flashdata('message', $message);
					redirect('btb/Cform');
			}
		}
	}

	function get_pkp_tipe_pajak($supp) 
	{
		$rows = array();
		if(isset($supp)) {
			//$stmt = $pdo->prepare("SELECT variety FROM fruit WHERE name = ? ORDER BY variety");
			$rows = $this->Mmaster->getpkp($supp)->result();
			
			//$stmt->execute(array($_GET['fruitName']));
			//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		echo json_encode($rows);
	}

	function tambahsatkonv($isatuan,$esatuan,$baris,$proses)
	{
		if($this->input->post('isatawal') && $this->input->post('satuankonv')){
			$query 	= $this->db->query("SELECT current_timestamp as c");
		   	$row   	= $query->row();
		    $now	  = $row->c;
			$this->db->trans_begin(); 
			$baris = $this->input->post('baris');

		    $qikonversi = $this->db->query("select i_konversi+1 as i_konversi from tr_satuan_konversi order by i_konversi desc limit 1")->row();
		    $data2['i_konversi'] = $qikonversi->i_konversi;
		    $esatuan = $this->input->post('esatawal');
		    $data2['i_satuan'] = $this->input->post('isatawal');
		    $tmp = explode('-', $this->input->post('satuankonv'));
		    $isatuankonv = $tmp[0];
		    $esatuankonv = $tmp[1];
		    var_dump($esatuankonv);
		    $data2['i_satuan_konversi'] = $isatuankonv;
		    $data2['i_formula'] = $this->input->post('rumus');
		    $data2['n_formula_factor'] = $this->input->post('angkafaktor',TRUE);
		    $data2['d_entry'] = $now;
		    $qcek = $this->db->query("select * from tr_satuan_konversi where i_satuan='$isatuan' and i_satuan_konversi='$isatuankonv'");
		    if($qcek->num_rows()>0) {
		    	$data3['sukses'] = FALSE;
		    	$data3['esatuan'] = $esatuan;
		    	$data3['esatuankonv'] = $esatuankonv;
		    	$data3['baris'] = $baris;
		    	$this->load->view('btb/satuankonv',$data3);
		    } else {
		    	$this->Mmaster->insertsatuankonv($data2);
		    }

		    if($this->db->trans_status()===FALSE)
		    {
		    	$this->db->trans_rollback();
		    } else {
		    	$this->db->trans_commit();
		    	$data3['sukses'] = TRUE;
		    	$data3['isatuan'] = $isatuan;
		    	$data3['iformula'] = $data2['i_formula'];
		    	$data3['nformula'] = $data2['n_formula_factor'];
		    	$data3['isatuankonv'] = $isatuankonv;
		    	$data3['esatuan'] = $esatuan;
		    	$data3['esatuankonv'] = $esatuankonv;
		    	$data3['baris'] = $baris;
		    	$this->load->view('btb/satuankonv',$data3);
		    }
		}

		if($proses=='slow')
		{
			$data['isatuan'] = $isatuan;
			$data['esatuan'] = $esatuan;
			$data['baris'] = $baris;
			$data['isisatuan'] = $this->Mmaster->getsatuan();
			$data['page_title'] = "Input Data Satuan Konversi";
			$this->load->view('btb/vformsatuankonv',$data);
		}
	}
}