<?php
// Irawan 2018-11-02
class Cform_bisbisan extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('btb/Mmaster_bisbisan');
	}

	function index(){

		$cari = trim($this->input->post('cari'));
		
		//Pagination start
		$config['base_url'] = base_url().'index.php/btb/Cform_bisbisan/'.$cari.'/';
		$config['total_rows'] = $this->Mmaster_bisbisan->getsjCari($cari)->num_rows();
		$config['per_page'] = '10';
		$config['full_tag_open'] = '<ul class="pagination a">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'Awal';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Akhir';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Selanjutnya';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Sebelumnya';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_page'] = $this->uri->segment(5);
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$config['offset'] = $this->uri->segment(5);
		$this->pagination->initialize($config);
		
		//data for view
		$data['total'] = $config['total_rows'];
		$data['page_title'] = "Bukti Terima Barang";
		$data['cari'] = $cari;
		$data['isi'] = $this->Mmaster_bisbisan->getsj($config['per_page'], $config['offset'],$cari)->result();
		$this->load->view('btb/vformview_bisbisan', $data);
	}

	function add()
	{
		$data = array();
		$data2 = array();
		if($this->input->post('dsj')){
			$this->form_validation->set_rules('dsj','Tanggal SJ','required');
			$this->form_validation->set_rules('isj','Nomor SJ','required');

			if ($this->form_validation->run() === TRUE) {
				$query 	= $this->db->query("SELECT current_timestamp as c");
		   		$row   	= $query->row();
		    	$now	  = $row->c;

		    	$dsj = $this->input->post("dsj",true);
		    	if($dsj){
		    		 $tmp 	= explode('-', $dsj);
		    		 $day 	= $tmp[0];
		    		 $month = $tmp[1];
		    		 $year	= $tmp[2];
		    		 $yearmonth = $year.$month;
		    		 $datesj = $year.'-'.$month.'-'.$day;
		    	}
		    	$this->db->trans_begin();
		    	$data['i_sj'] = $this->input->post("isj",TRUE);
		    	$data['d_sj'] = $datesj;
		    	$data['i_supplier'] = $this->input->post("isupplier",TRUE);
		    	$data['i_payment_type'] = $this->input->post("paymenttype",TRUE);
		    	$data['i_tax_type'] = '';
		    	$data['v_tax'] = str_replace(',','',$this->input->post("totppn",TRUE));
		    	$data['v_total'] = str_replace(',','',$this->input->post("grandtot",TRUE));
		    	$data['v_sisa'] = str_replace(',','',$this->input->post("grandtot",TRUE));
		    	$data['e_desc'] = $this->input->post("eremark",TRUE);
		    	$data['d_entry'] = $now;
		    	$data['i_makloon_type'] = 1;

		    	$count = $this->input->post("jml",TRUE);

		    	for($i=1; $i<=$count; $i++)
		    	{
		    		$data2['i_sj'] = $data['i_sj'];
		    		$data2['i_op'] = '0';
		    		$data2['i_material'] = $this->input->post("imaterial".$i,TRUE);
		    		$data2['n_qty'] = $this->input->post("nquantity".$i,TRUE);
		    		$data2['n_discount'] = $this->input->post("diskon".$i,TRUE);
		    		$data2['v_tax_unit'] = str_replace(',','',$this->input->post("ppn".$i,TRUE));
		    		$data2['v_unit_price'] = str_replace(',','',$this->input->post("vprice".$i,TRUE));
		    		$data2['i_unit'] = $this->input->post("isatuan".$i,TRUE);
		    		$data2['i_unit_conversion'] = $this->input->post("isatkonv".$i,TRUE);
		    		$data2['d_entry'] = $now;
		    		$data2['f_unit_conversion'] = $this->input->post("konversi".$i,TRUE)=='tidak'?'f':'t';
		    		$data2['i_formula'] = $this->input->post("iformula".$i,TRUE);
		    		$data2['n_formula_factor'] = $this->input->post("nformulafactor".$i,TRUE);
		    		$data2['i_no_item'] = $i;
		    		$data2['n_qty_conv'] = $this->input->post("nquantitykonv".$i,TRUE);
		    		$data2['i_cut_type'] = $this->input->post("ipotongan".$i,TRUE);
		    		$data2['i_size_type'] = $this->input->post("iukuran".$i,TRUE);

		    		if($data2['i_material']!='')
		    		{
		    			$this->Mmaster_bisbisan->inserthrgsup($data2['i_material'],$data2['v_unit_price'],$data['i_supplier'],$data2['i_unit'],$data2['i_sj']);
		    			$querydetail = $this->Mmaster_bisbisan->insertdetail($data2);
		    		}
		    	}
		    	$query = $this->Mmaster_bisbisan->insertheader($data);
		    	if (($this->db->trans_status() === FALSE))
				{
				   	$this->db->trans_rollback();
				}
				else{
					$this->db->trans_commit();	
				}
				if ($query && $querydetail){
					$message = array('status' => true, 'message' => 'Berhasil Menambahkan '.$data['i_sj']);
			    }else{
			       	$message = array('status' => false, 'message' => 'Gagal Menambahkan '.$data['i_sj'].' Karna Nomor Sudah Ada atau ada kesalahan program tanyakan ke bagian MIS');
				}
				$this->session->set_flashdata('message', $message);
				redirect('btb/Cform_bisbisan');
		    }
		}

		$data['page_title'] = "BTB bisbisan Tanpa OP";
		$this->load->view('btb/vformlain_bisbisan',$data);
	}

	function edit($isj=null){

		if($this->input->post('dsj')){

			$this->form_validation->set_rules('dsj','Tanggal SJ','required');
			$this->form_validation->set_rules('isj','Nomor SJ','required');

			if ($this->form_validation->run() === TRUE) {
				$query 	= $this->db->query("SELECT current_timestamp as c");
		   		$row   	= $query->row();
		    	$now	  = $row->c;

		    	$dsj = $this->input->post("dsj",true);
		    	if($dsj){
		    		 $tmp 	= explode('-', $dsj);
		    		 $day 	= $tmp[0];
		    		 $month = $tmp[1];
		    		 $year	= $tmp[2];
		    		 $yearmonth = $year.$month;
		    		 $datesj = $year.'-'.$month.'-'.$day;
		    	}

				$this->db->trans_begin(); 
				$i_sj = $this->input->post("isj",TRUE);
				$data['d_sj'] = $datesj;
				$data['i_supplier'] = $this->input->post("isupplier",TRUE);
				$data['i_payment_type'] = $this->input->post('paymenttype',TRUE);
				// $data['i_tax_type'] = $this->input->post("tipepajak",TRUE);
				$data['v_tax'] = str_replace(',','',$this->input->post("totppn",TRUE));
				// $data['f_pkp'] = $this->input->post("pkp")=='cek'?'t':'f';
				$data['v_total'] = str_replace(',','',$this->input->post("grandtot",TRUE));
				// $data['v_total_op'] = str_replace(',','',$this->input->post("grandtotop",TRUE));
				$data['v_sisa'] = str_replace(',','',$this->input->post("grandtot",TRUE));
				$data['e_desc'] = $this->input->post('eremark',TRUE);
				$data['d_update'] = $now;
				$query = $this->Mmaster_bisbisan->updateheader($data,$i_sj);

				$querydelete = $this->Mmaster_bisbisan->delete($i_sj);
				$count = $this->input->post("jml",true);

				for($i=1;$i<=$count;$i++){
					$data2['i_sj'] = $i_sj;
		    		$data2['i_op'] = '0';
		    		$data2['i_material'] = $this->input->post("imaterial".$i,TRUE);
		    		$data2['n_qty'] = $this->input->post("nquantity".$i,TRUE);
		    		$data2['n_discount'] = $this->input->post("diskon".$i,TRUE);
		    		$data2['v_tax_unit'] = str_replace(',','',$this->input->post("ppn".$i,TRUE));
		    		$data2['v_unit_price'] = str_replace(',','',$this->input->post("vprice".$i,TRUE));
		    		$data2['i_unit'] = $this->input->post("isatuan".$i,TRUE);
		    		$data2['i_unit_conversion'] = $this->input->post("isatkonv".$i,TRUE);
		    		$data2['d_entry'] = $now;
		    		$data2['f_unit_conversion'] = $this->input->post("konversi".$i,TRUE)=='tidak'?'f':'t';
		    		$data2['f_bonm_approve'] = $this->input->post("bonmapprove".$i,TRUE);
		    		$data2['i_formula'] = $this->input->post("iformula".$i,TRUE);
		    		$data2['n_formula_factor'] = $this->input->post("nformulafactor".$i,TRUE);
		    		$data2['i_no_item'] = $i;
		    		$data2['n_qty_conv'] = $this->input->post("nquantitykonv".$i,TRUE);
		    		$data2['i_cut_type'] = $this->input->post("ipotongan".$i,TRUE);
		    		$data2['i_size_type'] = $this->input->post("iukuran".$i,TRUE);
		    		$ishapus = $this->input->post("hapus".$i,TRUE);
		    		// var_dump($ishapus);
		    		if($data2['i_material']!='' && $ishapus!='yes')
		    		{
		    			$querydetail = $this->Mmaster_bisbisan->insertdetail($data2);
		    			$this->Mmaster_bisbisan->inserthrgsup($data2['i_material'],$data2['v_unit_price'],$data['i_supplier'],$data2['i_unit'],$data2['i_sj']);
		    		}
				}
				// die;
			if (($this->db->trans_status() === FALSE))
			{
			   	$this->db->trans_rollback();
			}
			else{
				$this->db->trans_commit();	
			}
			if ($query){
				$message = array('status' => true, 'message' => 'Berhasil Edit '.$i_sj);
        	}else{
        	 	$message = array('status' => false, 'message' => 'Gagal Edit (Hubungi Pihak MIS)');
			}
			$this->session->set_flashdata('message', $message);
			redirect('btb/Cform_bisbisan');

	    	}

		}

		$data['page_title'] = $this->lang->line("Edit BTB");
		if(strpos($isj, '_') == true)
		{
			$sjfake = str_replace('_', '/', $isj);
		} else {
			$sjfake = $isj;
		}
		$data['isi'] = $this->Mmaster_bisbisan->getHeader($sjfake);
		// $data['listpp'] = $this->Mmaster_bisbisan->getListPP($sjfake);
		$data['detail'] = $this->Mmaster_bisbisan->getItem($sjfake);
		$this->load->view('btb/vformupdate_bisbisan', $data);
		
	}

	function cancel($isj)
	{
		if(strpos($isj,'_') == true)
		{
			$sjfake = str_replace('_', '/', $isj);
		} else {
			$sjfake = $isj;
		}
		$this->db->trans_begin();
		$query = $this->Mmaster_bisbisan->cancelsj($sjfake);
		if($this->db->trans_status()===FALSE)
		{
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}

		if ($query){
			$message = array('status' => true, 'message' => 'Berhasil Cancel '.$sjfake);
        }else{
         	$message = array('status' => false, 'message' => 'Gagal Cancel');
		}
		$this->session->set_flashdata('message', $message);
		redirect('btb/Cform_bisbisan');
	}

	function supplier()
	{
		
		$cari=strtoupper($this->input->post("cari"));

      	if($this->uri->segment(4)!='x01'){
        if($cari=='') $cari=$this->uri->segment(4);
       		$config['base_url'] = base_url().'index.php/btb/Cform_bisbisan/supplier/'.$cari.'/';
     	}else{
        	$config['base_url'] = base_url().'index.php/btb/Cform_bisbisan/supplier/x01/';
      	}

		$config['total_rows'] = $this->Mmaster_bisbisan->bacasuppliercari($cari)->num_rows();
		$config['per_page'] = '10';
		$config['full_tag_open'] = '<ul class="pagination a">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'Awal';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Akhir';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Selanjutnya';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Sebelumnya';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_page'] = $this->uri->segment(5);
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$data['page_title'] = $this->lang->line('list_product');
		$data['isi']=$this->Mmaster_bisbisan->bacasupplier($config['per_page'],$this->uri->segment(5),$cari)->result();
		$this->load->view('btb/vlistsupplier', $data);
	}

	function potongan()
	{
		$cari=strtoupper($this->input->post("cari"));
		$baris = $this->input->post("baris");
		if($baris==''){ 
 			$baris=$this->uri->segment(4); 
 		}
		$config['base_url'] = base_url().'index.php/btb/Cform_bisbisan/potongan/'.$baris.'/x01/';
		$config['total_rows'] = $this->Mmaster_bisbisan->bacapotongancari($cari)->num_rows();
		$config['per_page'] = '10';
		$config['full_tag_open'] = '<ul class="pagination a">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'Awal';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Akhir';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Selanjutnya';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Sebelumnya';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_page'] = $this->uri->segment(6);
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$data['page_title'] = "List Potongan";
		$data['baris'] = $baris;
		$data['isi']=$this->Mmaster_bisbisan->bacapotongan($config['per_page'],$this->uri->segment(6),$cari)->result();
		$this->load->view('btb/vlistpotongan', $data);
	}

	function ukuran()
	{
		$cari=strtoupper($this->input->post("cari"));
		$baris = $this->input->post("baris");
		if($baris==''){ 
 			$baris=$this->uri->segment(4); 
 		}
		$config['base_url'] = base_url().'index.php/btb/Cform_bisbisan/ukuran/'.$baris.'/x01/';
		$config['total_rows'] = $this->Mmaster_bisbisan->bacaukurancari($cari)->num_rows();
		$config['per_page'] = '10';
		$config['full_tag_open'] = '<ul class="pagination a">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'Awal';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Akhir';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Selanjutnya';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Sebelumnya';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_page'] = $this->uri->segment(6);
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$data['page_title'] = "List Ukuran";
		$data['baris'] = $baris;
		$data['isi']=$this->Mmaster_bisbisan->bacaukuran($config['per_page'],$this->uri->segment(6),$cari)->result();
		$this->load->view('btb/vlistukuran', $data);
	}

	function material()
	{		
			$cari = strtoupper($this->input->post('cari', TRUE));
      		$baris = strtoupper($this->input->post('baris', TRUE));
      		$gudang = strtoupper($this->input->post('gudang',TRUE));
      		$isupplier = strtoupper($this->input->post('isupplier',TRUE));
 			if($baris==''){ 
 				$baris=$this->uri->segment(4); 
 			}
 			if($gudang==''){
 				$gudang=$this->uri->segment(5);
 			}
 			if($isupplier==''){
 				$isupplier=$this->uri->segment(6);
 			}
     	    if($this->uri->segment(7)!='x01'){
        	if($cari=='') $cari=$this->uri->segment(7);
       		$config['base_url'] = base_url().'index.php/btb/Cform_bisbisan/material/'.$baris.'/'.$gudang.'/'.$isupplier.'/'.$cari.'/';
     		}else{
        	$config['base_url'] = base_url().'index.php/btb/Cform_bisbisan/material/'.$baris.'/'.$gudang.'/'.$isupplier.'/x01/';
      		}

			$config['total_rows'] = $this->Mmaster_bisbisan->getmaterialCari($cari,$gudang,$isupplier)->num_rows(); 
			$config['per_page'] = '10';
			$config['full_tag_open'] = '<ul class="pagination a">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = 'Awal';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Akhir';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			$config['next_link'] = 'Selanjutnya';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = 'Sebelumnya';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			$config['cur_page'] = $this->uri->segment(8);
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);

			$this->load->model('btb/Mmaster_bisbisan');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$baris;
			$data['isi']=$this->Mmaster_bisbisan->bacamaterial($config['per_page'],$this->uri->segment(8),$cari,$gudang,$isupplier);
			$this->load->view('btb/vlistmaterial_bisbisan', $data);
	}

	function gudang()
	{		
			$cari = strtoupper(trim($this->input->post('cari')));
			$baris = $this->input->post("baris");
			if($baris==''){ 
 				$baris=$this->uri->segment(4); 
 			}
     	    if($this->uri->segment(5)!='x01'){
        	if($cari=='') $cari=$this->uri->segment(5);
       		$config['base_url'] = base_url().'index.php/btb/Cform_bisbisan/gudang/'.$baris.'/'.$cari.'/';
     		}else{
        	$config['base_url'] = base_url().'index.php/btb/Cform_bisbisan/gudang/'.$baris.'/x01/';
      		}
			// $config['base_url'] = base_url().'index.php/btb/Cform/gudang/'.$cari.'/';
			$config['total_rows'] = $this->Mmaster_bisbisan->getgudangCari($cari)->num_rows(); 
			$config['per_page'] = '10';
			$config['full_tag_open'] = '<ul class="pagination a">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = 'Awal';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Akhir';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			$config['next_link'] = 'Selanjutnya';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = 'Sebelumnya';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			$config['cur_page'] = $this->uri->segment(6);
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('gudang');
			$data['baris']=$baris;
			$data['isi']=$this->Mmaster_bisbisan->bacagudang($config['per_page'],$config['cur_page'],$cari);
			$this->load->view('btb/vlistgudang', $data);	
	}
}