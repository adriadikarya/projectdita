<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Mmaster extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function baca($dawal,$dakhir,$kodeprsh)
	{
		$this->db->select("a.*, b.e_nama_pelanggan");
		$this->db->from("tm_nota a");
		$this->db->join("tr_pelanggan b","a.i_pelanggan=b.i_pelanggan");
		$this->db->where("a.d_nota>=",$dawal);
		$this->db->where("a.d_nota<=",$dakhir);
		$this->db->where("a.f_nota_cancel","0");
		$this->db->where("a.i_kode_perusahaan",$kodeprsh);
		return $this->db->get();
	}

	function bacaitem($dawal,$dakhir,$kodeprsh)
	{
		$this->db->select("a.i_id_nota, a.i_no_nota, a.d_nota, a.d_jth_tempo, c.e_nama_pelanggan,b.n_qty, b.v_harga_satuan, d.e_nama_brg, b.i_kode_brg, b.i_no_item
			from tm_nota a, tm_nota_item b, tr_pelanggan c, tr_barang d
			where a.i_id_nota=b.i_id_nota and b.i_kode_brg=d.i_kode_brg and a.i_pelanggan=c.i_pelanggan	and a.d_nota>='$dawal' and a.d_nota<='$dakhir' and a.i_kode_perusahaan='$kodeprsh'
			order by a.i_no_nota, b.i_no_item",false);
		return $this->db->get();
	}
}