<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Mmaster extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function daftarpel($kodeprsh)
	{
		$this->db->where("i_kode_perusahaan",$kodeprsh);
		return $this->db->get("tr_pelanggan");
	}

	function daftarnota($kodeprsh)
	{
		$this->db->select("a.*, b.e_nama_pelanggan");
		$this->db->from("tm_nota a");
		$this->db->join("tr_pelanggan b","a.i_pelanggan=b.i_pelanggan");
		$this->db->where("a.i_kode_perusahaan",$kodeprsh);
		$this->db->where("a.f_nota_cancel",0);
		$this->db->order_by("a.i_id_nota");
		return $this->db->get();
	}

	function daftarbrg($kodeprsh)
	{
		$this->db->where("i_kode_perusahaan",$kodeprsh);
		return $this->db->get("tr_barang");
	}

	function insertheader($data)
	{
		return $this->db->insert('tm_nota',$data);
	}

	function insertdetail($data2)
	{
		return $this->db->insert("tm_nota_item",$data2);
	}

	function insertwarna($data3)
	{
		return $this->db->insert("tm_nota_item_warna",$data3);
	}

	function cekidnota()
	{
		$this->db->select('i_id_nota');
		$this->db->from('tm_nota');
		$this->db->order_by('i_id_nota','desc');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row();
			$i_id_nota = $row->i_id_nota+1;
			return $i_id_nota;
		} else {
			return 1;
		}
	}

	function cekiditemnota()
	{
		$this->db->select('i_id_item_nota');
		$this->db->from('tm_nota_item');
		$this->db->order_by('i_id_item_nota','desc');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row();
			$i_id_item_nota = $row->i_id_item_nota+1;
			return $i_id_item_nota;
		} else {
			return 1;
		}
	}

	function canceldata($idnota)
	{
		$this->db->set('f_nota_cancel',1);
		$this->db->where('i_id_nota',$idnota);
		return $this->db->update('tm_nota');
	}

}