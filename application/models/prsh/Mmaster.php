<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Mmaster extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function dataperusahaan($kodeprsh)
	{
		$this->db->where("i_kode_perusahaan",$kodeprsh);
		return $this->db->get('tr_perusahaan');
	}

	function update($data,$kodeprsh,$idprsh)
	{
		$this->db->where('i_kode_perusahaan',$kodeprsh);
		return $this->db->update('tr_perusahaan',$data);
	}
}