<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Mmaster extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function baca($tgljthtempo,$kodeprsh)
	{
		$this->db->select("a.i_no_nota, a.d_nota, a.d_jth_tempo, a.v_total as piutang, a.v_total_sisa as saldosisa, DATEDIFF(a.d_jth_tempo,a.d_nota) as top, a.i_pelanggan,
			b.e_nama_pelanggan
			from tm_nota a, tr_pelanggan b
			where a.i_pelanggan=b.i_pelanggan and a.f_nota_cancel='0' and a.f_status_lunas='0' and a.v_total_sisa>0 and (a.d_jth_tempo < '$tgljthtempo' OR a.d_jth_tempo >='$tgljthtempo') and a.i_kode_perusahaan='$kodeprsh'",false);
		return $this->db->get();
	}
}