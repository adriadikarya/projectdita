<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Mmaster extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function daftarpel($kodeprsh)
	{
		$this->db->where("i_kode_perusahaan",$kodeprsh);
		return $this->db->get("tr_pelanggan");
	}

	function daftarnota($kodeprsh,$pelanggan)
	{
		$where = array("i_pelanggan"=>$pelanggan,"i_kode_perusahaan"=>$kodeprsh,"f_nota_cancel"=>"0","v_total_sisa>"=>"0");
		$this->db->where($where);
		return $this->db->get("tm_nota");
	}

	function daftarvoucher($kodeprsh,$voc)
	{
		$where = array("i_id_pelunasan"=>$voc);
		$this->db->where($where);
		return $this->db->get("tm_pelunasan_item");
	}

	function insertdata($data)
	{
		return $this->db->insert('tm_pelunasan',$data);
	}

	function insertdetail($data2)
	{
		return $this->db->insert('tm_pelunasan_item',$data2);
	}

	function updatenota($i_id_nota,$v_bayar_nota)
	{
		$this->db->select("v_total_sisa from tm_nota where i_id_nota='$i_id_nota'",false);
		$query = $this->db->get();
		$row = $query->row();
		$vsisa = $row->v_total_sisa;

		if($vsisa>$v_bayar_nota){
			$updatesisa = $vsisa-$v_bayar_nota;
		} else {
			$updatesisa = 0;
		}

		if($updatesisa==0){
			return $this->db->query("update tm_nota set v_total_sisa='$updatesisa', f_status_lunas='1' where i_id_nota='$i_id_nota'");
		} else {
			return $this->db->query("update tm_nota set v_total_sisa='$updatesisa' where i_id_nota='$i_id_nota'");
		}
	}

	function ambilVoucher($kodeprsh) {
        $this->datatables->select('i_no_voucher, d_voucher, v_pelunasan, i_id_pelunasan, e_keterangan,f_pelunasan_cancel');
        $this->datatables->from('tm_pelunasan');
        $this->datatables->where('i_kode_perusahaan',$kodeprsh);
        $this->datatables->add_column('view', '<a alt="Detail" id="detail" title="Lihat Detail" data-toggle="modal" data-target="#myModal" class="detailbtn" href="javascript:void(0)" data-id="$4" data-no="$1"><button class="btn btn-info btn-rounded btn-sm"><i class="glyphicon glyphicon-info-sign"></i></button></a> <a href="#" class="hapus_record btn btn-danger btn-sm" data-id="$4" data-no="$1" title="Batal"><i class="glyphicon glyphicon-trash"></i></a>','i_no_voucher,d_voucher,v_pelunasan,i_id_pelunasan,e_keterangan,f_pelunasan_cancel');
        return $this->datatables->generate();
        // button edit tambahan datatable sementara tidak dipakai dulu
        // <a href="javascript:void(0);" class="edit_record btn btn-info btn-sm" data-id="$1" data-no="$2" data-tgl="$3" data-bayar="$4" data-ket="$5">Edit</a>,'barang_kode,barang_nama,barang_harga,kategori_id,kategori_nama'
  	}

  	function canceldata($id)
	{
		$this->db->select("i_id_nota, v_bayar_nota from tm_pelunasan_item where i_id_pelunasan='$id'",false);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach ($query->result() as $row) {
				$idnota = $row->i_id_nota;
				$vbayar = $row->v_bayar_nota;
				$this->db->query("update tm_nota set f_status_lunas='0', v_total_sisa=v_total_sisa+".$vbayar." where i_id_nota='$idnota'");	
			}
			$this->db->set('f_pelunasan_cancel',1);
			$this->db->where('i_id_pelunasan',$id);
			return $this->db->update('tm_pelunasan');
		} else {
			$this->db->set('f_pelunasan_cancel',1);
			$this->db->where('i_id_pelunasan',$id);
			return $this->db->update('tm_pelunasan');
		}
	}
}