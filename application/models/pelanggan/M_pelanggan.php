<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class M_pelanggan extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function daftargrup($ikodeprsh)
	{
		$this->db->where("i_kode_perusahaan",$ikodeprsh);
		return $this->db->get("tr_grup_pelanggan");
	}

	function daftarperusahaan()
	{
		return $this->db->get("tr_perusahaan");
	}

	function daftarpel($kodeprsh)
	{
		$this->db->select("a.*,b.e_nama_grup");
		$this->db->from("tr_pelanggan a");
		$this->db->join("tr_grup_pelanggan b","a.i_id_grup=b.i_id_grup","left");
		$this->db->where("a.i_kode_perusahaan",$kodeprsh);
		$this->db->order_by("a.e_nama_pelanggan");
		return $this->db->get();
	}

	function caripel($ipelanggan,$kodeprsh)
	{
		$this->db->where("i_pelanggan",$ipelanggan);
		$this->db->where("i_kode_perusahaan",$kodeprsh);
		$query = $this->db->get("tr_pelanggan");
		return $query;
	}

	function insertdata($data)
	{
		return $this->db->insert("tr_pelanggan",$data);
	}

	function updatedata($data,$ikode)
	{
		$this->db->where("i_pelanggan",$ikode);
		return $this->db->update("tr_pelanggan",$data);
	}

	function caripelanggan($ikode)
	{
		$this->db->where("i_pelanggan",$ikode);
		return $this->db->get("tr_pelanggan")->row();
	}

	function deletedata($ikode)
	{
		$this->db->where("i_pelanggan",$ikode);
		return $this->db->delete("tr_pelanggan");
	}
}