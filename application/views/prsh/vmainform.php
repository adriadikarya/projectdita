<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
		<!-- general form elements -->
			<div class="box box-danger">
				<div class="box-header with-border">
			    	<h3 class="box-title">From Perusahaan</h3>
				</div>
				<form role="form" method="post" action="<?php echo base_url() ?>prsh/Cform/Simpan">
					<?php if($message = $this->session->flashdata('message')): ?>
			                <div class="alert <?php echo ($message['status']) ? 'alert-success' : 'alert-danger'; ?>" >
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $message['message']; ?>
			                </div>
			    <?php endif; ?>
					<div class="box-body">
				        <div class="form-group col-md-12 col-lg-12 col-sm-12">
				            <label for="tipeprsh">Tipe Perusahaan</label>
				            <select name="tipeprsh" id="tipeprsh" class="form-control">
				            	<option value="1" <?php if($isi->i_tipe_perusahaan=='1') echo "selected" ?>>Asesoris</option>
				            	<option value="2" <?php if($isi->i_tipe_perusahaan=='2') echo "selected" ?>>Percetakan</option>
				            </select>
				        </div>
				        <div class="form-group col-md-12 col-lg-12 col-sm-12">
				        	<label for="nmprsh">Nama Perusahaan</label>
				        	<input type="text" name="nmprsh" id="nmprsh" class="form-control" value="<?php echo $isi->e_nama_perusahaan ?>" placeholder="Nama Perusahaan" maxlength="100">
				        </div>
				        <div class="form-group col-md-12 col-lg-12 col-sm-12">
				        	<label for="alamat">Alamat</label>
				        	<input type="text" name="alamat" id="alamat" class="form-control" value="<?php echo $isi->e_alamat ?>" placeholder="Alamat" maxlength="200">
				        </div>
				        <div class="form-group col-md-12 col-lg-12 col-sm-12">
				        	<label for="telp">Telpon</label>
				        	<input type="text" name="telp" id="telp" class="form-control" value="<?php echo $isi->e_telepon ?>" placeholder="Telepon" maxlength="16">
				        </div>
				        <div class="form-group col-md-12 col-lg-12 col-sm-12">
				        	<label for="fax">Fax</label>
				        	<input type="text" name="fax" id="fax" class="form-control" value="<?php echo $isi->e_fax ?>" placeholder="Fax" maxlength="30">
				        </div>
				        <div class="form-group col-md-12 col-lg-12 col-sm-12">
				        	<label for="kontak">Kontak</label>
				        	<input type="text" name="kontak" id="kontak" class="form-control" value="<?php echo $isi->e_kontak ?>" placeholder="Kontak" maxlength="100">
				        </div>
				        <div class="form-group col-md-12 col-lg-12 col-sm-12">
				        	<label for="norek1">No Rek 1</label>
				        	<input type="text" name="norek1" id="norek1" class="form-control" value="<?php echo $isi->e_no_rek1 ?>" placeholder="No Rekening 1" maxlength="30">
				        </div>
				        <div class="form-group col-md-12 col-lg-12 col-sm-12">
				        	<label for="bank1">Bank 1</label>
				        	<input type="text" name="bank1" id="bank1" class="form-control" value="<?php echo $isi->e_nama_bank1 ?>" placeholder="Nama Bank 1" maxlength="30">
				        </div>
				        <div class="form-group col-md-12 col-lg-12 col-sm-12">
				        	<label for="atasnama1">Atas Nama 1</label>
				        	<input type="text" name="atasnama1" id="atasnama1" class="form-control" value="<?php echo $isi->e_atas_nama1 ?>" placeholder="Atas Nama Bank 1" maxlength="100">
				        </div>
				        <div class="form-group col-md-12 col-lg-12 col-sm-12">
				        	<label for="norek2">No Rek 2</label>
				        	<input type="text" name="norek2" id="norek2" class="form-control" value="<?php echo $isi->e_no_rek2 ?>" placeholder="No Rekening 2" maxlength="30">
				        </div>
				        <div class="form-group col-md-12 col-lg-12 col-sm-12">
				        	<label for="bank2">Bank 2</label>
				        	<input type="text" name="bank2" id="bank2" class="form-control" value="<?php echo $isi->e_nama_bank2 ?>" placeholder="Nama Bank 2" maxlength="30">
				        </div>
				        <div class="form-group col-md-12 col-lg-12 col-sm-12">
				        	<label for="atasnama2">Atas Nama 2</label>
				        	<input type="text" name="atasnama2" id="atasnama2" class="form-control" value="<?php echo $isi->e_atas_nama2 ?>" placeholder="Atas Nama Bank 2" maxlength="100">
				        </div>
					</div>
					<div class="box-footer">
						<input type="hidden" name="kodeperusahaan" value="<?php echo $isi->i_kode_perusahaan ?>">
						<input type="hidden" name="idperusahaan" value="<?php echo $isi->i_id_perusahaan ?>">
				    	<input type="submit" name="submit" id="submit" class="btn btn-success" value="Update">
				        <a href="<?php echo base_url() ?>" class="btn btn-default">Kembali</a>
				    </div>
				</form>
			</div>
		</div>
	</div>		
</section>