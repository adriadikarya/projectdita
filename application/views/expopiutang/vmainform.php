<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
		<!-- general form elements -->
			<div class="box box-info">
				<div class="box-header with-border">
			    	<h3 class="box-title">From Export Laporan Piutang</h3>
				</div>
				<div class="box-body">
					<div class="form-group col-md-12 col-lg-6 col-sm-12">
				      	<label for="datepicker"><i class="fa fa-calendar"></i>&nbsp;Tgl Jth Tempo</label>
				        <input type="text" class="form-control" id="datepicker" name="djthtempo" placeholder="Tanggal Jth Tempo" value="<?php echo date('d/m/Y')?>" readonly>
				        <input type="button" class="btn btn-default btn-sm" id="tglskrg" value="Tanggal Sekarang">
				    </div>
				</div>
				<div class="box-footer">
					<a align="right" class="btn btn-info" id="export" href="#" onclick='exportlink();' target='blank'>Export Ke Excel</a>
					<!-- <a align="right" class="btn btn-primary" id="exportitem" href="#" onclick='exportitemlink();' target='blank'>Export Ke Excel (Per Item)</a> -->
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	var tglskrg = document.getElementById('tglskrg');
	tglskrg.addEventListener('click', function(e){
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();
		var output = ((''+day).length<2 ? '0' : '') + day + '/' +
		    ((''+month).length<2 ? '0' : '') + month + '/' + 
		    d.getFullYear();
		document.getElementById('datepicker').value = output;
	});
	function exportlink() {
		var tgljthtempo  = $('#datepicker').val();
		tmp1 = tgljthtempo.split('/');
		tgljthtempo = tmp1[2]+'-'+tmp1[1]+'-'+tmp1[0];
		var new_url = "<?php echo base_url()?>expopiutang/Cform/export/"+tgljthtempo;
		$("#export").attr("href", new_url);
	}
</script>