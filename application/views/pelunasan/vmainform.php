<section class="content">
	<div class="modal fade" id="myModal" role="dialog">
  		<div class="modal-dialog">
  			<!-- Modal content-->
        	<div class="modal-content">
  			</div>     
  		</div>
  	</div>
  	<div class="row">
		<!-- left column -->
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
			    	<h3 class="box-title">From Pelunasan Nota</h3>
			    	<?php if($message = $this->session->flashdata('message')): ?>
			                <div class="alert <?php echo ($message['status']) ? 'alert-success' : 'alert-danger'; ?>" >
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $message['message']; ?>
			                </div>
			    	<?php endif; ?>
			    </div>
			    <!-- /.box-header -->
				<!-- form start -->
				<form role="form" id="myForm" method="post" action="<?php echo base_url() ?>pelunasan/Cform/simpan">
					<div class="box-body">
						<div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="novoc">No Voucher</label>
				            <input type="text" class="form-control" id="novoc" name="novoc" placeholder="Isi No Voucher" value="" onkeydown="spasitidak();" maxlength="50">
				        </div>
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="jmlbayar">Jml Pembayaran</label>
				            <input type="text" class="form-control" id="jmlbayar" name="jmlbayar" placeholder="Isi Jumlah Pembayaran" onkeydown="hanyaangka();" autocomplete="off">
				            <input type="hidden" class="form-control" id="jmlsisa" name="jmlsisa">
				        </div>
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="pelanggan">Pelanggan</label>
				            <select class="select2 form-control" id="pelanggan" name="pelanggan" style="width: 100%;">
				            	<option value="">Pilih Pelanggan</option>
					        <?php 
					           	if(!empty($pel))
					           	{

					           		foreach ($pel as $row) {
					        ?>
					    	     		<option value="<?php echo $row->i_pelanggan?>"><?php echo $row->e_nama_pelanggan ?></option>
					        <?php			
					        		}
					           	} else {
					           		echo "<option value=\"\">Maaf Tidak Ada Pelanggan!</option>";
					           	}
					        ?>
					        </select>
				        </div>
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="ket">Keterangan Bayar</label>
				            <input type="text" class="form-control" id="ket" name="ket" placeholder="Keterangan Pembayaran" maxlength="200">
				        </div>
				        <div class="form-group col-md-12 col-lg-6 col-sm-12">
				        	<label for="datepicker"><i class="fa fa-calendar"></i>&nbsp;Tgl Bayar</label>
				            <input type="text" class="form-control" id="datepicker" name="dbayar" placeholder="Tanggal Bayar" readonly>
				        </div>
					</div>
					<div class="box-footer">
						<input type="hidden" id="totrow" name="totrow" value="0">
				    	<input type="button" name="submit" id="submit" class="btn btn-success" onclick="return validasi();" value="Simpan">
				        <a href="<?php echo base_url() ?>pelunasan/Cform" class="btn btn-default">Kembali</a>
				        <button type="button" onclick="addRow();" class="btn btn-primary">Tambah</button>
				        <!-- <button type="button" onclick="deleteRow();" class="btn btn-warning">Kurang</button> -->
				    </div>
				    <div class="box box-warning">
						<div class="box-header with-border">
					    	<h3 class="box-title">Detail Item Nota</h3>
					    </div>
					    <div id="detailhead" class="box-body table-responsive">
				    	<table class="table table-hover condensed" style="width:100%;">
				    		<thead>
				    			<th class="col-sm-1" style="text-align: center;">No</th>
				    			<th class="col-sm-3" style="text-align: center;">No Nota</th>
				    			<th class="col-sm-2" style="text-align: center;">Tgl Nota</th>
				    			<th class="col-sm-2s"style="text-align: center;">Nilai Nota</th>
				    			<th class="col-sm-2" style="text-align: center;">Nilai Bayar</th>
				    			<th class="col-sm-2" style="text-align: center;">Sisa</th>
				    		</thead>
				    		<tbody id="detailbody">
				    		</tbody>
				    	</table>
				    </div>
				</form>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	var input = document.getElementById('jmlbayar');
	input.addEventListener('keyup', function(e){
		var totrow = $('#totrow').val();
		this.value = formatuang2(this.value);
		$('#jmlsisa').val(formatulang(this.value));	
		if(totrow!='0' || totrow!=0)
		{
			var jmlsisa = $('#jmlsisa').val();
			var tmpsisa = 0;
			for(var i=1; i<=totrow; i++)
			{
				var vbayar = formatulang($('#vbayar'+i).val());
				var vsisa = formatulang($('#vsisa'+i).val());
				if(parseFloat(jmlsisa)>parseFloat(vbayar))
			    {
			       tmpsisa  = parseFloat(jmlsisa)-parseFloat(vbayar);
			    } else if(parseFloat(jmlsisa)==parseFloat(vbayar)){
			       tmpsisa  = 0;
			    } else {
			       tmpsisa  = 0;
			    }
			    $('#vsisa'+i).val(formatuang(tmpsisa));
			}
			$('#jmlsisa').val(tmpsisa);
		}
	});

	function addRow()
	{
		var pel = $('#pelanggan').val();
		var jmlbayar = $('#jmlbayar').val();
		// cols = $('#detailbody').html();
		cols = $('#detailbody').html();
		cols = cols.trim();
		if(pel=='' || (jmlbayar==0 || jmlbayar=='')){
			swal(
		  	    'Pilih Pelanggan / Isi jumlah bayar',
		  	    'Pilih dulu dong pelanggannya atau Isi dulu jumlah bayarnya :)',
		  	    'warning'
		  	);
		} else {
			if(cols==''){
				var counter = parseInt($('#totrow').val())+1;
				// var newRow = $("<tr>");
			    cols = '<tr>';
			    cols += '<td class="col-sm-1" align="center"><input type="hidden" class="form-control input-sm" name="baris'+counter+'" id="baris'+counter+'" value="'+counter+'"/>'+counter+'</td>';
			    cols += '<td class="col-sm-3" align="center"><input type="text" name="inotacode'+counter+'" id="inotacode'+counter+'" class="form-control input-sm" value="" readonly><input type="hidden" name="inota'+counter+'" id="inota'+counter+'" value="" readonly></td>';
			    cols += '<td class="col-sm-2" align="center"><input type="text" id="dnota'+counter+'" name="dnota'+counter+'" class="form-control input-sm" value="" readonly></td>';
			    cols += '<td class="col-sm-2"><input type="text" id="vnota'+counter+'" name="vnota'+counter+'" class="form-control input-sm" value="" readonly></td>';
			    cols += '<td class="col-sm-2"><input type="text" id="vbayar'+counter+'" name="vbayar'+counter+'" class="form-control input-sm" value="" readonly></td>';
			    cols += '<td class="col-sm-2"><input type="text" id="vsisa'+counter+'" name="vsisa'+counter+'" class="form-control input-sm" value="" readonly></td>';
			    cols += '</tr>';

			    // $("#detailbody").html(cols);
		    $('#totrow').val(counter);
			} else {
				var counter = parseInt($('#totrow').val())+1;
				// var newRow = $("<tr>");
			    cols  = cols+'<tr>';
			    cols += '<td class="col-sm-1" align="center"><input type="hidden" class="form-control input-sm" name="baris'+counter+'" id="baris'+counter+'" value="'+counter+'"/>'+counter+'</td>';
			    cols += '<td class="col-sm-3" align="center"><input type="text" name="inotacode'+counter+'" id="inotacode'+counter+'" class="form-control input-sm" value="" readonly><input type="hidden" name="inota'+counter+'" id="inota'+counter+'" value="" readonly></td>';
			    cols += '<td class="col-sm-2" align="center"><input type="text" id="dnota'+counter+'" name="dnota'+counter+'" class="form-control input-sm" value="" readonly></td>';
			    cols += '<td class="col-sm-2"><input type="text" id="vnota'+counter+'" name="vnota'+counter+'" class="form-control input-sm" value="" readonly></td>';
			    cols += '<td class="col-sm-2"><input type="text" id="vbayar'+counter+'" name="vbayar'+counter+'" class="form-control input-sm" value="" readonly></td>';
			    cols += '<td class="col-sm-2"><input type="text" id="vsisa'+counter+'" name="vsisa'+counter+'" class="form-control input-sm" value="" readonly></td>';
			    cols += '</tr>';

			    // $("#detailbody").append(cols);
			    $('#totrow').val(counter);
			}

			j=0;
		    var baris        =Array();
		    var inota 	     =Array();	    
		    var inotacode  	 =Array();
		    var tglnota      =Array();
		    var vnota        =Array();
		    var vbayar       =Array();
		    var vsisa        =Array();
		    for(i=1;i<counter;i++){
		      j++;
		      baris[j]    		= document.getElementById("baris"+i).value;
		      inota[j]        	= document.getElementById("inota"+i).value;
		      inotacode[j]    	= document.getElementById("inotacode"+i).value;
		      tglnota[j]      	= document.getElementById("dnota"+i).value;
		      vnota[j]     		= document.getElementById("vnota"+i).value;
		      vbayar[j]     	= document.getElementById("vbayar"+i).value;
		      vsisa[j]     		= document.getElementById("vsisa"+i).value;
		    }
		    $('#detailbody').html(cols);
		    j=0;
		    for(i=1;i<counter;i++){
		      j++;
		      document.getElementById("baris"+i).value		=baris[j];
		      document.getElementById("inota"+i).value		=inota[j];
		      document.getElementById("inotacode"+i).value	=inotacode[j];
		      document.getElementById("dnota"+i).value		=tglnota[j];
		      document.getElementById("vnota"+i).value		=vnota[j];
		      document.getElementById("vbayar"+i).value		=vbayar[j];
		      document.getElementById("vsisa"+i).value		=vsisa[j];
		  	}

		  	var url = '<?php echo base_url() ?>pelunasan/Cform/listnota/'+pel+'/'+counter;
		  	$('.modal-content').load(url,function(){
	        	$('#myModal').modal({show:true});
	    	});
		}
	}

	function validasi()
	{
		var totrow = $('#totrow').val();
		var pelanggan = $('#pelanggan').val();
		var tglvoucher = $('#datepicker').val();
		var jmlbayar = $('#jmlbayar').val();
		var novoc = $('#novoc').val();

		if(novoc==''){
			swal(
				'Peringatan!',
				'No Voucher Tidak Boleh Kosong! :)',
				'warning'
			);
			return false;
		} else if(pelanggan==''){
			swal(
				'Peringatan!',
				'Pelanggan Tidak Boleh Kosong! :)',
				'warning'
			);
			return false;
		} else if(tglvoucher=='') {
			swal(
				'Peringatan!',
				'Tanggal Voucher Tidak Boleh Kosong! :)',
				'warning'
			);
			return false;
		} else if(jmlbayar=='') {
			swal(
				'Peringatan!',
				'Jumlah Bayar Tidak Boleh Kosong! :)',
				'warning'
			);
			return false;
		} else if(totrow==0 || totrow=='0') {
			swal(
				'Peringatan!',
				'Item Nota Tidak Boleh Kosong! :)',
				'warning'
			);
			return false;
		} else {
			swal({
			    title: "Yakin Simpan?",
			    text: "Yakin data-data sudah terisi dengan benar?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#6A9944",
				confirmButtonText: "Ya, Simpan aja",
				cancelButtonText: "Tidak, Jangan dulu",
				closeOnConfirm: true,
  	  			closeOnCancel: false
				},
				function(isConfirm) {
					if (isConfirm) {
				    	$('#submit').removeAttr("type").attr("type", "submit");			
						$('#submit').trigger('click', {'send': true});
				        // $('#myForm').submit();
				        return true;
				    } else {
				    	swal('Tidak Jadi','Data tidak jadi disimpan','error');
				    	return false;
				    }
				}
			);
		}
	}
</script>