   
          <div class="modal-header" style="background: black;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color:white;"><?php echo $title_modal.' '.$voucher ?></h4>
          </div>
          <div class="modal-body">
            <table id="myTableList" class="table table-bordered table-hover" style="width:100%">
              <thead>
                <th align="center">No Nota</th>
                <th align="center">Nilai Bayar</th>
              </thead>
              <tbody>
                <?php 
                  if(!empty($isi))
                  {
                    foreach ($isi as $key) {
                ?>
                      <tr>
                        <td><?php echo $key->i_no_nota; ?></td>
                        <td><?php echo number_format($key->v_bayar_nota) ?></td>
                      </tr>
                <?php
                    }
                  } else {
                    echo "<tr><td colspan=\"2\" style=\"text-align:center;\">Maaf Tidak Ada Nota</td></tr>";
                  }
                ?>
              </tbody>
            </table>
          </div>
          <div class="modal-footer" style="background: black;">
            <button type="button" id="batal" onclick="bbatal();" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>

<script>
  $(".modal").on("hidden.bs.modal", function(){
    $(this).removeData();
  });
  // $(function () {
  //   $('#myTableList').DataTable({
  //     'scrollX': true,
  //     'lengthChange': false,
  //   })
  // })
</script>
      
