   
          <div class="modal-header" style="background: black;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color:white;"><?php echo $title_modal ?></h4>
          </div>
          <div class="modal-body">
            <table id="myTable" class="table table-bordered table-hover" style="width:100%">
              <thead>
                <th align="center">No Nota</th>
                <th align="center">Tgl Nota</th>
                <th align="center">Nilai Nota</th>
              </thead>
              <tbody>
                <input type="hidden" id="barisan" name="barisan" value="<?php echo $baris ?>">
                <?php 
                  if(!empty($isi))
                  {
                    foreach ($isi as $key) {
                    echo
                      "<tr>
                        <td align=\"center\"><a href=\"javascript:setValue('$baris','$key->i_no_nota','$key->i_id_nota','$key->d_nota','$key->v_total_sisa')\">$key->i_no_nota</a></td>
                        <td align=\"center\"><a href=\"javascript:setValue('$baris','$key->i_no_nota','$key->i_id_nota','$key->d_nota','$key->v_total_sisa')\">$key->d_nota</a></td>
                        <td align=\"center\"><a href=\"javascript:setValue('$baris','$key->i_no_nota','$key->i_id_nota','$key->d_nota','$key->v_total_sisa')\">".number_format($key->v_total_sisa)."</a></td>
                      </tr>";
                
                    }
                  } else {
                    echo "<tr><td colspan=\"3\" style=\"text-align:center;\">Maaf Tidak Ada Nota</td></tr>";
                  }
                ?>
              </tbody>
            </table>
          </div>
          <div class="modal-footer" style="background: black;">
            <button type="button" id="batal" onclick="bbatal();" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>

<script>
  $(".modal").on("hidden.bs.modal", function(){
    $(this).removeData();
  });
  function bbatal(){
    barisan = <?php echo $baris ?>;
    si_inner= $("#detailbody").html();
    si_inner= si_inner.trim();
    var temp= new Array();
    temp  = si_inner.split('<tr>');
    if($("#inotacode"+barisan).val()=='') {
      si_inner="";
      for(x=1;x<barisan;x++){
        si_inner=si_inner+'<tr>'+temp[x];
      }
        j=0;
        var baris        =Array();
        var inota        =Array();      
        var inotacode    =Array();
        var tglnota      =Array();
        var vnota        =Array();
        var vbayar       =Array();
        var vsisa        =Array();
        for(i=1;i<barisan;i++){
          j++;
          baris[j]        = $("#baris"+i).val();
          inota[j]        = $("#inota"+i).val();
          inotacode[j]    = $("#inotacode"+i).val();
          tglnota[j]      = $("#dnota"+i).val();
          vnota[j]        = $("#vnota"+i).val();
          vbayar[j]       = $("#vbayar"+i).val();
          vsisa[j]        = $("#vsisa"+i).val();
        }
        $('#detailbody').html(si_inner);
        j=0;
        for(i=1;i<barisan;i++){
          j++;
          $("#baris"+i).val(baris[j]);
          $("#inota"+i).val(inota[j]);
          $("#inotacode"+i).val(inotacode[j]);
          $("#dnota"+i).val(tglnota[j]);
          $("#vnota"+i).val(vnota[j]);
          $("#vbayar"+i).val(vbayar[j]);
          $("#vsisa"+i).val(vsisa[j]);
        }
        var totrownew = parseFloat($("#totrow").val())-1;
        $("#totrow").val(totrownew);
    }
  }
  $(function () {
    $('#myTable').DataTable({
      'scrollX': true,
      'lengthChange': false,
    })
  })
  function setValue(a,b,c,d,e)
  {
    var ada = false;
    var amount=$('#jmlsisa').val();
    var vbayar=0;
    var vsisa=0;
    for(i=1;i<=a;i++) {
      if(
        (c==$("#inota"+i).val())
      ){
        swal(
          'Peringatan!',
          'Nota '+b+' sdh ada!',
          'warning'
        );
        ada=true;
        break;
      }else{
        ada=false;
      }
    }
    
    if (!ada)
    {
      $("#inota"+a).val(c);
      $("#inotacode"+a).val(b);
      $("#dnota"+a).val(d);
      $("#vnota"+a).val(formatuang(e));
      if(parseFloat(amount)>parseFloat(e))
      {
        vbayar = e;
        vsisa  = parseFloat(amount)-parseFloat(e);
      } else if(parseFloat(amount)==parseFloat(e)){
        vbayar = e;
        vsisa  = 0;
      } else {
        vbayar = parseFloat(amount);
        vsisa  = 0;
      }
      $("#vbayar"+a).val(formatuang(vbayar));
      $("#vsisa"+a).val(formatuang(vsisa));
      $('#jmlsisa').val(vsisa);
      $('#myModal').modal('hide');
    }
  }
</script>
      
