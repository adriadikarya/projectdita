<section class="content">
	<div class="modal fade" id="myModal" role="dialog">
  		<div class="modal-dialog">
  			<!-- Modal content-->
        	<div class="modal-content">
  			</div>     
  		</div>
  	</div>
	<div class="row">
		<!-- left column -->
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
			    	<h3 class="box-title">List Pelunasan Nota</h3>
			    </div>
			    <div class="box-body">
			    <?php if($message = $this->session->flashdata('message')): ?>
			                <div class="alert <?php echo ($message['status']) ? 'alert-success' : 'alert-danger'; ?>" >
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $message['message']; ?>
			                </div>
			    <?php endif; ?>
			    <div class="row">
			    	<div class="col-sm-12 col-lg-12">&nbsp;</div>
			    </div>
			    <div class="row">
			    	<div class="col-sm-3 col-lg-3">
			    		<a href="<?php echo base_url() ?>pelunasan/Cform/tambah" title="tambah data" class="btn btn-success btn-rounded"><i class="fa fa-plus"></i> &nbsp;Tambah Data</a>
			    	</div>	
			    </div>
			    <div class="row">
			    	<div class="col-sm-12 col-lg-12">&nbsp;</div>
			    </div>
			    	<table id="myTable" class="table table-bordered table-hover display nowrap" style="width:100%">
			            <thead>
			              	<th>No</th>
			              	<th>No Voucher</th>
			              	<th>Tgl Voucher</th>
			              	<th>Nilai Bayar</th>
			              	<th>Keterangan</th>
			              	<th>Status</th>
			            	<th>Aksi</th>
			        	</thead>
			        </table>
			    </div>
			</div>
		</div>
	</div>
</section>