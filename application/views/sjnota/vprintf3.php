<style type="text/css">
	@page{
    /*size: 21cm 29.7cm;*/
    	margin: 0.05in 0.25in 0.05in 0.05in /*(29mm top) (29mm right) (30mm bottom) (30mm left)*/
    ;}
    .border { 
        border: solid black 1px; 
    }
    .kepala {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    	font-size: 25px;
	}
	.kepalaxx {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
	}
	.isihead {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    	font-size: 12px;
	}
	.judul {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    	font-size: 16px;
    	text-align: center;
	}
	.isicar{
		border-right: solid black 1px;
		text-align: center;
	}
	.isiangka{
		border-right: solid black 1px;
		text-align: right;
	}
	#kotak {
        border-collapse: collapse;
        border: 1px solid black;
    }
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
</style>
<?php include_once ("funcs/terbilang.php"); ?>
<table width="100%" border="0">
	<tr>
		<td class="kepala"><b>Harapan Jaya</b></td>
	</tr>
	<tr>
		<td class="kepalaxx">&nbsp;</td>
	</tr>
	<tr>
		<td class="judul">&nbsp;</td>
	</tr>
	<tr>
		<td class="judul"><b>INVOICE</b></td>
	</tr>
	<tr>
		<td class="judul"><b>No. <?php echo $head->i_no_nota ?></b></td>
	</tr>
	<tr>
		<td class="judul">&nbsp;</td>
	</tr>
</table>
<table width="100%" border="0">
	<table width="100%" border="0">
		<tr>
			<td colspan="5" class="isihead">Customer:</td>
			<td>&nbsp;</td>
			<td style="width: 50%">&nbsp;</td>
			<td class="isihead" style="text-align: right;">Tanggal:</td>
			<?php setlocale(LC_ALL, 'IND');
			$tglnota = strftime('%d %B %Y',strtotime($head->d_nota));
		?>
			<td class="isihead"><?php echo $tglnota ?></td>
		</tr>	
	</table>
	<table width="100%" border="0">
		<tr>
			<td class="isihead">Nama</td>
			<td class="isihead">:</td>
			<td class="isihead"><?php echo $head->e_nama_pelanggan ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td class="isihead">Alamat</td>
			<td class="isihead">:</td>
			<td class="isihead"><?php echo $head->e_alamat_pelanggan ?></td>
			<td class="isihead">&nbsp;</td>
			<td class="isihead" style="text-align: right;">No. Order:</td>
			<td class="isihead"><?php echo $head->i_no_order ?></td>
		</tr>
		<tr>
			<td class="isihead">Kota</td>
			<td class="isihead">:</td>
			<td class="isihead"><?php echo $head->e_nama_kota ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td class="isihead">Telepon</td>
			<td class="isihead">:</td>
			<td class="isihead"><?php echo $head->e_telp_pelanggan ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</table>
<table width="100%" border="0" id="kotak">
	<thead>
		<tr>
			<th style="border-right: solid black 1px; border-bottom: solid black 1px" class="kepalaxx">Nama Barang</th>
			<th style="border-right: solid black 1px; border-bottom: solid black 1px" class="kepalaxx">Satuan</th>
			<th style="border-right: solid black 1px; border-bottom: solid black 1px" class="kepalaxx">Jumlah</th>
			<th style="border-right: solid black 1px; border-bottom: solid black 1px" class="kepalaxx">Harga</th>
			<th style="border-bottom: solid black 1px" class="kepalaxx">Total</th>
		</tr>
	</thead>
	<tbody id="kotak">
		<?php 
		$i=1;
		$grandtot=0;
		$warna = "";
			if(!empty($item))
			{
				foreach ($item as $row) {
					# code...
					$subtot = $row->n_qty*$row->v_harga_satuan;
                    $isiwarna = $this->Mmaster->daftarwarnaitem($row->i_id_item_nota);
                    if(!empty($isiwarna)){
                        foreach ($isiwarna as $wrn) {
                    		$warna.= $wrn->e_nama_warna.' ('.$wrn->n_qty_warna.') ';
                    	}  
                    } else {
                    	$warna.= 'Tidak Ada Warna';
                    }
		?>
		<tr>
			<td class="isicar isihead"><?php echo $row->e_nama_brg.', Warna: '.$warna ?></td>
			<td class="isicar isihead"><?php echo $row->e_satuan ?></td>
			<td class="isicar isihead"><?php echo $row->n_qty ?></td>
			<td class="isicar isihead"><?php echo number_format($row->v_harga_satuan) ?></td>
			<td style="text-align: right;" class="isihead"><?php echo number_format($subtot)?></td>
		</tr>
		<?php 		$warna="";
					$grandtot+=$subtot;
					$i++;
				}
			} else {
				$subtot=0;
				$grandtot=0;
		?>	
			<tr>
				<td colspan="5" style="border-right: 1px solid black; text-align: center;">Maaf Tidak Ada Data!</td>
			</tr>
		<?php
			}
		?>
	</tbody>
	<tfoot>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td style="text-align: center;"><b>Total</b></td>
			<td>&nbsp;</td>
			<td style="text-align: right; width: 31%;" id="kotak" class="isihead"><b><?php echo number_format($grandtot) ?></b></td>
		</tr>
	</tfoot>
</table>
<table width="100%" border="0" align="right">
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>	
	<tr>
		<td><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tanda Terima</b></td>
		<td align="right"><b>Hormat Kami&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><b>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</b></td>
		<td align="right"><b>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</b></td>
	</tr>
</table>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>