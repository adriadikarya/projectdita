<style type="text/css">
	@page{
    /*size: 21cm 29.7cm;*/
    	margin: 0.05in 0.25in 0.05in 0.05in /*(29mm top) (29mm right) (30mm bottom) (30mm left)*/
    ;}
    .border { 
    	font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
        border: solid black 1px; 
    }
    .kepala {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    	font-size: 25px;
	}
	.kepalaxx {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
	}
	.isihead {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    	font-size: 12px;
	}
	.judul {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    	font-size: 18px;
	}
	.isicar{
		border-right: solid black 1px;
		text-align: center;
	}
	.isiangka{
		border-right: solid black 1px;
		text-align: right;
	}
	#kotak {
        border-collapse: collapse;
        border: 1px solid black;
    }
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
</style>
<?php include_once ("funcs/terbilang.php"); ?>
<table width="100%" border="0">
	<tr>
		<td class="kepala"><b>Sinar Mulia</b></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="kepalaxx"><?php echo $tf->e_alamat ?></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="kepalaxx"><?php echo $tf->e_telepon ?></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 40%">&nbsp;</td>
		<td class="judul"><b>FAKTUR PENJUALAN</b></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<?php setlocale(LC_ALL, 'IND');
			$tglnota = strftime('%d %B %Y',strtotime($head->d_nota));
		?>
		<td class="kepalaxx">Tanggal: <?php echo $tglnota ?></td>
		<td>&nbsp;</td>
		<td class="kepalaxx" style="text-align: right">No: <?php echo $head->i_no_nota ?></td>
	</tr>
	<tr>
		<td class="kepalaxx" colspan="2">Nama Konsumen: <?php echo $head->e_nama_pelanggan; ?></td>
		<!-- <td class="kepalaxx" style="text-align: center">No Telepon: <?php echo $head->e_telp_pelanggan ?></td> -->
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="kepalaxx">Alamat: <?php echo $head->e_alamat_pelanggan; ?></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<table width="100%" border="0" id="kotak" class="border">
	<thead>
		<th class="border">No</th>
		<th class="border">Deskripsi</th>
		<th class="border">Unit</th>
		<th class="border">Sat</th>
		<th class="border">Harga Sat</th>
		<th class="border">Jumlah</th>
	</thead>
	<tbody>
		<?php 
		$i=1;
		$grandtot=0;
		$warna = "";
			if(!empty($item))
			{
				foreach ($item as $row) {
					# code...
					$subtot = $row->n_qty*$row->v_harga_satuan;
                    $isiwarna = $this->Mmaster->daftarwarnaitem($row->i_id_item_nota);
                    if(!empty($isiwarna)){
                        foreach ($isiwarna as $wrn) {
                    		$warna.= $wrn->e_nama_warna.' ('.$wrn->n_qty_warna.') ';
                    	}  
                    } else {
                    	$warna.= 'Tidak Ada Warna';
                    }
		?>
		<tr>
			<td align="center" class="border"><?php echo $i ?></td>
			<td class="border"><?php echo $row->e_nama_brg.', Warna: '.$warna ?></td>
			<td align="center" class="border"><?php echo $row->n_qty ?></td>
			<td align="center" class="border"><?php echo $row->e_satuan ?></td>
			<td align="center" class="border"><?php echo number_format($row->v_harga_satuan) ?></td>
			<td align="right" class="border"><?php echo number_format($subtot)?></td>
		</tr>
		<?php 		$warna="";
					$grandtot+=$subtot;
					$i++;
				}
			} else {
				$subtot=0;
				$grandtot=0;
		?>	
			<tr>
				<td colspan="5" style="border-right: 1px solid black; text-align: center;">Maaf Tidak Ada Data!</td>
			</tr>
		<?php
			}
		?>
		<tr>
			<td colspan="5" class="border"><b>Total Penjualan</b></td>
			<td align="right" class="border"><b><?php echo number_format($grandtot) ?></b></td>
		</tr>
	</tbody>
</table>
<table width="100%" border="0">	
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;Tanda Terima</td>
		<td>&nbsp;</td>
		<td colspan="2" align="right">Hormat Kami&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
		<td>&nbsp;</td>
		<td colspan="2" align="right">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
	</tr>
</table>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
