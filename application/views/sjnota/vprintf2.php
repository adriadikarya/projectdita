<style type="text/css">
	@page{
    /*size: 21cm 29.7cm;*/
    	margin: 0.05in 0.25in 0.05in 0.05in;
    ;}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
</style>
<?php include_once ("funcs/terbilang.php"); ?>

<table width="100%" border="0">
	<tr>
		<td colspan="7"><hr></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3" style="font-size: 16px"><b>FAKTUR</b></td>
		<td colspan="2" style="font-size: 16px; font-style: italic; text-align: right;"><b>Cahaya Gumilang</b></td>
		<td colspan="2" style="font-size: 12px">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="7"><hr></td>
	</tr>
</table>
<table width="100%" border="0">
	<tr>
		<td>&nbsp;</td>
		<td>Nomor Nota :</td>
		<td><?php echo $head->i_no_nota ?></td>
		<td>&nbsp;</td>
		<td>Tanggal :</td>
		<?php setlocale(LC_ALL, 'IND');
			$tglnota = strftime('%d %B %Y',strtotime($head->d_nota));

		?>
		<td><?php echo $tglnota ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nomor Order :</td>
		<td><?php echo $head->i_no_order ?></td>
		<td>&nbsp;</td>
		<td>Up :</td>
		<td><?php echo $head->e_kepada ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Kepada :</td>
		<td><?php echo $head->e_nama_pelanggan ?></td>
		<td>&nbsp;</td>
		<td>Mata Uang :</td>
		<td>IDR - (Rupiah)</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>Term :</td>
		<td><?php if($head->i_jenis_pembayaran=='1')
		{
			echo "Cash";
		} else {
			echo "Transfer";
		}
		?>		
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<table width="100%" border="0">
	<thead>
		<tr>
			<td colspan="7"><hr></td>
		</tr>
		<tr>
			<th>Nama Barang/Pesanan</th>
			<th>Jumlah</th>
			<th>Unit</th>
			<th>Harga@</th>
			<th>Sub Total</th>
		</tr>
		<tr>
			<td colspan="7"><hr></td>
		</tr>
	</thead>
	<tbody>
		<?php 
		$i=1;
		$grandtot=0;
		$warna = "";
			if(!empty($item))
			{
				foreach ($item as $row) {
					# code...
					$subtot = $row->n_qty*$row->v_harga_satuan;
                    $isiwarna = $this->Mmaster->daftarwarnaitem($row->i_id_item_nota);
                    if(!empty($isiwarna)){
                        foreach ($isiwarna as $wrn) {
                    		$warna.= $wrn->e_nama_warna.' ('.$wrn->n_qty_warna.') ';
                    	}  
                    } else {
                    	$warna.= 'Tidak Ada Warna';
                    }
		?>
		<tr>
			<td style="text-align: left;"><?php echo $row->e_nama_brg.', Warna: '.$warna ?></td>
			<td style="text-align: center;"><?php echo $row->n_qty ?></td>
			<td style="text-align: center;"><?php echo $row->e_satuan ?></td>
			<td style="text-align: center;"><?php echo number_format($row->v_harga_satuan) ?></td>
			<td style="text-align: right;"><?php echo number_format($subtot)?></td>
		</tr>
		<?php 		$warna="";
					$grandtot+=$subtot;
					$i++;
				}
			} else {
				$subtot=0;
				$grandtot=0;
		?>	
			<tr>
				<td colspan="5" style="border-right: 1px solid black; text-align: center;">Maaf Tidak Ada Data!</td>
			</tr>
		<?php
			}
		?>
	</tbody>
</table>
<table width="100%" border="0">
	<tr>
		<td colspan="7"><hr></td>
	</tr>
	<tr>
		<td rowspan="2" style="width: 65%; font-style: italic;"><b><?php $terbil=terbilangxx($grandtot,3); ?><?php echo $terbil ?> Rupiah</b></td>
		<td>Total :</td>
		<td style="text-align: right;"><b><?php echo number_format($grandtot) ?></b></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
</table>
<table width="100%" border="0">
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2" style="text-align: center; font-style: italic;"><?php echo $head->e_nama_pelanggan; ?></td>
		<td colspan="3" style="width: 42%">&nbsp;</td>
		<td colspan="2" style="text-align: center; font-style: italic;">Cahaya Gumilang</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2" style="text-align: center;"><?php echo $head->e_kepada ?></td>
		<td colspan="3">&nbsp;</td>
		<td colspan="2" style="text-align: center;">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2"><hr></td>
		<td colspan="3">&nbsp;</td>
		<td colspan="2"><hr></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2" style="text-align: center;">&nbsp;</td>
		<td colspan="3">&nbsp;</td>
		<td colspan="2" style="text-align: center;">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>