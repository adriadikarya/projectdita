   
          <div class="modal-header" style="background: black;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color:white;"><?php echo $title_modal.' ('.$nonota.')'; ?></h4>
          </div>
          <div class="modal-body">
            <table id="myTable" class="table table-bordered table-hover" style="width:100%">
              <thead>
                <th align="center">Nama Barang</th>
                <th align="center">Warna (Qty)</th>
                <th align="center">Qty Total</th>
                <th align="center">Harga Satuan</th>
                <th align="center">Keterangan</th>
              </thead>
              <tbody>
                <?php
                  if(!empty($isi))
                  {
                    $kodebrg = "";
                    $i=1;
                    $colspan=0;
                    foreach ($isi as $row) {
                      $ket = $row->e_desc==''?"Tidak Ada":$row->e_desc;
                      if($row->i_kode_brg!=$kodebrg)
                      {
                        echo '<tr>';
                          echo '<td colspan="'.$colspan.'">'.$row->i_kode_brg.'</td>';
                          if($row->i_id_warna!='' || $row->i_id_warna!=null)
                          {
                            echo '<td>';
                            $isiwarna = $this->Mmaster->daftarwarnaitem($row->i_id_item_nota);
                            if(!empty($isiwarna)){
                              foreach ($isiwarna as $wrn) {
                                echo $wrn->e_nama_warna.' ('.$wrn->n_qty_warna.') ';
                              }  
                            } else {
                              echo 'Tidak Ada Warna';
                            }
                            echo '</td>';
                          } else {
                            echo '<td>Tidak Ada Warna</td>';
                          }
                          echo '<td colspan="'.$colspan.'">'.$row->n_qty.'</td>';
                          echo '<td colspan="'.$colspan.'">'.$row->v_harga_satuan.'</td>';
                          echo '<td colspan="'.$colspan.'">'.$ket.'</td>';    
                        echo '<tr>';                       
                      }
                      $kodebrg = $row->i_kode_brg;
                    }
                  } else {
                    echo '<tr><td colspan="5" style="text-align:center;">Maaf Tidak Ada Data!</td></tr>';
                  }
                ?>
              </tbody>
            </table>
          </div>
          <div class="modal-footer" style="background: black;">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
<script>
  $(".modal").on("hidden.bs.modal", function(){
    $(this).removeData();
  });
</script>    
