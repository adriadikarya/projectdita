<div class="modal-header" style="background: black;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color:white;"><?php echo $title_modal ?></h4>
          </div>
          <div id="printThis">
          <div class="modal-body">
<style type="text/css">
@media screen {
  #printSection {
      display: none;
  }



   .noDisplay{
    display:none;
    }

    .pagebreak{
      page-break-before: always;
    }
   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
    
    }
    
    .kepala {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 25px;
  }
  .kepalaxx {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
  }
  .kepala2 {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 14px;
  }
  .kepala3 {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 16px;
  }
  .detailbrg {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 14px;
  }
  .detailbrg3 {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 16px;
  }
  .detailbrg2 {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
  }
  .kotak {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 11px;
    border-collapse:collapse;
    border:1px solid black;
    }
    
    .tabelheader {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
          font-size: 13px;}
    
    .subtotal {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 14px;
  }
  
  .garisbawah { 
    border-bottom:#000000 0.1px solid;
  }

  .parallelogram {
     height: 10px;
     width: 20px; 
     border: 1px solid black;
     -webkit-transform: skew(-25deg); 
     -moz-transform: skew(-25deg); 
     -o-transform: skew(-25deg);
     transform: skew(-25deg);
  }
}

@media print {
  body * {
    visibility:hidden;
  }
  @page{
    /*size: 21cm 29.7cm;*/
    /*margin: 0.05in 0.79in 0.10in 0.60in (29mm top) (29mm right) (30mm bottom) (30mm left)
    ;*/
    margin: 0.05in 0.25in 0.05in 0.05in
    ;
}
  #printSection, #printSection * {
    visibility:visible;
  }
  #printSection {
    position:absolute;
    left:0;
    top:0;
  }
}

/*@media print {
  div {
    color: #f00;
  }
} */

/*yang ini buat setting ukuran kertasnya assumsi A4/letter, sementara ga dipake */
/*#Letter {background-color:#FFFFFF;
left:1px;
right:1px;
height:11in ; */ /*Ukuran Panjang Kertas */
/*width: 8.50in; */ /*Ukuran Lebar Kertas */
/*margin:1px solid #FFFFFF; */
 
/* font-family:Georgia, "Times New Roman", Times, serif; } */

/*@page { size: Letter; margin: 0.29in 0.29in 0.3in 0.60in;}*/
</style>


<?php include_once ("funcs/terbilang.php"); ?>
<table border="0" width="100%" class="tabelheader">
  <tr>
    <td colspan="2"><h2>Tri Makmur Abadi</h2></td>
    <td width="25%"><u>Cimahi, <?php setlocale(LC_ALL, 'IND'); echo strftime('%d %B %Y',strtotime($head->d_nota)); ?></u></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"></td>
    <td>Kepada Yth:</td>
  </tr>
  <tr>
    <td colspan="2"><div class="kepala"><b>Faktur</b></div></td>
    <td><?php echo $head->e_nama_pelanggan ?></td>
  </tr>
  <tr>
    <td colspan="2"></td>
    <td rowspan="2"><?php echo $head->e_alamat_pelanggan ?></td>
  </tr>
  <tr>
    <td>No Nota: <?php echo $head->i_no_nota ?></td>
  </tr>
  <tr>
    <td>No Order: <?php echo $head->i_no_order ?></td>
  </tr>
</table>
<table class="isinya" style="width: 100%; border-collapse: collapse; border: 2px solid black;">
  <thead style="border: 2px solid black; ">
    <tr >
      <th style="border: 1px solid black; ">No.</th>
      <th style="border: 1px solid black; ">Nama Barang</th>
      <th style="border: 1px solid black; ">Banyaknya</th>
      <th style="border: 1px solid black; ">Harga Satuan</th>
      <th style="border: 1px solid black; ">Jumlah</th>
    </tr>
    <!-- <tr>
      <th colspan="5"><hr></th>
      
    </tr> -->
  </thead>
  <tbody>
    <?php 
    $i=1;
    $grandtot=0;
    $warna = "";
      if(!empty($item))
      {
        foreach ($item as $row) {
          # code...
          $subtot = $row->n_qty*$row->v_harga_satuan;
                    $isiwarna = $this->Mmaster->daftarwarnaitem($row->i_id_item_nota);
                    if(!empty($isiwarna)){
                        foreach ($isiwarna as $wrn) {
                        $warna.= $wrn->e_nama_warna.' ('.$wrn->n_qty_warna.') ';
                      }  
                    } else {
                      $warna.= 'Tidak Ada Warna';
                    }
    ?>
    <tr>
      <td align="center" style="border-right: 1px solid black; "><?php echo $i ?></td>
      <td style="border-right: 1px solid black; "><?php echo $row->e_nama_brg.', Warna: '.$warna ?></td>
      <td align="right" style="border-right: 1px solid black; "><?php echo $row->n_qty ?></td>
      <td align="right" style="border-right: 1px solid black; "><?php echo number_format($row->v_harga_satuan) ?></td>
      <td align="right" style="border-right: 1px solid black; "><?php echo number_format($subtot)?></td>
    </tr>
    <?php     $warna="";
          $grandtot+=$subtot;
          $i++;
        }
      } else {
        $subtot=0;
        $grandtot=0;
    ?>  
      <tr>
        <td colspan="5" style="border-right: 1px solid black; text-align: center;">Maaf Tidak Ada Data!</td>
      </tr>
    <?php
      }
    ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="3" style="border-top: 1px solid black;">&nbsp;</td>
      <td style="border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black">Total</td>
      <td align="right" style="border-top: 1px solid black;"><?php echo number_format($grandtot) ?></td>
    </tr>
  </tfoot>
</table>
<table width="100%">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="15%" align="center">Terbilang :</td>
    <td class="parallelogram kepala3" align="justify" ><?php $terbil=terbilangxx($grandtot,3); ?><?php echo $terbil ?> Rupiah</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="100%">
  <thead>
    <tr>
      <td><u><b>Pembayaran Mohon Di Transfer Ke:</b></u></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </thead>
</table>
<table width="100%">
  <tbody>
    <tr>
      <td style="width: 15%">No Rekening</td>
      <td>:</td>
      <td><?php echo $tf->e_no_rek1; ?></td>
      <td align="center">Hormat Kami</td>
    </tr>
    <tr>
      <td>Bank</td>
      <td>:</td>
      <td><?php echo $tf->e_nama_bank1; ?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Atas Nama</td>
      <td>:</td>
      <td><?php echo $tf->e_atas_nama1; ?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Atau</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>No Rekening</td>
      <td>:</td>
      <td><?php echo $tf->e_no_rek2; ?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Bank</td>
      <td>:</td>
      <td><?php echo $tf->e_nama_bank2; ?></td>
      <td align="center">(<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>)</td>
    </tr>
    <tr>
      <td>Atas Nama</td>
      <td>:</td>
      <td><?php echo $tf->e_atas_nama2; ?></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
          </div>
        </div>
          <div class="modal-footer" style="background: black;">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button id="btnPrint" type="button" class="btn btn-default">Print</button>
          </div>

<script>
  $(".modal").on("hidden.bs.modal", function(){
    $(this).removeData();
  });
  document.getElementById("btnPrint").onclick = function () {
    printElement(document.getElementById("printThis"));
  }

  function printElement(elem) {
      var domClone = elem.cloneNode(true);
      
      var $printSection = document.getElementById("printSection");
      
      if (!$printSection) {
          var $printSection = document.createElement("div");
          $printSection.id = "printSection";
          document.body.appendChild($printSection);
      }
      
      $printSection.innerHTML = "";
      $printSection.appendChild(domClone);
      window.print();
  }
</script>