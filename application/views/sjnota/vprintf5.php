<style type="text/css">
	@page{
    /*size: 21cm 29.7cm;*/
    	margin: 0.05in 0.25in 0.05in 0.05in /*(29mm top) (29mm right) (30mm bottom) (30mm left)*/
    ;}
    .border { 
        border: solid black 1px; 
    }
    .kepala {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    	font-size: 18px;
	}
	.kepalaxx {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 13px;
	}
	.kepalax {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 20px;
	}
	.isihead {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    	font-size: 12px;
	}
	.judul {
		font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    	font-size: 16px;
	}
	.isicar{
		border-right: solid black 1px;
		text-align: center;
	}
	.isiangka{
		border-right: solid black 1px;
		text-align: right;
	}
	#kotak {
        border-collapse: collapse;
        border: 1px solid black;
    }
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
</style>
<?php include_once ("funcs/terbilang.php"); ?>
<table width="100%" border="0">
	<tr>
		<td class="kepalax"><b>ANUGERAH PERCETAKAN</b></td>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="kepala" colspan="2"><b><u>FAKTUR TAGIHAN</u></b></td>
		<?php setlocale(LC_ALL, 'IND');
			$tglnota = strftime('%d %B %Y',strtotime($head->d_nota));
		?>
		<td class="kepalaxx" align="center">Cimahi, <?php echo $tglnota; ?></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
		<td class="kepalaxx" align="center">Kepada Yth,</td>
	</tr>
	<tr>
		<td class="kepalaxx">No. Faktur : <?php echo $head->i_no_nota ?></td>
		<td class="kepalaxx" align="right" colspan="2">Tuan/Toko:</td>
		<td class="kepalaxx" align="center"><b><?php echo $head->e_nama_pelanggan ?></b></td>
	</tr>
</table>
<table width="100%" border="0">
	<thead>
		<tr>
			<th colspan="5"><hr></th>
		</tr>
		<tr>
			<th class="isihead">Banyaknya</th>
			<th class="isihead">Sat</th>
			<th class="isihead">Nama Barang</th>
			<th class="isihead">Harga Satuan</th>
			<th class="isihead">Jumlah</th>
		</tr>
		<tr>
			<th colspan="5"><hr></th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$i=1;
		$grandtot=0;
		$warna = "";
			if(!empty($item))
			{
				foreach ($item as $row) {
					# code...
					$subtot = $row->n_qty*$row->v_harga_satuan;
                    // $isiwarna = $this->Mmaster->daftarwarnaitem($row->i_id_item_nota);
                    // if(!empty($isiwarna)){
                    //     foreach ($isiwarna as $wrn) {
                    // 		$warna.= $wrn->e_nama_warna.' ('.$wrn->n_qty_warna.') ';
                    // 	}  
                    // } else {
                    // 	$warna.= 'Tidak Ada Warna';
                    // }
		?>
		<tr>
			<td align="center" class="isihead"><?php echo number_format($row->n_qty) ?></td>
			<td align="center" class="isihead"><?php echo $row->e_satuan ?></td>
			<td class="isihead"><?php echo $row->e_nama_brg?></td>
			<td align="center" class="isihead"><?php echo number_format($row->v_harga_satuan) ?></td>
			<td align="center" class="isihead"><?php echo number_format($subtot)?></td>
		</tr>
		<?php 		$warna="";
					$grandtot+=$subtot;
					$i++;
				}
			} else {
				$subtot=0;
				$grandtot=0;
		?>	
			<tr>
				<td colspan="5" style="border-right: 1px solid black; text-align: center;">Maaf Tidak Ada Data!</td>
			</tr>
		<?php
			}
		?>
		<tr>
			<td colspan="5"><hr></td>
		</tr>
		<tr>
			<td colspan="4" align="right">JUMLAH Rp.</td>
			<td align="center" style="text-align: center;" class="border"><b><?php echo number_format($grandtot) ?></b></td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" align="center">Tanda Terima</td>
			<td colspan="2">&nbsp;</td>
			<td align="center">Hormat Kami</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="2">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="2">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" align="center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
			<td colspan="2">&nbsp;</td>
			<td align="center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
		</tr>
	</tbody>
</table>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>