   
          <div class="modal-header" style="background: black;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color:white;"><?php echo $title_modal ?></h4>
          </div>
          <div class="modal-body">
            <table id="myTable" class="table table-bordered table-hover" style="width:100%">
              <thead>
                <th align="center">Barang</th>
                <th align="center">Warna</th>
              </thead>
              <tbody>
                <?php 
                  if(!empty($isibrg))
                  {
                    $namabrg = "";
                    foreach ($isibrg as $key) {
                    echo
                      "<tr>
                        <td align=\"center\"><a href=\"javascript:setValue('$key->i_kode_brg','$key->e_nama_brg','$key->v_hjp','$baris')\">$key->i_kode_brg</a></td>
                        <td align=\"center\"><a href=\"javascript:setValue('$key->i_kode_brg','$key->e_nama_brg','$key->v_hjp','$baris')\">$key->e_nama_brg</a></td>
                      </tr>";
                
                    }
                  } else {
                    echo "<tr><td colspan=\"2\" style=\"text-align:center;\">Maaf Tidak Ada Warna</td></tr>";
                  }
                ?>
              </tbody>
            </table>
          </div>
          <div class="modal-footer" style="background: black;">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>

<script>
  $(".modal").on("hidden.bs.modal", function(){
    $(this).removeData();
  });
  $(function () {
    $('#myTable').DataTable({
      'scrollX': true,
      'lengthChange': false,
    })
  })
  function setValue(a,b,c,d)
  {
    var ada = false;
    for(i=1;i<=d;i++) {
      if(
        (a==$("#kodebrg"+i).val())
      ){
        swal(
          'Peringatan!',
          'Nama Barang '+b+' sdh ada!',
          'warning'
        );
        ada=true;
        break;
      }else{
        ada=false;
      }
    }
    
    if (!ada)
    {
      $('#namabrg'+d).val(b);
      $('#namabrg'+d).attr('disabled','disabled');
      $('#kodebrg'+d).val(a);
      $('#hrg'+d).val(formatuang(c));
      $('#jml'+d).focus();
      $('#myModal').modal('hide');
    }
  }
</script>
      
