<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">List Pelanggan</a></li>
              <li><a href="#tab_2" data-toggle="tab">Form Input Pelanggan</a></li>
              <!-- <li><a href="#tab_3" data-toggle="tab">Tab 3</a></li> -->
          	</ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane  active" id="tab_1">
            	<div class="row">
					<div class="col-lg-12 col-sm-12">
						<div class="box">
			            <div class="box-header">
			              <h3 class="box-title">Data Pelanggan</h3>
			            <?php if($message = $this->session->flashdata('message')): ?>
			                <div class="alert <?php echo ($message['status']) ? 'alert-success' : 'alert-danger'; ?>" >
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $message['message']; ?>
			                </div>
			            <?php endif; ?>
			            </div>
			            <!-- /.box-header -->
			            <div class="box-body">
			              <table id="datatabel" class="table table-bordered table-hover display nowrap" style="width:100%">
			              	<thead>
			              		<th>Kode</th>
			              		<th>Nama</th>
			              		<th>Alamat</th>
			              		<th>Telpon</th>
			              		<th>Grup Pelanggan</th>
			              		<th>Action</th>
			              	</thead>
			              	<tbody>
			              		<?php
			              			if(!empty($pel))
			              			{	
			              				foreach ($pel as $row) {
			              				$urldelete = "C_pelanggan/delete/".$row->i_pelanggan."/".$row->i_kode_pelanggan;
			              		?>
			              			<tr>
			              				<td><?php echo $row->i_kode_pelanggan; ?></td>
			              				<td><?php echo $row->e_nama_pelanggan; ?></td>
			              				<td><?php echo $row->e_alamat_pelanggan; ?></td>
			              				<td><?php echo $row->e_telp_pelanggan; ?></td>
			              				<td><?php echo $row->e_nama_grup; ?></td>
			              				<td align="center">
			              					<a href="<?php echo base_url('pelanggan/C_pelanggan/edit/' . $row->i_pelanggan); ?>"><button class="btn btn-success btn-rounded btn-sm"><i class="glyphicon glyphicon-edit"></i></button></a>
			                                <a alt="Delete" id="delete" href="#"><button class="btn btn-danger btn-rounded btn-sm" onclick="return hapus('<?php echo $urldelete; ?>');"><i class="glyphicon glyphicon-trash"></i></button></a>
			              				</td>
			              			</tr>
			              		<?php
			              				}
			              			} else {
								?>
									<tr>
										<td colspan="6" style="text-align: center">Maaf Tidak Ada Data!</td>
									</tr>
								<?php              				
			              			}
			              		?>
			              	</tbody>
			              </table>
			          	</div>
						</div>
					</div>
				</div>
            </div>
            <div class="tab-pane" id="tab_2">
            	<div class="row">
			        <!-- left column -->
			        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
			          	<!-- general form elements -->
			          	<div class="box box-primary">
			            	<div class="box-header with-border">
			        		    <h3 class="box-title">From Input Pelanggan</h3>
			    	        </div>
			    	        <!-- /.box-header -->
				            <!-- form start -->
				            <form role="form" method="post" action="<?php echo base_url() ?>pelanggan/C_pelanggan">
				              <div class="box-body">
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="kodepel">Kode Pelanggan</label>
				                  <input type="text" class="form-control" id="kodepel" name="kodepel" placeholder="Isi Kode Pelanggan" required maxlength="6">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="namapel">Nama Pelanggan</label>
				                  <input type="text" class="form-control" id="namapel" name="namapel" placeholder="Isi Nama Pelanggan" required maxlength="255">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="npwp">NPWP Pelanggan</label>
				                  <input type="text" class="form-control" id="npwp" name="npwp" placeholder="000.000.000.000.000" value="" maxlength="20">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="ntop">TOP</label>
				                  <input type="text" class="form-control" id="ntop" name="ntop" placeholder="30" value="" maxlength="5">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="telp">Telepon</label>
				                  <input type="text" class="form-control" id="telp" name="telp" value="" placeholder="Isi Dengan Nomor Telepon Pelanggan" maxlength="50">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="fax">Fax</label>
				                  <input type="text" class="form-control" id="fax" name="fax" value="" placeholder="Isi Dengan Nomor Fax Pelanggan" maxlength="50">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="kontak">Kontak</label>
				                  <input type="text" class="form-control" id="kontak" name="kontak" value="" placeholder="Isi Dengan Nama Kontak orang yg dapat dihubungi" maxlength="150">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="ktp">KTP</label>
				                  <input type="text" class="form-control" id="ktp" name="ktp" value="" placeholder="Isi Dengan Nomor KTP Pelanggan (Kalau ada)" maxlength="25">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="ekspedisi">Ekspedisi</label>
				                  <input type="text" class="form-control" id="ekspedisi" name="ekspedisi" value="" placeholder="Ekspedisi" maxlength="11">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="gruppel">Grup Pelanggan</label>
				                  <select id="gruppel" name="gruppel" class="form-control">
				                  	<?php 
				                  		if(!empty($grup))
				                  		{
				                  			foreach ($grup as $row) {
				                  	?>
				                  		<option value="<?php echo $row->i_id_grup ?>"><?php echo $row->e_nama_grup ?></option>
				                  	<?php
				                  			}
				                  		} else {
				                  			echo "<option value=\"\">Tidak Ada Grup!</option>";
				                  		}
				                  	?>
				                  </select>
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                	<label for="pkp">PKP</label>
				                  	<div class="checkbox">
				                  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="pkp" name="pkp" value="y">	PKP/Non-PKP
				              	    </div>
				                </div>
				                <!-- <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="prsh">Pelanggan Perusahaan?</label>
				                  <select id="prsh" name="prsh" class="form-control">
				                  	<?php 
				                  		if(!empty($prsh))
				                  		{
				                  			foreach ($prsh as $row) {
				                  	?>
				                  		<option value="<?php echo $row->i_kode_perusahaan ?>"><?php echo $row->e_nama_perusahaan ?></option>
				                  	<?php
				                  			}
				                  		} else {
				                  			echo "<option value=\"\">Tidak Ada Perusahaan!</option>";
				                  		}
				                  	?>
				                  </select>
				                </div> -->
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="alamat">Alamat</label>
				                   <textarea class="form-control" rows="3" id="alamat" name="alamat" placeholder="Isikan Alamat ..." maxlength="200" required ></textarea>
				                </div>
				              </div>
				              <!-- /.box-body -->

				              <div class="box-footer">
				                <button type="submit" name="submit" class="btn btn-success">Simpan</button>
				                <a href="<?php echo base_url() ?>pelanggan/C_pelanggan" class="btn btn-default">Kembali</a>
				              </div>
				            </form>
				        </div>
			        </div>
			    </div>
            </div>
        </div>
	</div>
    
</section>