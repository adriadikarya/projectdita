<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
		<!-- general form elements -->
			<div class="box box-info">
				<div class="box-header with-border">
			    	<h3 class="box-title">From Export Laporan Penjualan</h3>
				</div>
				<div class="box-body">
					<div class="form-group col-md-12 col-lg-6 col-sm-12">
				      	<label for="datepicker"><i class="fa fa-calendar"></i>&nbsp;Tgl Awal</label>
				        <input type="text" class="form-control" id="datepicker" name="dawal" placeholder="Tanggal Awal" readonly>
				    </div>
				    <div class="form-group col-md-12 col-lg-6 col-sm-12">
				      	<label for="datepicker1"><i class="fa fa-calendar"></i>&nbsp;Tgl Akhir</label>
				        <input type="text" class="form-control" id="datepicker2" name="dakhir" placeholder="Tanggal Akhir" readonly>
				    </div>
				</div>
				<div class="box-footer">
					<a align="right" class="btn btn-info" id="export" href="#" onclick='exportlink();' target='blank'>Export Ke Excel</a>
					<a align="right" class="btn btn-primary" id="exportitem" href="#" onclick='exportitemlink();' target='blank'>Export Ke Excel (Per Item)</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- jQuery 3 -->
<script src="<?php echo base_url() ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
	function exportlink() {
		var tglawal  = $('#datepicker').val();
		tmp1 = tglawal.split('/');
		tglawal = tmp1[2]+'-'+tmp1[1]+'-'+tmp1[0];
		var tglakhir = $('#datepicker2').val();
		tmp2 = tglakhir.split('/');
		tglakhir = tmp2[2]+'-'+tmp2[1]+'-'+tmp2[0];
		var new_url = "<?php echo base_url()?>infosjnota/Cform/export/"+tglawal+"/"+tglakhir+"/";
		$("#export").attr("href", new_url);
	}

	function exportitemlink()
	{
		var tglawal  = $('#datepicker').val();
		tmp1 = tglawal.split('/');
		tglawal = tmp1[2]+'-'+tmp1[1]+'-'+tmp1[0];
		var tglakhir = $('#datepicker2').val();
		tmp2 = tglakhir.split('/');
		tglakhir = tmp2[2]+'-'+tmp2[1]+'-'+tmp2[0];
		var new_url = "<?php echo base_url()?>infosjnota/Cform/exportitem/"+tglawal+"/"+tglakhir+"/";
		$("#exportitem").attr("href", new_url);
	}
</script>