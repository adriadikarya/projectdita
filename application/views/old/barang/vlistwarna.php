   
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><?php echo $title_modal ?></h4>
          </div>
          <div class="modal-body">
            <table id="myTable" class="table table-bordered table-hover display nowrap" style="width:100%">
              <thead>
                <th style="text-align:center;">Barang</th>
                <th style="text-align:center;">Warna</th>
              </thead>
              <tbody>
                <?php 
                  if(!empty($isibrgwarna))
                  {
                    $namabrg = "";
                    foreach ($isibrgwarna as $key) {
                      if($namabrg=="")
                      {
                        echo '<tr>
                               <td rowspan="'.($totalrows+1).'" style="text-align:center; vertical-align : middle; word-wrap: break-word;min-width: 160px;max-width: 160px;">'.$key->e_nama_brg.'</td>';  
                          foreach ($isibrgwarna as $row) {
                              echo  '<tr><td style="text-align:center;">'.$row->e_nama_warna.'</td></tr>';
                          }
                        echo  '</tr>';
                        $namabrg=$row->e_nama_brg;
                      }
                    }
                  } else {
                    echo "<tr><td colspan=\"2\" style=\"text-align:center;\">Maaf Tidak Ada Warna</td></tr>";
                  }
                ?>
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>

<script>
  $(".modal").on("hidden.bs.modal", function(){
    $(this).removeData();
  });
</script>
      
