<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">List Pelanggan</a></li>
              <!-- <li><a href="#tab_2" data-toggle="tab">Form Input Pelanggan</a></li> -->
              <!-- <li><a href="#tab_3" data-toggle="tab">Tab 3</a></li> -->
          	</ul>
        </div>
        <div class="tab-content">
        	<div class="tab-pane active" id="tab_1">
            	<div class="row">
			        <!-- left column -->
			        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
			          	<!-- general form elements -->
			          	<div class="box box-primary">
			            	<div class="box-header with-border">
			        		    <h3 class="box-title">From Input Pelanggan</h3>
			    	        </div>
			    	        <!-- /.box-header -->
				            <!-- form start -->
				            <form role="form" method="post" action="<?php echo base_url() ?>pelanggan/C_pelanggan/edit">
				              <div class="box-body">
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="kodepel">Kode Pelanggan</label>
				                  <input type="text" class="form-control" id="kodepel" name="kodepel" placeholder="Isi Kode Pelanggan" required maxlength="6" value="<?php echo $kodepel ?>">
				                  <input type="hidden" name="idpel" value="<?php echo $idpel ?>">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="namapel">Nama Pelanggan</label>
				                  <input type="text" class="form-control" id="namapel" name="namapel" placeholder="Isi Nama Pelanggan" required maxlength="255" value="<?php echo $namapel ?>">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="npwp">NPWP Pelanggan</label>
				                  <input type="text" class="form-control" id="npwp" name="npwp" placeholder="000.000.000.000.000" maxlength="20" value="<?php echo $npwp ?>">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="ntop">TOP</label>
				                  <input type="text" class="form-control" id="ntop" name="ntop" placeholder="30" maxlength="5" value="<?php echo $ntop ?>">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="telp">Telepon</label>
				                  <input type="text" class="form-control" id="telp" name="telp" placeholder="Isi Dengan Nomor Telepon Pelanggan" maxlength="50" value="<?php echo $telp ?>">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="fax">Fax</label>
				                  <input type="text" class="form-control" id="fax" name="fax" placeholder="Isi Dengan Nomor Fax Pelanggan" maxlength="50" value="<?php echo $fax ?>">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="kontak">Kontak</label>
				                  <input type="text" class="form-control" id="kontak" name="kontak" placeholder="Isi Dengan Nama Kontak orang yg dapat dihubungi" maxlength="150" value="<?php echo $kontak ?>">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="ktp">KTP</label>
				                  <input type="text" class="form-control" id="ktp" name="ktp" placeholder="Isi Dengan Nomor KTP Pelanggan (Kalau ada)" maxlength="25" value="<?php echo $ktp ?>">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="ekspedisi">Ekspedisi</label>
				                  <input type="text" class="form-control" id="ekspedisi" name="ekspedisi"  placeholder="Ekspedisi" maxlength="11" value="<?php echo $ekspedisi ?>">
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="gruppel">Grup Pelanggan</label>
				                  <select id="gruppel" name="gruppel" class="form-control">
				                  	<?php 
				                  		if(!empty($grup))
				                  		{
				                  			foreach ($grup as $row) {
				                  			$selected = $gruppel==$row->i_id_grup?"selected":"";
				                  	?>
				                  		<option value="<?php echo $row->i_id_grup ?>" <?php echo $selected?>><?php echo $row->e_nama_grup ?></option>
				                  	<?php
				                  			}
				                  		} else {
				                  			echo "<option value=\"\">Tidak Ada Grup!</option>";
				                  		}
				                  	?>
				                  </select>
				                </div>
				                <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                	<label for="pkp">PKP</label>
				                  	<div class="checkbox">
				                  		<?php 
				                  			$checked = $pkp=='1'?"checked":"";
				                  		?>
				                  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="pkp" name="pkp" value="y" <?php echo $checked ?>>	PKP/Non-PKP
				              	    </div>
				                </div>
				                <!-- <div class="form-group col-md-12 col-lg-6 col-sm-12">
				                  <label for="prsh">Pelanggan Perusahaan?</label>
				                  <select id="prsh" name="prsh" class="form-control">
				                  	<?php 
				                  		if(!empty($prsh))
				                  		{
				                  			foreach ($prsh as $row) {
				                  	?>
				                  		<option value="<?php echo $row->i_kode_perusahaan ?>"><?php echo $row->e_nama_perusahaan ?></option>
				                  	<?php
				                  			}
				                  		} else {
				                  			echo "<option value=\"\">Tidak Ada Perusahaan!</option>";
				                  		}
				                  	?>
				                  </select>
				                </div> -->
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="alamat">Alamat</label>
				                   <textarea class="form-control" rows="3" id="alamat" name="alamat" placeholder="Isikan Alamat ..." maxlength="200" required><?php echo htmlspecialchars($alamat); ?></textarea>
				                </div>
				              </div>
				              <!-- /.box-body -->

				              <div class="box-footer">
				                <button type="submit" name="submit" class="btn btn-info">Update</button>
				                <a href="<?php echo base_url() ?>pelanggan/C_pelanggan" class="btn btn-default">Kembali</a>
				              </div>
				            </form>
				        </div>
			        </div>
			    </div>
            </div>
        </div>
    </div>
</section>