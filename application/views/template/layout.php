<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Program</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- sweetalert2 -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/sweetalert2.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/select2/dist/css/select2.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url() ?>Main/layout" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><?php echo $this->session->userdata('kode_perusahaan')?></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?php echo $this->session->userdata('nama_perusahaan')?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url() ?>assets/dist/img/user-160x160.jpg" class="user-image" alt="User Image">
              <!-- <img src="<?php echo base_url() ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
              <span class="hidden-xs">Hai <?php echo $this->session->userdata('nama_user'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-body">
                <!-- <img src="<?php echo base_url() ?>assets/dist/img/avatar5.png" class="img-circle" alt="User Image"> -->
                <p>
                  <?php echo $this->session->userdata('nama_user').' - '. $this->session->userdata('id_user')?>
                  <!-- <small>Member since Nov. 2012</small> -->
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url() ?>prsh/Cform" class="btn btn-default btn-flat">Perusahaan</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url() ?>Main/logout" class="btn btn-default btn-flat">Log Out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url() ?>assets/dist/img/smile.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('id_user'); ?></p>
          <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
        </div>
      </div>
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN MENU</li>
        <li class="treeview">
        	<a href="#">
        		<i class="fa fa-home"></i> <span>Master</span>
        		<span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        	</a>
        	<ul class="treeview-menu">
        		<li class="treeview">
	              <a href="#"><i class="fa fa-circle-o text-aqua"></i>Pelanggan
	                <span class="pull-right-container">
	                  <i class="fa fa-angle-left pull-right"></i>
	                </span>
	              </a>
	              <ul class="treeview-menu">
	                <!-- <li><a href="<?php echo base_url() ?>pelanggan/C_pelanggan_grup"><i class="fa fa-circle-o text-green"></i> Group Pelanggan</a></li> -->
	                <li><a href="<?php echo base_url() ?>pelanggan/C_pelanggan"><i class="fa fa-circle-o text-green"></i> Pelanggan</a></li>
	                <!-- <li><a href="<?php echo base_url() ?>pelanggan/C_cabang"><i class="fa fa-circle-o text-green"></i> Cabang</a></li> -->
	              </ul>
	            </li>
				<li class="treeview">
	              <a href="#"><i class="fa fa-circle-o text-aqua"></i>Barang
	                <span class="pull-right-container">
	                  <i class="fa fa-angle-left pull-right"></i>
	                </span>
	              </a>
	              <ul class="treeview-menu">
	              	<li class="treeview">
		              <a href="#"><i class="fa fa-circle-o"></i>Atribut Barang
		                <span class="pull-right-container">
		                  <i class="fa fa-angle-left pull-right"></i>
		                </span>
		              </a>
		              <ul class="treeview-menu">
		              	<li><a href="<?php echo base_url() ?>warna/Cform"><i class="fa fa-circle-o text-green"></i> Master Warna</a></li>
                    <li><a href="<?php echo base_url() ?>satuan/Cform"><i class="fa fa-circle-o text-green"></i> Master Satuan</a></li>
		              </ul>
		          	</li>
	                <li><a href="<?php echo base_url() ?>barang/C_barang"><i class="fa fa-circle-o text-green"></i> Barang</a></li>
                  <?php if($this->session->userdata('tipe_perusahaan')=='1') {?>
	                <li><a href="<?php echo base_url() ?>barang/C_brgwarna"><i class="fa fa-circle-o text-green"></i> Barang+Warna</a></li>
                  <?php } ?>
	              </ul>
	            </li>
        	</ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Penjualan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url() ?>sjnota/Cform"><i class="fa fa-circle-o text-aqua"></i> SJ (Nota)</a></li>
            <li><a href="<?php echo base_url() ?>infosjnota/Cform"><i class="fa fa-circle-o text-aqua"></i> Laporan Penjualan</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Keuangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url() ?>pelunasan/Cform"><i class="fa fa-circle-o text-green"></i>Pelunasan</a></li>
            <li><a href="<?php echo base_url() ?>expopiutang/Cform"><i class="fa fa-circle-o text-green"></i>Laporan Piutang</a></li>
            <!-- <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Level One
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li> -->
          </ul>
        </li>
        <!-- <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $page_title; ?>
        <small><?php echo $litle_title; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>Main/layout"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $page_title; ?></li>
      </ol>
    </section>
    <?php echo $content; ?>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <!-- <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <!-- <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li> -->
      <!-- <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li> -->
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url() ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url() ?>assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url() ?>assets/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url() ?>assets/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url() ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url() ?>assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- <script src="<?php echo base_url() ?>assets/bower_components/chart.js/Chart.js"></script> -->
<!-- daterangepicker -->
<script src="<?php echo base_url() ?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url() ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url() ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url() ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() ?>assets/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- sweetalert2 -->
<script src="<?php echo base_url() ?>assets/sweetalert2.min.js"></script>
<!-- chart js -->
<script src="<?php echo base_url() ?>assets/chart.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script>
  $(function () {
    var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#tgl_awal').val(start.format("YYYY-MM-DD"));
        $('#tgl_akhir').val(end.format("YYYY-MM-DD"));
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        // manggil function   
        getDashboard();     
    }

      $('#daterange-btn').daterangepicker({
        startDate: start,
        endDate: end,
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }        
      }, cb)    

      cb(start, end)
  })

  function getDashboard(){
    var tgl_awal = $('#tgl_awal').val();
    var tgl_akhir = $('#tgl_akhir').val();
    var prod = $('#brg').val();
    $.ajax({
      url: "<?php echo base_url() ?>Main/dashboard/"+tgl_awal+"/"+tgl_akhir+"/"+prod, 
      method: "GET",            
      beforeSend: function(){        
      },
      success: function(respon){    
      
      }
    }).done(function(res){          
      var res = $.parseJSON(res)
      console.log(res)
      $('#nilai_penj').text('IDR '+addCommas(res.tot_penj))
      $('#nilai_piut').text('IDR '+addCommas(res.tot_piut))
      $('#tot_nota').text(addCommas(res.tot_nota))
      $('#tot_qty').text(addCommas(res.tot_qty))    
      showLineChart(res.tot_penj_per_item, tgl_awal, tgl_akhir)
    })
  }

  function showLineChart(data, tgl_awal, tgl_akhir) { 
   var dataChart = data; 
   var jmldataChart = data.length; 
   if (jmldataChart > 0 ) { 
    var labelData = [];     
    // change label data to uppercase 
    dataChart.forEach(function(item, idx) { 
      labelData.push(item.d_nota); 
    }); 
    console.log(labelData)
    // var maxValue = data.maxValueLineChart; 

    var data_color = [];
    for(var i=0; i<jmldataChart; i++){
      data_color.push('#'+(Math.random()*0xFFFFFF<<0).toString(16));
    } 
    var idx=0;
    var data = { 
     labels: labelData, 
     datasets: [] 
    }; 
    var nilaipenj = [];
    var labeldataset = '';
    dataChart.forEach(function (item, idx) {       
      labeldataset = item.e_nama_brg;
      nilaipenj.push(item.tot_qty)
    });
     data.datasets.push({ 
       label: labeldataset, 
       data: nilaipenj, 
       fill: false, 
       lineTension: 0.1, 
       // backgroundColor: "rgba(225,0,0,0.4)", 
       backgroundColor: data_color[idx], 
       borderColor: data_color[idx], // The main line color 
       borderCapStyle: 'square', 
       borderDash: [], // try [5, 15] for instance 
       borderDashOffset: 0.0, 
       borderJoinStyle: 'miter', 
       pointBorderColor: "black", 
       pointBackgroundColor: "white", 
       pointBorderWidth: 1, 
       pointHoverRadius: 8, 
       pointHoverBackgroundColor: data_color[idx], 
       pointHoverBorderColor: "brown", 
       pointHoverBorderWidth: 2, 
       pointRadius: 4, 
       pointHitRadius: 10, 
       fontColor: 'black',
       // notice the gap in the data and the spanGaps: true        
       spanGaps: true, 
     });     
// Notice the scaleLabel at the same level as Ticks 
    var options = { 
     responsive: true, 
      title: { 
       display: true, 
       text: 'PENJUALAN PER ITEM RANGE TANGGAL ' + tgl_awal + ' s/d ' + tgl_akhir,
       fontColor: 'black',
       fontSize: 30,
      }, 
      scales: { 
         xAxes: [{ 
          ticks: {
            fontColor: 'black'
          },
          gridLines: { 
           display: true,
           fontColor: 'black', 
          }, 
          scaleLabel: {             
           display: true, 
           labelString: 'Hari', 
           fontSize: 15,
           fontColor: 'black'            
          } 
         }],             
         yAxes: [{ 
          ticks: { 
            min: 0, 
            beginAtZero: true,
            callback: function(value, index, values) {
              if(parseInt(value) >= 1000){
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              } else {
                return value;
              }
            },
            fontColor: 'black',
           // max: maxValue 
          }, 
          scaleLabel: { 
           display: true, 
           labelString: 'Jumlah Nilai', 
           fontSize: 15,
           fontColor: 'black'            
          }          
         }]             
        },
        tooltips: {            
            callbacks: {
                label: function(t, d) {
                    var xLabel = d.datasets[t.datasetIndex].label;
                    var yLabel = t.yLabel >= 1000 ? t.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : +t.yLabel;
                    return xLabel + ': ' + yLabel;
                }
            }
        }  
     }; 
 
    // Chart declaration: 
    $("#containter-chart").html(`<canvas id="content-chart" height="100"> </canvas>`); 
    var objLine = document.getElementById("content-chart"); 
    var myLineChart = new Chart(objLine, { 
     type: 'line', 
     data: data, 
     options: options 
    }); 
 
   } else { 
    var elm = `<div class="tab-pane active" id="m_widget5_tab1_content" aria-expanded="true"> 
        <div class="m-widget5"> 
         <div class="m-widget5__item" style="margin: 0;padding: 0;font-size: 16px;text-align: center;"> 
          Penjualan Per Item Range Tanggal ${tgl_awal} s.d ${tgl_akhir} Tidak Tersedia 
         </div> 
        </div> 
       </div>`; 
    $("#content-chart").html(""); 
    $("#containter-chart").html(elm); 
    // toastr.info('Data pada tahun ' + filter + ' tidak tersedia'); 
   } 
  }

  function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
  }

  $(function () {
  	$('#datatabel').DataTable({
  		"scrollX": true
  	})
  })

  $(function () {
  	$('.select2').select2()

    $('#datepicker').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        todayHighlight: true
      })

    $('#datepicker2').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        todayHighlight: true
      })
  })
  function hapus(newUrl)
  {
  	swal({
  	  title: 'Kamu Yakin?',
  	  text: 'Hati-hati data ini ga akan hilang permanent!',
  	  type: 'warning',
  	  showCancelButton: true,
  	  confirmButtonColor: '#3085d6',
  	  cancelButtonColor: '#d33',
  	  confirmButtonText: 'Ya, Hapus aja!',
  	  cancelButtonText: 'Tidak, Jangan pls!',
  	  confirmButtonClass: 'confirm-class',
  	  cancelButtonClass: 'cancel-class',
  	  closeOnConfirm: false,
  	  closeOnCancel: false
  	},
  	function(isConfirm) {
  	  if (isConfirm) {
  	    location.href=newUrl; 
  	    swal(
  	      'Terhapus',
  	      'Data tadi sudah hilang secara permanent :)',
  	      'success'
  	    );
  	  } else {
  	    swal(
  	      'Terbatalkan',
  	      'Data ini aman :)',
  	      'error'
  	    );
  	    return false;
  	  }
  	});
  }

  // javascript untuk format uang saat mengganti value
  function formatuang(uang)
  {
    // format money co: 2,300,500.22
      var   bilangan = uang;
      var number_string = bilangan.toString(),
        split = number_string.split('.'),
        sisa  = split[0].length % 3,
        rupiah  = split[0].substr(0, sisa),
        ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);
          
      if (ribuan) {
        separator = sisa ? ',' : '';
        rupiah += separator + ribuan.join(',');
      }
      rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;

      return rupiah;
  }

  // javascript untuk format uang saat mengetik
  function formatuang2(uang)
  {
    // format money co: 2,300,500.22
      var   bilangan = uang;
      var number_string = bilangan.replace(/[^.\d]/g, '').toString(),
        split = number_string.split('.'),
        sisa  = split[0].length % 3,
        rupiah  = split[0].substr(0, sisa),
        ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);
          
      if (ribuan) {
        separator = sisa ? ',' : '';
        rupiah += separator + ribuan.join(',');
      }
      rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;

      return rupiah;
  }

  /*  end umum  */
  function formatulang(a){
    var s = a.replace(/\,/g,'');
    return s;
  }

  function hanyaangka()
  {
    key = event.which || event.keyCode;
    if (  
       key != 190 // Comma
       && key != 8 // Backspace
       && key != 16
       && key != 110
       // && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
       && (key < 48 || key > 57) // Non digit
       && (key < 96 || key > 105)
       // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
      ) 
    {
      event.preventDefault();
      return;
    }
  }

  function spasitidak()
	{
		key = event.which || event.keyCode;
	    if(key===32)
	    {
	    	event.preventDefault();
	    	return;
	    }
	}

  $(document).ready(function(){
    // Setup datatables
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

      var table = $("#myTable").dataTable({
          initComplete: function() {
              var api = this.api();
              $('#myTable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'pelunasan/Cform/ambil_json_voucher'?>", "type": "POST"},
                  columns: [
                        {"data": null, "searchable": false},
                        {"data": "i_no_voucher"},
                        {"data": "d_voucher"},
                        //render nilai bayar dengan format angka
                        {"data": "v_pelunasan", render: $.fn.dataTable.render.number(',', '.', '')},
                        {"data": "e_keterangan"},
                        {"data": "f_pelunasan_cancel", "searchable": false,
                            "render": function (data, type, row) {                 
                              if (data == '0') {
                                return '<p style="color:green">Aman</p>';
                              } else {
                                return '<p style="color:red">Batal</p>';
                              }
                            }
                        },
                        {"data": "view",
                          render: function (data, type, row) {
                            var tmp = data.split('<a href="#"');
                            if (row.f_pelunasan_cancel == 0) {
                              return tmp[0]+'<a href="#"'+tmp[1];
                            } else {
                              return tmp[0];
                            }
                          }
                        }
                  ],
              order: [[2, 'desc']],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              var index = page * length + (iDisplayIndex + 1);
              $('td:eq(0)', row).html(index);
          }

      });
      // end setup datatablesW
      // view Detail Records
      $('#myTable').on('click','.detailbtn',function(){
            var id=$(this).data('id');
            var no=$(this).data('no');
            var url = '<?php echo base_url() ?>pelunasan/Cform/listvoucher/'+id+'/'+no;
            $('.modal-content').load(url,function(){
              $('#myModal').modal({show:true});
            });
      });
      // End view Detail Records
      // view Detail Records
      $('#myTable').on('click','.hapus_record',function(){
            var id=$(this).data('id');
            var no=$(this).data('no');
            var url = '<?php echo base_url() ?>pelunasan/Cform/batalvoucher/'+id+'/'+no;
            return hapus(url);
      });
      // End view Detail Records
  });
</script>
</body>
</html>
