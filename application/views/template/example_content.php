<!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3>Dashboard</h3>
          </div>
          <div class="box-body">
            <center><h1>Selamat Datang di program <?php echo $this->session->userdata('nama_perusahaan'); ?></h1></center>
            <div class="form-group">
              <label>Filter Tanggal:</label>

              <div class="input-group">
                <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                  <span>
                    <i class="fa fa-calendar"></i> Pilih Tanggal
                  </span>
                  <i class="fa fa-caret-down"></i>
                </button>
                <input type="hidden" id="tgl_awal" name="tgl_awal"/>
                <input type="hidden" id="tgl_akhir" name="tgl_akhir"/>
              </div>
            </div>
            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <!-- <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span> -->
                <span class="info-box-icon bg-green"><i class="fa fa-dollar"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Nilai Penjualan</span>
                  <span class="info-box-number" id="nilai_penj">760</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-dollar"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Nilai Piutang</span>
                  <span class="info-box-number" id="nilai_piut">760</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Total Nota</span>
                  <span class="info-box-number" id="tot_nota">760</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Total Qty</span>
                  <span class="info-box-number" id="tot_qty">760</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Total Penjualan Per Item</h3>  
                  <br>
                  <select class="select2 form-control" name="brg" id="brg" onchange="getDashboard();">
                    <option value="">Pilih Barang</option>
                    <?php    
                    if(!empty($product)) {                   
                      foreach($product as $prod){
                        echo '<option value="'.$prod->i_kode_brg.'">'.$prod->i_kode_brg.'-'.$prod->e_nama_brg.'</option>';
                      }              
                    }  else {
                      echo "<option value=\"\">Maaf Tidak Ada Product!</option>";
                    }     
                    ?>
                  </select>                
                </div>
                <div class="box-body" id="containter-chart">
                  <canvas id="content-chart" height="100"></canvas> 
                </div>
                <!-- /.box-body -->
              </div>
            </div>
          </div>
        </div>
      </div>  
      <!-- /.row -->
    </section>
    <!-- /.content -->