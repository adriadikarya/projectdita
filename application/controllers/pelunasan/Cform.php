<?php
/**
 * 
 */
class Cform extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('pelunasan/Mmaster');
		$this->load->library('datatables');
	}

	function index()
	{
		if($this->session->userdata('loggedin'))
		{
			$data['page_title'] = "Pelunasan Nota";
			$data['litle_title'] = "Pelunasan Nota";
			$datalay['content'] = $this->load->view('pelunasan/vformview',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function ambil_json_voucher() { //data data produk by JSON object
    	header('Content-Type: application/json');
    	$kodeprsh = $this->session->userdata('kode_perusahaan');
    	echo $this->Mmaster->ambilVoucher($kodeprsh);
  	}

	function tambah()
	{
		if($this->session->userdata('loggedin'))
		{
			$data['page_title'] = "Pelunasan";
			$data['litle_title'] = "Pelunasan";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$data['pel'] = $this->Mmaster->daftarpel($kodeprsh)->result();
			$datalay['content'] = $this->load->view('pelunasan/vmainform',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function listnota()
	{
		if($this->session->userdata('loggedin'))
		{
			$pelanggan = $this->uri->segment(4);
			$data_modal['pelanggan'] = $this->uri->segment(4);
			$data_modal['baris'] = $this->uri->segment(5);
			$kodeprsh = $this->session->userdata("kode_perusahaan");
			$data_modal['isi'] = $this->Mmaster->daftarnota($kodeprsh,$pelanggan)->result();
			$data_modal['title_modal'] = "List Nota";
			$this->load->view("pelunasan/vlistnota",$data_modal);
		} else {
			redirect('Main');
		}
	}

	function listvoucher()
	{
		if($this->session->userdata('loggedin'))
		{
			$voc = $this->uri->segment(4);
			$no  = $this->uri->segment(5);
			$data_modal['voucher'] = $this->uri->segment(5);
			$kodeprsh = $this->session->userdata("kode_perusahaan");
			$data_modal['isi'] = $this->Mmaster->daftarvoucher($kodeprsh,$voc)->result();
			$data_modal['title_modal'] = "List";
			$this->load->view("pelunasan/vlistdet",$data_modal);
		} else {
			redirect('Main');
		}
	}

	function simpan()
	{
		if($this->session->userdata('loggedin'))
		{
			$tmp = explode('/', $this->input->post('dbayar'));
			$dbayar = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
			$qid = $this->db->query("select i_id_pelunasan as idpelunasan from tm_pelunasan order by i_id_pelunasan desc");
			if($qid->num_rows()==0)
			{
				$id=1;
			} else {
				$rid = $qid->row();
				$id=$rid->idpelunasan+1;
			}
			$data['i_id_pelunasan'] = $id;
			$data['i_no_voucher'] = $this->input->post('novoc',TRUE);
			$data['d_voucher']	  = $dbayar;
			$data['v_pelunasan']  = str_replace(',','',$this->input->post('jmlbayar',TRUE));
			$data['e_keterangan'] = $this->input->post('ket',TRUE);
			$data['i_kode_perusahaan'] = $this->session->userdata('kode_perusahaan');
			$this->db->trans_begin();
			$insert = $this->Mmaster->insertdata($data);

			$totrow = $this->input->post('totrow',TRUE);

			for($i=1; $i<=$totrow; $i++)
			{
				$data2['i_id_pelunasan'] = $id;
				$data2['i_id_nota']		 = $this->input->post('inota'.$i,TRUE);
				$data2['i_no_nota']		 = $this->input->post('inotacode'.$i,TRUE);
				$data2['v_bayar_nota']	 = str_replace(',','',$this->input->post('vbayar'.$i,TRUE));

				$this->Mmaster->insertdetail($data2);
				$this->Mmaster->updatenota($data2['i_id_nota'],$data2['v_bayar_nota']);
			}
			if($this->db->trans_status()===FALSE){
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
			// cek jika query berhasil
				if ($insert) {
				   	// Set success message
					$message = array('status' => true, 'message' => 'Berhasil Melakukan Pelunasan dengan no voucher: '.$data['i_no_voucher']);
				} else {
					// Set error message
					$message = array('status' => false, 'message' => 'Gagal Melakukan Pelunasan dengan no voucher: '.$data['i_no_voucher']);
				}

				// simpan message sebagai session
			    $this->session->set_flashdata('message', $message);
			    // refresh page
			    redirect('pelunasan/Cform');
		} else {
			redirect('Main');
		}
	}

	function batalvoucher()
	{
		if($this->session->userdata('loggedin'))
		{	
			$idpelunasan = $this->uri->segment(4);
			$nopelunasan = $this->uri->segment(5);
			$this->db->trans_begin();
			$qdelete = $this->Mmaster->canceldata($idpelunasan);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
			// cek jika query berhasil
			if ($qdelete) {
				// Set success message
			    $message = array('status' => true, 'message' => 'Berhasil Cancel Data Pelunasan '.$nopelunasan);
			} else {
				// Set error message
			    $message = array('status' => false, 'message' => 'Gagal Cancel Data Pelunasan '.$nopelunasan);
			}
			// simpan message sebagai session
			$this->session->set_flashdata('message', $message);
			// refresh page
			redirect('pelunasan/Cform');
		} else {
			redirect('Main');
		}
	}
}