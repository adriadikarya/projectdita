<?php 

/**
 * 
 */
class C_brgwarna extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('barang/M_brgwarna');
	}

	function index()
	{
		if($this->session->userdata('loggedin'))
		{
			if($this->input->post("kodebrg")!='')
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$data['i_kode_brg'] = $this->input->post("kodebrg",TRUE);
				$data['i_id_warna'] = $this->input->post("warna",TRUE);
				$data['i_kode_perusahaan'] = $this->session->userdata('kode_perusahaan');
				$data['d_input']	= $now;

				$this->db->trans_begin();

				$insert = $this->M_brgwarna->insertdata($data);

				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
				// cek jika query berhasil
				if ($insert) {
				   	// Set success message
					$message = array('status' => true, 'message' => 'Berhasil Menambah Data Barang + Warna, Kode ('.$data['i_kode_brg'].')');
				} else {
					// Set error message
					$message = array('status' => false, 'message' => 'Gagal Menambah Data Barang + Warna, Kode ('.$data['i_kode_brg'].')');
				}

				// simpan message sebagai session
			    $this->session->set_flashdata('message', $message);
			    // refresh page
			    redirect('barang/C_brgwarna');

			}
			$data['page_title'] = "List Warna Barang";
			$data['litle_title'] = "List & Form Input";
			$ikodeprsh = $this->session->userdata('kode_perusahaan');
			$data['isi'] = $this->M_brgwarna->daftarbrgwarna($ikodeprsh)->result();
			$data['brg'] = $this->M_brgwarna->daftarbrg($ikodeprsh)->result();
			$data['warna'] = $this->M_brgwarna->daftarwarna()->result();
			$datalay['content'] = $this->load->view('barang/vmainform_warna',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

}