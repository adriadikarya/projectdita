<?php 

/**
 * 
 */
class Cform extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('prsh/Mmaster');
	}

	function index()
	{
		if($this->session->userdata('loggedin'))
		{
			$data['page_title'] = "Perusahaan";
			$data['litle_title'] = "Form Edit Perusahaan";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$data['isi']   = $this->Mmaster->dataperusahaan($kodeprsh)->row();
			$datalay['content'] = $this->load->view('prsh/vmainform',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function simpan()
	{
		if($this->session->userdata('loggedin'))
		{
			$kodeprsh = $this->input->post('kodeperusahaan',TRUE);
			$idprsh = $this->input->post('idperusahaan',TRUE);
			$data['i_tipe_perusahaan'] = $this->input->post('tipeprsh',TRUE);
			$data['e_nama_perusahaan'] = $this->input->post('nmprsh',TRUE);
			$data['e_alamat']		   = $this->input->post('alamat',TRUE);
			$data['e_telepon']		   = $this->input->post('telp',TRUE);
			$data['e_fax']			   = $this->input->post('fax',TRUE);
			$data['e_kontak']		   = $this->input->post('kontak',TRUE);
			$data['e_no_rek1']		   = $this->input->post('norek1',TRUE);
			$data['e_nama_bank1']      = $this->input->post('bank1',TRUE);
			$data['e_atas_nama1']	   = $this->input->post('atasnama1',TRUE);
			$data['e_no_rek2']		   = $this->input->post('norek2',TRUE);
			$data['e_nama_bank2']      = $this->input->post('bank2',TRUE);
			$data['e_atas_nama2']	   = $this->input->post('atasnama2',TRUE);

			$this->db->trans_begin();
			$query = $this->Mmaster->update($data,$kodeprsh,$idprsh);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}

			// cek jika query berhasil
			if ($query) {
				   	// Set success message
				$message = array('status' => true, 'message' => 'Berhasil Update Silahkan Logout');
			} else {
					// Set error message
				$message = array('status' => false, 'message' => 'Gagal Update');
			}
			// simpan message sebagai session
			$this->session->set_flashdata('message', $message);
			redirect('prsh/Cform');
		} else {
			redirect('Main');
		}
	}
}