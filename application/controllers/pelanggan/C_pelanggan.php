<?php 

/**
 * 
 */
class C_Pelanggan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('pelanggan/M_pelanggan');
	}

	function index()
	{
		if($this->session->userdata('loggedin')){
			if($this->input->post("kodepel")!='' && $this->input->post("namapel") != '')
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$data['i_kode_pelanggan']   = $this->input->post("kodepel",TRUE);
				$data['e_nama_pelanggan']   = $this->input->post("namapel",TRUE);
				$data['e_alamat_pelanggan'] = $this->input->post("alamat",TRUE);
				$data['f_pelanggan_pkp']    = $this->input->post("pkp",TRUE)=='y'?1:0;
				$data['e_npwp']				= $this->input->post("npwp",TRUE);
				$data['n_top']				= $this->input->post("ntop",TRUE);
				$data['e_telp_pelanggan']	= $this->input->post("telp",TRUE);
				$data['e_fax_pelanggan']	= $this->input->post("fax",TRUE);
				$data['e_kontak_pelanggan']	= $this->input->post("kontak",TRUE);
				$data['i_id_grup']			= $this->input->post("gruppel",TRUE);
				$data['e_ktp_pelanggan']	= $this->input->post("ktp",TRUE);
				$data['n_ekspedisi_pelanggan'] = $this->input->post("ekspedisi",TRUE);
				$data['i_kode_perusahaan']  = $this->session->userdata('kode_perusahaan');
				$data['d_input']			= $now;

				$this->db->trans_begin();
				$insert = $this->M_pelanggan->insertdata($data);

				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
				// cek jika query berhasil
			    if ($insert) {
			    	// Set success message
			    	$message = array('status' => true, 'message' => 'Berhasil Menambah Data Pelanggan '.$data['i_kode_pelanggan']);
				} else {
			    	// Set error message
			    	$message = array('status' => false, 'message' => 'Gagal Menambah Data Pelanggan '.$data['i_kode_pelanggan']);
			    }
			    // simpan message sebagai session
			    $this->session->set_flashdata('message', $message);
			    // refresh page
			    redirect('pelanggan/C_pelanggan');
			}

			$data['page_title'] = "Pelanggan";
			$data['litle_title'] = "Form Input";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$data['pel']  = $this->M_pelanggan->daftarpel($kodeprsh)->result();
			$data['grup'] = $this->M_pelanggan->daftargrup($kodeprsh)->result();
			$data['prsh'] = $this->M_pelanggan->daftarperusahaan()->result();
			$datalay['content'] = $this->load->view('pelanggan/vmainform',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function edit()
	{
		if($this->session->userdata('loggedin')){
			if($this->input->post("kodepel")!='' && $this->input->post("namapel") != '')
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$idpelanggan 				= $this->input->post("idpel",TRUE);
				$data['i_kode_pelanggan']   = $this->input->post("kodepel",TRUE);
				$data['e_nama_pelanggan']   = $this->input->post("namapel",TRUE);
				$data['e_alamat_pelanggan'] = $this->input->post("alamat",TRUE);
				$data['f_pelanggan_pkp']    = $this->input->post("pkp",TRUE)=='y'?1:0;
				$data['e_npwp']				= $this->input->post("npwp",TRUE);
				$data['n_top']				= $this->input->post("ntop",TRUE);
				$data['e_telp_pelanggan']	= $this->input->post("telp",TRUE);
				$data['e_fax_pelanggan']	= $this->input->post("fax",TRUE);
				$data['e_kontak_pelanggan']	= $this->input->post("kontak",TRUE);
				$data['i_id_grup']			= $this->input->post("gruppel",TRUE);
				$data['e_ktp_pelanggan']	= $this->input->post("ktp",TRUE);
				$data['n_ekspedisi_pelanggan'] = $this->input->post("ekspedisi",TRUE);
				$data['i_kode_perusahaan']  = $this->session->userdata('kode_perusahaan');
				$data['d_update']			= $now;

				$this->db->trans_begin();
				$insert = $this->M_pelanggan->updatedata($data,$idpelanggan);

				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
				// cek jika query berhasil
			    if ($insert) {
			    	// Set success message
			    	$message = array('status' => true, 'message' => 'Berhasil Mengubah Data Pelanggan '.$data['i_kode_pelanggan']);
				} else {
			    	// Set error message
			    	$message = array('status' => false, 'message' => 'Gagal Mengubah Data Pelanggan '.$data['i_kode_pelanggan']);
			    }
			    // simpan message sebagai session
			    $this->session->set_flashdata('message', $message);
			    // refresh page
			    redirect('pelanggan/C_pelanggan');
			}

			$ipelanggan = $this->uri->segment(4);
			$data['page_title'] = "Pelanggan";
			$data['litle_title'] = "Form Edit";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$pel  = $this->M_pelanggan->caripel($ipelanggan,$kodeprsh);
			if($pel->num_rows()>0)
			{
				$rpel = $pel->row();
				$data['idpel'] = $rpel->i_pelanggan;
				$data['kodepel'] = $rpel->i_kode_pelanggan;
				$data['namapel'] = $rpel->e_nama_pelanggan;
				$data['npwp'] = $rpel->e_npwp;
				$data['ntop'] = $rpel->n_top;
				$data['telp'] = $rpel->e_telp_pelanggan;
				$data['fax']  = $rpel->e_fax_pelanggan;
				$data['kontak'] = $rpel->e_kontak_pelanggan;
				$data['ktp'] = $rpel->e_ktp_pelanggan;
				$data['ekspedisi'] = $rpel->n_ekspedisi_pelanggan;
				$data['gruppel'] = $rpel->i_id_grup;
				$data['pkp'] = $rpel->f_pelanggan_pkp;
				$data['alamat'] = $rpel->e_alamat_pelanggan;
			} else {
				$data['idpel'] = '';
				$data['kodepel'] = '';
				$data['namapel'] = '';
				$data['npwp'] = '';
				$data['ntop'] = '';
				$data['telp'] = '';
				$data['fax']  = '';
				$data['kontak'] = '';
				$data['ktp'] = '';
				$data['ekspedisi'] = '';
				$data['gruppel'] = '';
				$data['pkp'] = '';
				$data['alamat'] = '';
			}

			$data['grup'] = $this->M_pelanggan->daftargrup($kodeprsh)->result();
			$data['prsh'] = $this->M_pelanggan->daftarperusahaan()->result();
			$datalay['content'] = $this->load->view('pelanggan/vformedit',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function delete()
	{
		if($this->session->userdata('loggedin')){
			$idpel = $this->uri->segment(4);
			$ikode = $this->uri->segment(5);
			$this->db->trans_begin();
			$qdelete = $this->M_pelanggan->deletedata($idpel);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
			// cek jika query berhasil
			if ($qdelete) {
				// Set success message
			    $message = array('status' => true, 'message' => 'Berhasil Menghapus Data Pelanggan '.$ikode);
			} else {
				// Set error message
			    $message = array('status' => false, 'message' => 'Gagal Menghapus Data Pelanggan '.$ikode);
			}
			// simpan message sebagai session
			$this->session->set_flashdata('message', $message);
			// refresh page
			redirect('pelanggan/C_pelanggan');
		} else {
			redirect('Main');
		}
	}
}