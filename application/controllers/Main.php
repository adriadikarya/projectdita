<?php

/**
 * 
 */
class Main extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Mainmodel');
	}
	// function index()
	// {
	// 	$data['isi'] = $this->Mainmodel->daftarperusahaan();
	//     $this->load->view('login',$data);
	// }

	public function index()
	{
		if($this->session->userdata('loggedin'))
        {
        	//jika memang session sudah terdaftar, maka redirect ke halaman dahsboard
            redirect("Main/layout");
		}else{
        	//jika session belum terdaftar
			// if($this->input->post("submit")){
				//set form validation
	            $this->form_validation->set_rules('iduser', 'ID User', 'required');
	            $this->form_validation->set_rules('password', 'Password', 'required');
	            // $this->form_validation->set_rules('perusahaan','Perusahaan','required');

	            //set message form validation
	            $this->form_validation->set_message('required', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i>{field}</b> harus diisi</div>');

	                //cek validasi
				if ($this->form_validation->run() == TRUE) {

	                //get data dari FORM
	                $iduser = $this->input->post("iduser", TRUE);
	                $password = MD5($this->input->post('password', TRUE));
	                // $perusahaan = $this->input->post("perusahaan", TRUE);

	                //checking data via model
	                $checking = $this->Mainmodel->check_login('tm_user a', array('i_nama_user' => $iduser), array('i_password' => $password));

	                //jika ditemukan, maka create session
	                if ($checking != FALSE) {
	                    foreach ($checking as $row) {

	                        $session_data = array(
	                            'id_user'   		=> $row->i_nama_user,
	                            'nama_user' 		=> $row->e_nama_user,
	                          	'kode_perusahaan' 	=> $row->i_kode_perusahaan, 
	                          	'level' 			=> $row->i_level,
	                          	'nama_perusahaan'	=> $row->e_nama_perusahaan,
	                          	'tipe_perusahaan'	=> $row->i_tipe_perusahaan,
	                          	'loggedin'			=> true,
	                        );
	                        //set session userdata
	                        $this->session->set_userdata($session_data);

	                        redirect('Main/layout');

	                    }
	                }else{
	                    $data['error'] = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>ID user dan Password</b> Salah</div>';
	                    $data['isi'] = $this->Mainmodel->daftarperusahaan();
	                    $this->load->view('login', $data);
	                }
	            }else{
	            	$data['isi'] = $this->Mainmodel->daftarperusahaan();
	                $this->load->view('login', $data);
	            }
	        // }
        }
	}

	function register()
	{
		if($this->input->post("submit")){
			//set form validation
	        $this->form_validation->set_rules('iduserreg', 'ID User', 'required');
	        $this->form_validation->set_rules('namauserreg', 'Nama User', 'required');
			$this->form_validation->set_rules('passwordreg', 'Password', 'required');
			$this->form_validation->set_rules('perusahaanreg','Perusahaan','required|callback_check_default');

			//set message form validation
	        $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px"><div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');

	        if ($this->form_validation->run() == TRUE) {
	        	// get data dari FORM
		        $iduser = $this->input->post("iduserreg", TRUE);
		        $namauser = $this->input->post("namauserreg", TRUE);
		        $password = MD5($this->input->post('passwordreg', TRUE));
		        $perusahaan = $this->input->post("perusahaanreg", TRUE);
		        $namaperusahaan = $this->input->post("namaprshreg",TRUE);
		        $level = $this->input->post("levelreg", TRUE);

		        //checking data via model
		        $checking = $this->Mainmodel->check_login('tm_user', array('i_nama_user' => $iduser), array('i_password' => $password));

		        if ($checking == FALSE) {
		        	$qdentry = $this->db->query("select now() as now");
		        	$rdentry = $qdentry->row();
		        	$now  = $rdentry->now;

		        	$this->Mainmodel->inputuser($iduser,$namauser,$password,$perusahaan,$level,$now);

		            $session_data = array(
		                'id_user'   		=> $iduser,
		                'nama_user' 		=> $namauser,
		             	'kode_perusahaan' 	=> $perusahaan, 
		             	'nama_perusahaan'   => $namaperusahaan,
		             	'level' 			=> $level, 
		             	'loggedin'			=> true,
		            );
		            //set session userdata
		            $this->session->set_userdata($session_data);
		            redirect('dashboard/');
		        }else{
		            $data['error'] = '<div class="alert alert-danger" style="margin-top: 3px">
		                        <div class="header"><b><i class="fa fa-exclamation-circle"></i> ERROR</b> IDuser Sudah Ada!</div></div>';
		            $data['isi'] = $this->Mainmodel->daftarperusahaan();
		        	$this->load->view('login', $data);
		        }
			}else{
		    	$data['isi'] = $this->Mainmodel->daftarperusahaan();
		        $this->load->view('login', $data);
		    }
		}

		$this->load->view("register");
	}

	function logout()
    {
        $this->session->sess_destroy();
        redirect('Main');
    }

    public function layout()
    {
    	$data['page_title'] = "Dashboard";
		$data['litle_title'] = "Control Panel";
		$kodeprsh = $this->session->userdata('kode_perusahaan');
		$product = $this->db->query("SELECT * FROM tr_barang WHERE i_kode_perusahaan='".$kodeprsh."'");
		$data['product'] = $product->result();		
    	$data['content'] = $this->load->view('template/example_content',$data,TRUE);
    	$this->load->view("template/layout",$data);
    }

    public function check_default($abc)
	{
	  if($abc == '')
	  { 
	  	//set message form validation
        $this->form_validation->set_message('check_default', '<div class="alert alert-danger" style="margin-top: 3px"><div class="header"><b><i class="fa fa-exclamation-circle"></i> Perusahaan</b> harus diisi</div></div>');
	    return FALSE;
	  } else {
	  	return TRUE;	
	  }
	}

	function carinamaprsh()
	{
		$kodepr = $this->input->post("kodepr",TRUE);
		$query = $this->db->query("select * from tr_perusahaan where i_kode='$kodepr'");
		if($query->num_rows()>0)
		{
			$row = $query->row();
			$namaprsh = "<input type=\"hidden\" name=\"namaprshreg\" value=\"".$row->e_nama_perusahaan."\">";
		} else {
			$namaprsh = "";
		}

		echo $namaprsh;
	}

	function dashboard()
	{
		$tgl_awal = $this->uri->segment(3);
		$tgl_akhir = $this->uri->segment(4);
		$brg = $this->uri->segment(5);
		$kodeprsh = $this->session->userdata('kode_perusahaan');
		$qtot = $this->db->query("SELECT SUM(x.total_penjualan) AS total_penjualan, SUM(x.total_piutang) AS total_piutang,
					SUM(x.tot_nota) AS tot_nota, SUM(x.tot_qty) AS tot_qty
				FROM (
				SELECT coalesce(SUM(v_total),0) AS total_penjualan, COALESCE(SUM(v_total_sisa),0) AS total_piutang, 
					COUNT(tn.i_id_nota) AS tot_nota, 0 AS tot_qty
				FROM tm_nota tn
				WHERE d_nota>='".$tgl_awal."' AND d_nota<='".$tgl_akhir."' AND tn.i_kode_perusahaan='".$kodeprsh."'
				UNION 
				SELECT 0 AS total_penjualan, 0 AS total_piutang, 0 AS tot_nota, SUM(n_qty) AS tot_qty
				FROM tm_nota_item tni
				JOIN tm_nota tn ON tni.i_id_nota=tn.i_id_nota
				WHERE d_nota>='".$tgl_awal."' AND d_nota<='".$tgl_akhir."' AND tn.i_kode_perusahaan='".$kodeprsh."') AS x");
		$qtotrow = $qtot->row();
		$data['tot_penj'] = $qtotrow->total_penjualan;
		$data['tot_piut'] = $qtotrow->total_piutang;
		$data['tot_nota'] = $qtotrow->tot_nota;
		$data['tot_qty']  = $qtotrow->tot_qty;

		$qrepeat = $this->db->query("SELECT tni.i_kode_brg, e_nama_brg, COUNT(tni.i_kode_brg) AS tot_repeat, coalesce(SUM(n_qty),0) AS tot_qty, 
		COALESCE((SUM(n_qty)/COUNT(tni.i_kode_brg)),0) AS rata2 
		FROM tm_nota_item tni 
		JOIN tm_nota tn ON tn.i_id_nota=tni.i_id_nota 
		JOIN tr_barang tb ON tb.i_kode_brg=tni.i_kode_brg
		WHERE d_nota>='".$tgl_awal."' AND d_nota<='".$tgl_akhir."' AND tn.i_kode_perusahaan='".$kodeprsh."'
		GROUP BY tni.i_kode_brg, e_nama_brg HAVING COUNT(tni.i_kode_brg) > 1");		
		if($qrepeat->num_rows()>0){
			foreach($qrepeat->result() as $row){								
				$tmp_nm_brg[] = $row->e_nama_brg;
				$tmp_tot_repeat[] = $row->tot_repeat;
				$tmp_tot_qty[] = $row->tot_qty;
				$tmp_rata2[] = $row->rata2; 
			}
		} else {			
			$tmp_nm_brg[] = "-";
			$tmp_tot_repeat[] = 0;
			$tmp_tot_qty[] = 0;
			$tmp_rata2[] = 0;
		}

		$data['nm_brg'] = $tmp_nm_brg;
		$data['tot_rep'] = $tmp_tot_repeat;
		$data['tot_qty_euy'] = $tmp_tot_qty;
		$data['rata_rata'] = $tmp_rata2;

		$totPenjPerItem = $this->db->query("SELECT e_nama_brg, COALESCE(ROUND((SUM(n_qty*v_harga_satuan)-(SUM(n_qty*v_harga_satuan)*(n_diskon/100)))),0) AS tot_qty, tn.d_nota
		FROM tm_nota_item tni
		JOIN tm_nota tn ON tn.i_id_nota=tni.i_id_nota 
		JOIN tr_barang tb ON tb.i_kode_brg=tni.i_kode_brg
		WHERE d_nota>='".$tgl_awal."' AND d_nota<='".$tgl_akhir."' AND tn.i_kode_perusahaan='".$kodeprsh."' AND tni.i_kode_brg='".$brg."'
		GROUP BY e_nama_brg, tn.d_nota
		ORDER BY tn.d_nota");

		$data['tot_penj_per_item'] = $totPenjPerItem->result();

		echo json_encode($data);
	}
}