<?php

/**
 * 
 */
class Cform extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory','fungsi'));
		$this->load->model('expopiutang/Mmaster');
	}

	function index()
	{
		if($this->session->userdata('loggedin'))
		{
			$data['page_title'] = "Laporan Piutang";
			$data['litle_title'] = "Laporan Piutang";
			$datalay['content'] = $this->load->view('expopiutang/vmainform',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function export()
	{
		if($this->session->userdata('loggedin'))
		{
			$tgljthtempo = $this->uri->segment(4);
			$tmp=explode("-",$tgljthtempo);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$jd1 = GregorianToJD($bl, $hr, $th);
			
			setlocale(LC_ALL, 'IND');
			$tgltop = strftime('%d %B %Y',strtotime($tgljthtempo));

			$kodeprsh = $this->session->userdata('kode_perusahaan');

			$query = $this->db->query("select e_nama_perusahaan from tr_perusahaan where i_kode_perusahaan='$kodeprsh'");
			if($query->num_rows()==0)
			{
				$nama_perusahaan = "Tidak Ada";
			} else {
				$rw = $query->row();
				$nama_perusahaan = $rw->e_nama_perusahaan;
			}

			$ObjPHPExcel = new PHPExcel();
			$query = $this->Mmaster->baca($tgljthtempo, $kodeprsh);

			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("Adriadi 2019");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("Adriadi 2019");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Penjualan")
				->setSubject("Laporan Penjualan")
				->setDescription("Laporan Penjualan")
				->setKeywords("Laporan Penjualan")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			$ObjPHPExcel->getActiveSheet()->setTitle("Piutang");
			
			$ObjPHPExcel->createSheet();

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:I2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan Piutang');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:I3');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 11
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);

			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $nama_perusahaan);

			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:I4');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'=>'Arial',
						'bold'=>true,
						'italic'=>false,
						'size'=>12
					),
					'alignment'=>array(
						'horizontal'=>Style_Alignment::HORIZONTAL_CENTER,
						'vertical'=>Style_Alignment::VERTICAL_CENTER,
						'wrap'=>true
					)
				),
				'A4'
			);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', "Tanggal Jatuh Tempo : ".$tgltop);

			$style_col = array(       
				'font' => array(
					'name'	=> 'Arial',
					'bold'  => true,
					'italic'=> false,
					'size'  => 10
				),					
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$style_col1 = array(       
				'font' => array(
					'name'	=> 'Arial',
					'bold'  => true,
					'italic'=> false,
					'size'  => 10
				),					
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					// 'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$style_row = array(
				'font' => array(
					'name'	=> 'Arial',
					'bold'  => false,
					'italic'=> false,
					'size'  => 9
				),
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					// 'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$style_row1 = array(
				'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom'=> array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
			    
				),
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => false,
				'italic'=> false,
				'size'  => 9
				),
			);

			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'Pelanggan');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'No Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'Tgl Nota');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'Tgl Jatuh Tempo');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'TOP');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'Piutang');
			$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('H6', 'Saldo Sisa');
			$ObjPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('I6', 'Umur Piutang');
			$ObjPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray($style_col);

			// $ObjPHPExcel->getActiveSheet()->getStyle('A6:I6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00FFFF');

			$i = 1;
			$j = 7;
			$saldo = 0;
			if($query->num_rows()>0){
				foreach ($query->result() as $row) {
					$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $i);
					$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $row->e_nama_pelanggan);
					$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $row->i_no_nota);
					$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, Shared_Date::PHPToExcel(strtotime('+1 day',strtotime($row->d_nota))));
					$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, Shared_Date::PHPToExcel(strtotime('+1 day',strtotime($row->d_jth_tempo))));
					$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $row->top,Cell_DataType::TYPE_NUMERIC);
					$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $row->piutang, Cell_DataType::TYPE_NUMERIC);
					$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED0);
					$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $row->saldosisa, Cell_DataType::TYPE_NUMERIC);
					$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED0);
					$tgl2  = $row->d_jth_tempo;
        			$pecah2= explode("-", $tgl2);
        			$hari  = $pecah2[2];
        			$bulan = $pecah2[1];
        			$tahun =  $pecah2[0];
        			$jd2   = GregorianToJD($bulan, $hari, $tahun);
        			$lama  = $jd1 - $jd2;
        			$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $lama);
					$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($style_row);

					if($lama<=0){
						$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j.':I'.$j)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00FF00');
					} else if($lama>=0 && $lama<=$row->top) {
						$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j.':I'.$j)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF00');
					} else {
						$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j.':I'.$j)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF0000');
					}
					$i++;
					$j++;
				}
			}
			$nmprsh = str_replace(' ', '.', $nama_perusahaan);
			$nama_file = "Laporan_Piutang_".$nmprsh."_JthTempo_".$tgljthtempo.".xls";
			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');    
			header('Content-Disposition: attachment; filename='.$nama_file.''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			$ObjWriter->save('php://output','w');
		} else {
			redirect('Main');
		}
	}
}