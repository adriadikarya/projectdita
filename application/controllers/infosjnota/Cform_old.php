<?php

/**
 * 
 */
class Cform extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('infosjnota/Mmaster');
	}

	function index()
	{
		if($this->session->userdata('loggedin'))
		{
			$data['page_title'] = "Laporan Penjualan";
			$data['litle_title'] = "Form Export Penjualan";
			$datalay['content'] = $this->load->view('infosjnota/vformexp',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function export()
	{
		$dawal  = $this->uri->segment(4);
		$dakhir = $this->uri->segment(5);
		$kodeprsh = $this->session->userdata('kode_perusahaan');

		$query = $this->db->query("select e_nama_perusahaan from tr_perusahaan where i_kode_perusahaan='$kodeprsh'");
		if($query->num_rows()==0)
		{
			$nama_perusahaan = "Tidak Ada";
		} else {
			$rw = $query->row();
			$nama_perusahaan = $rw->e_nama_perusahaan;
		}

		setlocale(LC_ALL, 'IND');
		$tglawal = strftime('%d %B %Y',strtotime($dawal));
		$tglakhir= strftime('%d %B %Y',strtotime($dakhir));
		
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$ObjPHPExcel = new PHPExcel();

		$query = $this->Mmaster->baca($dawal, $dakhir, $kodeprsh);

			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("Adriadi 2019");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("Adriadi 2019");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Penjualan")
				->setSubject("Laporan Penjualan")
				->setDescription("Laporan Penjualan")
				->setKeywords("Laporan Penjualan")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			
			$ObjPHPExcel->createSheet();

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:F2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan Penjualan');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:F3');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 11
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);

			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $nama_perusahaan);

			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:F4');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'=>'Arial',
						'bold'=>true,
						'italic'=>false,
						'size'=>12
					),
					'alignment'=>array(
						'horizontal'=>Style_Alignment::HORIZONTAL_CENTER,
						'vertical'=>Style_Alignment::VERTICAL_CENTER,
						'wrap'=>true
					)
				),
				'A4'
			);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', "Periode : ".$tglawal." s/d ".$tglakhir);

			$style_col = array(       
				'font' => array(
					'name'	=> 'Arial',
					'bold'  => true,
					'italic'=> false,
					'size'  => 10
				),					
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$style_col1 = array(       
				'font' => array(
					'name'	=> 'Arial',
					'bold'  => true,
					'italic'=> false,
					'size'  => 10
				),					
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					// 'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$style_row = array(
				'font' => array(
					'name'	=> 'Arial',
					'bold'  => false,
					'italic'=> false,
					'size'  => 9
				),
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					// 'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$style_row1 = array(
				'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom'=> array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
			    
				),
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => false,
				'italic'=> false,
				'size'  => 9
				),
			);

			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'Tgl Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl Jatuh Tempo');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'No Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'Nama Pelanggan');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'Total');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray($style_col);

			$ObjPHPExcel->getActiveSheet()->getStyle('A6:F6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ACEAA4');

			$i = 1;
			$j = 7;
			$grandtot = 0;
			if($query->num_rows()>0){
				foreach ($query->result() as $row) {
					$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $i);
					$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, Shared_Date::PHPToExcel(strtotime('+1 day',strtotime($row->d_nota))));
					$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
					$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, Shared_Date::PHPToExcel(strtotime('+1 day',strtotime($row->d_jth_tempo))));
					$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
					$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row->i_no_nota, Cell_DataType::TYPE_STRING);
					$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($style_row1);
					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->e_nama_pelanggan, Cell_DataType::TYPE_STRING);
					$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($style_row1);
					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $row->v_total, Cell_DataType::TYPE_NUMERIC);
					$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($style_row1);
					$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED0);
					$i++;
					$j++;
					$grandtot+=$row->v_total;
				}
			} else {
				$ObjPHPExcel->getActiveSheet()->mergeCells('A'.$j.':F'.$j);
				$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, "Maaf Tidak Ada Data");
				$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($style_row1);
			}
			
			$ObjPHPExcel->getActiveSheet()->mergeCells('A'.$j.':E'.$j);
			$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, "Total Penjualan");
			$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($style_row1);

			$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $grandtot, Cell_DataType::TYPE_NUMERIC);
			$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($style_row1);
			$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED0);

			$nmprsh = str_replace(' ', '.', $nama_perusahaan);
			$nama_file = "Laporan_Penjualan_".$nmprsh."_".$dawal."-".$dakhir.".xls";
			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');    
			header('Content-Disposition: attachment; filename='.$nama_file.''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			$ObjWriter->save('php://output','w');
	}

	function exportitem()
	{
		$dawal  = $this->uri->segment(4);
		$dakhir = $this->uri->segment(5);
		$kodeprsh = $this->session->userdata('kode_perusahaan');

		$query = $this->db->query("select e_nama_perusahaan from tr_perusahaan where i_kode_perusahaan='$kodeprsh'");
		if($query->num_rows()==0)
		{
			$nama_perusahaan = "Tidak Ada";
		} else {
			$rw = $query->row();
			$nama_perusahaan = $rw->e_nama_perusahaan;
		}

		setlocale(LC_ALL, 'IND');
		$tglawal = strftime('%d %B %Y',strtotime($dawal));
		$tglakhir= strftime('%d %B %Y',strtotime($dakhir));
		
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$ObjPHPExcel = new PHPExcel();

		$query = $this->Mmaster->bacaitem($dawal, $dakhir, $kodeprsh);

			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("Adriadi 2019");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("Adriadi 2019");
	
			$ObjPHPExcel->getProperties()
				->setTitle("Laporan Penjualan")
				->setSubject("Laporan Penjualan")
				->setDescription("Laporan Penjualan")
				->setKeywords("Laporan Penjualan")
				->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			
			$ObjPHPExcel->createSheet();

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 12
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A2'
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:I2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan Penjualan (Per Item)');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:I3');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
			array(
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => true,
				'italic'=> false,
				'size'  => 11
				),
				'alignment' => array(
				'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER,
				'wrap'      => true
				)
			),
			'A3'
			);

			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', $nama_perusahaan);

			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:I4');

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'=>'Arial',
						'bold'=>true,
						'italic'=>false,
						'size'=>12
					),
					'alignment'=>array(
						'horizontal'=>Style_Alignment::HORIZONTAL_CENTER,
						'vertical'=>Style_Alignment::VERTICAL_CENTER,
						'wrap'=>true
					)
				),
				'A4'
			);
			
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', "Periode : ".$tglawal." s/d ".$tglakhir);

			$style_col = array(       
				'font' => array(
					'name'	=> 'Arial',
					'bold'  => true,
					'italic'=> false,
					'size'  => 10
				),					
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$style_col1 = array(       
				'font' => array(
					'name'	=> 'Arial',
					'bold'  => true,
					'italic'=> false,
					'size'  => 10
				),					
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					// 'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$style_row = array(
				'font' => array(
					'name'	=> 'Arial',
					'bold'  => false,
					'italic'=> false,
					'size'  => 9
				),
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$style_row1 = array(
				'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom'=> array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
			    
				),
				'font' => array(
				'name'	=> 'Arial',
				'bold'  => false,
				'italic'=> false,
				'size'  => 9
				),
			);

			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'Tgl Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl Jatuh Tempo');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'No Faktur');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'Nama Pelanggan');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'Nama Barang');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'Qty');
			$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('H6', 'Harga');
			$ObjPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('I6', 'Total');
			$ObjPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray($style_col);

			$ObjPHPExcel->getActiveSheet()->getStyle('A6:I6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ACEAA4');

			$i = 1;
			$j = 7;
			$grandtot = 0;
			if($query->num_rows()>0){
				foreach ($query->result() as $row) {
					$subtotal = $row->n_qty*$row->v_harga_satuan;
				if($row->i_no_item=='1'){	
					$qcount = $this->db->query("select count(i_no_item) as total from tm_nota_item where i_id_nota='$row->i_id_nota'")->row();
					$akhir = ($j-1) + $qcount->total;
					$ObjPHPExcel->getActiveSheet()->mergeCells('A'.$j.':A'.$akhir);
					$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $i);
					$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j.':A'.$akhir)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->mergeCells('B'.$j.':B'.$akhir);
					$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, Shared_Date::PHPToExcel(strtotime('+1 day',strtotime($row->d_nota))));
					$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j.':B'.$akhir)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j.':B'.$akhir)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
					$ObjPHPExcel->getActiveSheet()->mergeCells('C'.$j.':C'.$akhir);
					$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, Shared_Date::PHPToExcel(strtotime('+1 day',strtotime($row->d_jth_tempo))));
					$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j.':C'.$akhir)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j.':C'.$akhir)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
					$ObjPHPExcel->getActiveSheet()->mergeCells('D'.$j.':D'.$akhir);
					$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row->i_no_nota, Cell_DataType::TYPE_STRING);
					$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j.':D'.$akhir)->applyFromArray($style_row);
					$ObjPHPExcel->getActiveSheet()->mergeCells('E'.$j.':E'.$akhir);
					$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->e_nama_pelanggan, Cell_DataType::TYPE_STRING);
					$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j.':E'.$akhir)->applyFromArray($style_row);
					$i++;
				}
					$ObjPHPExcel->getActiveSheet()->setCellValue('F'.$j, $row->e_nama_brg, Cell_DataType::TYPE_STRING);
					$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($style_row1);
					$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $row->n_qty, Cell_DataType::TYPE_NUMERIC);
					$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($style_row1);
					$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED0);
					$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $row->v_harga_satuan, Cell_DataType::TYPE_NUMERIC);
					$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($style_row1);
					$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED0);
					$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $subtotal, Cell_DataType::TYPE_NUMERIC);
					$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($style_row1);
					$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED0);
					
				$j++;
				$grandtot+=$subtotal;
				}
			} else {
				$ObjPHPExcel->getActiveSheet()->mergeCells('A'.$j.':I'.$j);
				$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, "Maaf Tidak Ada Data");
				$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($style_row1);
			}

			$ObjPHPExcel->getActiveSheet()->mergeCells('A'.$j.':H'.$j);
			$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, "Total Penjualan");
			$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j.':H'.$j)->applyFromArray($style_row);

			$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $grandtot, Cell_DataType::TYPE_NUMERIC);
			$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($style_row1);
			$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED0);

			$nmprsh = str_replace(' ', '.', $nama_perusahaan);
			$nama_file = "Laporan_Penjualan_".$nmprsh."_".$dawal."-".$dakhir.".xls";
			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');    
			header('Content-Disposition: attachment; filename='.$nama_file.''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			$ObjWriter->save('php://output','w');
	}
}