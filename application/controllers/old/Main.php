<?php

/**
 * 
 */
class Main extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Mainmodel');
	}
	// function index()
	// {
	// 	$data['isi'] = $this->Mainmodel->daftarperusahaan();
	//     $this->load->view('login',$data);
	// }

	public function index()
	{
		if($this->session->userdata('loggedin'))
        {
        	//jika memang session sudah terdaftar, maka redirect ke halaman dahsboard
            redirect("Main/layout");
		}else{
        	//jika session belum terdaftar
			// if($this->input->post("submit")){
				//set form validation
	            $this->form_validation->set_rules('iduser', 'ID User', 'required');
	            $this->form_validation->set_rules('password', 'Password', 'required');
	            // $this->form_validation->set_rules('perusahaan','Perusahaan','required');

	            //set message form validation
	            $this->form_validation->set_message('required', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-ban"></i>{field}</b> harus diisi</div>');

	                //cek validasi
				if ($this->form_validation->run() == TRUE) {

	                //get data dari FORM
	                $iduser = $this->input->post("iduser", TRUE);
	                $password = MD5($this->input->post('password', TRUE));
	                // $perusahaan = $this->input->post("perusahaan", TRUE);

	                //checking data via model
	                $checking = $this->Mainmodel->check_login('tm_user a', array('i_nama_user' => $iduser), array('i_password' => $password));

	                //jika ditemukan, maka create session
	                if ($checking != FALSE) {
	                    foreach ($checking as $row) {

	                        $session_data = array(
	                            'id_user'   		=> $row->i_nama_user,
	                            'nama_user' 		=> $row->e_nama_user,
	                          	'kode_perusahaan' 	=> $row->i_kode_perusahaan, 
	                          	'level' 			=> $row->i_level,
	                          	'nama_perusahaan'	=> $row->e_nama_perusahaan,
	                          	'loggedin'			=> true,
	                        );
	                        //set session userdata
	                        $this->session->set_userdata($session_data);

	                        redirect('Main/layout');

	                    }
	                }else{
	                    $data['error'] = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>ID user dan Password</b> Salah</div>';
	                    $data['isi'] = $this->Mainmodel->daftarperusahaan();
	                    $this->load->view('login', $data);
	                }
	            }else{
	            	$data['isi'] = $this->Mainmodel->daftarperusahaan();
	                $this->load->view('login', $data);
	            }
	        // }
        }
	}

	function register()
	{
		if($this->input->post("submit")){
			//set form validation
	        $this->form_validation->set_rules('iduserreg', 'ID User', 'required');
	        $this->form_validation->set_rules('namauserreg', 'Nama User', 'required');
			$this->form_validation->set_rules('passwordreg', 'Password', 'required');
			$this->form_validation->set_rules('perusahaanreg','Perusahaan','required|callback_check_default');

			//set message form validation
	        $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px"><div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');

	        if ($this->form_validation->run() == TRUE) {
	        	// get data dari FORM
		        $iduser = $this->input->post("iduserreg", TRUE);
		        $namauser = $this->input->post("namauserreg", TRUE);
		        $password = MD5($this->input->post('passwordreg', TRUE));
		        $perusahaan = $this->input->post("perusahaanreg", TRUE);
		        $namaperusahaan = $this->input->post("namaprshreg",TRUE);
		        $level = $this->input->post("levelreg", TRUE);

		        //checking data via model
		        $checking = $this->Mainmodel->check_login('tm_user', array('i_nama_user' => $iduser), array('i_password' => $password));

		        if ($checking == FALSE) {
		        	$qdentry = $this->db->query("select now() as now");
		        	$rdentry = $qdentry->row();
		        	$now  = $rdentry->now;

		        	$this->Mainmodel->inputuser($iduser,$namauser,$password,$perusahaan,$level,$now);

		            $session_data = array(
		                'id_user'   		=> $iduser,
		                'nama_user' 		=> $namauser,
		             	'kode_perusahaan' 	=> $perusahaan, 
		             	'nama_perusahaan'   => $namaperusahaan,
		             	'level' 			=> $level, 
		             	'loggedin'			=> true,
		            );
		            //set session userdata
		            $this->session->set_userdata($session_data);
		            redirect('dashboard/');
		        }else{
		            $data['error'] = '<div class="alert alert-danger" style="margin-top: 3px">
		                        <div class="header"><b><i class="fa fa-exclamation-circle"></i> ERROR</b> IDuser Sudah Ada!</div></div>';
		            $data['isi'] = $this->Mainmodel->daftarperusahaan();
		        	$this->load->view('login', $data);
		        }
			}else{
		    	$data['isi'] = $this->Mainmodel->daftarperusahaan();
		        $this->load->view('login', $data);
		    }
		}

		$this->load->view("register");
	}

	function logout()
    {
        $this->session->sess_destroy();
        redirect('Main');
    }

    public function layout()
    {
    	$data['page_title'] = "Dashboard";
		$data['litle_title'] = "Control Panel";
    	$data['content'] = $this->load->view('template/example_content','',TRUE);
    	$this->load->view("template/layout",$data);
    }

    public function check_default($abc)
	{
	  if($abc == '')
	  { 
	  	//set message form validation
        $this->form_validation->set_message('check_default', '<div class="alert alert-danger" style="margin-top: 3px"><div class="header"><b><i class="fa fa-exclamation-circle"></i> Perusahaan</b> harus diisi</div></div>');
	    return FALSE;
	  } else {
	  	return TRUE;	
	  }
	}

	function carinamaprsh()
	{
		$kodepr = $this->input->post("kodepr",TRUE);
		$query = $this->db->query("select * from tr_perusahaan where i_kode='$kodepr'");
		if($query->num_rows()>0)
		{
			$row = $query->row();
			$namaprsh = "<input type=\"hidden\" name=\"namaprshreg\" value=\"".$row->e_nama_perusahaan."\">";
		} else {
			$namaprsh = "";
		}

		echo $namaprsh;
	}
}