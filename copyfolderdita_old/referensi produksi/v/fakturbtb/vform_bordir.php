<style type="text/css">
	.tinggi{
		height: 25px !important;
	}
	.marjin{
		margin-bottom: 10px;
	}
	.bottom{
		margin-bottom: -5px; 
	}
	.td{
		padding:0px !important; 
	}
	.a{
		padding:0px !important; 
	}
</style>
<div class="tmp">
<?php echo $this->pquery->form_remote_tag(array('url'=>'fakturbtb/Cform_bordir/add/','update'=>'#main','type'=>'post'));?>	
	<!-- <div class="container"> -->
		<div class="panel panel-default"><h2><?php echo $page_title ?></h2>
		  <div class="panel-heading"></div>
		  <div class="panel-body td">
		  	<?php if(validation_errors()): ?>
              <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><?php echo validation_errors(); ?></strong>
            </div>
          <?php endif; ?>
		  	<table>
		  		<tr>
		  			<td style="width: 30%;">Supplier</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;">
		  				<input class="tinggi" type="text" name="e_supplier_name" id="e_supplier_name" onclick='showModal("fakturbtb/Cform_bordir/supplier/","#light");jsDlgShow("#konten *", "#fade", "#light");' readonly>
		  				<input class="tinggi" type="hidden" name="i_supplier" id="i_supplier">
		  				<input class="tinggi" type="hidden" name="f_pkp" id="f_pkp"></td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;">Jenis Pembelian</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;">
		  				<select name="jnspemb" id="jnspemb">
		  					<option value="">Pilih Jenis Pembelian!</option>
		  					<option value="0">Cash</option>
		  					<option value="1">Kredit</option>
		  				</select>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;">No Faktur</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;">
		  				<input class="tinggi" type="text" name="nofaktur" id="nofaktur" value="" required>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;">Tgl Faktur</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;">
		  				<input class="tinggi" type="text" name="dfaktur" id="dfaktur" value="" required onclick="showCalendar('',this,this,'','dfaktur',0,20,1)"  readonly>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;">Jumlah Total</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;">
		  				<input class="tinggi" type="text" name="totfaktur" id="totfaktur" value="0">
		  			</td>
		  		</tr>
		  		<tr>
		  			<td>&nbsp;</td>
		  			<td>&nbsp;</td>
		  			<td>
		  				<a onclick='takesj();' title='<?php echo "Cari"; ?>' class='btn btn-success' data-tooltip='tooltip' data-placement='top'>Cari</a>
		  			</td>
		  		</tr>
		  	</table>
			  	<div id="detailheader" align="center"></div>
			  		<table id="itemtem" class="table table-striped" style="width:100%;">
			  			<tr>
			  				<th style="width:3%;">No</th>
			  				<th style="width:12%;">No.SJ</th>
			  				<th style="width:11%;">Tgl.SJ</th>
			  				<th style="width:12%;">Supplier</th>
			  				<th style="width:6%;">Jumlah Total(Rp.)</th>
			  				<th style="width:2%;">Action</th>
			  			</tr>
			  		</table>
				<div id="detailisi" align="center">
					<div id="ajaxlistsj"></div>
					<div id="pesan"></div>
					<center><input align="center" name="login" id="login" value="Proses" type="submit" class="btn btn-success btn-sm" onclick="return validasi();"></center>
				</div>
		  </div>
		  <div class="panel-footer"></div>
		</div>
<!-- <?=form_close()?> -->
</div>
<script type="text/javascript">
  function validasi()
  {
 	var s=0;
 	var textinputs = document.querySelectorAll('input[type=checkbox]'); 
	var empty = [].filter.call( textinputs, function( el ) {
	   return !el.checked
	});
  	if (textinputs.length == empty.length) {
	    alert("Maaf Tolong Pilih Minimal 1 OP!");
	    return false;
	} else if(document.getElementById('dfaktur').value==''){
		// $('#login').attr('disabled','disabled');
		alert("Maaf Tolong Pilih Tanggal Faktur");
		return false;
	} else {
		return true
	}
  }

  function hitungnilai(i)
  {
  	var totfak = formatulang(document.getElementById('totfaktur').value);
  	if(document.getElementById('cek'+i).checked==true)
  	{
  		var nilaisj = document.getElementById('total'+i).value;
  		totakhir = parseFloat(totfak)+parseFloat(nilaisj);
  	} else {
  		var nilaisj = document.getElementById('total'+i).value;
  		totakhir = parseFloat(totfak)-parseFloat(nilaisj);
  	}
  	document.getElementById('totfaktur').value = formatcemua(totakhir);
  }

  function takesj()
  {
  	// alert('a');
  	var supplier = document.getElementById('i_supplier').value;
  	var jnspemb  = document.getElementById('jnspemb').value;
  	if(jnspemb=='')
  	{
  		alert('Pilih Jenis Pembelian!');
  		// return false;
  	} else if(supplier=='') {
  		alert('Pilih Supplier!');
  		// return false;
  	} else {
    	// start AJAX request
		$.ajax({
			// URL for the controller function that fetches and prints the data of the selected phone from dropdown list
		   	url : "<?php echo base_url() ?>fakturbtb/Cform_bordir/listSJ/"+supplier+"/"+jnspemb,
		    // method of getting data is POST..not GET..
		    method : "POST",
		    // Data got through input are sent to the server
		    // data: {
		    // 	sup:supplier, 
		    // 	jns:jnspemb
		    // },
		    // if data is fetched successfully, this function will be executed
		    success:function(data){
		     	// print the recieved data from server in the section called com_result in compare_phones view file
		    	$('#ajaxlistsj').html(data);
		    },
		    // type of the data sent and received
		   	dataType: "text"  
		});
	}
  }
</script>