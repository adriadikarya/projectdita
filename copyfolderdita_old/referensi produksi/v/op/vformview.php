<style>
tr td{
    padding: 3px !important;
}
</style>
<div id="tmp"> 
    <!-- <?php echo $this->pquery->form_remote_tag(array('url'=>'index.php/op/Cform/view/','update'=>'#main','type'=>'post'));?> -->
    <section class="panel panel-default"><h2><?php echo $page_title ?></h2>
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-3 col-xs-3">                
                  <a onclick='show("<?php echo 'op/Cform/add'; ?>","#main");' href='#' title='<?php echo "Tambah Data"; ?>' class='btn btn-success btn-sm' data-tooltip='tooltip' data-placement='top'><i class='glyphicon glyphicon-plus'></i></a>
                </div>
                <div class="col-md-4 col-xs-9">             
                 <?php #echo form_open(site_url('tm_kelompok_barang/search'), 'role="search" class="form"') ;?>       
                 <?php 
                  echo $this->pquery->form_remote_tag(array('url'=>'op/Cform/cariindex/','update'=>'#main','type'=>'post'));
                 ?>   
                           <div class="input-group pull-right">                      
                                 <input type="text" class="form-control" placeholder="Cari data" name="cari" id="cari" style="height:30px;" value="<?php echo $cari; ?>"> 
                                 <input type="hidden" name="is_cari" value= "1" >
                                 <span class="input-group-btn">
                                      <button class="btn btn-primary btn-sm" type="submit"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                 </span>
                           </div>
                  <?php echo form_close(); ?>   
                </div>
            </div>
        </header>
        <div class="">
            <?php if($message = $this->session->flashdata('message')): ?>
              <div class="alert alert-<?php echo ($message['status']) ? 'success' : 'danger'; ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><?php echo $message['message']; ?></strong>
              </div>
            <?php endif; ?>
            <table class="table table-bordered table-condensed" style="font-size:12px;">

                <thead>
                   <tr>
                      <th>No</th>
                      <th>No OP</th>
                      <th>Tanggal OP</th>
                      <th>Keterangan</th>                    
                      <th>Supplier</th>
                      <th>Status Batal</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <?php if($isi){ 
                      $i=0;
                      foreach($isi as $row) {
                      $i++; 
                        if($row->f_op_cancel=='t'){
                            $status = 'Batal';
                            $action = '';
                        }else{
                            $status = 'Tidak';
                            $action = '<a onclick=\'show("op/Cform/edit/'.$row->i_op.'/","#main");\' href=\'#\' title="Edit" class="btn btn-sm btn-success" data-tooltip="tooltip" data-placement="top"><i class="glyphicon glyphicon-edit"></i></a>
                              <a onclick=\'printop("'.$row->i_op.'");\' href="#" title="Print" class="btn btn-sm btn-primary" data-tooltip="tooltip" data-placement="top"><i class="glyphicon glyphicon-print"></i></a>
                              <a onclick=\'hapus("op/Cform/cancel/'.$row->i_op.'/","#main");\' href=\'#\' title="hapus" class="btn btn-sm btn-danger" data-tooltip="tooltip" data-placement="top"><i class="glyphicon glyphicon-trash"></i></a>';
                        } 

                        if($row->d_op){
                          $dop = date('d F Y', strtotime($row->d_op )); 
                        }else{
                          $dop="";
                        } ?>

                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row->i_op; ?></td>
                        <td><?php echo $dop; ?></td>
                        <td><?php echo $row->e_remark; ?></td>
                        <td><?php echo $row->e_supplier_name; ?></td>
                        <td><?php echo $status; ?></td>
                        <td><?php echo $action; ?></td>
                      </tr>
                <?php } 
                    } ?>
              </table>
        </div>


      <div class="panel-footer">
        <div class="row">
            <?php echo "<center>".$this->pagination->create_links()."</center>";?>
            <div class="col-md-3">
             Total Data
             <span class="label label-info">
                <?php echo $total; ?>
            </span>

        </div>  
        <div class="col-md-9">

        </div>
    </div>
</div>
</section>
</div>
<script type="text/javascript">
  function printop(iop)
  {
    lebar =800;
    tinggi=850;
    eval('window.open("<?php echo site_url(); ?>"+"/op/Cform/cetakop/"+iop,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,menubar=1, top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
</script>
