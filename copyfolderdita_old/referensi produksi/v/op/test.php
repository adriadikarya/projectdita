<style type="text/css">
	.tinggi{
		height: 25px !important;
	}
	.marjin{
		margin-bottom: 10px;
	}
	.bottom{
		margin-bottom: -5px; 
	}
	.td{
		padding:0px !important; 
	}
	.a{
		padding:0px !important; 
	}
</style>
<div class="tmp">
<?php echo $this->pquery->form_remote_tag(array('url'=>'op/Cform/proses/','update'=>'#main','type'=>'post'));?>	
	<!-- <div class="container"> -->
		<div class="panel panel-default"><h2><?php echo $page_title ?></h2>
		  <div class="panel-heading"></div>
		  <div class="panel-body td">
		  	<?php if(validation_errors()): ?>
              <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><?php echo validation_errors(); ?></strong>
            </div>
          <?php endif; ?>
		  	<table>
		  		<tr>
		  			<td style="width: 30%;">No PP</td>
		  			<td style="width: 5%;">:</td>
		  			<?php if($pp){ ?>
		  			<td style="width: 65%;"><input class="tinggi" type="text" name="ipp" value="<?php foreach ($pp as $ipp){ echo $ipp." - "; } ?>" readonly></td>
		  		<?php } ?>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;">Supplier</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;">
		  				<input class="tinggi" type="text" name="esupplier" id="esupplier" readonly value="<?php echo $esuppliername; ?>">
		  				<input type="hidden" name="isupplier" id="isupplier" value="<?php echo $isupplier; ?>" required></td>
		  		</tr>
		  		<tr>
	
		  			<td style="width: 30%;">Tanggal OP</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;"><input class="tinggi" type="text" name="dop" id="dop" onclick="showCalendar('',this,this,'','dop',0,20,1)" readonly value=""></td>
		  		</tr>
		  		<tr>
	
		  			<td style="width: 30%;">Tanggal Kirim</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;"><input class="tinggi" type="text" name="dkirim" id="dkirim" onclick="showCalendar('',this,this,'','dop',0,20,1)" readonly value=""></td>
		  		</tr>
		  		<tr>
	
		  			<td style="width: 30%;">Tanggal Batas Kirim</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;"><input class="tinggi" type="text" name="dbatas" id="dbatas" onclick="showCalendar('',this,this,'','dop',0,20,1)" readonly value=""></td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;">Jenis Pembelian</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;"><select name="paymenttype">
		  										<option value="">-- Pilih --</option>
		  										<option value="0">Cash</option>
		  										<option value="1">Kredit</option>
		  									</select></td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;">Keterangan</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;"><input class="tinggi" type="text" name="eremark" value=""></td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;"></td>
		  			<td style="width: 5%;"></td>
		  			<td style="width: 65%;">
		  				<input name="login" id="login" value="Simpan" type="submit" class="btn btn-success btn-sm" onclick="return dipales(parseFloat(document.getElementById('jml').value));">
		  				<button type="button" class="btn btn-warning btn-sm" onclick="show('op/Cform/add/','#main')">
		  				Keluar</button>
		  				<!-- <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button" class="btn btn-primary btn-sm" onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);"> -->
		  				<!-- <input name="cmdkurangitem" id="cmdkurangitem" value="Kurang" type="button" class="btn btn-primary btn-sm" onclick="bbatal();"> -->
		  			</td>
		  		</tr>
		  	</table>
			  	<div id="detailheader" align="center">
			  		<table id="itemtem" class="table table-striped" style="width:100%;">
			  			<tr>
			  				<th style="width:2%;" align="center">No</th>
			  				<th style="width:5%;" align="center">Gudang</th>
			  				<th style="width:5%;" align="center">No PP</th>
			  				<th style="width:15%;" align="center">Kode Barang</th>
			  				<th style="width:5%;" align="center">Nama Barang</th>
			  				<th style="width:15%;" align="center">Satuan</th>
			  				<th style="width:5%;" align="center">Qty</th>
			  				<th style="width:5%;" align="center">Harga</th>
			  				<th style="width:5%;" align="center">Total</th>
			  				<th style="width:5%; text-align:right;">Keterangan</th>
			  			</tr>
			  		</table>
			  	</div>
				<div id="detailisi" align="center">
				<?php 
			 if($isi){
			 	$i=0;
			 	foreach($isi as $row){
			 		$i++;
			 		$v_price = $row[0]->v_price==0?$row[0]->hrgpp:$row[0]->v_price;
			 		// $total = $row[0]->n_quantity*$row[0]->v_price;
			 		$total = $row[0]->n_quantity*$v_price;
			 		$qtys = $row[0]->n_quantity-$row[0]->op;
			 echo	'<table class="table table-striped bottom" style="width:100%;" disabled="">
						<tbody>
							<tr>
								<td class="a" style="width:2%;">
									<input style="width:100%; height:25px;" readonly type="text" id="baris'.$i.'" name="baris'.$i.'" value="'.$i.'">
								</td>
								<td class="a" style="width:7%;">
									<input style="width:100%; height:25px;" readonly type="text" id="enamamaster'.$i.'" name="enamamaster'.$i.'" value="'.$row[0]->e_nama_master.'">
									<input style="width:100%; height:25px;" readonly type="hidden" id="ikodemaster'.$i.'" name="ikodemaster'.$i.'" value="'.$row[0]->i_kode_master.'">
								</td>
								<td class="a" style="width:7%;">
									<input style="width:100%; height:25px;" readonly type="text" id="ipp'.$i.'" name="ipp'.$i.'" value="'.$row[0]->i_pp.'">
								</td>
		    					<td class="a" style="width:5%;">
		    						<input style="width:100%; height:25px;" readonly type="text" id="imaterial'.$i.'" name="imaterial'.$i.'" value="'.$row[0]->i_material.'">
		    					</td>
					    		<td class="a" style="width:13%;">
					    			<input style="width:100%; height:25px;" readonly type="text" id="ematerialname'.$i.'" name="ematerialname'.$i.'" value="'.$row[0]->e_material_name.'">
					    		</td>
					    		<td class="a" style="width:5%;">
					    			<input readonly style="width:100%; height:25px;"  type="text" id="esatuan'.$i.'" name="esatuan'.$i.'" value="'.$row[0]->e_satuan.'">
					    			<input readonly type="hidden" id="isatuan'.$i.'" name="isatuan'.$i.'" value="'.$row[0]->i_satuan.'">
					    		</td>
					    		<td class="a" style="width:5%;">
					    			<input style="text-align:right; width:100%; height:25px;" type="text" id="nquantity'.$i.'" name="nquantity'.$i.'" onkeyup=\'calc('.$i.');\' autocomplete="off" value="'.$qtys.'">
					    		</td>
					    		<td class="a" style="width:5%;">
					    			<input style="text-align:right; width:100%; height:25px;" type="text" id="vprice'.$i.'" name="vprice'.$i.'" value="'.number_format($v_price,2).'" onkeyup=\'calc('.$i.');\'>
					    		</td>
					    		<td class="a" style="width:6%;">
					    			<input style="text-align:right; width:100%; height:25px;" type="text" id="vtotal'.$i.'" name="vtotal'.$i.'" readonly value="'.number_format($total,2).'">
					    		</td>
					    		<td class="a" style="width:5%;">
					    			<input style="width:100%; height:25px;" type="text" id="eremark'.$i.'" name="eremark'.$i.'" value="">
					    		</td>
					    	</tr>
					    </tbody>
					</table>'; 
				}
			}?>
				</div>
				<div id="pesan"></div>
				<input type="hidden" name="jml" id="jml" value="<?php echo $i; ?>">
		  </div>
		  <div class="panel-footer"></div>
		</div>
<?=form_close()?>
</div>
<script type="text/javascript">

 function tambah_item(a){
//    if(a<=30){
	      so_inner=document.getElementById("detailheader").innerHTML;
	      si_inner=document.getElementById("detailisi").innerHTML;
	      if(so_inner==''){
			  so_inner = '<table id="itemtem" class="table table-striped" style="width:100%;">';
			  so_inner+= '<tr><th style="width:2%;" align="center">No</th>';
			  so_inner+= '<th style="width:5%;" align="center">Kode</th>';
			  so_inner+= '<th style="width:15%;" align="center">Nama Barang</th>';
			  so_inner+= '<th style="width:5%;" align="center">Satuan</th>';
			  so_inner+= '<th style="width:15%;" align="center">Supplier</th>';
			  so_inner+= '<th style="width:5%;" align="center">Qty</th>';
			  so_inner+= '<th style="width:5%;" align="center">Harga</th>';
			  so_inner+= '<th style="width:5%;" align="center">Total</th>';
			  so_inner+= '<th style="width:5%; text-align:right;">Keterangan</th></tr></table>';
			  document.getElementById("detailheader").innerHTML=so_inner;
    }else{
  			so_inner=''; 
    }
    if(si_inner==''){
		    document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		    juml=document.getElementById("jml").value;	
		    si_inner='<table class="table table-striped bottom" style="width:100%;" disabled=""><tbody><tr><td class="a" style="width:2%;"><input style="width:100%; height:25px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
		    si_inner+='<td class="a" style="width:5%;"><input style="width:100%; height:25px;" readonly type="text" id="imaterial'+a+'" name="imaterial'+a+'" value="" onclick=\'showModal("pp/Cform/material/'+a+'/x01","#light");jsDlgShow("#konten *", "#fade", "#light");\'></td>';
		    si_inner+='<td class="a" style="width:13%;"><input style="width:100%; height:25px;" readonly type="text" id="ematerialname'+a+'" name="ematerialname'+a+'" value=""></td>';
		    si_inner+='<td class="a" style="width:5%;"><input readonly style="width:100%; height:25px;"  type="text" id="esatuan'+a+'" name="esatuan'+a+'" value=""><input readonly type="hidden" id="isatuan'+a+'" name="isatuan'+a+'" value=""></td>';
		    si_inner+='<td class="a" style="width:13%;"><input style="width:100%; height:25px;" type="text" id="esupplier'+a+'" name="esupplier'+a+'" value="" onclick=\'selectsupp('+a+')\' readonly><input readonly type="hidden" id="isupplier'+a+'" name="isupplier'+a+'" value=""></td>';
		    si_inner+='<td class="a" style="width:5%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="nquantity'+a+'" name="nquantity'+a+'" onkeyup=\'calc('+a+');\' autocomplete="off">';
		    si_inner+='<td class="a" style="width:5%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="vprice'+a+'" name="vprice'+a+'" onkeyup=\'calc('+a+');\'>';
		    si_inner+='<td class="a" style="width:6%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" readonly>';
		    si_inner+='<td class="a" style="width:5%;"><input style="width:100%; height:25px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td></tr></tbody></table>';
    }else{
			  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
			  juml=document.getElementById("jml").value;
		    si_inner=si_inner+'<table class="table table-striped bottom" style="width:100%;" disabled=""><tbody><tr><td class="a" style="width:2%;"><input style="width:100%; height:25px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
		    si_inner+='<td class="a" style="width:5%;"><input style="width:100%; height:25px;" readonly type="text" id="imaterial'+a+'" name="imaterial'+a+'" value="" onclick=\'showModal("pp/Cform/material/'+a+'/x01","#light");jsDlgShow("#konten *", "#fade", "#light");\'></td>';
		    si_inner+='<td class="a" style="width:13%;"><input style="width:100%; height:25px;" readonly type="text" id="ematerialname'+a+'" name="ematerialname'+a+'" value=""></td>';
		    si_inner+='<td class="a" style="width:5%;"><input readonly style="width:100%; height:25px;"  type="text" id="esatuan'+a+'" name="esatuan'+a+'" value=""><input readonly type="hidden" id="isatuan'+a+'" name="isatuan'+a+'" value=""></td>';
		    si_inner+='<td class="a" style="width:13%;"><input style="width:100%; height:25px;" type="text" id="esupplier'+a+'" name="esupplier'+a+'" value="" onclick=\'selectsupp('+a+')\' readonly><input readonly type="hidden" id="isupplier'+a+'" name="isupplier'+a+'" value=""></td>';
		    si_inner+='<td class="a" style="width:5%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="nquantity'+a+'" name="nquantity'+a+'" onkeyup=\'calc('+a+');\' autocomplete="off">';
		    si_inner+='<td class="a" style="width:5%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="vprice'+a+'" name="vprice'+a+'" onkeyup=\'calc('+a+');\'>';
		    si_inner+='<td class="a" style="width:6%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" readonly>';
		    si_inner+='<td class="a" style="width:5%;"><input style="width:100%; height:25px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td></tr></tbody></table>';
    }
	    j=0;
	    var baris			= Array();
	    var imaterial		= Array();
	    var ematerialname 	= Array(); 
  		var isatuan			= Array();
  		var esatuan			= Array();
  		var isupplier 		= Array();
  		var esupplier 		= Array();
  		var nquantity		= Array();
  		var vprice 			= Array();
  		var vtotal 			= Array();
	    var eremark			= Array();
	    for(i=1;i<a;i++){
	      j++;
	      baris[j]		   = document.getElementById("baris"+i).value;
	      imaterial[j]	   = document.getElementById("imaterial"+i).value;
	      ematerialname[j] = document.getElementById("ematerialname"+i).value;
	      isatuan[j]	   = document.getElementById("isatuan"+i).value;
	      esatuan[j]	   = document.getElementById("esatuan"+i).value;
	      isupplier[j]	   = document.getElementById("isupplier"+i).value;
	      esupplier[j] 	   = document.getElementById("esupplier"+i).value;
	      nquantity[j]	   = document.getElementById("nquantity"+i).value;
	      vprice[j]	   	   = document.getElementById("vprice"+i).value;
	      vtotal[j]	   	   = document.getElementById("vtotal"+i).value;
	      eremark[j]	   = document.getElementById("eremark"+i).value;	
	    }
	    document.getElementById("detailisi").innerHTML=si_inner;
	    j=0;
	    for(i=1;i<a;i++){
			  j++;
			  document.getElementById("baris"+i).value         = baris[j];
			  document.getElementById("imaterial"+i).value     = imaterial[j];
			  document.getElementById("ematerialname"+i).value = ematerialname[j];
			  document.getElementById("isatuan"+i).value       = isatuan[j];
			  document.getElementById("esatuan"+i).value       = esatuan[j];
			  document.getElementById("nquantity"+i).value     = nquantity[j];
			  document.getElementById("isupplier"+i).value     = isupplier[j];
			  document.getElementById("esupplier"+i).value 	   = esupplier[j];
			  document.getElementById("nquantity"+i).value     = nquantity[j];
			  document.getElementById("vprice"+i).value        = vprice[j];
			  document.getElementById("vtotal"+i).value        = vtotal[j];
			  document.getElementById("eremark"+i).value       = eremark[j];	
	    }
  }
  function bbatal(){
	  baris	= document.getElementById("jml").value;
	  si_inner= document.getElementById("detailisi").innerHTML;
	  var temp= new Array();
	  temp	= si_inner.split('<table class="table table-striped bottom" style="width:100%;" disabled="">');
	  if( (document.getElementById("imaterial"+baris).value=='')){
		  si_inner='';
		  for(x=1;x<baris;x++){
			  si_inner=si_inner+'<table class="table table-striped bottom" style="width:100%;" disabled="">'+temp[x];
		  }
		  j=0;
		  var barbar		= Array();
		  var imaterial		= Array();
		  var ematerialname	= Array();
		  var isatuan		= Array();
		  var esatuan		= Array();
		  var isupplier 	= Array();
		  var esupplier 	= Array();
		  var nquantity 	= Array();
		  var vprice 		= Array();
		  var vtotal 		= Array();
		  var eremark 		= Array();
	    for(i=1;i<baris;i++){
			j++;
		      barbar[j]		   = document.getElementById("baris"+i).value;
		      imaterial[j]	   = document.getElementById("imaterial"+i).value;
		      ematerialname[j] = document.getElementById("ematerialname"+i).value;
		      isatuan[j]	   = document.getElementById("isatuan"+i).value;
		      esatuan[j]	   = document.getElementById("esatuan"+i).value;
		      isupplier[j]	   = document.getElementById("isupplier"+i).value;
		      esupplier[j] 	   = document.getElementById("esupplier"+i).value;
		      nquantity[j]	   = document.getElementById("nquantity"+i).value;
		      vprice[j]	   	   = document.getElementById("vprice"+i).value;
		      vtotal[j]	   	   = document.getElementById("vtotal"+i).value;
		      eremark[j]	   = document.getElementById("eremark"+i).value;
		  }
		  document.getElementById("detailisi").innerHTML=si_inner;
		  j=0;
		  for(i=1;i<baris;i++){
			  j++;
			  document.getElementById("baris"+i).value         = barbar[j];
			  document.getElementById("imaterial"+i).value     = imaterial[j];
			  document.getElementById("ematerialname"+i).value = ematerialname[j];
			  document.getElementById("isatuan"+i).value       = isatuan[j];
			  document.getElementById("esatuan"+i).value       = esatuan[j];
			  document.getElementById("nquantity"+i).value     = nquantity[j];
			  document.getElementById("isupplier"+i).value     = isupplier[j];
			  document.getElementById("esupplier"+i).value 	   = esupplier[j];
			  document.getElementById("nquantity"+i).value     = nquantity[j];
			  document.getElementById("vprice"+i).value        = vprice[j];
			  document.getElementById("vtotal"+i).value        = vtotal[j];
			  document.getElementById("eremark"+i).value       = eremark[j];	
		  }
		  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)-1;
	  }
  }
  function selectsupp(a){
  	var items = document.getElementById("imaterial"+a).value;
  	if(items==''){
  		items = 'notfound';
  	}
  	showModal("pp/Cform/itemsupp/"+items+"/"+a,"#light");
  	jsDlgShow("#konten *", "#fade", "#light");
  }

  function calc(a){
  	var qty = document.getElementById("nquantity"+a).value;
  	var price = formatulang(document.getElementById("vprice"+a).value);
  	if(qty==''){
  		qty=0;
  	}
  	if(price==''){
  		price=0;
  	}
  	total = (parseFloat(qty)*parseFloat(price));
  	document.getElementById("vtotal"+a).value=formatcemua(total);
  }


  function dipales(a){
    cek='false';
    s=0;
    if((document.getElementById("dop").value!='')&&(document.getElementById("isupplier").value!='')&&(document.getElementById("paymenttype").value!='')) {
      if(a==0){
        alert('Isi data item minimal 1 !!!');
        s=1;
        return false;
      }else{
        for(i=1;i<=a;i++){
          if((document.getElementById("imaterial"+i).value=='') ||
             (document.getElementById("ematerialname"+i).value=='') ||
             (document.getElementById("nquantity"+i).value=='0')){
            	alert('Data item masih ada yang salah !!!');
            	s =1;
            	return false;
            	cek='false';
          	}else{
            	cek='true';	
          	} 
        }
      }
      if(cek=='true'){
        document.getElementById("login").disabled=true;
        document.getElementById("cmdtambahitem").disabled=true;
      }else{
        document.getElementById("login").disabled=false;
      }
    }else{
      alert('Data header masih ada yang salah !!!');
    }
  }
</script>