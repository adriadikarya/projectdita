<style>
	.padd {
		padding: 0px !important;
		margin-bottom: -15px;
		font-size:12px;
	}
	.padde{
		margin-bottom: 4px;
		}
	.tinggi{
		height: 25px !important;
	}
	.marjin{
		margin-bottom: 10px;
	}
	.bottom{
		margin-bottom: -5px; 
	}
	.td{
		padding:0px !important; 
	}
	.a{
		padding:0px !important; 
	}
</style>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'pp/Cform/gudang','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="table table-bordered padd">
	    <thead>
	      <tr>
		<td colspan="6" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th class="padd">No PP</th>
	    	<th class="padd">Kode Barang</th>
	    	<th class="padd">Nama Barang</th>
	    	<th class="padd">Satuan</th>
	    	<th class="padd">Qty</th>
	    	<th class="padd">Action</th>
	    <tbody>
	      <?php
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td class=\"padd\" >$row->i_pp</td>
				  <td class=\"padd\" >$row->i_material</td>
				  <td class=\"padd\" >$row->e_material_name</td>
				  <td class=\"padd\" >$row->e_satuan</td>
				  <td class=\"padd\" >$row->n_quantity</td>
				  <td class=\"padd\" ></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center class=\"padde\">".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center class="padde"><input type="button" id="batal" name="batal" value="Keluar" onclick="cbatal()" class="btn btn-warning"></center>
  	</div>
      </div>
      </div>
      <?php form_close()?>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b)
  {
    document.getElementById("igudang").value=a;
    document.getElementById("egudangname").value=b;
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function cbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
