<style>
	.padd {
		padding: 0px !important;
		margin-bottom: -15px;
		font-size:12px;
	}
	.padde{
		margin-bottom: 4px;
		}
	.tinggi{
		height: 25px !important;
	}
	.marjin{
		margin-bottom: 10px;
	}
	.bottom{
		margin-bottom: -5px; 
	}
	.td{
		padding:0px !important; 
	}
	.a{
		padding:0px !important; 
	}
</style>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'btb/Cform_aplikasi/supplier','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="table table-bordered padd">
	    <thead>
	      <tr>
		<td colspan="3" align="center">Cari data : <input type="text" style="height:25px;" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th class="padd">Kode Supplier</th>
			<th class="padd">Nama Supplier</th>
	    <tbody>
	      <?php
		if($isi){
			foreach($isi as $row){
				$esuppliername =trim($row->e_supplier_name);
			  echo "<tr> 
				  <td class=\"padd\"><a href=\"javascript:setValue('$row->i_supplier','$esuppliername','$row->f_pkp','$row->f_tipe_pajak')\">$row->i_supplier</a></td>
				  <td class=\"padd\"><a href=\"javascript:setValue('$row->i_supplier','$esuppliername','$row->f_pkp','$row->f_tipe_pajak')\">$esuppliername</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center class=\"padde\">".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center class="padde"><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()" class="btn btn-warning btn-sm"></center>
  	</div>
      </div>
      </div>
      <?php form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d)
  {
    document.getElementById("i_supplier").value=a;
    document.getElementById("e_supplier_name").value=b;
    c = c=='t'?true:false;
    document.getElementById("pkp").checked = c;
    document.getElementById("tipepajak").value = d; 
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
