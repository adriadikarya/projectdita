<style type="text/css">
	.tinggi{
		height: 25px !important;
	}
	.marjin{
		margin-bottom: 10px;
	}
	.bottom{
		margin-bottom: -5px; 
	}
	.td{
		padding:0px !important; 
	}
	.a{
		padding:0px !important; 
	}
</style>
<div class="tmp">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'btb/Cform/add/0/','update'=>'#main','type'=>'post'));?>	
	<section class="panel panel-default"><h2><?php echo $page_title ?></h2>
        	<div class="row">
        		<div class="col-md-3 col-xs-3">
        			<label>Tidak Ada Acuan OP</label>
        		</div>
        		<div class="col-md-6 col-xs-6">
        			<label>:</label>
        			<input type="checkbox" name="tidakadaop" id="tidakadaop" value="noop">
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-md-3 col-xs-3">
        			<label>Supplier</label>
        		</div>
        		<div class="col-md-6 col-xs-6">
        			<label>:</label>
        			<input class="tinggi" type="text" name="e_supplier_name" id="e_supplier_name" onclick='showModal("btb/Cform/supplier/1/x01/","#light");jsDlgShow("#konten *", "#fade", "#light");' readonly>
		  			<input class="tinggi" type="hidden" name="i_supplier" id="i_supplier">
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-md-3 col-xs-3">
        			<label>Jenis Pembelian</label>
        		</div>
        		<div class="col-md-9 col-xs-9">
        			<label>:</label>
        			<select name="jnspemb" id="jnspemb">
        				<option value="0">Cash</option>
        				<option value="1">Kredit</option>
        			</select>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-md-5 col-xs-5">
        			<center><input align="center" name="login" id="login" value="Proses" type="submit" class="btn btn-success btn-sm" onclick="return validasi();">&nbsp;<input align="center" name="batal" id="batal" value="Keluar" type="button" class="btn btn-warning btn-sm" onclick='show("btb/Cform/","#main")'>&nbsp;</center>
        		</div>
        	</div>
    </section>
    <?php form_close() ?>
</div>
<script type="text/javascript">
	function validasi()
	{
		var supplier = $('#i_supplier').val();
		var tidakadaop = $('#tidakadaop').is(':checked');
		if(tidakadaop)
		{
			return true;
		} else {
			if(supplier==''){
				alert('Pilih dulu suppliernya ya :D');
				return false;
			}
		}
		
	}
</script>