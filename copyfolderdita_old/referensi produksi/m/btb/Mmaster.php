<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
// Irawan 2018-11-02
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
    }

  function getsj($limit, $offset, $cari){

    if(!empty($cari)){
      $this->db->like('upper(x.i_sj)',$cari);
    }
    $this->db->select('x.*, 
      (case
        when (select i_sj from tm_bonmasuk_gudangdetail a, tm_bonmasuk_gudang b where 
              a.i_bonm=b.i_bonm and b.f_bonm_cancel=\'f\' and a.i_sj=x.i_sj limit 1)=x.i_sj
        then \'ada\'
        else \'tidak\'
      end) as status
      from(
      SELECT a.i_sj, a.d_sj, a.e_desc, a.f_sj_cancel, b.e_supplier_name
      FROM tm_sj_pembelian a 
      JOIN tr_supplier b ON a.i_supplier=b.i_supplier 
      WHERE a.i_makloon_type=\'0\'
      ORDER BY a.i_sj) as x');
    // $this->db->from('tm_sj_pembelian a');
    // $this->db->join('tr_supplier b','a.i_supplier=b.i_supplier');	
    // $this->db->where('a.i_makloon_type','0');
    // $this->db->order_by('a.i_sj');
    $this->db->limit($limit, $offset);
    $data = $this->db->get();
		return $data;
	}

	function getsjCari($cari){
    if(!empty($cari)){
      $this->db->like('upper(x.i_sj)',$cari);
    }   
    $this->db->select('x.*, 
      (case
        when (select a.i_sj 
        from tm_bonmasuk_gudangdetail a, tm_bonmasuk_gudang b 
        where a.i_bonm=b.i_bonm and b.f_bonm_cancel=\'f\' and a.i_sj=x.i_sj limit 1)=x.i_sj 
        then \'ada\' 
        else \'tidak\' 
      end) as status
      from(
      SELECT a.i_sj, a.d_sj, a.e_desc, a.f_sj_cancel, b.e_supplier_name
      FROM tm_sj_pembelian a 
      JOIN tr_supplier b ON a.i_supplier=b.i_supplier 
      WHERE a.i_makloon_type=\'0\'
      ORDER BY a.i_sj) as x');
    // $this->db->from('tm_sj_pembelian a');
    // $this->db->join('tr_supplier b','a.i_supplier=b.i_supplier'); 
    // $this->db->where('a.i_makloon_type','0');
    // $this->db->order_by('a.i_sj');
    $data = $this->db->get();
    return $data;

  }

  function getgudangCari($cari){
    if(!empty($cari)){
      $this->db->like('upper(i_kode_master)',$cari);
      $this->db->or_like('upper(e_nama_master)',$cari);
    }
    $data = $this->db->get('tr_master_gudang');

    return $data;
  }

  function bacagudang($limit,$offset,$cari){

    if(!empty($cari)){
      $this->db->like('upper(i_kode_master)',$cari);
      $this->db->or_like('upper(e_nama_master)',$cari);
    }
    //$this->db->select('upper(i_kode_master),upper(e_nama_master,i_kode_lokasi,d_input,d_update,i_kode_jenis');
    $this->db->limit($limit,$offset);
    $data = $this->db->get('tr_master_gudang')->result();

    return $data;

  }

    public function getmaterialCari($cari,$gudang){

      if(!empty($cari)){
        $this->db->like('upper(i_material)',$cari);
        $this->db->or_like('upper(e_material_name)',$cari);
      }
      $this->db->select("a.*,b.e_satuan");
      $this->db->from("tr_material a");
      $this->db->join("tr_satuan b","a.i_satuan=b.i_satuan");
      $this->db->where("i_store = '$gudang'");
    
      $data = $this->db->get();

      return $data;
    }

    public function bacamaterial($limit,$offset,$cari,$gudang){

      if(!empty($cari)){
        $this->db->like('upper(i_material)',$cari);
        $this->db->or_like('upper(e_material_name)',$cari);
      }
      $this->db->select("a.*,b.e_satuan");
      $this->db->from("tr_material a");
      $this->db->join("tr_satuan b","a.i_satuan=b.i_satuan");
      $this->db->where("i_store='$gudang'");
      $this->db->limit($limit,$offset);
      $data = $this->db->get()->result();

      return $data;

    }

    public function getPrice($material)
    {
      $this->db->select("a.*,b.e_satuan,c.e_supplier_name,d.e_material_name");
      $this->db->from("tr_supplier_materialprice a");
      $this->db->join("tr_satuan b","a.i_satuan=b.i_satuan");
      $this->db->join("tr_supplier c","a.i_supplier=c.i_supplier");
      $this->db->join("tr_material d","a.i_material=d.i_material");
      $this->db->where("a.i_material",$material);
      return $this->db->get()->result();
    }

    function runningnumber($yearmonth){
      $th = substr($yearmonth,0,4);
      $asal=$yearmonth;
      $yearmonth=substr($yearmonth,2,2).substr($yearmonth,4,2);
      $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='OP'
                          and i_area='00'
                          and e_periode='$asal' 
                          and substring(e_periode,1,4)='$th' for update", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        foreach($query->result() as $row){
          $terakhir=$row->max;
        }
        $noop  =$terakhir+1;
              $this->db->query("update tm_dgu_no 
                          set n_modul_no=$noop
                          where i_modul='OP'
                          and e_periode='$asal' 
                          and i_area='00'
                          and substring(e_periode,1,4)='$th'", false);
        settype($noop,"string");
        $a=strlen($noop);
        while($a<7){
          $noop="0".$noop;
          $a=strlen($noop);
        }
          $noop  ="OP-".$yearmonth."-".$noop;
        return $noop;
      }else{
        $noop  ="0000001";
        $noop  ="OP-".$yearmonth."-".$noop;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('OP','00','$asal',1)");
        return $noop;
      }
    }

    function insertheader($data)
    {
      // Jalankan query
      $db_debug = $this->db->db_debug;
      $this->db->db_debug = FALSE;
      $this->db->insert("tm_sj_pembelian", $data);
      // $query = $this->db->query($sql);
      $error = $this->db->error();
      if ( ! empty($error['message'])) {
          return false;
          $this->db->db_debug = $db_debug;
      } else {
          return true;
          $this->db->db_debug = $db_debug;
      }

      // Return hasil query
      // return $query;
    }

    function insertdetail($data2)
    {
      // Jalankan query
      $query = $this->db->insert("tm_sj_pembelian_detail", $data2);

      // Return hasil query
      return $query;
    }

    function getHeader($isj){
      $this->db->select("a.*,b.e_supplier_name,b.f_tipe_pajak");
      $this->db->from("tm_sj_pembelian a");
      $this->db->join("tr_supplier b","a.i_supplier=b.i_supplier");
      $this->db->where('a.i_sj',$isj);
      // $this->db->where('a.f_sj_cancel','f');
      return $this->db->get()->row();
    }

    function getItem($isj){
      $this->db->select("a.*,b.e_material_name,c.e_satuan,e.i_kode_master,e.e_nama_master");
      $this->db->from("tm_sj_pembelian_detail a");
      $this->db->join("tr_material b","a.i_material=b.i_material");
      $this->db->join("tr_satuan c","a.i_unit=c.i_satuan");
      // $this->db->join("tm_opbb_item d","a.i_op=d.i_op");
      $this->db->join("tr_master_gudang e","b.i_store=e.i_kode_master");
      $this->db->where("i_sj",$isj);
      $this->db->order_by("a.i_no_item");
      return $this->db->get()->result();
    }

    function updateheader($data,$i_sj){
      $query = $this->db->where("i_sj",$i_sj)->update("tm_sj_pembelian",$data);
      return $query;
    }

    public function delete($isj)
    {
      // Jalankan query
      $query = $this->db
        ->where('i_sj', $isj)
        ->delete("tm_sj_pembelian_detail");
      
      // Return hasil query
      return $query;
    }

    public function getOPitem($jnspemb,$supplier){
        $this->db->select("xx.*
          from (select x.*,
          coalesce((select sum(n_qty) from tm_sj_pembelian_detail b, tm_sj_pembelian a
          where a.i_sj=b.i_sj and a.f_sj_cancel='f' and b.i_op=x.i_op and b.i_material=x.i_material
            and b.i_unit=x.i_satuan
          ),0) as qtysj
          from(
          select a.i_op,a.i_material,a.i_satuan,a.v_price,b.d_op,b.i_payment_type,
            c.e_satuan,e.e_material_name,a.n_quantity,b.i_supplier,d.e_supplier_name,d.f_pkp,d.f_tipe_pajak,d.n_top
          from tm_opbb_item a
          join tm_opbb b on a.i_op=b.i_op and b.f_op_cancel='f'
          join tr_satuan c on a.i_satuan=c.i_satuan
          join tr_supplier d on b.i_supplier=d.i_supplier
          join tr_material e on a.i_material=e.i_material
          where b.i_payment_type='$jnspemb' AND b.i_supplier='$supplier'
          order by a.i_op
          ) as x) as xx
          where xx.n_quantity-xx.qtysj<>0
          order by xx.i_op
          ");
        $data = $this->db->get();
        if($data->num_rows() > 0){
          return $data->result();
        }
    }

    function bacasupplier($limit,$offset,$cari){

        if(!empty($cari)){
            $this->db->like('i_supplier',$cari);
            $this->db->or_like('e_supplier_name',$cari);
        }

        $data = $this->db->order_by("i_supplier","asc")->limit($limit, $offset)->get("tr_supplier");
        return $data;

    }

    function bacasuppliercari($cari)
    {
     
      if(!empty($cari)){
            $this->db->like('i_supplier',$cari);
            $this->db->or_like('e_supplier_name',$cari);
        }
      $data = $this->db->order_by('i_supplier','asc')->get("tr_supplier");

      return $data;

    }

    function getHarga($imaterial,$iop,$isupplier)
    {
      $this->db->select("x.*,
        (select sum(a.n_qty) from tm_sj_pembelian_detail a, tm_sj_pembelian b
        where a.i_sj=b.i_sj and b.f_sj_cancel='f' and a.i_material=x.i_material and a.i_unit=x.i_satuan
          and a.i_op=x.i_op
        ) as qtysj
        from (
        select a.i_op, b.e_supplier_name, b.f_pkp, b.f_tipe_pajak, b.n_top,
          a.i_kode_master, c.e_nama_master, a.i_material, d.e_material_name,
          a.i_satuan, e.e_satuan, a.n_quantity, a.v_price as hrgop, (a.n_quantity * a.v_price) as totalop,
          g.i_payment_type
        from tm_opbb_item a
        join tm_opbb g on a.i_op=g.i_op and g.f_op_cancel='f'
        join tr_supplier b on b.i_supplier=g.i_supplier 
        join tr_master_gudang c on a.i_kode_master=c.i_kode_master
        join tr_material d on a.i_material=d.i_material
        join tr_satuan e on e.i_satuan=a.i_satuan
        where a.i_material='$imaterial' and g.i_supplier='$isupplier' and a.i_op='$iop') as x
        order by x.i_op",false);
      //$this->db->where('b.i_supplier',$isupplier);
      $data = $this->db->get();
      if($data->num_rows() > 0){
        return $data->result();
      }
    }

    function cekqty($iop,$imaterial)
    {
      $this->db->select('b.n_qty as sj, a.n_quantity as op');
      $this->db->from('tm_opbb_item a');
      $this->db->join('tm_sj_pembelian_detail b','a.i_op=b.i_op and a.i_material=b.i_material and b.i_unit=a.i_satuan','left');
      $this->db->where('a.i_op',$iop);
      $this->db->where('a.i_material',$imaterial);
      $data = $this->db->get();
      if($data->num_rows() > 0){
        return $data->row();
      }
    }

    function itemComplete($dataop,$op,$material,$satuan)
    {
      // Jalankan query
      $this->db->where('i_op',$op);
      $this->db->where('i_material',$material);
      $this->db->where('i_satuan',$satuan);
      $query = $this->db->update("tm_opbb_item", $dataop);

      // Return hasil query
      return $query;
    }

    function getListPP($iop)
    {
      $this->db->where('i_op',$iop);
      $data = $this->db->get('tm_opbb_item');
      if($data->num_rows()>0){
        return $data->result();
      }
    }

    function getsatkonvCari($cari,$satuan)
    {
      if(!empty($cari)){
        $this->db->like('upper(namasatuan)',$cari);
      }
      $this->db->select("a.i_satuan, b.e_satuan, a.i_satuan_konversi, c.e_satuan as namasatuan,
            a.i_formula, 
            (case 
              when a.i_formula='1' then 'Dikali'
              when a.i_formula='2' then 'Dibagi'
              when a.i_formula='3' then 'Ditambah'
              else 'Dikurang'
            end) as nmformula
            , a.n_formula_factor",false);
      $this->db->from("tr_satuan_konversi a");
      $this->db->join("tr_satuan b","a.i_satuan=b.i_satuan");
      $this->db->join("(select i_satuan,e_satuan from tr_satuan) as c","c.i_satuan=a.i_satuan_konversi");
      $this->db->where("a.i_satuan",$satuan);
      return $this->db->get();
    }

    function bacasatkonv($limit,$offset,$cari,$satuan)
    {
      if(!empty($cari)){
        $this->db->like('upper(namasatuan)',$cari);
      }
      $this->db->select("a.i_satuan, b.e_satuan, a.i_satuan_konversi, c.e_satuan as namasatuan,
            a.i_formula, 
            (case 
              when a.i_formula='1' then 'Dikali'
              when a.i_formula='2' then 'Dibagi'
              when a.i_formula='3' then 'Ditambah'
              else 'Dikurang'
            end) as nmformula
            , a.n_formula_factor",false);
      $this->db->from("tr_satuan_konversi a");
      $this->db->join("tr_satuan b","a.i_satuan=b.i_satuan");
      $this->db->join("(select i_satuan,e_satuan from tr_satuan) as c","c.i_satuan=a.i_satuan_konversi");
      $this->db->where("a.i_satuan",$satuan);
      $this->db->limit($limit,$offset);
      return $this->db->get()->result();
    }

    function getsatuan()
    {
      return $this->db->get('tr_satuan')->result();
    }

    function insertsatuankonv($data2)
    {
      return $this->db->insert('tr_satuan_konversi',$data2);
    }

    function getpkp($supp)
    {
      $this->db->where('i_supplier',$supp);
      return $this->db->get('tr_supplier');
    }

    function cancelsj($isj)
    {
      $this->db->set('f_sj_cancel','t');
      $this->db->where('i_sj',$isj);
      $query = $this->db->update('tm_sj_pembelian');
      return $query;
    }
}
//End 