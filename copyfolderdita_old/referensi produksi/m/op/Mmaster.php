<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
// Irawan 2018-11-02
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
    }

  function getop($limit, $offset, $cari){

    if(!empty($cari)){
      $this->db->like('upper(a.i_op)',$cari);
    }
    $this->db->select('a.* , b.e_supplier_name');
    $this->db->from('tm_opbb a');
    $this->db->join('tr_supplier b','a.i_supplier=b.i_supplier');	
    $this->db->limit($limit, $offset);
    $data = $this->db->get();
		return $data;
	}

	function getppCari($cari){
    if(!empty($cari)){
      $this->db->like('upper(a.i_op)',$cari);
    }   
    $this->db->select('a.* , b.e_supplier_name');
    $this->db->from('tm_opbb a');
    $this->db->join('tr_supplier b','a.i_supplier=b.i_supplier'); 
    $data = $this->db->get();
    return $data;

  }

  function getgudangCari($cari){
    if(!empty($cari)){
      $this->db->like('upper(i_kode_master)',$cari);
      $this->db->or_like('upper(e_nama_master)',$cari);
    }
    $data = $this->db->get('tr_master_gudang');

    return $data;
  }

  function bacagudang($limit,$offset,$cari){

    if(!empty($cari)){
      $this->db->like('upper(i_kode_master)',$cari);
      $this->db->or_like('upper(e_nama_master)',$cari);
    }
    //$this->db->select('upper(i_kode_master),upper(e_nama_master,i_kode_lokasi,d_input,d_update,i_kode_jenis');
    $this->db->limit($limit,$offset);
    $data = $this->db->get('tr_master_gudang')->result();

    return $data;

  }

    public function getmaterialCari($cari){

      if(!empty($cari)){
        $this->db->like('upper(i_material)',$cari);
        $this->db->or_like('upper(e_material_name)',$cari);
      }
      $this->db->select("a.*,b.e_satuan");
      $this->db->from("tr_material a");
      $this->db->join("tr_satuan b","a.i_satuan=b.i_satuan");
    
      $data = $this->db->get();

      return $data;
    }

    public function bacamaterial($limit,$offset,$cari){

      if(!empty($cari)){
        $this->db->like('upper(i_material)',$cari);
        $this->db->or_like('upper(e_material_name)',$cari);
      }
      $this->db->select("a.*,b.e_satuan");
      $this->db->from("tr_material a");
      $this->db->join("tr_satuan b","a.i_satuan=b.i_satuan");
      $this->db->limit($limit,$offset);
      $data = $this->db->get()->result();

      return $data;

    }

    public function getPrice($material)
    {
      $this->db->select("a.*,b.e_satuan,c.e_supplier_name,d.e_material_name");
      $this->db->from("tr_supplier_materialprice a");
      $this->db->join("tr_satuan b","a.i_satuan=b.i_satuan");
      $this->db->join("tr_supplier c","a.i_supplier=c.i_supplier");
      $this->db->join("tr_material d","a.i_material=d.i_material");
      $this->db->where("a.i_material",$material);
      return $this->db->get()->result();
    }

    function runningnumber($yearmonth){
      $th = substr($yearmonth,0,4);
      $asal=$yearmonth;
      $yearmonth=substr($yearmonth,2,2).substr($yearmonth,4,2);
      $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='OP'
                          and i_area='00'
                          and e_periode='$asal' 
                          and substring(e_periode,1,4)='$th' for update", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        foreach($query->result() as $row){
          $terakhir=$row->max;
        }
        $noop  =$terakhir+1;
              $this->db->query("update tm_dgu_no 
                          set n_modul_no=$noop
                          where i_modul='OP'
                          and e_periode='$asal' 
                          and i_area='00'
                          and substring(e_periode,1,4)='$th'", false);
        settype($noop,"string");
        $a=strlen($noop);
        while($a<7){
          $noop="0".$noop;
          $a=strlen($noop);
        }
          $noop  ="OP-".$yearmonth."-".$noop;
        return $noop;
      }else{
        $noop  ="0000001";
        $noop  ="OP-".$yearmonth."-".$noop;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('OP','00','$asal',1)");
        return $noop;
      }
    }

    function insertheader($data)
    {
      // Jalankan query
      $query = $this->db->insert("tm_opbb", $data);

      // Return hasil query
      return $query;
    }

    function insertdetail($data2)
    {
      // Jalankan query
      $query = $this->db->insert("tm_opbb_item", $data2);

      // Return hasil query
      return $query;
    }

    function getHeader($iop){
      $this->db->select("a.*,b.e_supplier_name");
      $this->db->from("tm_opbb a");
      $this->db->join("tr_supplier b","a.i_supplier=b.i_supplier");
      $this->db->where('a.i_op',$iop);
      return $this->db->get()->row();
    }

    function getItem($iop){
      $this->db->select("a.*,b.e_material_name,c.e_satuan,d.i_kode_master,e.e_nama_master");
      $this->db->from("tm_opbb_item a");
      $this->db->join("tr_material b","a.i_material=b.i_material");
      $this->db->join("tr_satuan c","a.i_satuan=c.i_satuan");
      $this->db->join("tm_pp d","a.i_pp=d.i_pp");
      $this->db->join("tr_master_gudang e","d.i_kode_master=e.i_kode_master");
      $this->db->where("i_op",$iop);
      $this->db->order_by("a.n_item_no");
      return $this->db->get()->result();
    }

    function updateheader($data,$i_op){
      $query = $this->db->where("i_op",$i_op)->update("tm_opbb",$data);
      return $query;
    }

    public function delete($ipp)
    {
      // Jalankan query
      $query = $this->db
        ->where('i_op', $ipp)
        ->delete("tm_opbb_item");
      
      // Return hasil query
      return $query;
    }

    public function getPPitem(){
        $this->db->select("x.* ,
          (select sum(a.n_quantity) from tm_opbb_item a, tm_opbb b
          where a.i_op=b.i_op and a.i_pp=x.i_pp and a.i_material=x.i_material and a.i_satuan=x.i_satuan and b.f_op_cancel='f'
          ) as qty_op
          from
          (
          SELECT a.i_pp, a.i_kode_master, f.e_nama_master, b.i_material, d.e_material_name, b.i_satuan, 
            e.e_satuan, b.n_quantity, b.v_price
          FROM tm_pp a 
          JOIN tm_pp_item b ON a.i_pp=b.i_pp and b.f_op_complete='false'
          JOIN tr_material d ON b.i_material=d.i_material 
          JOIN tr_satuan e ON b.i_satuan=e.i_satuan 
          JOIN tr_master_gudang f ON a.i_kode_master=f.i_kode_master 
          WHERE f_pp_cancel = 'f' 
          ORDER BY a.i_pp ) as x",false);
        $data = $this->db->get();
        if($data->num_rows() > 0){
          return $data->result();
        }
    }

    function bacasupplier($limit,$offset,$cari){

        if(!empty($cari)){
            $this->db->like('i_supplier',$cari);
            $this->db->or_like('e_supplier_name',$cari);
        }

        $data = $this->db->order_by("i_supplier","asc")->limit($limit, $offset)->get("tr_supplier");
        return $data;

    }

    function bacasuppliercari($cari)
    {
     
      if(!empty($cari)){
            $this->db->like('i_supplier',$cari);
            $this->db->or_like('e_supplier_name',$cari);
        }
      $data = $this->db->order_by('i_supplier','asc')->get("tr_supplier");

      return $data;

    }

    function getHarga($imaterial,$ipp,$isupplier)
    {
      $this->db->select("x.*, 
        (select sum(a.n_quantity) as op from tm_opbb_item a, tm_opbb b
        where a.i_op=b.i_op and a.i_material=x.i_material and a.i_satuan=x.i_satuan and a.i_pp=x.i_pp and b.f_op_cancel='f'
        ) as op
        from
        (SELECT a.i_pp, a.i_material, c.e_material_name, a.i_satuan, d.e_satuan, a.n_quantity, 
        b.v_price, a.v_price as hrgpp, f.i_kode_master, g.e_nama_master 
        FROM tm_pp_item a 
        LEFT JOIN tr_supplier_materialprice b ON a.i_material=b.i_material and a.i_satuan=b.i_satuan 
          and b.i_supplier='$isupplier' 
        JOIN tr_material c ON a.i_material=c.i_material 
        JOIN tr_satuan d ON a.i_satuan=d.i_satuan 
        JOIN tm_pp f ON a.i_pp=f.i_pp 
        JOIN tr_master_gudang g ON f.i_kode_master=g.i_kode_master 
        WHERE a.i_material = '$imaterial' AND a.i_pp = '$ipp' 
        GROUP BY a.i_pp, a.i_material, c.e_material_name, a.i_satuan, d.e_satuan, a.n_quantity, 
          b.v_price, a.v_price, f.i_kode_master, g.e_nama_master, b.i_price_no 
        ORDER BY b.i_price_no DESC ) as x",false);
      //$this->db->where('b.i_supplier',$isupplier);
      $data = $this->db->get();
      if($data->num_rows() > 0){
        return $data->result();
      }
    }

    function cekqty($ipp,$imaterial)
    {
      $this->db->select('a.n_quantity as pp, b.n_quantity as op');
      $this->db->from('tm_pp_item a');
      $this->db->join('tm_opbb_item b','a.i_pp=b.i_pp and a.i_material=b.i_material and a.i_satuan=a.i_satuan','left');
      $this->db->where('a.i_pp',$ipp);
      $this->db->where('a.i_material',$imaterial);
      $data = $this->db->get();
      if($data->num_rows() > 0){
        return $data->row();
      }
    }

    function updatedoppp($i_pp,$dop)
    {
      $this->db->set('d_op',$dop);
      $this->db->where('i_pp',$i_pp);
      return $this->db->update('tm_pp');
    }

    function itemComplete($datapp,$pp,$material,$satuan)
    {
      // Jalankan query
      $this->db->where('i_pp',$pp);
      $this->db->where('i_material',$material);
      $this->db->where('i_satuan',$satuan);
      $query = $this->db->update("tm_pp_item", $datapp);

      // Return hasil query
      return $query;
    }

    function getListPP($iop)
    {
      $this->db->where('i_op',$iop);
      $data = $this->db->get('tm_opbb_item');
      if($data->num_rows()>0){
        return $data->result();
      }
    }

    function inserthrgsup($i_material,$v_price,$i_supplier,$i_satuan,$iop)
    {
      $query  = $this->db->query("SELECT current_timestamp as c");
      $rowd   = $query->row();
      $now    = $rowd->c;
      $this->db->where('i_supplier',$i_supplier);
      $this->db->where('i_material',$i_material);
      $this->db->where('i_satuan',$i_satuan);
      $this->db->order_by('i_price_no','desc');
      $this->db->limit(1);
      $query = $this->db->get('tr_supplier_materialprice');
      if($query->num_rows()>0)
      {
        foreach ($query->result() as $row) {
          $priceno = $row->i_price_no;
          if($v_price!=$row->v_price && $row->i_satuan==$i_satuan)
          {
            $data = array(
              'i_supplier' => $i_supplier,
              'i_material' => $i_material,
              'v_price'    => $v_price,
              'i_satuan'   => $i_satuan,
              'd_insert'   => $now,
              'i_price_no' => $row->i_price_no+1
            );
            $this->db->insert('tr_supplier_materialprice',$data);

            $this->db->set('i_price_no',$row->i_price_no+1);
            $this->db->where('i_op',$iop);
            $this->db->where('i_material',$i_material);
            $this->db->update('tm_opbb_item');
          } else if($v_price!=$row->v_price && $row->i_satuan!=$i_satuan) {
            $data = array(
              'i_supplier' => $i_supplier,
              'i_material' => $i_material,
              'v_price'    => $v_price,
              'i_satuan'   => $i_satuan,
              'd_insert'   => $now,
              'i_price_no' => 1
            );
            $this->db->insert('tr_supplier_materialprice',$data);
            $this->db->set('i_price_no',1);
            $this->db->where('i_op',$iop);
            $this->db->where('i_material',$i_material);
            $this->db->update('tm_opbb_item');
          } else {
            $this->db->set('i_price_no',$priceno);
            $this->db->where('i_op',$iop);
            $this->db->where('i_material',$i_material);
            $this->db->update('tm_opbb_item');
          }
        }
      } else {
        $data = array(
              'i_supplier' => $i_supplier,
              'i_material' => $i_material,
              'v_price'    => $v_price,
              'i_satuan'   => $i_satuan,
              'd_insert'   => $now,
              'i_price_no' => 1
            );
        $this->db->insert('tr_supplier_materialprice',$data);

        $this->db->set('i_price_no',1);
        $this->db->where('i_op',$iop);
        $this->db->where('i_material',$i_material);
        $this->db->update('tm_opbb_item');
      }
    }

    function updatehrgsup($i_material,$i_satuan,$i_price_no,$supp,$vprice)
    {
      $query  = $this->db->query("SELECT current_timestamp as c");
      $rowd   = $query->row();
      $now    = $rowd->c;
      $this->db->where('i_supplier',$supp);
      $this->db->where('i_material',$i_material);
      $this->db->where('i_satuan',$i_satuan);
      $this->db->where('i_price_no',$i_price_no);
      $this->db->order_by('i_price_no','desc');
      $this->db->limit(1);
      $query = $this->db->get('tr_supplier_materialprice');
      if($query->num_rows()>0)
      {
        foreach ($query->result() as $row) {
          $this->db->set('d_update',$now);
          $this->db->set('v_price',$vprice);
          $this->db->where('i_material',$i_material);
          $this->db->where('i_satuan',$i_satuan);
          $this->db->where('i_price_no',$i_price_no);
          $this->db->update('tr_supplier_materialprice');
        }
      } 
    }

    function cancelop($iop)
    {
      $this->db->where('i_op',$iop);
      $query = $this->db->get('tm_opbb_item');
      if($query->num_rows()>0)
      {
        foreach($query->result() as $row)
        {
          $this->db->set('f_op_complete','f');
          $this->db->where(array('i_pp'=>$row->i_pp, 'i_material'=>$row->i_material, 'i_satuan'=>$row->i_satuan));
          $this->db->update('tm_pp_item');
        }
      }
      $this->db->set('f_op_cancel','t');
      $this->db->where('i_op',$iop);
      return $this->db->update('tm_opbb');
    }

    function getCompany()
    {
      return $this->db->get('tr_company')->result();
    }
}
//End 