<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Mmaster_aplikasi extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function getnotaCari($cari)
	{
		if(!empty($cari))
		{
			$this->db->like('upper(a.i_nota)',$cari);
			// $this->db->or_like('upper(a.i_sj)',$cari);
		}

		$this->db->select("a.*, b.e_supplier_name");
		$this->db->from("tm_notabtbapp a");
		$this->db->join("tr_supplier b","a.i_supplier=b.i_supplier");
	    $this->db->order_by('a.i_nota');
		$data = $this->db->get();
    	return $data;
	}

	function getnota($limit, $offset, $cari)
	{
		if(!empty($cari))
		{
			$this->db->like('upper(a.i_nota)',$cari);
			// $this->db->or_like('upper(a.i_sj)',$cari);
		}

		$this->db->select("a.*, b.e_supplier_name");
		$this->db->from("tm_notabtbapp a");
		$this->db->join("tr_supplier b","a.i_supplier=b.i_supplier");
	    $this->db->order_by('a.i_nota');
		$this->db->limit($limit, $offset);
    	$data = $this->db->get();
		return $data;
	}

	function getbtbitem($jnspemb,$supplier)
	{
		$this->db->select("a.*, b.e_supplier_name");
		$this->db->from("tm_sj_pembelian a");
		$this->db->join("tr_supplier b","a.i_supplier=b.i_supplier");
		$this->db->where("a.i_supplier",$supplier);
		$this->db->where("a.i_payment_type",$jnspemb);
		$this->db->where("a.f_faktur_created","f");
		$this->db->where("a.i_makloon_type","3");
		$this->db->where("a.f_sj_cancel","f");
		$this->db->order_by("a.i_sj");
		return $this->db->get()->result();
	}

	function bacasupplier($limit,$offset,$cari){

        if(!empty($cari)){
            $this->db->like('i_supplier',$cari);
            $this->db->or_like('e_supplier_name',$cari);
        }
         // $this->db->where("i_category = '02'");
        $data = $this->db->order_by("i_supplier","asc")->limit($limit, $offset)->get("tr_supplier");
        return $data;

    }

    function bacasuppliercari($cari)
    {
     
      if(!empty($cari)){
            $this->db->like('i_supplier',$cari);
            $this->db->or_like('e_supplier_name',$cari);
        }
         // $this->db->where("i_category = '02'");
      $data = $this->db->order_by('i_supplier','asc')->get("tr_supplier");

      return $data;

    }

    function insertheader($data)
    {
      // Jalankan query
      $db_debug = $this->db->db_debug;
      $this->db->db_debug = FALSE;
      $this->db->insert("tm_notabtbapp", $data);
      // $query = $this->db->query($sql);
      $error = $this->db->error();
      if ( ! empty($error['message'])) {
          return false;
          $this->db->db_debug = $db_debug;
      } else {
          return true;
          $this->db->db_debug = $db_debug;
      }

      // Return hasil query
      // return $query;
    }

    function insertdetail($data2)
    {
      // Jalankan query
      $query = $this->db->insert("tm_notabtbapp_item", $data2);

      // Return hasil query
      return $query;
    }

    function updatesj($isj)
    {
    	$this->db->set('f_faktur_created','t');
    	$this->db->where('i_sj',$isj);
    	$this->db->where('f_sj_cancel','f');
    	return $this->db->update('tm_sj_pembelian');
    }

    function deletenotasj($i_nota)
    {
    	//update f_faktur_created sj
      	$this->db->where('i_nota',$i_nota);
      	$query = $this->db->get('tm_notabtbapp_item');
      	if($query->num_rows()>0)
      	{
      		foreach ($query->result() as $row) {
      			$isj = trim($row->i_sj);
      			// var_dump($isj);
      			$this->db->set('f_faktur_created','f');
      			$this->db->where('i_sj',$isj);
      			$this->db->update('tm_sj_pembelian');
      		}
      		// die;

      	}
      	//update tm_notabtbapp
	    $qdelete = $this->db->where('i_nota',$i_nota)->delete("tm_notabtbapp_item");
	      	// $qupdate = $this->db->update('tm_notabtbapp');
	    return $qdelete;
    }

    function cancelnota($inota)
    {
    	//update f_faktur_created sj
      	$this->db->where('i_nota',$inota);
      	$query = $this->db->get('tm_notabtbapp_item');
      	if($query->num_rows()>0)
      	{
      		foreach ($query->result() as $row) {
      			$isj = trim($row->i_sj);
      			// var_dump($isj);
      			$this->db->set('f_faktur_created','f');
      			$this->db->where('i_sj',$isj);
      			$this->db->update('tm_sj_pembelian');
      		}
      		// die;

      		//update tm_notabtbapp
	      	$this->db->set('f_nota_cancel','t');
	      	$this->db->where('i_nota',$inota);
	      	$qupdate = $this->db->update('tm_notabtbapp');
	      	return $qupdate;
      	}
    }

    function getitemedit($inota,$supplier,$ipayment)
    {
    	$this->db->select("(select a.i_sj from tm_notabtbapp_item a, tm_notabtbapp b where 
              a.i_nota=b.i_nota and b.i_nota='$inota' and b.f_nota_cancel='f' and a.i_sj=x.i_sj) as sjnota,
				x.*
			from 
			(select a.*, b.e_supplier_name 
			from tr_supplier b, tm_sj_pembelian a
			where a.i_supplier=b.i_supplier and a.f_sj_cancel='f' and a.i_supplier='$supplier'
				and a.i_payment_type='$ipayment' and a.i_makloon_type='3'
			order by a.i_sj ) as x",false);
    	return $this->db->get()->result();
    }

    function getheader($inota)
    {
    	$this->db->select("a.*, b.e_supplier_name");
    	$this->db->from("tm_notabtbapp a");
    	$this->db->join("tr_supplier b","a.i_supplier=b.i_supplier");
    	$this->db->where("i_nota",$inota);
    	return $this->db->get()->row();
    }

    function updateheader($data,$i_nota)
    {
    	$query = $this->db->where("i_nota",$i_nota)->update("tm_notabtbapp",$data);
      	return $query;
    }
}