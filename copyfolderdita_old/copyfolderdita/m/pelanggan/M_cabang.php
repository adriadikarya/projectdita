<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class M_cabang extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function daftargrup($ikodeprsh)
	{
		$this->db->where("i_kode_perusahaan",$ikodeprsh);
		return $this->db->get("tr_grup_pelanggan");
	}

	function daftarperusahaan()
	{
		return $this->db->get("tr_perusahaan");
	}

	function daftarcab($kodeprsh)
	{
		$this->db->select("a.*,b.e_nama_pelanggan");
		$this->db->from("tr_cabang a");
		$this->db->join("tr_pelanggan b","a.i_pelanggan=b.i_pelanggan");
		$this->db->where("a.i_kode_perusahaan",$kodeprsh);
		$this->db->order_by("a.e_nama_cabang");
		return $this->db->get();
	}

	function daftarpel($kodeprsh)
	{
		$this->db->where("i_kode_perusahaan",$kodeprsh);
		$this->db->order_by("e_nama_pelanggan");
		return $this->db->get("tr_pelanggan");
	}

	function caricab($cabang,$kodeprsh)
	{
		$this->db->where("i_cabang",$cabang);
		$this->db->where("i_kode_perusahaan",$kodeprsh);
		$query = $this->db->get("tr_cabang");
		return $query;
	}

	function insertdata($data)
	{
		return $this->db->insert("tr_cabang",$data);
	}

	function updatedata($data,$ikode)
	{
		$this->db->where("i_cabang",$ikode);
		return $this->db->update("tr_cabang",$data);
	}

	function caripelanggan($ikode)
	{
		$this->db->where("i_pelanggan",$ikode);
		return $this->db->get("tr_pelanggan")->row();
	}

	function deletedata($ikode)
	{
		$this->db->where("i_cabang",$ikode);
		return $this->db->delete("tr_cabang");
	}
}