<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class M_barang extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function daftarbrg($kodeprsh)
	{
		$this->db->where("i_kode_perusahaan",$kodeprsh);
		return $this->db->get("tr_barang");
	}

	function daftargrup($ikodeprsh)
	{
		$this->db->where("i_kode_perusahaan",$ikodeprsh);
		return $this->db->get("tr_grup_pelanggan");
	}

	function daftarperusahaan()
	{
		return $this->db->get("tr_perusahaan");
	}

	function daftarpel($kodeprsh)
	{
		$this->db->select("a.*,b.e_nama_grup");
		$this->db->from("tr_pelanggan a");
		$this->db->join("tr_grup_pelanggan b","a.i_id_grup=b.i_id_grup");
		$this->db->where("a.i_kode_perusahaan",$kodeprsh);
		$this->db->order_by("a.e_nama_pelanggan");
		return $this->db->get();
	}

	function caripel($ipelanggan,$kodeprsh)
	{
		$this->db->where("i_pelanggan",$ipelanggan);
		$this->db->where("i_kode_perusahaan",$kodeprsh);
		$query = $this->db->get("tr_pelanggan");
		return $query;
	}

	function caribrg($kodeprsh,$ikodebrg)
	{
		$where = array("i_kode_perusahaan"=>$kodeprsh,"i_kode_brg"=>$ikodebrg);
		$this->db->where($where);
		return $this->db->get("tr_barang");
	}

	function insertdata($data)
	{
		return $this->db->insert("tr_barang",$data);
	}

	function updatedata($data,$ikode,$kodeprsh)
	{
		$this->db->where("i_kode_brg",$ikode);
		$this->db->where("i_kode_perusahaan",$kodeprsh);
		return $this->db->update("tr_barang",$data);
	}

	function caripelanggan($ikode)
	{
		$this->db->where("i_pelanggan",$ikode);
		return $this->db->get("tr_pelanggan")->row();
	}

	function deletedata($ikode,$kodeprsh)
	{
		$this->db->where("i_kode_brg",$ikode);
		$this->db->where("i_kode_perusahaan",$kodeprsh);
		return $this->db->delete("tr_barang");
	}

	function ambilkodebrg($kodeprsh)
	{
		$pjgs = strlen($kodeprsh)+1;
		$pjgw = strlen($kodeprsh);
		$this->db->select("max(substr(i_kode_brg,".$pjgs.",4)) as max 
		                    from tr_barang 
		                    where substr(i_kode_brg,".$pjgw.",".$pjgw.")='$kodeprsh'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nogj  =$terakhir+1;
			settype($nogj,"string");
			$a=strlen($nogj);
			while($a<4){
			  $nogj="0".$nogj;
			  $a=strlen($nogj);
			}
			$nogj  =$kodeprsh.$nogj;
			return $nogj;
		}else{
			$nogj  ="0001";
			$nogj  =$kodeprsh.$nogj;
			return $nogj;
		}
	}

	function ambilbrgwarna($ikodebrg,$kodeprsh)
	{
		$this->db->select("a.*, b.e_nama_warna, c.e_nama_brg");
		$this->db->from("tr_barang_warna a");
		$this->db->join("tr_warna b","a.i_id_warna=b.i_id_warna");
		$this->db->join("tr_barang c","c.i_kode_brg=a.i_kode_brg");
		$this->db->where("a.i_kode_perusahaan",$kodeprsh);
		$this->db->where("a.i_kode_brg",$ikodebrg);
		$this->db->order_by("b.e_nama_warna");
		return $this->db->get();
	}
}