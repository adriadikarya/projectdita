<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Mainmodel extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function loggedin()
	{
		return $this->session->userdata('loggedin');
	}

	function check_login($tabel, $kolom1, $kolom2)
	{
		$this->db->select("a.*,b.e_nama_perusahaan");
		$this->db->from($tabel);
		$this->db->join("tr_perusahaan b","a.i_kode_perusahaan=b.i_kode_perusahaan");
		$this->db->where($kolom1);
		$this->db->where($kolom2);
		// $this->db->where("i_kode_perusahaan",$perusahaan);
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows()==0)
		{
			return FALSE;
		} else {
			return $query->result();
		}
	}

	function daftarperusahaan()
	{
		return $this->db->get("tr_perusahaan")->result();
	}

	function inputuser($iduser,$namauser,$password,$perusahaan,$level,$now)
	{
		$data = array(
			'i_nama_user' 		=> $iduser,
			'e_nama_user' 		=> $namauser,
			'i_password'  		=> $password,
			'i_level'     		=> $level,
			'i_user_aktif'		=> 1,
			'd_user_gabung'		=> $now,
			'i_kode_perusahaan' => $perusahaan,
		);
		return $this->db->insert("tm_user",$data);
	}
}
