<?php
/**
 * 
 */
class C_barang extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('barang/M_barang');
	}

	function index()
	{
		if($this->session->userdata('loggedin'))
		{
			if($this->input->post("kodebrg")!='' && $this->input->post("namabrg") != '')
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$data['i_kode_brg']   		= $this->input->post("kodebrg",TRUE);
				$data['e_nama_brg']   		= $this->input->post("namabrg",TRUE);
				$data['v_hjp'] 	      		= $this->input->post("hjp",TRUE);
				$data['v_hpp']		  		= $this->input->post("hpp",TRUE);
				$data['i_kode_perusahaan']  = $this->session->userdata('kode_perusahaan');
				$data['d_input']			= $now;

				$this->db->trans_begin();
				$insert = $this->M_barang->insertdata($data);

				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
				// cek jika query berhasil
			    if ($insert) {
			    	// Set success message
			    	$message = array('status' => true, 'message' => 'Berhasil Menambah Data Barang '.$data['i_kode_brg']);
				} else {
			    	// Set error message
			    	$message = array('status' => false, 'message' => 'Gagal Menambah Data Barang '.$data['i_kode_brg']);
			    }
			    // simpan message sebagai session
			    $this->session->set_flashdata('message', $message);
			    // refresh page
			    redirect('barang/C_barang');
			}
			$data['page_title'] = "Barang";
			$data['litle_title'] = "Form Input";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$data['contentmodal'] = '';
			$data['kodebrg'] = $this->M_barang->ambilkodebrg($kodeprsh);
			$data['lbrg'] = $this->M_barang->daftarbrg($kodeprsh)->result();
			$datalay['content'] = $this->load->view('barang/vmainform',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function edit()
	{
		if($this->session->userdata('loggedin'))
		{
			if($this->input->post("kodebrg")!='' && $this->input->post("namabrg") != '')
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$i_kode_brg   				= $this->input->post("kodebrg",TRUE);
				$data['e_nama_brg']   		= $this->input->post("namabrg",TRUE);
				$data['v_hjp'] 	      		= $this->input->post("hjp",TRUE);
				$data['v_hpp']		  		= $this->input->post("hpp",TRUE);
				$data['i_kode_perusahaan']  = $this->session->userdata('kode_perusahaan');
				$data['d_update']			= $now;

				$this->db->trans_begin();
				$insert = $this->M_barang->updatedata($data,$i_kode_brg,$data['i_kode_perusahaan']);

				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
				// cek jika query berhasil
			    if ($insert) {
			    	// Set success message
			    	$message = array('status' => true, 'message' => 'Berhasil Mengubah Data Barang '.$i_kode_brg);
				} else {
			    	// Set error message
			    	$message = array('status' => false, 'message' => 'Gagal Mengubah Data Barang '.$i_kode_brg);
			    }
			    // simpan message sebagai session
			    $this->session->set_flashdata('message', $message);
			    // refresh page
			    redirect('barang/C_barang');
			}
			$ikodebrg = $this->uri->segment(4);
			$data['page_title'] = "Barang";
			$data['litle_title'] = "Form Input";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			// $data['kodebrg'] = $this->M_barang->ambilkodebrg($kodeprsh);
			$qcaribrg = $this->M_barang->caribrg($kodeprsh,$ikodebrg);
			if($qcaribrg->num_rows()>0)
			{
				$rcaribrg = $qcaribrg->row();
				$data['kodebrg'] = $rcaribrg->i_kode_brg;
				$data['namabrg'] = $rcaribrg->e_nama_brg;
				$data['hjp']     = $rcaribrg->v_hjp;
				$data['hpp']     = $rcaribrg->v_hpp;
			} else {
				$data['kodebrg'] = '';
				$data['namabrg'] = '';
				$data['hjp']     = 0;
				$data['hpp']     = 0;
			}
			$data['lbrg'] = $this->M_barang->daftarbrg($kodeprsh)->result();
			$datalay['content'] = $this->load->view('barang/vformedit',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function delete()
	{
		if($this->session->userdata('loggedin')){
			$ikode = $this->uri->segment(4);
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$this->db->trans_begin();
			$qdelete = $this->M_barang->deletedata($ikode,$kodeprsh);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
			// cek jika query berhasil
			if ($qdelete) {
				// Set success message
			    $message = array('status' => true, 'message' => 'Berhasil Menghapus Data Barang '.$ikode);
			} else {
				// Set error message
			    $message = array('status' => false, 'message' => 'Gagal Menghapus Data Barang '.$ikode);
			}
			// simpan message sebagai session
			$this->session->set_flashdata('message', $message);
			// refresh page
			redirect('barang/C_barang');
		} else {
			redirect('Main');
		}
	}

	function listwarna()
	{
		if($this->session->userdata('loggedin'))
		{
			$ikodebrg = $this->uri->segment(4);
			$kodeprsh = $this->session->userdata("kode_perusahaan");
			$data_modal['isibrgwarna'] = $this->M_barang->ambilbrgwarna($ikodebrg,$kodeprsh)->result();
			$data_modal['totalrows']   = $this->M_barang->ambilbrgwarna($ikodebrg,$kodeprsh)->num_rows();
			$data_modal['title_modal'] = "List Warna";
			$this->load->view("barang/vlistwarna",$data_modal);
		} else {
			redirect('Main');
		}
	}
}