<!-- Main content -->
<section class="content">
	<div class="modal fade" id="myModal" role="dialog">
  		<div class="modal-dialog modal-sm">
  			<!-- Modal content-->
        	<div class="modal-content">
  			</div>     
  		</div>
  	</div>
	<div class="row">
		<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">List Barang</a></li>
              <li><a href="#tab_2" data-toggle="tab">Form Input Barang</a></li>
              <!-- <li><a href="#tab_3" data-toggle="tab">Tab 3</a></li> -->
          	</ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane  active" id="tab_1">
            	<div class="row">
					<div class="col-lg-12 col-sm-12">
						<div class="box">
			            <div class="box-header">
			              <h3 class="box-title">Data Barang</h3>
			            <?php if($message = $this->session->flashdata('message')): ?>
			                <div class="alert <?php echo ($message['status']) ? 'alert-success' : 'alert-danger'; ?>" >
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $message['message']; ?>
			                </div>
			            <?php endif; ?>
			            </div>
			            <!-- /.box-header -->
			            <div class="box-body">
			              <table id="datatabel" class="table table-bordered table-hover display nowrap" style="width:100%">
			              	<thead>
			              		<th>Kode</th>
			              		<th>Nama</th>
			              		<th>Harga Jual</th>
			              		<th>Harga Awal</th>
			              		<th>Detail Warna</th>
			              		<th>Action</th>
			              	</thead>
			              	<tbody>
			              		<?php
			              			if(!empty($lbrg))
			              			{	
			              				foreach ($lbrg as $row) {
			              				$urldelete = "C_barang/delete/".$row->i_kode_brg;
			              		?>
			              			<tr>
			              				<td><?php echo $row->i_kode_brg; ?></td>
			              				<td><?php echo $row->e_nama_brg; ?></td>
			              				<td><?php echo $row->v_hjp; ?></td>
			              				<td><?php echo $row->v_hpp; ?></td>
			              				<td>
			              					<a id="myBtn" data-toggle="modal" data-target="#myModal" href="<?php echo base_url('barang/C_barang/listwarna/' .$row->i_kode_brg); ?>"><button class="btn btn-info btn-rounded btn-sm">Detail Warna</i></button></a>
			              				</td>
			              				<td align="center">
			              					<a href="<?php echo base_url('barang/C_barang/edit/' .$row->i_kode_brg); ?>"><button class="btn btn-success btn-rounded btn-sm"><i class="glyphicon glyphicon-edit"></i></button></a>
			                                <a alt="Delete" id="delete" href="#"><button class="btn btn-danger btn-rounded btn-sm" onclick="return hapus('<?php echo $urldelete; ?>');"><i class="glyphicon glyphicon-trash"></i></button></a>
			              				</td>
			              			</tr>
			              		<?php
			              				}
			              			} else {
								?>
									<tr>
										<td colspan="6" style="text-align: center">Maaf Tidak Ada Data!</td>
									</tr>
								<?php              				
			              			}
			              		?>
			              	</tbody>
			              </table>
			          	</div>
						</div>
					</div>
				</div>
            </div>
            <div class="tab-pane" id="tab_2">
            	<div class="row">
			        <!-- left column -->
			        <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
			          	<!-- general form elements -->
			          	<div class="box box-primary">
			            	<div class="box-header with-border">
			        		    <h3 class="box-title">From Input Barang</h3>
			    	        </div>
			    	        <!-- /.box-header -->
				            <!-- form start -->
				            <form role="form" method="post" action="<?php echo base_url() ?>barang/C_barang">
				              <div class="box-body">
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="kodebrg">Kode Barang</label>
				                  <input type="text" class="form-control" id="kodebrg" name="kodebrg" placeholder="Isi Kode Barang" value="<?php echo $kodebrg; ?>" required maxlength="10" readonly>
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="namabrg">Nama Barang</label>
				                  <input type="text" class="form-control" id="namabrg" name="namabrg" placeholder="Isi Nama Pelanggan" required maxlength="120">
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="hjp">Harga Jual</label>
				                  <input type="text" class="form-control" id="hjp" name="hjp" placeholder="0" value="0">
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="hpp">Harga Awal</label>
				                  <input type="text" class="form-control" id="hpp" name="hpp" placeholder="0" value="0">
				                </div>
				              </div>
				              <!-- /.box-body -->

				              <div class="box-footer">
				                <button type="submit" name="submit" class="btn btn-success">Simpan</button>
				                <a href="<?php echo base_url() ?>barang/C_barang" class="btn btn-default">Kembali</a>
				              </div>
				            </form>
				        </div>
			        </div>
			    </div>
            </div>
        </div>
	</div>
    
</section>