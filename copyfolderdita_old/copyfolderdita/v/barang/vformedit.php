<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Form Edit Barang</a></li>
              <!-- <li><a href="#tab_2" data-toggle="tab">Form Edit Barang</a></li> -->
              <!-- <li><a href="#tab_3" data-toggle="tab">Tab 3</a></li> -->
          	</ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
            	<div class="row">
			        <!-- left column -->
			        <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
			          	<!-- general form elements -->
			          	<div class="box box-primary">
			            	<div class="box-header with-border">
			        		    <h3 class="box-title">From Edit Barang</h3>
			    	        </div>
			    	        <!-- /.box-header -->
				            <!-- form start -->
				            <form role="form" method="post" action="<?php echo base_url() ?>barang/C_barang/edit">
				              <div class="box-body">
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="kodebrg">Kode Barang</label>
				                  <input type="text" class="form-control" id="kodebrg" name="kodebrg" placeholder="Isi Kode Barang" value="<?php echo $kodebrg; ?>" required maxlength="10" readonly>
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="namabrg">Nama Barang</label>
				                  <input type="text" class="form-control" id="namabrg" name="namabrg" placeholder="Isi Nama Pelanggan" value="<?php echo $namabrg; ?>" required maxlength="120">
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="hjp">Harga Jual</label>
				                  <input type="text" class="form-control" id="hjp" name="hjp" placeholder="0" value="<?php echo $hjp; ?>">
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="hpp">Harga Awal</label>
				                  <input type="text" class="form-control" id="hpp" name="hpp" placeholder="0" value="<?php echo $hpp; ?>">
				                </div>
				              </div>
				              <!-- /.box-body -->

				              <div class="box-footer">
				                <button type="submit" name="submit" class="btn btn-info">Update</button>
				                <a href="<?php echo base_url() ?>barang/C_barang" class="btn btn-default">Kembali</a>
				              </div>
				            </form>
				        </div>
			        </div>
			    </div>
            </div>
        </div>
	</div>
    
</section>