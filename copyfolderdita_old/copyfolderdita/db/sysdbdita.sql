-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Bulan Mei 2019 pada 02.15
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sysdbdita`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_user`
--

CREATE TABLE `tm_user` (
  `i_nama_user` varchar(20) NOT NULL,
  `e_nama_user` varchar(50) NOT NULL,
  `i_password` varchar(50) NOT NULL,
  `i_level` int(3) NOT NULL,
  `i_user_aktif` int(1) NOT NULL,
  `d_user_gabung` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `i_kode_perusahaan` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_user`
--

INSERT INTO `tm_user` (`i_nama_user`, `e_nama_user`, `i_password`, `i_level`, `i_user_aktif`, `d_user_gabung`, `i_kode_perusahaan`) VALUES
('adminA', 'adriadikarya', '21232f297a57a5a743894a0e4a801fc3', 1, 1, '2019-05-12 07:29:25', 'A'),
('adminB', 'karya', '21232f297a57a5a743894a0e4a801fc3', 1, 1, '2019-05-12 07:29:56', 'B');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_cabang`
--

CREATE TABLE `tr_cabang` (
  `i_cabang` int(11) NOT NULL,
  `i_pelanggan` int(11) NOT NULL,
  `i_kode_cabang` varchar(10) DEFAULT NULL,
  `e_nama_cabang` varchar(200) NOT NULL,
  `e_alamat_cabang` varchar(255) NOT NULL,
  `e_kota_cabang` varchar(50) DEFAULT NULL,
  `e_inisial` varchar(10) DEFAULT NULL,
  `i_kode_perusahaan` varchar(2) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tr_cabang`
--

INSERT INTO `tr_cabang` (`i_cabang`, `i_pelanggan`, `i_kode_cabang`, `e_nama_cabang`, `e_alamat_cabang`, `e_kota_cabang`, `e_inisial`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
(2, 1, 'WKM1', 'PT. Wahana Kasih Mulia (01)', 'Jl. Jawa', 'Jawa Timur', 'WKM(01)', 'A', '2019-05-12 10:18:39', '2019-05-12 10:18:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_grup_pelanggan`
--

CREATE TABLE `tr_grup_pelanggan` (
  `i_id_grup` int(11) NOT NULL,
  `i_kode_grup` varchar(4) NOT NULL,
  `e_nama_grup` varchar(50) NOT NULL,
  `i_kode_perusahaan` varchar(4) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tr_grup_pelanggan`
--

INSERT INTO `tr_grup_pelanggan` (`i_id_grup`, `i_kode_grup`, `e_nama_grup`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
(1, 'Dst', 'Distributor Update', 'A', '2019-05-12 05:58:11', '2019-05-12 07:23:53'),
(2, 'TK', 'Toko Kelontong', 'A', '2019-05-12 07:23:30', NULL),
(4, 'Dis', 'Distributor', 'B', '2019-05-12 07:30:52', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_pelanggan`
--

CREATE TABLE `tr_pelanggan` (
  `i_pelanggan` int(11) NOT NULL,
  `i_kode_pelanggan` varchar(6) NOT NULL,
  `e_nama_pelanggan` varchar(255) NOT NULL,
  `e_alamat_pelanggan` varchar(200) NOT NULL,
  `f_pelanggan_pkp` tinyint(1) NOT NULL DEFAULT '0',
  `e_npwp` varchar(20) NOT NULL DEFAULT '000.000.000.000.000',
  `n_top` int(5) NOT NULL DEFAULT '30',
  `e_telp_pelanggan` varchar(50) DEFAULT NULL,
  `e_fax_pelanggan` varchar(50) DEFAULT NULL,
  `e_kontak_pelanggan` varchar(150) DEFAULT NULL,
  `i_id_grup` int(11) NOT NULL DEFAULT '0',
  `e_ktp_pelanggan` varchar(25) DEFAULT NULL,
  `n_ekspedisi_pelanggan` int(11) DEFAULT '0',
  `i_kode_perusahaan` varchar(2) NOT NULL DEFAULT '0',
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tr_pelanggan`
--

INSERT INTO `tr_pelanggan` (`i_pelanggan`, `i_kode_pelanggan`, `e_nama_pelanggan`, `e_alamat_pelanggan`, `f_pelanggan_pkp`, `e_npwp`, `n_top`, `e_telp_pelanggan`, `e_fax_pelanggan`, `e_kontak_pelanggan`, `i_id_grup`, `e_ktp_pelanggan`, `n_ekspedisi_pelanggan`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
(1, 'WKM', 'PT. Wahana Kasih Mulia', 'jl.industri 1 no.1', 1, '000.000.000.000.000', 30, '(022)7452154', '', 'Dita', 1, '', 0, 'A', NULL, NULL),
(2, 'DGU', 'PT.DIalogue Garmindo Utama', 'Jl. Industri 1 No.1 (Dekat Rs Kasih Bunda)', 1, '015.485.717.441.000', 45, '(022)7452153', '', 'Ana', 1, '', 0, 'A', '2019-05-12 08:58:09', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_perusahaan`
--

CREATE TABLE `tr_perusahaan` (
  `i_kode_perusahaan` varchar(3) NOT NULL,
  `e_nama_perusahaan` varchar(100) NOT NULL,
  `e_alamat` varchar(200) DEFAULT NULL,
  `e_npwp` varchar(25) DEFAULT NULL,
  `e_telepon` varchar(16) DEFAULT NULL,
  `e_fax` varchar(30) DEFAULT NULL,
  `e_kontak` varchar(100) DEFAULT NULL,
  `i_status_aktif` int(11) DEFAULT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tr_perusahaan`
--

INSERT INTO `tr_perusahaan` (`i_kode_perusahaan`, `e_nama_perusahaan`, `e_alamat`, `e_npwp`, `e_telepon`, `e_fax`, `e_kontak`, `i_status_aktif`, `d_input`, `d_update`) VALUES
('A', 'Perusahaan 1', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, 1, '2019-05-11 07:52:38', '2019-05-11 07:52:38'),
('B', 'Perusahaan 2', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, 1, '2019-05-11 07:52:36', '2019-05-11 07:52:36'),
('C', 'Perusahaan 3', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, 1, '2019-05-11 07:52:31', NULL),
('D', 'Perusahaan 4', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, 1, '2019-05-11 07:52:57', NULL),
('E', 'Perusahaan 5', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, 1, '2019-05-11 07:53:07', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tm_user`
--
ALTER TABLE `tm_user`
  ADD PRIMARY KEY (`i_nama_user`);

--
-- Indeks untuk tabel `tr_cabang`
--
ALTER TABLE `tr_cabang`
  ADD PRIMARY KEY (`i_cabang`,`i_pelanggan`);

--
-- Indeks untuk tabel `tr_grup_pelanggan`
--
ALTER TABLE `tr_grup_pelanggan`
  ADD PRIMARY KEY (`i_id_grup`);

--
-- Indeks untuk tabel `tr_pelanggan`
--
ALTER TABLE `tr_pelanggan`
  ADD PRIMARY KEY (`i_pelanggan`);

--
-- Indeks untuk tabel `tr_perusahaan`
--
ALTER TABLE `tr_perusahaan`
  ADD PRIMARY KEY (`i_kode_perusahaan`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tr_cabang`
--
ALTER TABLE `tr_cabang`
  MODIFY `i_cabang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tr_grup_pelanggan`
--
ALTER TABLE `tr_grup_pelanggan`
  MODIFY `i_id_grup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tr_pelanggan`
--
ALTER TABLE `tr_pelanggan`
  MODIFY `i_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
