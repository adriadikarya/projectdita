-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 14, 2019 at 03:51 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sysdbdita`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_nota`
--

CREATE TABLE `tm_nota` (
  `i_id_nota` int(11) NOT NULL,
  `i_no_nota` varchar(16) NOT NULL,
  `i_no_order` varchar(25) NOT NULL,
  `d_nota` date NOT NULL,
  `d_jth_tempo` date NOT NULL,
  `i_pelanggan` int(11) NOT NULL,
  `v_ppn` double NOT NULL DEFAULT '0',
  `v_pengiriman` double NOT NULL DEFAULT '0',
  `v_total_diskon` double NOT NULL DEFAULT '0',
  `v_total_fppn` double NOT NULL DEFAULT '0',
  `v_total_fppn_sisa` double NOT NULL DEFAULT '0',
  `f_nota_cancel` tinyint(1) NOT NULL DEFAULT '0',
  `f_status_lunas` tinyint(1) NOT NULL DEFAULT '0',
  `i_jenis_pembayaran` int(11) NOT NULL COMMENT '1=Cash, 2=Transfer',
  `e_gudang` varchar(25) DEFAULT NULL,
  `e_kepada` varchar(50) DEFAULT NULL,
  `i_kode_perusahaan` varchar(4) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tm_nota_item`
--

CREATE TABLE `tm_nota_item` (
  `i_id_item` int(11) NOT NULL,
  `i_id_nota` int(11) NOT NULL,
  `i_kode_brg` int(11) NOT NULL,
  `i_id_warna` int(11) NOT NULL DEFAULT '0',
  `n_qty` int(11) NOT NULL,
  `v_harga_satuan` int(11) NOT NULL,
  `n_diskon` double NOT NULL DEFAULT '0',
  `e_desc` varchar(255) DEFAULT NULL,
  `i_no_item` int(11) NOT NULL DEFAULT '1',
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tm_user`
--

CREATE TABLE `tm_user` (
  `i_nama_user` varchar(20) NOT NULL,
  `e_nama_user` varchar(50) NOT NULL,
  `i_password` varchar(50) NOT NULL,
  `i_level` int(3) NOT NULL,
  `i_user_aktif` int(1) NOT NULL,
  `d_user_gabung` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `i_kode_perusahaan` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tm_user`
--

INSERT INTO `tm_user` (`i_nama_user`, `e_nama_user`, `i_password`, `i_level`, `i_user_aktif`, `d_user_gabung`, `i_kode_perusahaan`) VALUES
('adminA', 'adriadikarya', '21232f297a57a5a743894a0e4a801fc3', 1, 1, '2019-05-12 07:29:25', 'A'),
('adminB', 'karya', '21232f297a57a5a743894a0e4a801fc3', 1, 1, '2019-05-12 07:29:56', 'B');

-- --------------------------------------------------------

--
-- Table structure for table `tr_barang`
--

CREATE TABLE `tr_barang` (
  `i_kode_brg` varchar(10) NOT NULL,
  `e_nama_brg` varchar(120) DEFAULT NULL,
  `v_hjp` double NOT NULL DEFAULT '0',
  `v_hpp` double DEFAULT '0',
  `i_kode_perusahaan` varchar(4) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_barang`
--

INSERT INTO `tr_barang` (`i_kode_brg`, `e_nama_brg`, `v_hjp`, `v_hpp`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
('A0001', 'Barang Tes', 4500, 2000, 'A', '2019-05-13 08:13:44', '2019-05-13 08:41:37'),
('A0002', 'Barang Tes 2', 3500, 2000, 'A', '2019-05-14 00:59:35', NULL),
('A0003', 'Barang Tes 3', 10000, 8500, 'A', '2019-05-14 00:59:49', NULL),
('A0004', 'Barang Tes 4', 1600, 800, 'A', '2019-05-14 01:00:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_barang_warna`
--

CREATE TABLE `tr_barang_warna` (
  `i_kode_brg` varchar(10) NOT NULL,
  `i_id_warna` int(11) NOT NULL,
  `i_kode_perusahaan` varchar(4) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_barang_warna`
--

INSERT INTO `tr_barang_warna` (`i_kode_brg`, `i_id_warna`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
('A0001', 1, 'A', '2019-05-14 01:00:35', NULL),
('A0001', 2, 'A', '2019-05-14 01:00:39', NULL),
('A0001', 3, 'A', '2019-05-14 01:00:45', NULL),
('A0002', 4, 'A', '2019-05-14 01:01:08', NULL),
('A0002', 5, 'A', '2019-05-14 01:00:55', NULL),
('A0003', 1, 'A', '2019-05-14 01:01:15', NULL),
('A0004', 1, 'A', '2019-05-14 01:01:25', NULL),
('A0004', 5, 'A', '2019-05-14 01:01:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_cabang`
--

CREATE TABLE `tr_cabang` (
  `i_cabang` int(11) NOT NULL,
  `i_pelanggan` int(11) NOT NULL,
  `i_kode_cabang` varchar(10) DEFAULT NULL,
  `e_nama_cabang` varchar(200) NOT NULL,
  `e_alamat_cabang` varchar(255) NOT NULL,
  `e_kota_cabang` varchar(50) DEFAULT NULL,
  `e_inisial` varchar(10) DEFAULT NULL,
  `i_kode_perusahaan` varchar(2) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_cabang`
--

INSERT INTO `tr_cabang` (`i_cabang`, `i_pelanggan`, `i_kode_cabang`, `e_nama_cabang`, `e_alamat_cabang`, `e_kota_cabang`, `e_inisial`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
(2, 1, 'WKM1', 'PT. Wahana Kasih Mulia (01)', 'Jl. Jawa', 'Jawa Timur', 'WKM(01)', 'A', '2019-05-12 10:18:39', '2019-05-12 10:18:49');

-- --------------------------------------------------------

--
-- Table structure for table `tr_grup_pelanggan`
--

CREATE TABLE `tr_grup_pelanggan` (
  `i_id_grup` int(11) NOT NULL,
  `i_kode_grup` varchar(4) NOT NULL,
  `e_nama_grup` varchar(50) NOT NULL,
  `i_kode_perusahaan` varchar(4) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_grup_pelanggan`
--

INSERT INTO `tr_grup_pelanggan` (`i_id_grup`, `i_kode_grup`, `e_nama_grup`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
(1, 'Dst', 'Distributor Update', 'A', '2019-05-12 05:58:11', '2019-05-12 07:23:53'),
(2, 'TK', 'Toko Kelontong', 'A', '2019-05-12 07:23:30', NULL),
(4, 'Dis', 'Distributor', 'B', '2019-05-12 07:30:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_pelanggan`
--

CREATE TABLE `tr_pelanggan` (
  `i_pelanggan` int(11) NOT NULL,
  `i_kode_pelanggan` varchar(6) NOT NULL,
  `e_nama_pelanggan` varchar(255) NOT NULL,
  `e_alamat_pelanggan` varchar(200) NOT NULL,
  `f_pelanggan_pkp` tinyint(1) NOT NULL DEFAULT '0',
  `e_npwp` varchar(20) NOT NULL DEFAULT '000.000.000.000.000',
  `n_top` int(5) NOT NULL DEFAULT '30',
  `e_telp_pelanggan` varchar(50) DEFAULT NULL,
  `e_fax_pelanggan` varchar(50) DEFAULT NULL,
  `e_kontak_pelanggan` varchar(150) DEFAULT NULL,
  `i_id_grup` int(11) NOT NULL DEFAULT '0',
  `e_ktp_pelanggan` varchar(25) DEFAULT NULL,
  `n_ekspedisi_pelanggan` int(11) DEFAULT '0',
  `i_kode_perusahaan` varchar(4) NOT NULL DEFAULT '0',
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_pelanggan`
--

INSERT INTO `tr_pelanggan` (`i_pelanggan`, `i_kode_pelanggan`, `e_nama_pelanggan`, `e_alamat_pelanggan`, `f_pelanggan_pkp`, `e_npwp`, `n_top`, `e_telp_pelanggan`, `e_fax_pelanggan`, `e_kontak_pelanggan`, `i_id_grup`, `e_ktp_pelanggan`, `n_ekspedisi_pelanggan`, `i_kode_perusahaan`, `d_input`, `d_update`) VALUES
(1, 'WKM', 'PT. Wahana Kasih Mulia', 'jl.industri 1 no.1', 1, '000.000.000.000.000', 30, '(022)7452154', '', 'Dita', 1, '', 0, 'A', NULL, NULL),
(2, 'DGU', 'PT.DIalogue Garmindo Utama', 'Jl. Industri 1 No.1 (Dekat Rs Kasih Bunda)', 1, '015.485.717.441.000', 45, '(022)7452153', '', 'Ana', 1, '', 0, 'A', '2019-05-12 08:58:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_perusahaan`
--

CREATE TABLE `tr_perusahaan` (
  `i_kode_perusahaan` varchar(3) NOT NULL,
  `i_tipe_perusahaan` int(11) NOT NULL,
  `e_nama_perusahaan` varchar(100) NOT NULL,
  `e_alamat` varchar(200) DEFAULT NULL,
  `e_npwp` varchar(25) DEFAULT NULL,
  `e_telepon` varchar(16) DEFAULT NULL,
  `e_fax` varchar(30) DEFAULT NULL,
  `e_kontak` varchar(100) DEFAULT NULL,
  `e_no_rek1` varchar(30) DEFAULT NULL,
  `e_nama_bank1` varchar(30) DEFAULT NULL,
  `e_atas_nama1` varchar(100) DEFAULT NULL,
  `e_no_rek2` varchar(30) NOT NULL,
  `e_nama_bank2` varchar(30) NOT NULL,
  `e_atas_nama2` varchar(100) NOT NULL,
  `i_status_aktif` int(11) DEFAULT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_perusahaan`
--

INSERT INTO `tr_perusahaan` (`i_kode_perusahaan`, `i_tipe_perusahaan`, `e_nama_perusahaan`, `e_alamat`, `e_npwp`, `e_telepon`, `e_fax`, `e_kontak`, `e_no_rek1`, `e_nama_bank1`, `e_atas_nama1`, `e_no_rek2`, `e_nama_bank2`, `e_atas_nama2`, `i_status_aktif`, `d_input`, `d_update`) VALUES
('A', 1, 'Perusahaan 1', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 1, '2019-05-11 07:52:38', '2019-05-11 07:52:38'),
('B', 1, 'Perusahaan 2', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 1, '2019-05-11 07:52:36', '2019-05-11 07:52:36'),
('C', 1, 'Perusahaan 3', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 1, '2019-05-11 07:52:31', NULL),
('D', 1, 'Perusahaan 4', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 1, '2019-05-11 07:52:57', NULL),
('E', 2, 'Perusahaan 5', 'Cimahi', '000.000.000.000.000', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 1, '2019-05-11 07:53:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_warna`
--

CREATE TABLE `tr_warna` (
  `i_id_warna` int(11) NOT NULL,
  `e_nama_warna` varchar(25) NOT NULL,
  `d_input` timestamp NULL DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_warna`
--

INSERT INTO `tr_warna` (`i_id_warna`, `e_nama_warna`, `d_input`, `d_update`) VALUES
(1, 'Biru', '2019-05-14 00:58:06', NULL),
(2, 'Merah', '2019-05-14 00:58:09', NULL),
(3, 'Merah Maroon', '2019-05-14 00:58:21', NULL),
(4, 'Kuning', '2019-05-14 00:58:26', NULL),
(5, 'Coklat', '2019-05-14 00:58:31', NULL),
(6, 'Pink', '2019-05-14 00:58:37', NULL),
(7, 'Ungu', '2019-05-14 00:58:41', NULL),
(8, 'Hijau', '2019-05-14 00:58:45', NULL),
(9, 'Hitam', '2019-05-14 00:58:49', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tm_nota`
--
ALTER TABLE `tm_nota`
  ADD PRIMARY KEY (`i_id_nota`,`i_no_nota`);

--
-- Indexes for table `tm_nota_item`
--
ALTER TABLE `tm_nota_item`
  ADD PRIMARY KEY (`i_id_item`,`i_id_nota`,`i_no_item`);

--
-- Indexes for table `tm_user`
--
ALTER TABLE `tm_user`
  ADD PRIMARY KEY (`i_nama_user`);

--
-- Indexes for table `tr_barang`
--
ALTER TABLE `tr_barang`
  ADD PRIMARY KEY (`i_kode_brg`);

--
-- Indexes for table `tr_barang_warna`
--
ALTER TABLE `tr_barang_warna`
  ADD PRIMARY KEY (`i_kode_brg`,`i_id_warna`);

--
-- Indexes for table `tr_cabang`
--
ALTER TABLE `tr_cabang`
  ADD PRIMARY KEY (`i_cabang`,`i_pelanggan`);

--
-- Indexes for table `tr_grup_pelanggan`
--
ALTER TABLE `tr_grup_pelanggan`
  ADD PRIMARY KEY (`i_id_grup`);

--
-- Indexes for table `tr_pelanggan`
--
ALTER TABLE `tr_pelanggan`
  ADD PRIMARY KEY (`i_pelanggan`);

--
-- Indexes for table `tr_perusahaan`
--
ALTER TABLE `tr_perusahaan`
  ADD PRIMARY KEY (`i_kode_perusahaan`);

--
-- Indexes for table `tr_warna`
--
ALTER TABLE `tr_warna`
  ADD PRIMARY KEY (`i_id_warna`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tm_nota`
--
ALTER TABLE `tm_nota`
  MODIFY `i_id_nota` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tm_nota_item`
--
ALTER TABLE `tm_nota_item`
  MODIFY `i_id_item` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_cabang`
--
ALTER TABLE `tr_cabang`
  MODIFY `i_cabang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tr_grup_pelanggan`
--
ALTER TABLE `tr_grup_pelanggan`
  MODIFY `i_id_grup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tr_pelanggan`
--
ALTER TABLE `tr_pelanggan`
  MODIFY `i_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tr_warna`
--
ALTER TABLE `tr_warna`
  MODIFY `i_id_warna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
