<style type="text/css">
  body {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; }
   
   .isinya {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;}
    
    .isinya2 {
    font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
    font-size: 12px;
    border-collapse:collapse;
    border:1px solid black;}

</style>

<style type="text/css" media="print">
    @page{
      size: auto;
      margin: 5.0mm;
    }
    .printable{ 
      page-break-after: always;
    }
    .no_print{
      display: none;
    }
</style>
<table class="isinya2" border='1' align="center" width="100%">
  <tr>
    <td>
      <table class="isinya" border='0' align="center" width="90%">
        <tr>
          <?php 
            if(!empty($isicompany)){
              foreach ($isicompany as $row) {
          ?>
                <td style="width: 35%"><b><?php echo $row->e_company_name; ?></b></td>
                <td>&nbsp;</td>
                <td align="right"><?php echo $row->e_company_city.", ".date('d F Y', strtotime($isihead->d_op)); ?></td>
          <?php
              }
            }else{
              echo "<td>Kosong</td>";
              echo "<td>&nbsp;</td>";
              echo "<td>&nbsp;</td>";
            }
          ?>
        </tr>
        <tr>
          <?php 
            if(!empty($isicompany)){
              foreach ($isicompany as $row) {
          ?>
                <td style="width: 35%"><?php echo $row->e_company_address; ?></td>
                <td>Kepada Yth.</td>
                <td>&nbsp;</td>
          <?php
              }
            }else{
              echo "<td>Kosong</td>";
              echo "<td>Kepada Yth.</td>";
              echo "<td>&nbsp;</td>";
            }
          ?>
        </tr>
        <tr>
          <?php 
            if(!empty($isicompany)){
              foreach ($isicompany as $row) {
          ?>
                <td style="width: 35%"><?php echo $row->e_company_city; ?></td>
          <?php
                if(!empty($isihead))
                {
          ?>
                    <td><?php echo $isihead->e_supplier_name ?></td>
          <?php 
                } else {
                  echo "<td>Kosong</td>";
                }
          ?>
                <td>&nbsp;</td>
          <?php
              }
            }else{
              echo "<td>Kosong</td>";
              echo "<td>Kepada Yth.</td>";
              echo "<td>&nbsp;</td>";
            }
          ?>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="center" style="font-size: 20px"><b><?php echo $page_title ?></b></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Dengan Hormat,</td>
          <td>&nbsp;</td>
          <td align="right">Jenis Pembelian: <?php if($isihead->i_payment_type=='1') echo "Kredit"; else echo "Cash"; ?></td>
        </tr>
        <tr>
          <td>mohon segera dikirim barang ini:</td>
          <td>&nbsp;</td>
          <td align="right">Nomor OP: <?php echo $isihead->i_op; ?></td>
        </tr>
        <tr>
          <td>Tgl Delivery: 
          <?php 
            // $next_date = date('y-m-d', strtotime(date('y-m-d') . "+1 days")); 
            if($isihead->d_deliv=='')
            {
              echo '';
            } else {
              echo date('d F Y', strtotime($isihead->d_deliv));  
            }
            // echo $isihead->d_deliv;
          ?>
          </td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Tgl Expired: 
          <?php 
            // $next_date = date('y-m-d', strtotime(date('y-m-d') . "+1 days")); 
            if($isihead->d_expired=='')
            {
              echo '';
            } else {
              echo date('t F Y', strtotime($isihead->d_expired));  
            }
            // echo $isihead->d_expired;
          ?>
          </td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="5">
            <table border='1' class="isinya2" width="100%">
              <thead>
                <th width="5%">No</th>
                <th width="40%">Nama Barang</th>
                <th width="5%">Satuan</th>
                <th width="5%">Qty</th>
                <th width="5%">Harga (Rp.)</th>
                <!--<th width="10%">Diskon</th>
                <th width="10%">PPN</th>-->
                <th width="15%">Subtotal (Rp.)</th>
                <th width="10%">Keterangan</th>
              </thead>
              <tbody>
                <?php 
                $i=0;
                $totalall=0;
                  if(!empty($isiitem))
                  {
                    foreach($isiitem as $row)
                    {
                      $i++;
                      $subtotal = $row->n_quantity*$row->v_price;
                ?>
                  <tr>
                    <td align="center"><?php echo $i ?></td>
                    <td align="left"><?php echo $row->e_material_name ?></td>
                    <td align="left"><?php echo $row->e_satuan ?></td>
                    <td align="right"><?php echo $row->n_quantity ?></td>
                    <td align="right"><?php echo $row->v_price ?></td>
                    <td align="right"><?php echo number_format($subtotal,2) ?></td>
                    <td align="left"><?php echo $row->e_remark ?></td>
                  </tr>
                <?php
                      $totalall+=$subtotal;
                    }
                ?> 
                  <tr>
                    <td colspan="5" align="right">Total:</td>
                    <td align="right"><?php echo "Rp.".number_format($totalall,2); ?></td>
                    <td>&nbsp;</td>
                  </tr>
                <?php
                  } else {
                    echo "<tr><td colspan=\"7\" style=\"text-align:center;\">Maaf Tidak Ada Data Item</td></tr>";
                    echo "<tr><td colspan=\"7\" style=\"text-align:center;\">&nbsp;</td></tr>";
                  }
                ?>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td align="center">Hormat Kami,</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td align="center">Dibuat<br>Bag. Pembelian</td>
          <td align="center">PPIC</td>
          <td align="center">Mengetahui,<br>Bag. Keuangan</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <?php 
            if(!empty($isicompany)){
              foreach ($isicompany as $row) {
          ?>
                <td align="center"><u><?php echo $row->e_bag_pemb ?></u></td>
                <td align="center"><u>Hendrik Setiawan</u></td>
                <td align="center"><u><?php echo $row->e_kepala_bagian ?></u></td>
          <?php 
              }
            } else {
                echo "<td align=\"center\"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>";
                echo "<td align=\"center\"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>";
                echo "<td align=\"center\"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>";
            }
          ?>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<br>
<div align="center"><a style="font-size: 20px;" href="#" class="no_print" onclick="window.print()"><b><u>Print</u></b></a></div>