<style type="text/css">
	.tinggi{
		height: 30px !important;
	}
	.marjin{
		margin-bottom: 10px;
	}
	.bottom{
		margin-bottom: -5px; 
	}
	.td{
		padding:0px !important; 
	}
	.a{
		padding:0px !important; 
	}
</style>
<!DOCTYPE html>
<html>
<head>
	<title><?Php echo $page_title; ?></title>
</head>
<body>
	<?php echo form_open('btb/Cform/tambahsatkonv/'.$isatuan.'/'.$esatuan.'/'.$baris.'/add/', array('id' => 'satuankonv'));?>	
	<table>
		<center><b><?php echo strtoupper($page_title) ?></b></center>
		<tr>
			<td>Satuan Awal</td>
			<td>:</td>
			<td>
				<input class="tinggi" type="text" name="esatawal" id="esatawal" value="<?php echo $esatuan ?>" readonly>
				<input class="tinggi" type="hidden" name="isatawal" id="isatawal" value="<?php echo $isatuan ?>" readonly>
				<input class="tinggi" type="hidden" name="baris" id="baris" value="<?php echo $baris ?>" readonly>
			</td>
		</tr>
		<tr>
			<td>Satuan Konversi</td>
			<td>:</td>
			<td>
				<select name="satuankonv" id="satuankonv" class="tinggi">
					<option value="">Pilih Satuan</option>
				<?php 
				if(!(empty($isisatuan))){
					foreach ($isisatuan as $row) {
						echo "<option value=\"".$row->i_satuan."-".$row->e_satuan."\">".$row->e_satuan."</option>";
					}
				} else {
					echo "<option value=\"0\">Tidak Ada Pilihan Satuan</option>";
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Rumus Konversi</td>
			<td>:</td>
			<td>
				<select name="rumus" id="rumus" class="tinggi">
					<option value="">Pilih Rumus</option>
					<option value="1">Qty Sat Konv = QtySatAwal * Angka Faktor</option>
					<option value="2">Qty Sat Konv = QtySatAwal / Angka Faktor</option>
					<option value="3">Qty Sat Konv = QtySatAwal + Angka Faktor</option>
					<option value="4">Qty Sat Konv = QtySatAwal - Angka Faktor</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Angka Faktor</td>
			<td>:</td>
			<td><input type="text" class="tinggi" name="angkafaktor" id="angkafaktor"></td>
		</tr>
		<tr>
			<td colspan="3"><input type="submit" name="simpan" id="simpan" value="Simpan" onclick="return validasi();">
			<input type="button" name="batal" id="batal" value="Keluar" onclick="batalkan();"></td>
		</tr>
	</table>
	<?php form_close() ?>
</body>
</html>
<script type="text/javascript">
	function validasi()
	{
		var satuankonv = document.getElementById('satuankonv').value;
		var rumus = document.getElementById('rumus').value;
		var angkafaktor = document.getElementById('angkafaktor').value;

		if(satuankonv=='')
		{
			alert('Pilih Satuan Konversi!');
			return false;
		} else if(rumus==''){
			alert('Pilih Rumus');
			return false;
		} else if(angkafaktor=='') {
			alert('Isi Angka Faktor');
			return false;
		} else {
			// document.getElementById('satuankonv').disabled=true;
			// document.getElementById('rumus').disabled=true;
			// document.getElementById('angkafaktor').disabled=true;
			document.getElementById('simpan').setAttribute('disabled',true);
			document.getElementById('batal').setAttribute('disabled',true);
			return true;
		}
	}
	function batalkan()
	{
		var baris = document.getElementById('baris').value;
		opener.document.getElementById("konversi"+baris).value='tidak'
		opener.document.getElementById("isatkonv"+baris).value='0';
		opener.document.getElementById("esatkonv"+baris).value='Tidak Ada';
		opener.document.getElementById("iformula"+baris).value='0';
		opener.document.getElementById("nformulafactor"+baris).value='0';
		this.close();
	}
</script>