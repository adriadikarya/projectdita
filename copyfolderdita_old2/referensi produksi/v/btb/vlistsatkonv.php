<style>
	.padd {
		padding: 0px !important;
		margin-bottom: -15px;
		font-size:12px;
	}
	.padde{
		margin-bottom: 4px;
		}
	.tinggi{
		height: 25px !important;
	}
	.marjin{
		margin-bottom: 10px;
	}
	.bottom{
		margin-bottom: -5px; 
	}
	.td{
		padding:0px !important; 
	}
	.a{
		padding:0px !important; 
	}
</style>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Dialogue Garmindo Utama : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php
		$tujuan = 'btb/Cform/satkonversi';
	?>
	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="table table-bordered padd">
	    <thead>
	      <tr>
		<td colspan="5" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"><input type="hidden" id="baris" name="baris" value="<?php echo $baris;?>"></td>
	      </tr>
	    </thead>
			<th class="padd">Satuan Konversi</th>
	    	<th class="padd">Rumus</th>
	    	<th class="padd">Angka Faktor</th>
	    <tbody>
	      <?php
		if(!empty($isi)){
			foreach($isi as $row){
				//$kor = str_replace('"', '&quot;', $row->nama_brg);
				//$kor = str_replace("'", '&quot;', $row->nama_brg);
				// $ematerialname = str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($row->e_material_name ) );
				//$kor = str_replace("'", '', $row->nama_brg);
				//echo $kor;
			  echo "<tr>
				  <td class=\"padd\"><a href=\"javascript:setValue('$row->i_satuan','$row->e_satuan',$baris,'$row->i_satuan_konversi','$row->namasatuan','$row->i_formula','$row->n_formula_factor')\">$row->namasatuan</a></td>
				  <td class=\"padd\"><a href=\"javascript:setValue('$row->i_satuan','$row->e_satuan',$baris,'$row->i_satuan_konversi','$row->namasatuan','$row->i_formula','$row->n_formula_factor')\">$row->nmformula</a></td>
				  <td class=\"padd\"><a href=\"javascript:setValue('$row->i_satuan','$row->e_satuan',$baris,'$row->i_satuan_konversi','$row->namasatuan','$row->i_formula','$row->n_formula_factor')\">$row->n_formula_factor</a></td>
				</tr>";
			}
		} else {
			echo "<tr>
				<td class=\"padd\" colspan=\"3\" style=\"text-align:center;\">Maaf Tidak Ada Data Konversi!</td>
				</tr>
			";
		}	
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center class=\"padde\">".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center class="padde"><input type="button" id="batal" name="batal" value="Keluar" class="btn btn-warning" onclick="ebatal()"></center>
  	</div>
      </div>
      </div>
      <?php form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</body>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e,f,g)
  {
	document.getElementById("isatkonv"+c).value=d;
	document.getElementById("esatkonv"+c).value=e;
	document.getElementById("iformula"+c).value=f;
	document.getElementById("nformulafactor"+c).value=g;
	jsDlgHide("#konten *", "#fade", "#light");
  }
  function ebatal(){	  
  	var c = document.getElementById('baris').value;
  	document.getElementById("konversi"+c).value='tidak'
	document.getElementById("isatkonv"+c).value='0';
	document.getElementById("esatkonv"+c).value='Tidak Ada';
	document.getElementById("iformula"+c).value='0';
	document.getElementById("nformulafactor"+c).value='0';
	document.getElementById("payment").setAttribute("disabled",true);
	jsDlgHide("#konten *", "#fade", "#light");	  
  }
</script>
