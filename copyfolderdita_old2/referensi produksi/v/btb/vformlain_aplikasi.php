<style type="text/css">
	.tinggi{
		height: 25px !important;
	}
	.marjin{
		margin-bottom: 10px;
	}
	.bottom{
		margin-bottom: -5px; 
	}
	.td{
		padding:0px !important; 
	}
	.a{
		padding:0px !important; 
	}
</style>
<div class="tmpx">
<?php echo $this->pquery->form_remote_tag(array('url'=>'btb/Cform_aplikasi/simpantnpop/','update'=>'#main','type'=>'post'));?>	
	<!-- <div class="container"> -->
		<div class="panel panel-default"><h2><?php echo $page_title ?></h2>
		  <div class="panel-heading"></div>
		  <div class="panel-body td">
		  	<?php if(validation_errors()): ?>
              <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><?php echo validation_errors(); ?></strong>
            </div>
          <?php endif; ?>
		  	<table>
		  		<!-- <tr>
		  			<td style="width: 30%;">No OP</td>
		  			<td style="width: 5%;">:</td>
		  			<?php if($op){ ?>
		  			<td style="width: 65%;"><input class="tinggi" type="text" name="iop" value="<?php foreach ($op as $iop){ echo $iop." - "; } ?>" readonly></td>
		  		<?php } ?>
		  		</tr> -->
		  		<tr>
		  			<td style="width: 30%;">Supplier</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;">
		  				<input class="tinggi" type="text" name="esupplier" id="e_supplier_name" readonly onclick='showModal("btb/Cform_aplikasi/supplier/x01/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		  				<input type="hidden" name="isupplier" id="i_supplier" required></td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;">Pkp</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;">
		  				<!-- <?php 
		  					if($getpkp->f_pkp=='t'){
		  						$check = "checked";
		  					} else {
		  						$check = "";
		  					}
		  				?> -->
		  					<input type="checkbox" name="pkp" id="pkp" value="cek" class="tinggi" style="position: relative;bottom: 2px;" onclick="hitungnilai();">&nbsp;&nbsp;&nbsp;<span id="topnya"></span>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;">Include</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;">
		  				<select name="tipepajak" id="tipepajak" style="position: relative;height: 25px;bottom: 3px;" onchange="hitungnilai();">
		  					<option value="I">Include</option>
		  					<option value="E">Exclude</option>
		  				</select>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;">Nomor SJ</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;"><input class="tinggi" type="text" name="isj" id="isj"></td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;">Tanggal SJ</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;"><input class="tinggi" type="text" name="dsj" id="dsj" onclick="showCalendar('',this,this,'','dsj',0,20,1)" readonly value=""></td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;">Jenis Pembelian</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;">
		  				<select name="paymenttype" id="payment" style="position: relative;height: 25px;bottom: 3px;">
		  					<option value="">-- Pilih --</option>
		  					<option value="0">Cash</option>
		  					<option value="1">Kredit</option>
		  				</select>
		  				<!-- <input type="hidden" name="paymenttype" value="<?php echo $jnspemb; ?>"> -->
		  			</td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;">Keterangan</td>
		  			<td style="width: 5%;">:</td>
		  			<td style="width: 65%;"><input class="tinggi" type="text" name="eremark" value=""></td>
		  		</tr>
		  		<tr>
		  			<td style="width: 30%;"></td>
		  			<td style="width: 5%;"></td>
		  			<td style="width: 65%;">
		  				<input name="login" id="login" value="Simpan" type="submit" class="btn btn-success btn-sm" onclick="return dipales(parseFloat(document.getElementById('jml').value));">
		  				<button type="button" class="btn btn-warning btn-sm" onclick="show('btb/Cform_aplikasi/','#main')">
		  				Keluar</button>
		  				<input name="tambahitem" id="tambahitem" value="Tambah Item" type="button" class="btn btn-primary btn-sm" onclick="tes(parseFloat(document.getElementById('jml').value)+1);">
		  				<input name="kurangitem" id="kurangitem" value="Kurang" type="button" class="btn btn-primary btn-sm" onclick="bbatal();">
		  			</td>
		  		</tr>
		  	</table>
			  	<div id="detailheader" align="center">
			  		<table id="itemtem" class="table table-striped" style="width:100%;">
			  			<tr>
			  				<th style="width: 2%;" align="center">No</th>
			  				<th style="width: 8%;" align="center">Gudang</th>
			  				<th style="width: 10%;" align="center">Kode Barang</th>
			  				<th style="width: 11%;" align="center">Nama Barang</th>
			  				<th style="width: 6%;" align="center">Satuan</th>
			  				<!-- <th style="width: 6%;" align="center">Konversi</th> -->
			  				<!-- <th style="width: 5%;" align="center">Satuan Konversi</th> -->
			  				<th style="width: 5%;" align="center">Qty</th>
			  				<!-- <th style="width: 7%;" align="center">Harga OP</th> -->
			  				<th style="width: 7%;" align="center">Harga</th>
			  				<th style="width: 7%;" align="center">Diskon</th>
			  				<!-- <th style="width: 9%;" align="center">Total OP</th> -->
			  				<th style="width: 7%;" align="center">PPN</th>
			  				<th style="width: 8%;" align="center">Total</th>
			  				<!-- <th style="width: 9%;" align="center">Selisih</th> -->
			  			</tr>
			  		</table>
			  	</div>
				<div id="detailisi" align="center"></div>
				<div id="detailfooter">
					<table>
						<tr>
				  			<td style="width: 30%;">DPP</td>
				  			<td style="width: 5%;">:</td>
				  			<td style="width: 65%;"><input class="tinggi" style="text-align: right;" type="text" name="totdpp" id="totdpp" value="0" readonly></td>
						</tr>
						<tr>
				  			<td style="width: 30%;">PPN</td>
				  			<td style="width: 5%;">:</td>
				  			<td style="width: 65%;"><input class="tinggi" style="text-align: right;" type="text" name="totppn"
				  			id="totppn" value="0" readonly></td>
						</tr>
						<!-- <tr>
				  			<td style="width: 30%;">Grand Total OP</td>
				  			<td style="width: 5%;">:</td>
				  			<td style="width: 65%;"><input class="tinggi" style="text-align: right;" type="text" name="grandtotop" id="grandtotop" value="0" readonly></td>
						</tr> -->
						<tr>
				  			<td style="width: 30%;">Grand Total</td>
				  			<td style="width: 5%;">:</td>
				  			<td style="width: 65%;"><input class="tinggi" style="text-align: right;" type="text" name="grandtot" id="grandtot" value="0" readonly></td>
						</tr>
						<!-- <tr>
				  			<td style="width: 30%;">Selisih</td>
				  			<td style="width: 5%;">:</td>
				  			<td style="width: 65%;"><input class="tinggi" style="text-align: right;" type="text" name="grandselisih" id="grandselisih" value="0" readonly></td>
						</tr> -->
						<!-- <tr>
				  			<td style="width: 30%;">Keterangan</td>
				  			<td style="width: 5%;">:</td>
				  			<td style="width: 65%;"><input class="tinggi" style="text-align: right;" type="text" name="ket" value=""></td>
						</tr> -->
					</table>
				</div>
				<div id="pesan"></div>
				<input type="hidden" name="jml" id="jml" value="0">
		  </div>
		  <div class="panel-footer"></div>
		</div>
<?php form_close()?>
</div>
<script type="text/javascript">
	function tes(a){
		console.log("golbok");
//    if(a<=30){
	      so_inner=document.getElementById("detailheader").innerHTML;
	      si_inner=document.getElementById("detailisi").innerHTML;
	      if(so_inner==''){
			  so_inner = '<table id="itemtem" class="table table-striped" style="width:100%;">';
			  so_inner+= '<tr><th style="width:2%;" align="center">No</th>';
			  so_inner+= '<th style="width:5%;" align="center">Kode</th>';
			  so_inner+= '<th style="width:15%;" align="center">Nama Barang</th>';
			  so_inner+= '<th style="width:5%;" align="center">Satuan</th>';
			  so_inner+= '<th style="width:15%;" align="center">Supplier</th>';
			  so_inner+= '<th style="width:5%;" align="center">Qty</th>';
			  so_inner+= '<th style="width:5%;" align="center">Harga</th>';
			  so_inner+= '<th style="width:5%;" align="center">Total</th>';
			  so_inner+= '<th style="width:5%; text-align:right;">Keterangan</th></tr></table>';
			  document.getElementById("detailheader").innerHTML=so_inner;
    }else{
  			so_inner=''; 
    }
    if(si_inner==''){
		    document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		    juml=document.getElementById("jml").value;	
		    si_inner='<table class="table table-striped bottom" style="width:100%;" disabled=""><tbody><tr><td class="a" style="width:3%;"><input style="width:100%; height:25px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
		    si_inner+='<td class="a" style="width:9%;"><input style="width:100%; height:25px;" readonly type="text" id="gudang'+a+'" name="gudang'+a+'" value="" onclick=\'showModal("btb/Cform_aplikasi/gudang/'+a+'/x01","#light");jsDlgShow("#konten *", "#fade", "#light");\'><input style="width:100%; height:25px;" readonly type="hidden" id="igudang'+a+'" name="igudang'+a+'" value=""></td>';
		    si_inner+='<td class="a" style="width:9%;"><input style="width:100%; height:25px;" readonly type="text" id="imaterial'+a+'" name="imaterial'+a+'" value="" onclick="modalmaterial('+a+');"></td>';
		    // si_inner+='<td class="a" style="width:12%;"><input style="width:100%; height:25px;" readonly type="text" id="ematerialname'+a+'" name="ematerialname'+a+'" value=""><label style="width:100%; height:20px;bottom: 8px;position: relative; text-align: right;">Konversi ?</label></td>';
		    si_inner+='<td class="a" style="width:12%;"><input style="width:100%; height:25px;" readonly type="text" id="ematerialname'+a+'" name="ematerialname'+a+'" value=""></td>';
		    // si_inner+='<td class="a" style="width:7%;"><input readonly style="width:100%; height:25px;"  type="text" id="esatuan'+a+'" name="esatuan'+a+'" value=""><select name="konversi'+a+'" id="konversi'+a+'" style="width:100%; height:20px;bottom: 8px;position: relative;" onchange="konversi(this.value,'+a+');"><option value="tidak">Tidak</option><option value="ya">Ya</option><option value="baru">Baru</option></select><input readonly type="hidden" id="isatuan'+a+'" name="isatuan'+a+'" value=""></td>';
		    si_inner+='<td class="a" style="width:7%;"><input readonly style="width:100%; height:25px;"  type="text" id="esatuan'+a+'" name="esatuan'+a+'" value=""><input readonly type="hidden" id="isatuan'+a+'" name="isatuan'+a+'" value=""></td>';
		    // si_inner+='<td class="a" style="width:8%;"><input type="text" name="esatkonv'+a+'" id="esatkonv'+a+'" readonly="true" style="width:100%; height:25px;" value=""><input type="hidden" name="isatkonv'+a+'" id="isatkonv'+a+'" readonly="true" style="width:100%; height:25px;" value="0"><input type="hidden" name="iformula'+a+'" id="iformula'+a+'" readonly="true" value="0"><input type="hidden" name="nformulafactor'+a+'" id="nformulafactor'+a+'" readonly="true" value="0"></td>';
		    si_inner+='<td class="a" style="width:5%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="nquantity'+a+'" name="nquantity'+a+'" onkeyup=\'hitungnilai();\' onblur=\'hitungnilai();\' autocomplete="off" value="0"></td>';
		    si_inner+='<td class="a" style="width:7%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="vprice'+a+'" name="vprice'+a+'" value="0" onkeyup=\'hitungnilai();\' onblur=\'hitungnilai();\'></td>';
		    si_inner+='<td class="a" style="width:7%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="diskon'+a+'" name="diskon'+a+'" value="0" onkeyup=\'hitungnilai();\' onblur=\'hitungnilai();\'></td>';
		    si_inner+='<td class="a" style="width:7%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="ppn'+a+'" name="ppn'+a+'" value="0" readonly></td>';
		    si_inner+='<td class="a" style="width:8%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" readonly value="0"></td></tr></tbody></table>';
		    // si_inner+='<td class="a" style="width:9%;"><input style="width:100%; height:25px; text-align:right;" type="text" id="selisih'+a+'" name="selisih'+a+'" value="0" readonly></td></tr></tbody></table>';
    }else{
    	// onclick=\'showModal("btb/Cform/material/'+a+'/'+document.getElementById("igudang"+a).value+'/x01","#light");jsDlgShow("#konten *", "#fade", "#light");\'
			  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
			  juml=document.getElementById("jml").value;
		    si_inner=si_inner+'<table class="table table-striped bottom" style="width:100%;" disabled=""><tbody><tr><td class="a" style="width:3%;"><input style="width:100%; height:25px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
		    si_inner+='<td class="a" style="width:9%;"><input style="width:100%; height:25px;" readonly type="text" id="gudang'+a+'" name="gudang'+a+'" value="" onclick=\'showModal("btb/Cform_aplikasi/gudang/'+a+'/x01","#light");jsDlgShow("#konten *", "#fade", "#light");\'><input style="width:100%; height:25px;" readonly type="hidden" id="igudang'+a+'" name="igudang'+a+'" value=""></td>';
		    si_inner+='<td class="a" style="width:9%;"><input style="width:100%; height:25px;" readonly type="text" id="imaterial'+a+'" name="imaterial'+a+'" value="" onclick="modalmaterial('+a+');"></td>';
		    // si_inner+='<td class="a" style="width:12%;"><input style="width:100%; height:25px;" readonly type="text" id="ematerialname'+a+'" name="ematerialname'+a+'" value=""><label style="width:100%; height:20px;bottom: 8px;position: relative; text-align: right;">Konversi ?</label></td>';
		    si_inner+='<td class="a" style="width:12%;"><input style="width:100%; height:25px;" readonly type="text" id="ematerialname'+a+'" name="ematerialname'+a+'" value=""></td>';
		    // si_inner+='<td class="a" style="width:7%;"><input readonly style="width:100%; height:25px;"  type="text" id="esatuan'+a+'" name="esatuan'+a+'" value=""><select name="konversi'+a+'" id="konversi'+a+'" style="width:100%; height:20px;bottom: 8px;position: relative;" onchange="konversi(this.value,'+a+');"><option value="tidak">Tidak</option><option value="ya">Ya</option><option value="baru">Baru</option></select><input readonly type="hidden" id="isatuan'+a+'" name="isatuan'+a+'" value=""></td>';
		    si_inner+='<td class="a" style="width:7%;"><input readonly style="width:100%; height:25px;"  type="text" id="esatuan'+a+'" name="esatuan'+a+'" value=""><input readonly type="hidden" id="isatuan'+a+'" name="isatuan'+a+'" value=""></td>';
		    // si_inner+='<td class="a" style="width:8%;"><input type="text" name="esatkonv'+a+'" id="esatkonv'+a+'" readonly="true" style="width:100%; height:25px;" value=""><input type="hidden" name="isatkonv'+a+'" id="isatkonv'+a+'" readonly="true" style="width:100%; height:25px;" value="0"><input type="hidden" name="iformula'+a+'" id="iformula'+a+'" readonly="true" value="0"><input type="hidden" name="nformulafactor'+a+'" id="nformulafactor'+a+'" readonly="true" value="0"></td>';
		    si_inner+='<td class="a" style="width:5%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="nquantity'+a+'" name="nquantity'+a+'" onkeyup=\'hitungnilai();\' onblur=\'hitungnilai();\' autocomplete="off" value="0"></td>';
		    si_inner+='<td class="a" style="width:7%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="vprice'+a+'" name="vprice'+a+'" value="0" onkeyup=\'hitungnilai();\' onblur=\'hitungnilai();\'></td>';
		    si_inner+='<td class="a" style="width:7%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="diskon'+a+'" name="diskon'+a+'" value="0" onkeyup=\'hitungnilai();\' onblur=\'hitungnilai();\'></td>';
		    si_inner+='<td class="a" style="width:7%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="ppn'+a+'" name="ppn'+a+'" value="0" readonly></td>';
		    si_inner+='<td class="a" style="width:8%;"><input style="text-align:right; width:100%; height:25px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" readonly value="0"></td></tr></tbody></table>';
		    // si_inner+='<td class="a" style="width:9%;"><input style="width:100%; height:25px; text-align:right;" type="text" id="selisih'+a+'" name="selisih'+a+'" value="0" readonly></td></tr></tbody></table>';
    }
	    j=0;
	    var baris			= Array();
	    var igudang 		= Array();
	    var egudang			= Array();
	    var imaterial		= Array();
	    var ematerialname 	= Array(); 
  		var isatuan			= Array();
  		var esatuan			= Array();
  		// var konversi 		= Array();
  		// var isatkonv 		= Array();
  		// var esatkonv 		= Array();
  		// var iformula 		= Array();
  		// var nformulafactor  = Array();
  		var nquantity		= Array();
  		var vprice 			= Array();
  		var diskon 			= Array();
  		var vtotal 			= Array();
	    var ppn				= Array();
	    // var selisih 		= Array();
	    for(i=1;i<a;i++){
	      j++;
	      baris[j]		   = document.getElementById("baris"+i).value;
	      igudang[j]	   = document.getElementById("igudang"+i).value;
	      egudang[j] 	   = document.getElementById("gudang"+i).value;
	      imaterial[j]	   = document.getElementById("imaterial"+i).value;
	      ematerialname[j] = document.getElementById("ematerialname"+i).value;
	      isatuan[j]	   = document.getElementById("isatuan"+i).value;
	      esatuan[j]	   = document.getElementById("esatuan"+i).value;
	      // konversi[j] 	   = document.getElementById("konversi"+i).value;
	      // isatkonv[j]	   = document.getElementById("isatkonv"+i).value;
	      // esatkonv[j] 	   = document.getElementById("esatkonv"+i).value;
	      // iformula[j] 	   = document.getElementById("iformula"+i).value;
	      // nformulafactor[j]= document.getElementById("nformulafactor"+i).value;
	      nquantity[j]	   = document.getElementById("nquantity"+i).value;
	      vprice[j]	   	   = document.getElementById("vprice"+i).value;
	      vtotal[j]	   	   = document.getElementById("vtotal"+i).value;
	      ppn[j]		   = document.getElementById("ppn"+i).value;	
	      // selisih[j]	   = document.getElementById("selisih"+i).value;
	    }
	    document.getElementById("detailisi").innerHTML=si_inner;
	    j=0;
	    for(i=1;i<a;i++){
			  j++;
			  document.getElementById("baris"+i).value         = baris[j];
			  document.getElementById("igudang"+i).value	   = igudang[j];
			  document.getElementById("gudang"+i).value 	   = egudang[j];
			  document.getElementById("imaterial"+i).value     = imaterial[j];
			  document.getElementById("ematerialname"+i).value = ematerialname[j];
			  document.getElementById("isatuan"+i).value       = isatuan[j];
			  document.getElementById("esatuan"+i).value       = esatuan[j];
			  // document.getElementById("konversi"+i).value 	   = konversi[j];
			  // document.getElementById("isatkonv"+i).value 	   = isatkonv[j];
			  // document.getElementById("esatkonv"+i).value      = esatkonv[j];
			  // document.getElementById("iformula"+i).value 	   = iformula[j];
			  // document.getElementById("nformulafactor"+i).value= nformulafactor[j];
			  document.getElementById("nquantity"+i).value     = nquantity[j];
			  document.getElementById("vprice"+i).value        = vprice[j];
			  document.getElementById("vtotal"+i).value        = vtotal[j];
			  document.getElementById("ppn"+i).value       	   = ppn[j];	
			  // document.getElementById("selisih"+i).value 	   = selisih[j];
	    }
  	}
  	function bbatal(){
	  baris	= document.getElementById("jml").value;
	  si_inner= document.getElementById("detailisi").innerHTML;
	  var temp= new Array();
	  temp	= si_inner.split('<table class="table table-striped bottom" style="width:100%;" disabled="">');	
	  if( (document.getElementById("imaterial"+baris).value=='')){
		  si_inner='';
		  for(x=1;x<baris;x++){
			  si_inner=si_inner+'<table class="table table-striped bottom" style="width:100%;" disabled="">'+temp[x];
		  }
		  	j=0;
		    var baris			= Array();
		    var igudang 		= Array();
		    var egudang			= Array();
		    var imaterial		= Array();
		    var ematerialname 	= Array(); 
	  		var isatuan			= Array();
	  		var esatuan			= Array();
	  		// var konversi 		= Array();
	  		// var isatkonv 		= Array();
	  		// var esatkonv 		= Array();
	  		// var iformula 		= Array();
	  		// var nformulafactor  = Array();
	  		var nquantity		= Array();
	  		var vprice 			= Array();
	  		var diskon 			= Array();
	  		var vtotal 			= Array();
		    var ppn				= Array();
		    // var selisih 		= Array();
		    for(i=1;i<baris;i++){
		      j++;
		      baris[j]		   = document.getElementById("baris"+i).value;
		      igudang[j]	   = document.getElementById("igudang"+i).value;
		      egudang[j] 	   = document.getElementById("gudang"+i).value;
		      imaterial[j]	   = document.getElementById("imaterial"+i).value;
		      ematerialname[j] = document.getElementById("ematerialname"+i).value;
		      isatuan[j]	   = document.getElementById("isatuan"+i).value;
		      esatuan[j]	   = document.getElementById("esatuan"+i).value;
		      // konversi[j] 	   = document.getElementById("konversi"+i).value;
		      // isatkonv[j]	   = document.getElementById("isatkonv"+i).value;
		      // esatkonv[j] 	   = document.getElementById("esatkonv"+i).value;
		      // iformula[j] 	   = document.getElementById("iformula"+i).value;
		      // nformulafactor[j]= document.getElementById("nformulafactor"+i).value;
		      nquantity[j]	   = document.getElementById("nquantity"+i).value;
		      vprice[j]	   	   = document.getElementById("vprice"+i).value;
		      vtotal[j]	   	   = document.getElementById("vtotal"+i).value;
		      ppn[j]		   = document.getElementById("ppn"+i).value;	
		      // selisih[j]	   = document.getElementById("selisih"+i).value;
		    }
		    document.getElementById("detailisi").innerHTML=si_inner;
		    j=0;
		    for(i=1;i<baris;i++){
				  j++;
				  document.getElementById("baris"+i).value         = baris[j];
				  document.getElementById("igudang"+i).value	   = igudang[j];
				  document.getElementById("gudang"+i).value 	   = egudang[j];
				  document.getElementById("imaterial"+i).value     = imaterial[j];
				  document.getElementById("ematerialname"+i).value = ematerialname[j];
				  document.getElementById("isatuan"+i).value       = isatuan[j];
				  document.getElementById("esatuan"+i).value       = esatuan[j];
				  // document.getElementById("konversi"+i).value 	   = konversi[j];
				  // document.getElementById("isatkonv"+i).value 	   = isatkonv[j];
				  // document.getElementById("esatkonv"+i).value      = esatkonv[j];
				  // document.getElementById("iformula"+i).value 	   = iformula[j];
				  // document.getElementById("nformulafactor"+i).value= nformulafactor[j];
				  document.getElementById("nquantity"+i).value     = nquantity[j];
				  document.getElementById("vprice"+i).value        = vprice[j];
				  document.getElementById("vtotal"+i).value        = vtotal[j];
				  document.getElementById("ppn"+i).value       	   = ppn[j];	
				  // document.getElementById("selisih"+i).value 	   = selisih[j];
		    }
		  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)-1;
	  }
  	}
  	function konversi(a,baris)
  {
  	var isatuan = $('#isatuan'+baris).val();
  	var esatuan = $('#esatuan'+baris).val();
  	// isi value a ada 3 ('tidak','ya','baru');
  	if(a=='tidak')
  	{
  		$('#esatkonv'+baris).val('Tidak Ada');
  		$('#isatkonv'+baris).val('0');
  		$('#iformula'+baris).val('0');
  		$('#nformulafactor'+baris).val('0');
  	} else if(a=='ya')
  	{
  		showModal("btb/Cform/satkonversi/"+baris+"/"+isatuan+"/x01/","#light");
  		jsDlgShow("#konten *", "#fade", "#light");
  	} else {
  		lebar =450;
    	tinggi=350;
    	var popup = eval('window.open("<?php echo site_url(); ?>"+"btb/Cform/tambahsatkonv/"+isatuan+"/"+esatuan+"/"+baris+"/slow/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,menubar=1, top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');

    	popup.onbeforeunload = function (event) {
    		document.getElementById("konversi"+baris).value='tidak'
			document.getElementById("isatkonv"+baris).value='0';
			document.getElementById("esatkonv"+baris).value='Tidak Ada';
			document.getElementById("iformula"+baris).value='0';
			document.getElementById("nformulafactor"+baris).value='0';
    	}	
  	}
  }
  	function modalmaterial(a)
  	{
  		var gudang = document.getElementById('igudang'+a).value;
  		var i_supplier = document.getElementById('i_supplier').value;
  		if(i_supplier=='')
  		{
  			alert('Pilih Supplier Dulu Yah :D');
  		} else if(gudang!='')
  		{
  			showModal("btb/Cform_aplikasi/material/"+a+"/"+gudang+"/"+i_supplier+"/x01","#light");jsDlgShow("#konten *", "#fade", "#light");
  		}
  	}
  	function hitungnilai()
  {
  	var jml = $('#jml').val();
  	var pkp = $('#pkp').is(':checked');
  	var tipepajak = $('#tipepajak').val();
  	var totop=0;
  	var tot=0;
  	var selisih=0;
  	var dpp=0;
  	var ppn=0;
  	var gtotppn=0;
  	var gtotop=0;
  	var gtot=0;
  	var gtotselisih=0;

  	if(pkp)
  	{
  		for(var i=1; i<=jml; i++)
  		{
  			var qty = $('#nquantity'+i).val()==''?$('#nquantity'+i).val(0):qty;
  			qty = $('#nquantity'+i).val() || 0;
  			
  			// var hrgop = formatulang($('#vpriceop'+i).val());
  			
  			var hrg = formatulang($('#vprice'+i).val())==''?$('#vprice'+i).val(0):hrg;
  			hrg   = formatulang($('#vprice'+i).val()) || 0;	
  			
  			var diskon = $('#diskon'+i).val()==''?$('#diskon'+i).val(0):diskon;
  			diskon = $('#diskon'+i).val() || 0;

  			if(tipepajak=='I')
  			{
  				// totop = (parseFloat(hrgop)*parseFloat(qty))-parseFloat(diskon);
	  			// $('#vtotalop'+i).val(formatcemua(totop));
	  			tot = (parseFloat(hrg)*parseFloat(qty))-parseFloat(diskon);
	  			$('#vtotal'+i).val(formatcemua(tot));
	  			// selisih = totop-tot;
	  			// $('#selisih'+i).val(formatcemua(selisih));

	  			var pi = tot/1.1;
	  			ppn = tot-pi;
	  			$('#ppn'+i).val(formatMoney(ppn,2,',','.'));

	  			gtotppn += ppn;
	  			// gtotop += totop;
	  			gtot += tot;
  			} else {
  				// totop = (parseFloat(hrgop)*parseFloat(qty))-parseFloat(diskon);
	  			// $('#vtotalop'+i).val(formatcemua(totop));
	  			tot = (parseFloat(hrg)*parseFloat(qty))-parseFloat(diskon);
	  			$('#vtotal'+i).val(formatcemua(tot));
	  			// selisih = totop-tot;
	  			// $('#selisih'+i).val(formatcemua(selisih));

	  			// pe=pajak exclude
	  			var pe = tot*0.1;
	  			$('#ppn'+i).val(formatMoney(pe,2,',','.'));
	  			var newtot = parseFloat(pe)+parseFloat(tot);

	  			// peop=pajak exclude op
	  			// var peop = totop*0.1;
	  			// var newtotop = parseFloat(peop)+parseFloat(totop);

	  			gtotppn += pe;
	  			// gtotop += newtotop;
	  			gtot += newtot;
  			}

  			// $('#grandtotop').val(formatcemua(gtotop));
	  		$('#grandtot').val(formatcemua(gtot));
	  		$('#totppn').val(formatMoney(gtotppn,2,',','.'));
	  		dpp = gtot/1.1;
	  		$('#totdpp').val(formatMoney(dpp,2,',','.'));
	  		// gtotselisih = gtotop-gtot;
	  		// $('#grandselisih').val(formatcemua(gtotselisih));
  		}
  	} else {
  		for(var i=1; i<=jml; i++)
  		{
  			var qty = $('#nquantity'+i).val()==''?$('#nquantity'+i).val(0):qty;
  			qty = $('#nquantity'+i).val() || 0;
  			
  			// var hrgop = formatulang($('#vpriceop'+i).val());

  			var hrg = formatulang($('#vprice'+i).val())==''?$('#vprice'+i).val(0):hrg;
  			hrg   = formatulang($('#vprice'+i).val()) || 0;	
  			
  			var diskon = $('#diskon'+i).val()==''?$('#diskon'+i).val(0):diskon;
  			diskon = $('#diskon'+i).val() || 0;
  			$('#ppn'+i).val(0);
  			// totop = (parseFloat(hrgop)*parseFloat(qty))-parseFloat(diskon);
  			// $('#vtotalop'+i).val(formatcemua(totop));
  			tot = (parseFloat(hrg)*parseFloat(qty))-parseFloat(diskon);
  			$('#vtotal'+i).val(formatcemua(tot));
  			// selisih = totop-tot;
  			// $('#selisih'+i).val(formatcemua(selisih));

  			// gtotop += totop;
  			gtot += tot;
  		}
  		// $('#grandtotop').val(formatcemua(gtotop));
  		$('#grandtot').val(formatcemua(gtot));
  		$('#totppn').val(0);
	  	$('#totdpp').val(0);
  		// gtotselisih = gtotop-gtot;
  		// $('#grandselisih').val(formatcemua(gtotselisih));
  	}
  }
  function formatMoney(angka, decPlaces, thouSeparator, decSeparator) {
    var n = angka,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
  }
  function dipales(a){
    cek='false';
    s=0;
    if((document.getElementById("dsj").value!='')&&(document.getElementById("isj").value!='')&&(document.getElementById("isupplier").value!='')&&(document.getElementById("paymenttype").value!='')) {
        for(i=1;i<=a;i++){
            if((document.getElementById("vprice"+i)==0) || 
            (document.getElementById("nquantity"+i).value==0)){
            	alert('Data item masih ada yang salah !!!');
            	s =1;
            	return false;
            	cek='false';
          	}else{
            	cek='true';	
          	} 
        }
      if(cek=='true'){
        document.getElementById("login").disabled=true;
        document.getElementById("cmdtambahitem").disabled=true;
      }else{
        document.getElementById("login").disabled=false;
      }
    }else{
      alert('Data header masih ada yang salah !!!');
      return false;
    }
  }
</script>