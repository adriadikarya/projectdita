<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
// Irawan 2018-11-02
class Mmaster_bisbisan extends CI_Model{
	public function __construct()
    {
        parent::__construct();
    }

    function getsj($limit, $offset, $cari){
        if(!empty($cari)){
          $this->db->like('upper(x.i_sj)',$cari);
        }
        $this->db->select('x.*, 
        (case
          when (select a.i_sj from tm_bonmasuk_bisbisandetail a, tm_bonmasuk_bisbisan b where 
                a.i_bonm=b.i_bonm and b.f_bonm_cancel=\'f\' and a.i_sj=x.i_sj limit 1)=x.i_sj
          then \'ada\'
          else \'tidak\'
        end) as status
        from(
        SELECT a.i_sj, a.d_sj, a.e_desc, a.f_sj_cancel, b.e_supplier_name
        FROM tm_sj_pembelian a 
        JOIN tr_supplier b ON a.i_supplier=b.i_supplier 
        WHERE a.i_makloon_type=\'1\'
        ORDER BY a.i_sj) as x');
        // $this->db->select('a.* , b.e_supplier_name');
        // $this->db->from('tm_sj_pembelian a');
        // $this->db->join('tr_supplier b','a.i_supplier=b.i_supplier');	
        // $this->db->where("a.i_makloon_type = '1'");
        $this->db->limit($limit, $offset);
        $data = $this->db->get();
		return $data;
	}

	function getsjCari($cari){
        if(!empty($cari)){
          $this->db->like('upper(x.i_sj)',$cari);
        }   
        $this->db->select('x.*, 
        (case
          when (select i_sj from tm_bonmasuk_bisbisandetail a, tm_bonmasuk_bisbisan b where 
              a.i_bonm=b.i_bonm and b.f_bonm_cancel=\'f\' and i_sj=x.i_sj limit 1)=x.i_sj
          then \'ada\'
          else \'tidak\'
        end) as status
        from(
        SELECT a.i_sj, a.d_sj, a.e_desc, a.f_sj_cancel, b.e_supplier_name
        FROM tm_sj_pembelian a 
        JOIN tr_supplier b ON a.i_supplier=b.i_supplier 
        WHERE a.i_makloon_type=\'1\'
        ORDER BY a.i_sj) as x');
        // $this->db->select('a.* , b.e_supplier_name');
        // $this->db->from('tm_sj_pembelian a');
        // $this->db->join('tr_supplier b','a.i_supplier=b.i_supplier'); 
        // $this->db->where("a.i_makloon_type = '1'");
        $data = $this->db->get();
        return $data;
    }

    function bacasupplier($limit,$offset,$cari){

        if(!empty($cari)){
            $this->db->like('i_supplier',$cari);
            $this->db->or_like('e_supplier_name',$cari);
        }
        $this->db->where("i_category = '02'");
        $data = $this->db->order_by("i_supplier","asc")->limit($limit, $offset)->get("tr_supplier");
        return $data;

    }

    function bacasuppliercari($cari)
    {
     
      if(!empty($cari)){
            $this->db->like('i_supplier',$cari);
            $this->db->or_like('e_supplier_name',$cari);
        }

      $this->db->where("i_category = '02'");
      $data = $this->db->order_by('i_supplier','asc')->get("tr_supplier");

      return $data;

    }

    function bacapotongancari($cari)
    {
        if(!empty($cari)){
            $this->db->like('e_potongan',$cari);
        }
        $data = $this->db->order_by('i_potongan','asc')->get("tr_potongan_bisbisan");
        return $data;
    }

    function bacapotongan($limit,$offset,$cari) 
    {
        if(!empty($cari)){
            $this->db->like('e_potongan',$cari);
        }
        $data = $this->db->order_by("i_potongan","asc")->limit($limit, $offset)->get("tr_potongan_bisbisan");
        return $data;
    }

    function bacaukurancari($cari)
    {
        if(!empty($cari)){
            $this->db->like('e_nama_ukuran',$cari);
        }
        $data = $this->db->order_by('i_kode_ukuran','asc')->get("tr_ukuran_bisbisan");
        return $data;
    }

    function bacaukuran($limit,$offset,$cari) 
    {
        if(!empty($cari)){
            $this->db->like('e_nama_ukuran',$cari);
        }
        $data = $this->db->order_by("i_kode_ukuran","asc")->limit($limit, $offset)->get("tr_ukuran_bisbisan");
        return $data;
    }

    public function getmaterialCari($cari,$gudang,$isupplier){

      if(!empty($cari)){
        $this->db->like('upper(i_material)',$cari);
        $this->db->or_like('upper(e_material_name)',$cari);
      }
      $this->db->select("a.*, b.e_satuan");
      $this->db->from("tr_material a");
      $this->db->join("tr_satuan b","a.i_satuan=b.i_satuan");
      $this->db->where("i_store='$gudang'");
      $subQuery =  $this->db->get_compiled_select();
      $this->db->select("x.*,coalesce((select v_price
            from tr_supplier_materialprice a
            where i_supplier='$isupplier' and x.i_material=i_material
            order by i_price_no desc limit 1),0) as v_price",false);
      $this->db->from("($subQuery) as x");
    
      $data = $this->db->get();

      return $data;
    }

    public function bacamaterial($limit,$offset,$cari,$gudang,$isupplier){

      if(!empty($cari)){
        $this->db->like('upper(i_material)',$cari);
        $this->db->or_like('upper(e_material_name)',$cari);
      }
      $this->db->select("a.*, b.e_satuan");
      $this->db->from("tr_material a");
      $this->db->join("tr_satuan b","a.i_satuan=b.i_satuan");
      $this->db->where("i_store='$gudang'");
      $subQuery =  $this->db->get_compiled_select();
      $this->db->select("x.*,coalesce((select v_price
            from tr_supplier_materialprice a
            where i_supplier='$isupplier' and x.i_material=i_material
            order by i_price_no desc limit 1),0) as v_price",false);
      $this->db->from("($subQuery) as x");
      $this->db->limit($limit,$offset);
      $data = $this->db->get()->result();

      return $data;

    }
    
    function getgudangCari($cari){
    if(!empty($cari)){
      $this->db->like('upper(i_kode_master)',$cari);
      $this->db->or_like('upper(e_nama_master)',$cari);
    }
    $data = $this->db->get('tr_master_gudang');

    return $data;
  }

  function bacagudang($limit,$offset,$cari){

    if(!empty($cari)){
      $this->db->like('upper(i_kode_master)',$cari);
      $this->db->or_like('upper(e_nama_master)',$cari);
    }
    //$this->db->select('upper(i_kode_master),upper(e_nama_master,i_kode_lokasi,d_input,d_update,i_kode_jenis');
    $this->db->limit($limit,$offset);
    $data = $this->db->get('tr_master_gudang')->result();

    return $data;

  }

  function inserthrgsup($i_material,$v_price,$i_supplier,$i_satuan,$isj)
    {
      $querys  = $this->db->query("SELECT current_timestamp as c");
      $rowd   = $querys->row();
      $now    = $rowd->c;
      $now = date('Y-m-d h:i:s');
      $this->db->where('i_supplier',$i_supplier);
      $this->db->where('i_material',$i_material);
      $this->db->where('i_satuan',$i_satuan);
      $this->db->order_by('i_price_no','desc');
      $this->db->limit(1);
      $query = $this->db->get('tr_supplier_materialprice');
      if($query->num_rows()>0)
      {
        foreach ($query->result() as $row) {
          $priceno = $row->i_price_no;
          if($v_price!=$row->v_price && $row->i_satuan==$i_satuan)
          {
            $data = array(
              'i_supplier' => $i_supplier,
              'i_material' => $i_material,
              'v_price'    => $v_price,
              'i_satuan'   => $i_satuan,
              'd_insert'   => $now,
              'i_price_no' => $row->i_price_no+1
            );
            $this->db->insert('tr_supplier_materialprice',$data);
            $this->db->set('i_price_no',$row->i_price_no+1);
            $this->db->where('i_sj',$isj);
            $this->db->where('i_material',$i_material);
            $this->db->update('tm_sj_pembelian_detail');
          } else if($v_price!=$row->v_price && $row->i_satuan!=$i_satuan) {
            $data = array(
              'i_supplier' => $i_supplier,
              'i_material' => $i_material,
              'v_price'    => $v_price,
              'i_satuan'   => $i_satuan,
              'd_insert'   => $now,
              'i_price_no' => 1
            );
            $this->db->insert('tr_supplier_materialprice',$data);
            $this->db->insert('tr_supplier_materialprice',$data);
            $this->db->set('i_price_no',1);
            $this->db->where('i_sj',$isj);
            $this->db->where('i_material',$i_material);
            $this->db->update('tm_sj_pembelian_detail');
          } else {
            $this->db->set('i_price_no',$priceno);
            $this->db->where('i_sj',$isj);
            $this->db->where('i_material',$i_material);
            $this->db->update('tm_sj_pembelian_detail');
          } 
        }
      } else {
        $data = array(
              'i_supplier' => $i_supplier,
              'i_material' => $i_material,
              'v_price'    => $v_price,
              'i_satuan'   => $i_satuan,
              'd_insert'   => $now,
              'i_price_no' => 1
            );
        $this->db->insert('tr_supplier_materialprice',$data);
        $this->db->set('i_price_no',1);
        $this->db->where('i_sj',$isj);
        $this->db->where('i_material',$i_material);
        $this->db->update('tm_sj_pembelian_detail');
      }
    }

    function insertheader($data)
    {
      // Jalankan query
      $db_debug = $this->db->db_debug;
      $this->db->db_debug = FALSE;
      $query = $this->db->insert("tm_sj_pembelian", $data);
      // $query = $this->db->query($sql);
      $error = $this->db->error();
      $this->db->error();
      if ( ! empty($error['message'])) {
          return false;
          $this->db->db_debug = $db_debug;
      } else {
          return true;
          $this->db->db_debug = $db_debug;
      }

      // Return hasil query
      // return $query;
    }

    function insertdetail($data2)
    {
      // Jalankan query
      $db_debug = $this->db->db_debug;
      $this->db->db_debug = FALSE;
      $query = $this->db->insert("tm_sj_pembelian_detail", $data2);
      $error = $this->db->error();
      // $this->db->error();
      if ( ! empty($error['message'])) {
          return false;
          $this->db->db_debug = $db_debug;
      } else {
          return true;
          $this->db->db_debug = $db_debug;
      }
      // Return hasil query
      // return $query; 
    }

    function getHeader($isj){
      $this->db->select("a.*,b.e_supplier_name,b.f_tipe_pajak");
      $this->db->from("tm_sj_pembelian a");
      $this->db->join("tr_supplier b","a.i_supplier=b.i_supplier");
      $this->db->where('a.i_sj',$isj);
      // $this->db->where('a.f_sj_cancel','f');
      return $this->db->get()->row();
    }

    function getItem($isj){
      $this->db->select("a.*,b.e_material_name,c.e_satuan,e.i_kode_master,e.e_nama_master,a.i_cut_type as i_potongan, a.i_size_type as i_kode_ukuran, f.e_nama_ukuran, g.e_potongan");
      $this->db->from("tm_sj_pembelian_detail a");
      $this->db->join("tr_material b","a.i_material=b.i_material");
      $this->db->join("tr_satuan c","a.i_unit=c.i_satuan");
      // $this->db->join("tm_opbb_item d","a.i_op=d.i_op");
      $this->db->join("tr_master_gudang e","b.i_store=e.i_kode_master");
      $this->db->join("tr_ukuran_bisbisan f","f.i_kode_ukuran=a.i_size_type");
      $this->db->join("tr_potongan_bisbisan g","g.i_potongan=a.i_cut_type");
      $this->db->where("i_sj",$isj);
      $this->db->order_by("a.i_no_item");
      return $this->db->get()->result();
    }
    function updateheader($data,$i_sj){
      $query = $this->db->where("i_sj",$i_sj)->update("tm_sj_pembelian",$data);
      return $query;
    }
    public function delete($isj)
    {
      // Jalankan query
      $query = $this->db
        ->where('i_sj', $isj)
        ->delete("tm_sj_pembelian_detail");
      
      // Return hasil query
      return $query;
    }
    function cancelsj($isj)
    {
      $this->db->set('f_sj_cancel','t');
      $this->db->where('i_sj',$isj);
      $query = $this->db->update('tm_sj_pembelian');
      return $query;
    }
}