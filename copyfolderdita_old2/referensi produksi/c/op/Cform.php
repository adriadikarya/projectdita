<?php
// Irawan 2018-11-02
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('op/Mmaster');
	}

	function index(){

		$cari = trim($this->input->post('cari'));
		
		//Pagination start
		$config['base_url'] = base_url().'index.php/op/Cform/index/';
		$config['total_rows'] = $this->Mmaster->getppCari($cari)->num_rows();
		$config['per_page'] = '10';
		$config['full_tag_open'] = '<ul class="pagination a">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'Awal';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Akhir';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Selanjutnya';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Sebelumnya';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$config['cur_page'] = $this->uri->segment(4);
		$config['offset'] = $this->uri->segment(4);
		$this->pagination->initialize($config);
		
		//data for view
		$data['total'] = $this->Mmaster->getppCari($cari)->num_rows();
		$data['cari'] = $cari;
		$data['isi'] = $this->Mmaster->getop($config['per_page'], $config['offset'],$cari)->result();
		$data['page_title'] = 'Order Pembelian';
		$this->load->view('op/vformview', $data);
	
	}

	function cariindex()
	{
		$cari = trim($this->input->post('cari'));
		//Pagination start
		if($cari=='')$cari=$this->uri->segment(4);
		$config['base_url'] = base_url().'index.php/op/Cform/cariindex/'.$cari.'/';
		$config['total_rows'] = $this->Mmaster->getppCari($cari)->num_rows();
		$config['per_page'] = '10';
		$config['full_tag_open'] = '<ul class="pagination a">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'Awal';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Akhir';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Selanjutnya';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Sebelumnya';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$config['cur_page'] = $this->uri->segment(5);
		$config['offset'] = $this->uri->segment(5);
		$this->pagination->initialize($config);
		
		//data for view
		$data['total'] = $this->Mmaster->getppCari($cari)->num_rows();
		$data['cari'] = $cari;
		$data['isi'] = $this->Mmaster->getop($config['per_page'], $config['offset'],$cari)->result();
		$data['page_title'] = 'Order Pembelian';
		$this->load->view('op/vformview', $data);
	}

	function add(){

		if($this->input->post('igudang')&& $this->input->post('dpp')){
			$this->form_validation->set_rules('igudang', 'Gudang', 'required');
			$this->form_validation->set_rules('dpp', 'Tanggal PP', 'required');
		
			if ($this->form_validation->run() === TRUE) {

			$query 	= $this->db->query("SELECT current_timestamp as c");
	   		$row   	= $query->row();
	    	$now	  = $row->c;

	    	$dpp = $this->input->post("dpp",true);
	    	if($dpp){
	    		 $tmp 	= explode('-', $dpp);
	    		 $day 	= $tmp[0];
	    		 $month = $tmp[1];
	    		 $year	= $tmp[2];
	    		 $yearmonth = $year.$month;
	    		 $datepp = $year.'-'.$month.'-'.$day;
	    	}

			$this->db->trans_begin(); 
			$data['i_pp'] = $this->Mmaster->runningnumber($yearmonth);
			$data['d_pp'] = $datepp;
			$data['i_kode_master'] = $this->input->post("igudang",TRUE);
			$data['e_remark'] = $this->input->post('eremark',TRUE);
			$data['d_entry'] = $now;
			$query = $this->Mmaster->insertheader($data);

			$count = $this->input->post("jml",true);

			for($i=1;$i<=$count;$i++){
				$nqtypp = $this->input->post('n_quantitypp'.$i,TRUE);
				$nqtyop = $this->input->post('n_quantity'.$i,TRUE);
				if($nqtyop > $nqtypp){
					$nqtyop = $nqtypp;
				}
				$data2['i_pp'] 		 = $data['i_pp'];
				$data2['i_material'] = $this->input->post("imaterial".$i,TRUE);
				$data2['i_satuan'] 	 = $this->input->post("isatuan".$i,TRUE);
				$data2['i_supplier'] = $this->input->post("isupplier".$i,TRUE);
				$data2['n_quantity'] = $nqtyop;
				$data2['v_price'] 	 = str_replace(',','',$this->input->post("vprice".$i,TRUE));
				$data2['e_remark'] 	 = $this->input->post("eremark".$i,TRUE);
				$data2['n_item_no']	 = $i;
				$querydetail = $this->Mmaster->insertdetail($data2);
			}

		if (($this->db->trans_status() === FALSE))
			{
		    	$this->db->trans_rollback();
			}
			else{
				$this->db->trans_commit();	
			}
			if ($query){
			 $message = array('status' => true, 'message' => 'Berhasil Menambahkan '.$data['i_pp']);
        	}else{
        	 $message = array('status' => false, 'message' => 'Gagal Menambahkan');
			}
			$this->session->set_flashdata('message', $message);

			redirect('op/Cform');
			}
		}

		$data['page_title'] = $this->lang->line('op');
		$data['isi'] = $this->Mmaster->getPPitem();
		$this->load->view('op/vform', $data);
		
	}

	function edit($iop=null){

		if($this->input->post('iop') && $this->input->post('dop') && $this->input->post('isupplier')){
			$this->form_validation->set_rules('iop', 'No OP', 'required');
			$this->form_validation->set_rules('dop', 'Tanggal OP', 'required');
		
			if ($this->form_validation->run() === TRUE) {

			$query 	= $this->db->query("SELECT current_timestamp as c");
	   		$row   	= $query->row();
	    	$now	  = $row->c;

	    	$dop = $this->input->post("dop",true);
	    	if($dop){
	    		 $tmp 	= explode('-', $dop);
	    		 $day 	= $tmp[0];
	    		 $month = $tmp[1];
	    		 $year	= $tmp[2];
	    		 $yearmonth = $year.$month;
	    		 $dateop = $year.'-'.$month.'-'.$day;
	    	}
	    	$i_op = $this->input->post("iop",TRUE);
			$this->db->trans_begin(); 
			$data['d_op'] = $dateop;
			$data['i_payment_type'] = $this->input->post('paymenttype',TRUE);
			$data['e_remark'] = $this->input->post('eremark',TRUE);
			$data['d_update'] = $now;
			$supp = $this->input->post('isupplier',TRUE);
			$query = $this->Mmaster->updateheader($data,$i_op);

			$count = $this->input->post("jml",true);

			$querydelete = $this->Mmaster->delete($i_op);
			
			for($i=1;$i<=$count;$i++){
					$data2['i_op'] 		 = $this->input->post('iop',TRUE);
					$data2['i_pp'] 		 = $this->input->post('ipp'.$i,TRUE);
					$data2['i_material'] = $this->input->post("imaterial".$i,TRUE);
					$data2['i_satuan'] 	 = $this->input->post("isatuan".$i,TRUE);
					$data2['n_quantity'] = $this->input->post("nquantity".$i,TRUE);
					$data2['v_price'] 	 = str_replace(',','',$this->input->post("vprice".$i,TRUE));
					$data2['i_price_no'] = $this->input->post("iprice".$i,TRUE);
					$data2['e_remark'] 	 = $this->input->post("eremark".$i,TRUE);
					$data2['n_item_no']	 = $i;
					$querycek = $this->Mmaster->cekqty($data2['i_pp'],$data2['i_material']);
					$qtypp = $querycek->pp;
					$qtyop = $querycek->op;
					$totop = $qtyop+$data2['n_quantity'];
					if($totop >= $qtypp){
						$data2['n_quantity'] = $qtypp-$qtyop;
						$pp=$data2['i_pp'];
						$material=$data2['i_material'];
						$satuan=$data2['i_satuan'];
						$datapp['f_op_complete'] = true;
						$complete = $this->Mmaster->itemComplete($datapp,$pp,$material,$satuan);
					}
					if($totop < $qtypp){
						$pp=$data2['i_pp'];
						$material=$data2['i_material'];
						$satuan=$data2['i_satuan'];
						$datapp['f_op_complete'] = false;
						$complete = $this->Mmaster->itemComplete($datapp,$pp,$material,$satuan);
					}
					$this->Mmaster->updatehrgsup($data2['i_material'],$data2['i_satuan'],$data2['i_price_no'],$supp,$data2['v_price']);
					$querydetail = $this->Mmaster->insertdetail($data2);
			}

		if (($this->db->trans_status() === FALSE))
			{
		    	$this->db->trans_rollback();
			}
			else{
				$this->db->trans_commit();	
			}
			if ($query && $querydetail){
			 $message = array('status' => true, 'message' => 'Berhasil Update '.$i_op);
        	}else{
        	 $message = array('status' => false, 'message' => 'Gagal Menambahkan');
			}
			$this->session->set_flashdata('message', $message);

			redirect('op/Cform');
			}
		}

		$data['page_title'] = $this->lang->line('op');
		$data['isi'] = $this->Mmaster->getHeader($iop);
		$data['listpp'] = $this->Mmaster->getListPP($iop);
		$data['detail'] = $this->Mmaster->getItem($iop);
		$this->load->view('op/vformupdate', $data);
		
	}

	function material()
	{		
			$cari = strtoupper($this->input->post('cari', TRUE));
      		$baris = strtoupper($this->input->post('baris', TRUE));
 			if($baris==''){ 
 				$baris=$this->uri->segment(4); 
 			}
     	    if($this->uri->segment(5)!='x01'){
        	if($cari=='') $cari=$this->uri->segment(5);
       		$config['base_url'] = base_url().'index.php/op/Cform/material/'.$baris.'/'.$cari.'/';
     		}else{
        	$config['base_url'] = base_url().'index.php/op/Cform/material/'.$baris.'/x01/';
      		}

			$config['total_rows'] = $this->Mmaster->getmaterialCari($cari)->num_rows(); 
			$config['per_page'] = '10';
			$config['full_tag_open'] = '<ul class="pagination a">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = 'Awal';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Akhir';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			$config['next_link'] = 'Selanjutnya';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = 'Sebelumnya';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			$config['cur_page'] = $this->uri->segment(6);
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);

			$this->load->model('op/Mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$baris;
			$data['isi']=$this->Mmaster->bacamaterial($config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('op/vlistmaterial', $data);
	}

	function itemsupp(){
		$material = $this->uri->segment(4);
		$baris = $this->uri->segment(5);
		$data['page_title'] = $this->lang->line("listsupp");
		$data['baris'] = $baris;
		$data['list_price'] = $this->Mmaster->getPrice($material);
		$this->load->view('op/vlistprice',$data);
	}

	function ppitem(){

		$data['page_title'] = $this->lang->line("op");
		$data['isi'] = $this->Mmaster->getPPitem();
		$this->load->view("op/vlistpp",$data);
	}

	function supplier()
	{
      		$cari=strtoupper($this->input->post("cari"));

      		if($this->uri->segment(4)!='x01'){
        	if($cari=='') $cari=$this->uri->segment(4);
       		$config['base_url'] = base_url().'index.php/op/Cform/supplier/'.$cari.'/';
     		}else{
        	$config['base_url'] = base_url().'index.php/op/Cform/supplier/x01/';
      		}

			$config['total_rows'] = $this->Mmaster->bacasuppliercari($cari)->num_rows();
			$config['per_page'] = '10';
			$config['full_tag_open'] = '<ul class="pagination a">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = 'Awal';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Akhir';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			$config['next_link'] = 'Selanjutnya';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = 'Sebelumnya';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			$config['cur_page'] = $this->uri->segment(5);
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->Mmaster->bacasupplier($config['per_page'],$this->uri->segment(5),$cari)->result();
			$this->load->view('op/vlistsupplier', $data);
	}

	function proses(){

		if($this->input->post('dop')){

			$this->form_validation->set_rules('dop','Tanggal OP','required');
			$this->form_validation->set_rules('isupplier','Supplier','required');

			if ($this->form_validation->run() === TRUE) {
				$query 	= $this->db->query("SELECT current_timestamp as c");
		   		$row   	= $query->row();
		    	$now	  = $row->c;

		    	$dop = $this->input->post("dop",true);
		    	if($dop){
		    		 $tmp 	= explode('-', $dop);
		    		 $day 	= $tmp[0];
		    		 $month = $tmp[1];
		    		 $year	= $tmp[2];
		    		 $yearmonth = $year.$month;
		    		 $dateop = $year.'-'.$month.'-'.$day;
		    	}

				$this->db->trans_begin(); 
				$data['i_op'] = $this->Mmaster->runningnumber($yearmonth);
				$data['d_op'] = $dateop;
				$data['d_deliv'] = $this->input->post('dkirim',TRUE);
				$data['d_expired'] = $this->input->post('dbatas',TRUE);
				$data['i_supplier'] = $this->input->post("isupplier",TRUE);
				$data['i_payment_type'] = $this->input->post('paymenttype',TRUE);
				$data['e_remark'] = $this->input->post('eremark',TRUE);
				$data['d_entry'] = $now;
				$query = $this->Mmaster->insertheader($data);

				$count = $this->input->post("jml",true);

				for($i=1;$i<=$count;$i++){
					$data2['i_op'] 		 = $data['i_op'];
					$data2['i_pp'] 		 = $this->input->post('ipp'.$i,TRUE);
					$data2['i_material'] = $this->input->post("imaterial".$i,TRUE);
					$data2['i_satuan'] 	 = $this->input->post("isatuan".$i,TRUE);
					$data2['n_quantity'] = $this->input->post("nquantity".$i,TRUE);
					$data2['v_price'] 	 = str_replace(',','',$this->input->post("vprice".$i,TRUE));
					$data2['i_kode_master'] = $this->input->post('ikodemaster'.$i,TRUE);
					$data2['e_remark'] 	 = $this->input->post("eremark".$i,TRUE);
					$data2['n_item_no']	 = $i;
					$querycek = $this->Mmaster->cekqty($data2['i_pp'],$data2['i_material']);
					$qtypp = $querycek->pp;
					$qtyop = $querycek->op;
					$totop = $qtyop+$data2['n_quantity'];
					// var_dump($qtypp,$qtyop,$totop);die;
					if($totop >= $qtypp){
						$data2['n_quantity'] = $qtypp-$qtyop;
						$pp=$data2['i_pp'];
						$material=$data2['i_material'];
						$satuan=$data2['i_satuan'];
						$datapp['f_op_complete'] = true;
						$complete = $this->Mmaster->itemComplete($datapp,$pp,$material,$satuan);
					}
					$queryupdatepp = $this->Mmaster->updatedoppp($data2['i_pp'],$dateop);
					$querydetail = $this->Mmaster->insertdetail($data2);
					$this->Mmaster->inserthrgsup($data2['i_material'],$data2['v_price'],$data['i_supplier'],$data2['i_satuan'],$data2['i_op']);
				}

			if (($this->db->trans_status() === FALSE))
				{
			    	$this->db->trans_rollback();
				}
				else{
					$this->db->trans_commit();	
				}
			if ($query){
				$message = array('status' => true, 'message' => 'Berhasil Menambahkan '.$data['i_op']);
        	}else{
        	 	$message = array('status' => false, 'message' => 'Gagal Menambahkan');
			}
			$this->session->set_flashdata('message', $message);
			redirect('op/Cform');

	    	}

		}

		$isupplier = $this->input->post('isupplierhead',FALSE);
		$jum = $this->input->post('jml',TRUE);
		for($i=1;$i<=$jum;$i++){
			$imaterial = $this->input->post("i_material".$i,TRUE);
			$ipp = $this->input->post("i_pp".$i,TRUE);
			$cek = $this->input->post('cek'.$i,TRUE);
 			if($cek){
 				$pp[] = $ipp;
 				$data['pp'] = array_unique($pp);
				$data['isi'][] = $this->Mmaster->getHarga($imaterial,$ipp,$isupplier);
			}
		}
		// var_dump($isupplier);die;
		$data['isupplier'] = $isupplier;
		$data['esuppliername'] = $this->input->post("e_supplier_name",TRUE);
		$data['page_title'] = $this->lang->line('op');
		$this->load->view("op/test",$data);
	}

	function cancel($iop)
	{
		$this->db->trans_begin();
		$query = $this->Mmaster->cancelop($iop);
		if (($this->db->trans_status() === FALSE))
		{
		   	$this->db->trans_rollback();
		}
		else{
			$this->db->trans_commit();	
		}
		if ($query){
		 $message = array('status' => true, 'message' => 'Berhasil Cancel '.$i_pp);
        }else{
         $message = array('status' => false, 'message' => 'Gagal Cancel');
		}
		$this->session->set_flashdata('message', $message);
		redirect('op/Cform');
	}

	function cetakop($iop)
	{
		$data['page_title'] = 'Order Pembelian';
		$data['isicompany'] = $this->Mmaster->getCompany();
		$data['isihead'] 	= $this->Mmaster->getHeader($iop);
		$data['isiitem']	= $this->Mmaster->getItem($iop);

		$this->load->view('op/vprintop',$data);
	}
}