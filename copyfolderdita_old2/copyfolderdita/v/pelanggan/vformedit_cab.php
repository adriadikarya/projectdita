<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">List Pelanggan</a></li>
              <!-- <li><a href="#tab_2" data-toggle="tab">Form Input Pelanggan</a></li> -->
              <!-- <li><a href="#tab_3" data-toggle="tab">Tab 3</a></li> -->
          	</ul>
        </div>
        <div class="tab-content">
        	<div class="tab-pane active" id="tab_1">
            	<div class="row">
			        <!-- left column -->
			        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
			          	<!-- general form elements -->
			          	<div class="box box-primary">
			            	<div class="box-header with-border">
			        		    <h3 class="box-title">From Input Pelanggan</h3>
			    	        </div>
			    	        <!-- /.box-header -->
				            <!-- form start -->
				            <form role="form" method="post" action="<?php echo base_url() ?>pelanggan/C_cabang/edit">
				              <div class="box-body">
				              	<div class="form-group col-md-12 col-lg-12 col-sm-12">
				              	  <label>Kode Cabang</label> 
				              	  <select class="select2 form-control" id="idpel" name="idpel" style="width: 100%;">
					                <?php 
					                	if(!empty($pel))
					                	{
					                		foreach ($pel as $row) {
					                		$selected = $idpel==$row->i_pelanggan?"selected":"";
					                ?>
					                		<option value="<?php echo $row->i_pelanggan?>" <?php echo $selected; ?>><?php echo $row->e_nama_pelanggan ?></option>
					                <?php			
					                		}
					                	} else {
					                		echo "<option value=\"\">Maaf Tidak Ada Pelanggan!</option>";
					                	}
					                ?>
					              </select>
				              	</div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="kodecab">Kode Cabang</label>
				                  <input type="text" class="form-control" id="kodecab" name="kodecab" placeholder="Isi Kode Cabang" required maxlength="10" value="<?php echo $kodecab ?>">
				                  <input type="hidden" name="idcab" value="<?php echo $idcab ?>">
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="namacab">Nama Cabang</label>
				                  <input type="text" class="form-control" id="namacab" name="namacab" placeholder="Isi Nama Cabang" required maxlength="200" value="<?php echo $namacab ?>">
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="alamatcab">Alamat</label>
				                   <textarea class="form-control" rows="3" id="alamatcab" name="alamatcab" placeholder="Isikan Alamat ..." maxlength="255" required ><?php echo $alamatcab ?></textarea>
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="kotacab">Kota</label>
				                  <input type="text" class="form-control" id="kotacab" name="kotacab" placeholder="Isi Kota Cabang" maxlength="50" value="<?php echo $kotacab ?>">
				                </div>
				                <div class="form-group col-md-12 col-lg-12 col-sm-12">
				                  <label for="inisialcab">Inisial</label>
				                  <input type="text" class="form-control" id="inisialcab" name="inisialcab" value="<?php echo $inisialcab ?>" placeholder="Isi Dengan Inisial" maxlength="10">
				                </div>
				              </div>
				              <!-- /.box-body -->

				              <div class="box-footer">
				                <button type="submit" name="submit" class="btn btn-info">Update</button>
				                <a href="<?php echo base_url() ?>pelanggan/C_cabang" class="btn btn-default">Kembali</a>
				              </div>
				            </form>
				        </div>
			        </div>
			    </div>
            </div>
        </div>
    </div>
</section>