<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class M_brgwarna extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function daftarbrgwarna($ikodeprsh)
	{
		$this->db->select("a.*, b.e_nama_warna, c.e_nama_brg");
		$this->db->from("tr_barang_warna a");
		$this->db->join("tr_warna b","a.i_id_warna=b.i_id_warna");
		$this->db->join("tr_barang c","c.i_kode_brg=a.i_kode_brg");
		$this->db->where("a.i_kode_perusahaan",$ikodeprsh);
		$this->db->order_by("a.i_kode_brg");
		return $this->db->get();
	}

	function daftarbrg($ikodeprsh)
	{
		$this->db->where("i_kode_perusahaan",$ikodeprsh);
		return $this->db->get("tr_barang");
	}

	function daftarwarna()
	{
		return $this->db->get("tr_warna");
	}

	function insertdata($data)
	{	
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;
		$insert = $this->db->insert("tr_barang_warna",$data);
		$error = $this->db->error();
		if(!empty($error['message']))
		{
			$this->db->db_debug = $db_debug;
			return $insert;
		} else {
			$this->db->db_debug = $db_debug;
			return $insert;
		}
	}
}