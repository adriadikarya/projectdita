<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Mmaster extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function daftarpel($kodeprsh)
	{
		$this->db->where("i_kode_perusahaan",$kodeprsh);
		return $this->db->get("tr_pelanggan");
	}

	function daftarnota($kodeprsh)
	{
		$this->db->select("a.*, b.e_nama_pelanggan");
		$this->db->from("tm_nota a");
		$this->db->join("tr_pelanggan b","a.i_pelanggan=b.i_pelanggan");
		$this->db->where("a.i_kode_perusahaan",$kodeprsh);
		$this->db->where("a.f_nota_cancel",0);
		$this->db->order_by("a.i_id_nota");
		return $this->db->get();
	}

	function daftardetnota($idnota,$kodeprsh)
	{
		$this->db->select("a.i_id_item_nota, a.i_kode_brg, a.n_qty, a.v_harga_satuan, a.e_desc, b.i_id_warna, c.e_nama_warna, b.n_qty_warna from tm_nota_item a 
			left join tm_nota_item_warna b on a.i_id_item_nota=b.i_id_item_nota 
			left join tr_warna c on b.i_id_warna=c.i_id_warna
			where a.i_id_nota='$idnota' order by a.i_no_item",false);
		return $this->db->get();
	}

	function daftarwarnaitem($iditemnota)
	{
		$this->db->select("a.*, b.e_nama_warna");
		$this->db->from("tm_nota_item_warna a");
		$this->db->join("tr_warna b","a.i_id_warna=b.i_id_warna");
		$this->db->where("a.i_id_item_nota",$iditemnota);
		$this->db->order_by("a.i_no_item");
		return $this->db->get()->result();
	}

	function daftarbrg($kodeprsh)
	{
		$this->db->where("i_kode_perusahaan",$kodeprsh);
		return $this->db->get("tr_barang");
	}

	function insertheader($data)
	{
		return $this->db->insert('tm_nota',$data);
	}

	function insertdetail($data2)
	{
		return $this->db->insert("tm_nota_item",$data2);
	}

	function insertwarna($data3)
	{
		return $this->db->insert("tm_nota_item_warna",$data3);
	}

	function cekidnota()
	{
		$this->db->select('i_id_nota');
		$this->db->from('tm_nota');
		$this->db->order_by('i_id_nota','desc');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row();
			$i_id_nota = $row->i_id_nota+1;
			return $i_id_nota;
		} else {
			return 1;
		}
	}

	function cekiditemnota()
	{
		$this->db->select('i_id_item_nota');
		$this->db->from('tm_nota_item');
		$this->db->order_by('i_id_item_nota','desc');
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row();
			$i_id_item_nota = $row->i_id_item_nota+1;
			return $i_id_item_nota;
		} else {
			return 1;
		}
	}

	function canceldata($idnota)
	{
		$this->db->set('f_nota_cancel',1);
		$this->db->where('i_id_nota',$idnota);
		return $this->db->update('tm_nota');
	}

	function enota($idnota)
	{	
		$this->db->select("a.*, b.e_nama_pelanggan, b.e_alamat_pelanggan");
		$this->db->from("tm_nota a");
		$this->db->join("tr_pelanggan b","a.i_pelanggan=b.i_pelanggan");
		$this->db->where("a.i_id_nota",$idnota);
		return $this->db->get();
	}

	function enotaitem($idnota)
	{
		$this->db->select("a.*, b.e_nama_brg");
		$this->db->from("tm_nota_item a");
		$this->db->join("tr_barang b","a.i_kode_brg=b.i_kode_brg");
		$this->db->where("a.i_id_nota",$idnota);
		$this->db->order_by("a.i_no_item");
		return $this->db->get();	
	}

	function enotaitemwarna($iditemnota)
	{
		$this->db->select("a.*, b.e_nama_warna");
		$this->db->from("tm_nota_item_warna a");
		$this->db->join("tr_warna b","a.i_id_warna=b.i_id_warna");
		$this->db->where("a.i_id_item_nota",$iditemnota);
		$this->db->order_by("a.i_no_item");
		return $this->db->get()->result();	
	}

	function itemwarna($iditemnota)
	{
		$this->db->where("i_id_item_nota",$iditemnota);
		return $this->db->get("tm_nota_item_warna");
	}

	function deletenota($idnota)
	{
		$this->db->where("i_id_nota",$idnota);
		return $this->db->delete("tm_nota_item");
	}

	function deleteitem($iditemnota)
	{
		$this->db->where("i_id_item_nota",$iditemnota);
		return $this->db->delete("tm_nota_item");
	}

	function deletewarna($iditemnota)
	{

		$this->db->where("i_id_item_nota",$iditemnota);
		return $this->db->delete("tm_nota_item_warna");
	}

	function updatehead($data,$idnota)
	{
		$this->db->where("i_id_nota",$idnota);
		return $this->db->update("tm_nota",$data);
	}
}