<?php 

/**
 * 
 */
class C_Cabang extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('pelanggan/M_cabang');
	}

	function index()
	{
		if($this->session->userdata('loggedin')){
			if($this->input->post("kodecab")!='' && $this->input->post("namacab") != '')
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$data['i_pelanggan']   		= $this->input->post("idpel",TRUE);
				$data['i_kode_cabang']	 	= $this->input->post("kodecab",TRUE);
				$data['e_nama_cabang'] 		= $this->input->post("namacab",TRUE);
				$data['e_alamat_cabang']    = $this->input->post("alamatcab",TRUE);
				$data['e_kota_cabang']		= $this->input->post("kotacab",TRUE);
				$data['e_inisial']			= $this->input->post("inisialcab",TRUE);
				$data['i_kode_perusahaan']  = $this->session->userdata('kode_perusahaan');
				$data['d_input']			= $now;

				$this->db->trans_begin();
				$insert = $this->M_cabang->insertdata($data);

				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
				// cek jika query berhasil
			    if ($insert) {
			    	// Set success message
			    	$message = array('status' => true, 'message' => 'Berhasil Menambah Data Pelanggan '.$data['i_kode_cabang']);
				} else {
			    	// Set error message
			    	$message = array('status' => false, 'message' => 'Gagal Menambah Data Pelanggan '.$data['i_kode_cabang']);
			    }
			    // simpan message sebagai session
			    $this->session->set_flashdata('message', $message);
			    // refresh page
			    redirect('pelanggan/C_cabang');
			}

			$data['page_title'] = "Cabang Pelanggan";
			$data['litle_title'] = "Form Input";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$data['cab']  = $this->M_cabang->daftarcab($kodeprsh)->result();
			$data['pel']  = $this->M_cabang->daftarpel($kodeprsh)->result();
			// $data['grup'] = $this->M_cabang->daftargrup($kodeprsh)->result();
			// $data['prsh'] = $this->M_cabang->daftarperusahaan()->result();
			$datalay['content'] = $this->load->view('pelanggan/vmainform_cab',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function edit()
	{
		if($this->session->userdata('loggedin')){
			if($this->input->post("kodecab")!='' && $this->input->post("namacab") != '')
			{
				$qnow = $this->db->query('select now() as now');
				$rnow = $qnow->row();
				$now  = $rnow->now;

				$icabang 					= $this->input->post("idcab",TRUE);
				$data['i_pelanggan']   		= $this->input->post("idpel",TRUE);
				$data['i_kode_cabang']	 	= $this->input->post("kodecab",TRUE);
				$data['e_nama_cabang'] 		= $this->input->post("namacab",TRUE);
				$data['e_alamat_cabang']    = $this->input->post("alamatcab",TRUE);
				$data['e_kota_cabang']		= $this->input->post("kotacab",TRUE);
				$data['e_inisial']			= $this->input->post("inisialcab",TRUE);
				$data['i_kode_perusahaan']  = $this->session->userdata('kode_perusahaan');
				$data['d_update']			= $now;

				$this->db->trans_begin();
				$insert = $this->M_cabang->updatedata($data,$icabang);

				if($this->db->trans_status()===FALSE)
				{
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
				// cek jika query berhasil
			    if ($insert) {
			    	// Set success message
			    	$message = array('status' => true, 'message' => 'Berhasil Mengubah Data Pelanggan '.$data['i_kode_cabang']);
				} else {
			    	// Set error message
			    	$message = array('status' => false, 'message' => 'Gagal Mengubah Data Pelanggan '.$data['i_kode_cabang']);
			    }
			    // simpan message sebagai session
			    $this->session->set_flashdata('message', $message);
			    // refresh page
			    redirect('pelanggan/C_cabang');
			}

			$cabang = $this->uri->segment(4);
			$data['page_title'] = "Cabang Pelanggan";
			$data['litle_title'] = "Form Edit";
			$kodeprsh = $this->session->userdata('kode_perusahaan');
			$cab  = $this->M_cabang->caricab($cabang,$kodeprsh);
			if($cab->num_rows()>0)
			{
				$rcab = $cab->row();
				$data['idcab'] = $cabang;
				$data['idpel'] = $rcab->i_pelanggan;
				$data['kodecab'] = $rcab->i_kode_cabang;
				$data['namacab'] = $rcab->e_nama_cabang;
				$data['alamatcab'] = $rcab->e_alamat_cabang;
				$data['kotacab'] = $rcab->e_kota_cabang;
				$data['inisialcab'] = $rcab->e_inisial;
			} else {
				$data['idcab'] = '';
				$data['idpel'] = '';
				$data['kodecab'] = '';
				$data['namacab'] = '';
				$data['alamatcab'] = '';
				$data['kotacab'] = '';
				$data['inisialcab'] = '';
			}

			$data['pel']  = $this->M_cabang->daftarpel($kodeprsh)->result();
			$datalay['content'] = $this->load->view('pelanggan/vformedit_cab',$data,TRUE);
			$this->load->view('template/layout',$datalay);
		} else {
			redirect('Main');
		}
	}

	function delete()
	{
		if($this->session->userdata('loggedin')){
			$idcab = $this->uri->segment(4);
			$ikode = $this->uri->segment(5);
			$this->db->trans_begin();
			$qdelete = $this->M_cabang->deletedata($idcab);
			if($this->db->trans_status()===FALSE)
			{
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
			// cek jika query berhasil
			if ($qdelete) {
				// Set success message
			    $message = array('status' => true, 'message' => 'Berhasil Menghapus Data Cabang ');
			} else {
				// Set error message
			    $message = array('status' => false, 'message' => 'Gagal Menghapus Data Cabang ');
			}
			// simpan message sebagai session
			$this->session->set_flashdata('message', $message);
			// refresh page
			redirect('pelanggan/C_cabang');
		} else {
			redirect('Main');
		}
	}
}