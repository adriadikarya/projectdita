--query total piutang dan total penjualan
SELECT coalesce(SUM(v_total),0) AS total_penjualan, COALESCE(SUM(v_total_sisa),0) AS total_piutang 
FROM tm_nota WHERE d_nota>='2019-06-01' AND d_nota<='2019-06-31' AND i_kode_perusahaan='E'

--query dapet rata2 brg repeat
SELECT tni.i_kode_brg, e_nama_brg, COUNT(tni.i_kode_brg) AS tot_repeat, coalesce(SUM(n_qty),0) AS tot_qty, 
COALESCE((SUM(n_qty)/COUNT(tni.i_kode_brg)),0) AS rata2 
FROM tm_nota_item tni 
JOIN tm_nota tn ON tn.i_id_nota=tni.i_id_nota 
JOIN tr_barang tb ON tb.i_kode_brg=tni.i_kode_brg
WHERE d_nota>='2019-06-01' AND d_nota<='2019-06-31' AND tn.i_kode_perusahaan='D'
GROUP BY tni.i_kode_brg, e_nama_brg HAVING COUNT(tni.i_kode_brg) > 1;

SELECT * FROM tm_nota_item

--query total penjualan per item by tanggal
SELECT tb.i_kode_brg, e_nama_brg, COALESCE(ROUND((SUM(n_qty*v_harga_satuan)-(SUM(n_qty*v_harga_satuan)*(n_diskon/100)))),0) AS tot_qty, 
	tn.d_nota
FROM tm_nota_item tni
JOIN tm_nota tn ON tn.i_id_nota=tni.i_id_nota 
JOIN tr_barang tb ON tb.i_kode_brg=tni.i_kode_brg
WHERE d_nota>='2019-05-01' AND d_nota<='2019-11-31' AND tn.i_kode_perusahaan='E' AND tni.i_kode_brg = 'E0001'
GROUP BY e_nama_brg, tn.d_nota
ORDER BY tn.d_nota

SELECT e_nama_brg, COALESCE(ROUND((SUM(n_qty*v_harga_satuan)-(SUM(n_qty*v_harga_satuan)*(n_diskon/100)))),0) AS tot_qty, tn.d_nota FROM tm_nota_item tni JOIN tm_nota tn ON tn.i_id_nota=tni.i_id_nota JOIN tr_barang tb ON tb.i_kode_brg=tni.i_kode_brg WHERE d_nota>='2019-11-29' AND d_nota<='2019-11-29' AND tn.i_kode_perusahaan='E' AND tni.i_kode_brg='A0001' GROUP BY e_nama_brg, tn.d_nota ORDER BY tn.d_nota